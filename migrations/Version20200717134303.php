<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200717134303 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bookmark (id INT AUTO_INCREMENT NOT NULL, producer_name VARCHAR(64) NOT NULL, reference VARCHAR(32) NOT NULL, designation VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_bookmark (user_id INT NOT NULL, bookmark_id INT NOT NULL, INDEX IDX_3AEF761DA76ED395 (user_id), INDEX IDX_3AEF761D92741D25 (bookmark_id), PRIMARY KEY(user_id, bookmark_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_bookmark ADD CONSTRAINT FK_3AEF761DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_bookmark ADD CONSTRAINT FK_3AEF761D92741D25 FOREIGN KEY (bookmark_id) REFERENCES bookmark (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_bookmark DROP FOREIGN KEY FK_3AEF761D92741D25');
        $this->addSql('DROP TABLE bookmark');
        $this->addSql('DROP TABLE user_bookmark');
    }
}
