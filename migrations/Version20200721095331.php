<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200721095331 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE suggested_item (id INT AUTO_INCREMENT NOT NULL, distribution_id INT DEFAULT NULL, producer_name VARCHAR(64) NOT NULL, reference VARCHAR(32) NOT NULL, designation VARCHAR(255) NOT NULL, INDEX IDX_DD4605AA6EB6DDB5 (distribution_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE suggested_item_user (suggested_item_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_9D7E9B7750660DBB (suggested_item_id), INDEX IDX_9D7E9B77A76ED395 (user_id), PRIMARY KEY(suggested_item_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE suggested_item ADD CONSTRAINT FK_DD4605AA6EB6DDB5 FOREIGN KEY (distribution_id) REFERENCES distribution (id)');
        $this->addSql('ALTER TABLE suggested_item_user ADD CONSTRAINT FK_9D7E9B7750660DBB FOREIGN KEY (suggested_item_id) REFERENCES suggested_item (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE suggested_item_user ADD CONSTRAINT FK_9D7E9B77A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE tmp');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE suggested_item_user DROP FOREIGN KEY FK_9D7E9B7750660DBB');
        $this->addSql('CREATE TABLE tmp (id INT AUTO_INCREMENT NOT NULL, catalogue_id_id INT NOT NULL, reference VARCHAR(32) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, designation VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, brand VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, packaging VARCHAR(32) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, inspection VARCHAR(32) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, origin VARCHAR(32) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, label VARCHAR(32) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, unitary_price NUMERIC(6, 2) NOT NULL, vat NUMERIC(4, 2) NOT NULL, family VARCHAR(64) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, packing SMALLINT DEFAULT NULL, min_packing SMALLINT DEFAULT NULL, unpacked_price NUMERIC(6, 2) DEFAULT NULL, calibre_categ VARCHAR(16) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, comment LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, ed VARCHAR(16) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, min_order SMALLINT DEFAULT NULL, custom_field1 VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, custom_field2 VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, custom_field3 VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, custom_field4 VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_2CEF7D486758ECE6 (catalogue_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE tmp ADD CONSTRAINT FK_2CEF7D486758ECE6 FOREIGN KEY (catalogue_id_id) REFERENCES catalogue (id)');
        $this->addSql('DROP TABLE suggested_item');
        $this->addSql('DROP TABLE suggested_item_user');
    }
}
