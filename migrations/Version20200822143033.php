<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200822143033 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE shared_product (id INT AUTO_INCREMENT NOT NULL, item_id INT NOT NULL, sharing_proposal_id INT NOT NULL, product_unit SMALLINT NOT NULL, order_origin SMALLINT NOT NULL, INDEX IDX_F29D081A126F525E (item_id), INDEX IDX_F29D081A8C148271 (sharing_proposal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE shared_product ADD CONSTRAINT FK_F29D081A126F525E FOREIGN KEY (item_id) REFERENCES item (id)');
        $this->addSql('ALTER TABLE shared_product ADD CONSTRAINT FK_F29D081A8C148271 FOREIGN KEY (sharing_proposal_id) REFERENCES sharing_proposal (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE shared_product');
    }
}
