<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200713094106 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `order` ADD disribution_id INT NOT NULL');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F5299398EDA6DE3D FOREIGN KEY (disribution_id) REFERENCES distribution (id)');
        $this->addSql('CREATE INDEX IDX_F5299398EDA6DE3D ON `order` (disribution_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F5299398EDA6DE3D');
        $this->addSql('DROP INDEX IDX_F5299398EDA6DE3D ON `order`');
        $this->addSql('ALTER TABLE `order` DROP disribution_id');
    }
}
