<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200630175742 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE item (id INT AUTO_INCREMENT NOT NULL, catalogue_id_id INT NOT NULL, reference VARCHAR(32) NOT NULL, designation VARCHAR(255) NOT NULL, brand VARCHAR(255) DEFAULT NULL, packaging VARCHAR(32) DEFAULT NULL, inspection VARCHAR(32) DEFAULT NULL, origin VARCHAR(32) DEFAULT NULL, label VARCHAR(32) DEFAULT NULL, unitary_price NUMERIC(6, 2) NOT NULL, vat NUMERIC(4, 2) NOT NULL, family VARCHAR(64) DEFAULT NULL, packing SMALLINT DEFAULT NULL, min_packing SMALLINT DEFAULT NULL, unpacked_price NUMERIC(6, 2) DEFAULT NULL, calibre_categ VARCHAR(16) DEFAULT NULL, comment LONGTEXT DEFAULT NULL, ed VARCHAR(16) DEFAULT NULL, min_order SMALLINT DEFAULT NULL, custom_field1 VARCHAR(255) DEFAULT NULL, custom_field2 VARCHAR(255) DEFAULT NULL, custom_field3 VARCHAR(255) DEFAULT NULL, custom_field4 VARCHAR(255) DEFAULT NULL, INDEX IDX_1F1B251E6758ECE6 (catalogue_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tmp (id INT AUTO_INCREMENT NOT NULL, catalogue_id_id INT NOT NULL, reference VARCHAR(32) NOT NULL, designation VARCHAR(255) NOT NULL, brand VARCHAR(255) DEFAULT NULL, packaging VARCHAR(32) DEFAULT NULL, inspection VARCHAR(32) DEFAULT NULL, origin VARCHAR(32) DEFAULT NULL, label VARCHAR(32) DEFAULT NULL, unitary_price NUMERIC(6, 2) NOT NULL, vat NUMERIC(4, 2) NOT NULL, family VARCHAR(64) DEFAULT NULL, packing SMALLINT DEFAULT NULL, min_packing SMALLINT DEFAULT NULL, unpacked_price NUMERIC(6, 2) DEFAULT NULL, calibre_categ VARCHAR(16) DEFAULT NULL, comment LONGTEXT DEFAULT NULL, ed VARCHAR(16) DEFAULT NULL, min_order SMALLINT DEFAULT NULL, custom_field1 VARCHAR(255) DEFAULT NULL, custom_field2 VARCHAR(255) DEFAULT NULL, custom_field3 VARCHAR(255) DEFAULT NULL, custom_field4 VARCHAR(255) DEFAULT NULL, INDEX IDX_2CEF7D486758ECE6 (catalogue_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE item ADD CONSTRAINT FK_1F1B251E6758ECE6 FOREIGN KEY (catalogue_id_id) REFERENCES catalogue (id)');
        $this->addSql('ALTER TABLE tmp ADD CONSTRAINT FK_2CEF7D486758ECE6 FOREIGN KEY (catalogue_id_id) REFERENCES catalogue (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE item');
        $this->addSql('DROP TABLE tmp');
    }
}
