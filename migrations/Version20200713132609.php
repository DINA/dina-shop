<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200713132609 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F5299398EDA6DE3D');
        $this->addSql('DROP INDEX IDX_F5299398EDA6DE3D ON `order`');
        $this->addSql('ALTER TABLE `order` CHANGE disribution_id distribution_id INT NOT NULL');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F52993986EB6DDB5 FOREIGN KEY (distribution_id) REFERENCES distribution (id)');
        $this->addSql('CREATE INDEX IDX_F52993986EB6DDB5 ON `order` (distribution_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F52993986EB6DDB5');
        $this->addSql('DROP INDEX IDX_F52993986EB6DDB5 ON `order`');
        $this->addSql('ALTER TABLE `order` CHANGE distribution_id disribution_id INT NOT NULL');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F5299398EDA6DE3D FOREIGN KEY (disribution_id) REFERENCES distribution (id)');
        $this->addSql('CREATE INDEX IDX_F5299398EDA6DE3D ON `order` (disribution_id)');
    }
}
