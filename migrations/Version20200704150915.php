<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200704150915 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE distribution (id INT AUTO_INCREMENT NOT NULL, status VARCHAR(32) NOT NULL, delivery_date DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE distribution_catalogue (distribution_id INT NOT NULL, catalogue_id INT NOT NULL, INDEX IDX_1E31D9E96EB6DDB5 (distribution_id), INDEX IDX_1E31D9E94A7843DC (catalogue_id), PRIMARY KEY(distribution_id, catalogue_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE distribution_catalogue ADD CONSTRAINT FK_1E31D9E96EB6DDB5 FOREIGN KEY (distribution_id) REFERENCES distribution (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE distribution_catalogue ADD CONSTRAINT FK_1E31D9E94A7843DC FOREIGN KEY (catalogue_id) REFERENCES catalogue (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE distribution_catalogue DROP FOREIGN KEY FK_1E31D9E96EB6DDB5');
        $this->addSql('DROP TABLE distribution');
        $this->addSql('DROP TABLE distribution_catalogue');
    }
}
