<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200831150813 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE item DROP FOREIGN KEY FK_1F1B251E8C148271');
        $this->addSql('DROP INDEX IDX_1F1B251E8C148271 ON item');
        $this->addSql('ALTER TABLE item DROP sharing_proposal_id, DROP min_order');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE item ADD sharing_proposal_id INT DEFAULT NULL, ADD min_order SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE item ADD CONSTRAINT FK_1F1B251E8C148271 FOREIGN KEY (sharing_proposal_id) REFERENCES sharing_proposal (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_1F1B251E8C148271 ON item (sharing_proposal_id)');
    }
}
