<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200709095009 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE sharing_proposal (id INT AUTO_INCREMENT NOT NULL, distribution_id INT NOT NULL, UNIQUE INDEX UNIQ_AF9653586EB6DDB5 (distribution_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sharing_proposal ADD CONSTRAINT FK_AF9653586EB6DDB5 FOREIGN KEY (distribution_id) REFERENCES distribution (id)');
        $this->addSql('ALTER TABLE item ADD sharing_proposal_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE item ADD CONSTRAINT FK_1F1B251E8C148271 FOREIGN KEY (sharing_proposal_id) REFERENCES sharing_proposal (id)');
        $this->addSql('CREATE INDEX IDX_1F1B251E8C148271 ON item (sharing_proposal_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE item DROP FOREIGN KEY FK_1F1B251E8C148271');
        $this->addSql('DROP TABLE sharing_proposal');
        $this->addSql('DROP INDEX IDX_1F1B251E8C148271 ON item');
        $this->addSql('ALTER TABLE item DROP sharing_proposal_id');
    }
}
