<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200724142414 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE catalogue DROP FOREIGN KEY FK_59A699F5897B5A86');
        $this->addSql('ALTER TABLE catalogue CHANGE producer_id_id producer_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE catalogue ADD CONSTRAINT FK_59A699F5897B5A86 FOREIGN KEY (producer_id_id) REFERENCES producer (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE catalogue DROP FOREIGN KEY FK_59A699F5897B5A86');
        $this->addSql('ALTER TABLE catalogue CHANGE producer_id_id producer_id_id INT NOT NULL');
        $this->addSql('ALTER TABLE catalogue ADD CONSTRAINT FK_59A699F5897B5A86 FOREIGN KEY (producer_id_id) REFERENCES producer (id)');
    }
}
