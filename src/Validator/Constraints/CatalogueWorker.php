<?php

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class CatalogueWorker extends Constraint
{
    public $message = 'form.catalogue.no_file';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}