<?php

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Producer extends Constraint
{
    public $message = 'form.producer.code';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}