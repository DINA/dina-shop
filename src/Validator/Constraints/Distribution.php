<?php

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Distribution extends Constraint
{
    public $message_add = 'form.distribution.add_catalogue';
    public $message_remove = 'form.distribution.remove_catalogue';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}