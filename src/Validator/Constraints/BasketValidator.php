<?php

namespace App\Validator\Constraints;

use App\Controller\Shop\BasketController;
use App\Repository\BasketRepository;
use App\Repository\OrderRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class BasketValidator extends ConstraintValidator
{
    private $basketRepository;
    private $orderRepository;
   
    public function __construct(BasketRepository $basketRepository, OrderRepository $orderRepository)
    {
        $this->basketRepository = $basketRepository;
        $this->orderRepository = $orderRepository;
    }


    public function validate($basket, Constraint $constraint)
    {
        $nbIndividualItemsOrdered = $this->orderRepository->countItems('individual');
        $nbIndividualItems = $this->basketRepository->countItems('individual');
        if(($nbIndividualItems + $nbIndividualItemsOrdered) > BasketController::MAX_INDIVIDUAL_ITEMS){
            $this->context
                ->buildViolation($constraint->messageFull)
                ->addViolation();
        }
    }

}
