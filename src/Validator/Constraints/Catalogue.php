<?php

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Catalogue extends Constraint
{
    public $message = 'form.catalogue.code';
    public $messageBoth = 'form.catalogue.choice';
    public $messageRelaisVertType = 'form.catalogue.relaisVertType';
    public $messageEditType = 'form.catalogue.edit_type';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}