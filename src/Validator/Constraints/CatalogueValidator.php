<?php

namespace App\Validator\Constraints;

use App\Entity\Catalogue;
use App\Repository\ProducerRepository;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use App\Service\DistributionStatusCheck\DistributionStatusCheck;
use Normalizer;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CatalogueValidator extends ConstraintValidator
{
    private $catalogueConfigReader;
    private $producerRepository;
    private $distributionStatusCheck;
   
    public function __construct(CatalogueConfigReader $catalogueConfigReader, ProducerRepository $producerRepository, DistributionStatusCheck $distributionStatusCheck)
    {
        $this->catalogueConfigReader = $catalogueConfigReader;
        $this->producerRepository = $producerRepository;
        $this->distributionStatusCheck = $distributionStatusCheck;
    }


    public function validate($catalogue, Constraint $constraint)
    {
        /* Test than both file fields are not set */
        /** @var \App\Entity\Catalogue $catalogue */
        if($catalogue->getSpreadsheetFile() && ($catalogue->getDropboxFile() && $catalogue->getDropboxFile()->getDropboxFileLink())){
            $this->context
                ->buildViolation($constraint->messageBoth)
                ->atPath('spreadsheetFile')
                ->addViolation();
        }
        /* Edit a catalogue currently or already used in a distribution is not allowed */
        elseif (($catalogue->getStatus() != Catalogue::STATUS_ARCHIVED) &&  ($list = $this->distributionStatusCheck->checkLinkedDistribution($catalogue, false))) {
            $str =  implode(',', $list);
            $this->context
                ->buildViolation($constraint->messageEditType)
                ->setParameter('list', $str)
                ->addViolation();
        }
        /* The file structure for sec & frais & DINA producers is the same so add a check here based on the filename */
        elseif($catalogue->getSpreadsheetFile() || ($catalogue->getDropboxFile() && $catalogue->getDropboxFile()->getDropboxFileLink())){
            /* Normalization to avoid comparison error with accents due to file encoding (windows, macos...) */
            $fileName = Normalizer::normalize(($catalogue->getSpreadsheetFile())? $catalogue->getSpreadsheetFile()->getClientOriginalName(): $catalogue->getDropboxFile()->getDropboxFileName());
            $format = $this->catalogueConfigReader->findCatalogueFormat($catalogue->getType());
            /* No normalization here, because the string comes from an utf-8 file */
            $catalogueName = $this->catalogueConfigReader->getCataloguesNames()[$catalogue->getType()];
            if (($catalogue->getType() === 'RelaisVert_Catalogue_Sec') &&  (!stristr($fileName, 'sec'))) {
                $check = false;
            }
            elseif (($catalogue->getType() === 'RelaisVert_Catalogue_Frais') && (!stristr($fileName, 'frais'))) { 
                $check = false;
            } /* multibyte comparison to manage accents */
            elseif(($format === 'Format_DINA_Producer') && (!mb_stripos($fileName, $catalogueName)) ){
                $check = false;
            }else{
                $check = true;
            }

            if(!$check){
                $this->context
                ->buildViolation($constraint->messageRelaisVertType)
                ->atPath('type')
                ->addViolation();
            }
        }
        /* Check than the selected catalogue is always associated to a producer */
        else{
            $catCode = $this->catalogueConfigReader->findCatalogueCode($catalogue->getType());
            /** @var \App\Entity\Producer $producer */
            $producer = $this->producerRepository->findOneBy(['code' => $catCode]);
            if(!$producer){
                $this->context
                ->buildViolation($constraint->message)
                ->setParameter('code',$catCode)
                ->atPath('type')
                ->addViolation();
            }
        }
    }

}
