<?php

namespace App\Validator\Constraints;

use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DistributionValidator extends ConstraintValidator
{
    private $catalogueConfigReader;
   
    public function __construct(CatalogueConfigReader $catalogueConfigReader )
    {
        $this->catalogueConfigReader = $catalogueConfigReader;
    }


    public function validate($distribution, Constraint $constraint)
    {
        $allCatalogueTypes = $this->catalogueConfigReader->getCataloguesDefinition();
        /* We want only the Mercuriale and the Dina Producers */
        $deletableCatalogues = $this->catalogueConfigReader->filterBy($allCatalogueTypes, [
            'key' => 'RelaisVert_Mercuriale_FruitsLegumes',
            'format' => 'Format_DINA_Producer',
        ]);

        $deleted = $distribution->getCatalogues()->getDeleteDiff();
        $inserted = $distribution->getCatalogues()->getInsertDiff();

        $forbiddenCataloguesToDelete = array_filter($deleted, function ($catalogue) use ($deletableCatalogues) {
            //return !in_array($catalogue->getType(), $deletableCatalogues);
            return !array_key_exists($catalogue->getType(),  $deletableCatalogues);
        });
        $forbiddenCataloguesToDeleteNames = implode(' ', $forbiddenCataloguesToDelete);

        $forbiddenCataloguesToInsert = array_filter($inserted, function ($catalogue) use ($deletableCatalogues) {
            //return !in_array($catalogue->getType(), $deletableCatalogues);
            return !array_key_exists ($catalogue->getType() ,  $deletableCatalogues);
        });
        $forbiddenCataloguesToInsertNames = implode(' ', $forbiddenCataloguesToInsert);

        if (!empty($forbiddenCataloguesToDelete)) {
            $this->context->buildViolation($constraint->message_remove)
            ->atPath('Catalogues')
            ->setParameter('forbiddenCataloguesToDelete', $forbiddenCataloguesToDeleteNames)
            ->addViolation();
        }

        if (!empty($forbiddenCataloguesToInsert)) {
            $this->context->buildViolation($constraint->message_add)
                ->atPath('Catalogues')
                ->setParameter('forbiddenCataloguesToInsert', $forbiddenCataloguesToInsertNames)
                ->addViolation();
        }
        
    }

}