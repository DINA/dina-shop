<?php

namespace App\Validator\Constraints;

use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ProducerValidator extends ConstraintValidator
{
    private $catalogueConfigReader;
   
    public function __construct(CatalogueConfigReader $catalogueConfigReader )
    {
        $this->catalogueConfigReader = $catalogueConfigReader;
    }


    public function validate($producer, Constraint $constraint)
    {
        $codes = $this->catalogueConfigReader->getCataloguesCodes();

        // check if there is a catalogue with this code
        if (!in_array($producer->getCode(), $codes)) {
            $this->context->buildViolation($constraint->message)
            ->atPath('code')
            ->addViolation();
        }
    }

}
