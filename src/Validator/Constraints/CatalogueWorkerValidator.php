<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CatalogueWorkerValidator extends ConstraintValidator
{   

    public function validate($catalogue, Constraint $constraint)
    {
        /* Test that at least one file is provided */
        /** @var \App\Entity\Catalogue $catalogue */
        if (!$catalogue->getSpreadsheetFile() && !$catalogue->getDropboxFile()->getDropboxFileName()) {
            $this->context
                ->buildViolation($constraint->message)
                ->atPath('spreadsheetFile')
                ->addViolation();
        }
    }

}
