<?php

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Basket extends Constraint
{
    public $messageFull = 'form.basket.full';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}