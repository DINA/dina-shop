<?php

namespace App\EventSubscriber;

use App\Entity\Catalogue;
use App\Repository\BasketRepository;
use App\Repository\CatalogueRepository;
use App\Repository\DistributionRepository;
use App\Service\DinaSpreadsheet\DinaSpreadsheet;
use App\Service\Mail\AssoNotification;
use App\Service\Mail\UserNotification;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Workflow\Event\EnteredEvent;
use Symfony\Component\Workflow\Event\GuardEvent;
use Symfony\Component\Workflow\Event\TransitionEvent;
use Symfony\Component\Workflow\TransitionBlocker;

class DistributionSubscriber implements EventSubscriberInterface
{
    private $params;
    private $basketRepository;
    private $entityManager;
    private $distributionRepository;
    private $userNotification;
    private $catalogueRepository;
    private $dinaSpreadsheet;
    private $assoNotification;
    private $debug;
    private $security;

    public function __construct(ParameterBagInterface $params, 
                                                    EntityManagerInterface $entityManager, 
                                                    BasketRepository $basketRepository, 
                                                    DistributionRepository $distributionRepository, 
                                                    UserNotification $userNotification,
                                                    CatalogueRepository $catalogueRepository,
                                                    DinaSpreadsheet $dinaSpreadsheet,
                                                    AssoNotification $assoNotification,
                                                    string $debug,
                                                    Security $security
                                                    )
    {
        $this->params = $params;
        $this->basketRepository = $basketRepository;
        $this->entityManager = $entityManager;
        $this->distributionRepository = $distributionRepository;
        $this->userNotification = $userNotification;
        $this->catalogueRepository = $catalogueRepository;
        $this->dinaSpreadsheet = $dinaSpreadsheet;
        $this->assoNotification = $assoNotification;
        $this->debug = $debug;
        $this->security = $security;
    }

    public function guardShare(GuardEvent $event)
    {
        /** @var Distribution $distribution */
        $distribution = $event->getSubject();
       
        if($this->distributionRepository->findDistributionInProgress()){
            $event->addTransitionBlocker(new TransitionBlocker('manager.distribution.next_state.errorBis_shareinprogress', 0));
            $event->setBlocked(true,);
        }
        if($distribution->getCatalogues()->isEmpty()){
            $event->addTransitionBlocker(new TransitionBlocker('manager.distribution.next_state.error_shareinprogress', 1));
            $event->setBlocked(true);
        }
    }

    public function guardReshare(TransitionEvent $event)
    {
        $this->userNotification->blockDistributionNotification();
    }

    public function guardIsOpen(EnteredEvent $event)
    {
        $this->userNotification->opendDistributionNotification();
    }

    public function guardIsClosed(EnteredEvent $event)
    {
        /* Delete all baskets */
        $baskets = $this->basketRepository->findAll();
        foreach ($baskets as $basket) {
            $this->entityManager->remove($basket);
        }
        $this->entityManager->flush();

        /* Send mail with orders */
        if (!$event->getSubject()->getOrders()->isEmpty()) {
            $folder = $this->dinaSpreadsheet->generateBulk($event->getSubject());
            $this->assoNotification->mailFiles($folder, $event->getSubject(), $this->params->get('locale'));
        }

    }

    public function guardEnd(GuardEvent $event)
    {
        /** @var Distribution $distribution */
        $distribution = $event->getSubject();  
        $today = new DateTime();

        $isAdmin = $this->security->getUser()->hasRole('ROLE_ADMIN');
        /* Can't end a distribution before the delivery date, except for admin */
        if((!$isAdmin) && ($this->debug !== 'dev')){
            if ($today < $distribution->getDeliveryDate()) {
                $event->addTransitionBlocker(new TransitionBlocker('manager.distribution.next_state.error_end', 0));
                $event->setBlocked(true);
            }
        }
    }

    public function guardIsEnded(EnteredEvent $event)
    {
        /** @var \App\Entity\Distribution $distribution */
        $distribution = $event->getSubject();

        /* Remove suggested items */
        $suggestedItems = $distribution->getSuggestedItems();
        foreach ($suggestedItems as $suggestedItem) {
            $this->entityManager->remove($suggestedItem);
        }
        $this->entityManager->flush();

        
        $uploadPath = dirname(dirname(__DIR__)) . '/' . $this->params->get('uploadPath');   
        $dirname = $uploadPath.'/'.$distribution->getId();

        $this->delete_directory($dirname);

        /* Auto archive catalogue if needed. Note: an extra catalogue is an auto-archived catalogue */
        $autoArchiveCatalogues = $this->catalogueRepository->findAllAutoArchiveOnlineByDistribution($distribution);
        if ($autoArchiveCatalogues) {
            foreach ($autoArchiveCatalogues as $catalogue) {
                $catalogue->setStatus(Catalogue::STATUS_ARCHIVED);
                $this->entityManager->persist($catalogue);
            }
            $this->entityManager->flush();
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            'workflow.distribution.guard.share' => ['guardShare'],
            'workflow.distribution.transition.reshare' => ['guardReShare'],
            'workflow.distribution.guard.end' => ['guardEnd'],
            'workflow.distribution.entered.opened' => ['guardIsOpen'],
            'workflow.distribution.entered.closed' =>['guardIsClosed'],
            'workflow.distribution.entered.ended' => ['guardIsEnded'],
        ];
    }


    private function delete_directory($dirname)
        {
            $dir_handle = null;
            if (is_dir($dirname))
                $dir_handle = opendir($dirname);
            if (!$dir_handle)
                return false;
            while ($file = readdir($dir_handle)) {
                if ($file != "." && $file != "..") {
                    if (!is_dir($dirname . "/" . $file))
                    unlink($dirname . "/" . $file);
                    else
                        $this->delete_directory($dirname . '/' . $file);
                }
            }
            closedir($dir_handle);
            rmdir($dirname);
            return true;
        }
}
