<?php

namespace App\EventSubscriber;

use App\Entity\Catalogue;
use App\Entity\Distribution;
use App\Entity\Item;
use App\Entity\Producer;
use App\Entity\User;
use App\Service\Date\LocaleDate;
use App\Service\Log\LogHandler;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class DatabaseActivitySubscriber implements EventSubscriber
{
    use LocaleDate;
   
    protected $logHandler;
    private $params;
    private $em;

    public function __construct(ParameterBagInterface $params,LogHandler $logHandler, EntityManagerInterface $em)
    {
        $this->logHandler = $logHandler;
        $this->params = $params;
        $this->em = $em;
    }

    // this method can only return the event names; you cannot define a
    // custom method name to execute when each event triggers
    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
            Events::postRemove,
            Events::postUpdate,
        ];
    }

    // callback methods must be called exactly like the events they listen to;
    // they receive an argument of type LifecycleEventArgs, which gives you access
    // to both the entity object of the event and the entity manager itself
    public function postPersist(LifecycleEventArgs $args): void
    {
        $this->logActivity('persist', $args);
    }

    public function postRemove(LifecycleEventArgs $args): void
    {
        $this->logActivity('remove', $args);
    }

    public function postUpdate(LifecycleEventArgs $args): void
    {
        $this->logActivity('update', $args);
    }

    private function logActivity(string $action, LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        $log = ['action' => $action, 'level' => 0];

        if ($entity instanceof Distribution) {
            $log['context'] = 'distribution';
            $localeDate = $this->setShortFormatLocale($entity->getDeliveryDate(), $this->params->get('locale'));
            $log['message'] = $localeDate . '/' . $entity->getStatus();
        } elseif ($entity instanceof Catalogue) {
            $log['context'] = 'catalogue';
            $log['message'] = $entity->getName() . '/' . $entity->getStatus() ;
        } elseif ($entity instanceof User) {

            /* We only want to track real user profile changes, no each order of them*/
            $uow = $this->em->getUnitOfWork();
            $uow->computeChangeSets();
            $changeset = $uow->getEntityChangeSet($entity);
            if(empty($changeset)){
                return;
            }
            if(array_key_exists('price',$changeset)){
                return;
            }
            if (array_key_exists('basket', $changeset)) {
                return;
            }
            $log['context'] = 'user';
            $log['message'] =  $entity->getUsername() ;
        } elseif ($entity instanceof Producer) {
            $log['context'] = 'producer';
            $log['message'] =  $entity->getName();
        } elseif ($entity instanceof Item) {
            if($action !==  'update'){
                return; /* Log only item edition */
            }
            $log['context'] = 'item';
            $log['message'] = $entity->getReference(). ' - ' .  $entity->getDesignation();
        } else{
            return;
        }

        $this->logHandler->write($log);
    }
}
