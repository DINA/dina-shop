<?php

namespace App\Twig;

use App\Service\ProductManager\Price;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class PriceExtension extends AbstractExtension
{
    use Price;
    
    public function getFunctions()
    {
        return [
            new TwigFunction('displayUnitaryPrice', [$this, 'getUnitaryPrice']),
        ];
    }
}
