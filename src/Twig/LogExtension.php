<?php

namespace App\Twig;

use App\Entity\Catalogue;
use App\Entity\Log;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class LogExtension extends AbstractExtension
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('getIcon', [$this, 'getIcon']),
            new TwigFilter('getMessage', [$this, 'getString']),
        ];
    }

    public function getIcon(string $context): string
    {
        switch ($context) {
            case 'distribution':
                return '<i class="fas fa-shopping-bag mr-2"></i>';
                break;
            case 'catalogue':
                return '<i class="fas fa-book mr-2"></i>';
                break;
            case 'item':
                return '<i class="fas fa-apple-alt mr-2"></i>';
                break;
            case 'user':
                return '<i class="fas fa-user mr-2"></i>';
                break;
            case 'producer':
                return '<i class="fas fa-truck mr-2"></i>';
                break;
            default:
                return '';
                break;
        }
    }

    public function getString(Log $entry)
    {
        $message = ['key' => '', 'params' => ['key' => '', 'value' => '']];

        switch($entry->getContext()){
            case 'distribution':
                $values = explode('/', $entry->getMessage());
                $date = $values[0];
                $status = $values[1];
                if ($entry->getAction() === 'persist') {
                    $message['key'] = 'manager.log.distribution.new';
                } elseif ($entry->getAction() === 'update') {
                    $message['key'] = 'manager.log.distribution.updated';
                } elseif ($entry->getAction() === 'remove') {
                    $message['key'] = 'manager.log.distribution.deleted';
                }
                $message['params'] = ['key' => '%date%', 'value' => $date];
                break;
            case 'catalogue':
                $values = explode('/', $entry->getMessage());
                $name = $values[0];
                $status = $values[1];
                if ($entry->getAction() === 'persist') {
                    $message['key'] = 'manager.log.catalogue.new';
                } elseif ($entry->getAction() === 'update') {
                    $message['key'] = ($status == Catalogue::STATUS_ARCHIVED)? 'manager.log.catalogue.archive' : 'manager.log.catalogue.updated';
                } elseif ($entry->getAction() === 'remove') {
                    $message['key'] = 'manager.log.catalogue.deleted';
                }
                $message['params'] = ['key' => '%name%', 'value' => $name];
                break;
            case 'user':
                if ($entry->getAction() === 'persist') {
                    $message['key'] = 'manager.log.user.new';
                } elseif ($entry->getAction() === 'update'){
                    $message['key'] = 'manager.log.user.updated';
                } elseif ($entry->getAction() === 'remove') {
                    $message['key'] = 'manager.log.user.deleted';
                }
                $message['params'] = ['key' => '%name%', 'value' => $entry->getMessage()];
                break;
            case 'producer':
                if ($entry->getAction() === 'persist') {
                    $message['key'] = 'manager.log.producer.new';
                } elseif ($entry->getAction() === 'update') {
                    $message['key'] = 'manager.log.producer.updated';
                } elseif ($entry->getAction() === 'remove') {
                    $message['key'] = 'manager.log.producer.deleted';
                }
                $message['params'] = ['key' => '%name%', 'value' => $entry->getMessage()];
                break;
            case 'item':
                if ($entry->getAction() === 'update') {
                    $message['key'] = 'manager.log.item.updated';
                    $message['params'] = ['key' => '%item%', 'value' => $entry->getMessage()];
                }
                break;
            default:
                break;
        }

        return $this->translator->trans($message['key'], [$message['params']['key'] => $message['params']['value']]);
    }
}
