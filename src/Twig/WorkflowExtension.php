<?php

namespace App\Twig;

use App\Entity\Distribution;
use App\Repository\DistributionRepository;
use Symfony\Component\Workflow\Registry;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class WorkflowExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('workflow_has_previous_marked_place', [$this, 'workflow_has_previous_marked_place']),
        ];
    }

    public function workflow_has_previous_marked_place(Distribution $distribution): array
    {
        $status = Distribution::getStatusIndex($distribution->getStatus());
        return array_filter(Distribution::ALLOWED_STATUSES, function($statuses) use ($status){
             return (Distribution::getStatusIndex($statuses) <= $status)? true: false;          
        });
    }
}
