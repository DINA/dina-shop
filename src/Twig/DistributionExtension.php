<?php

namespace App\Twig;

use App\Entity\Catalogue;
use App\Repository\DistributionRepository;
use Exception;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class DistributionExtension extends AbstractExtension
{
    private $translator;
    private $distributionRepository;

    public function __construct(TranslatorInterface $translator, DistributionRepository $distributionRepository)
    {
        $this->translator = $translator;
        $this->distributionRepository = $distributionRepository;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('getDistributionStatus', [$this, 'getDistributionStatus']),
        ];
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getDistributionInProgress', [$this, 'getDistributionInProgress']),

        ];
    }

    public function getDistributionStatus(string $status)
    {
        switch($status){
            case 'new':
                $trans = $this->translator->trans('manager.distribution.status.new', []);
                break;
            case 'shareinprogress':
                $trans = $this->translator->trans('manager.distribution.status.shareinprogress', []);
                break;
            case 'sharedone':
                $trans = $this->translator->trans('manager.distribution.status.sharedone', []);
                break;
            case 'opened':
                $trans = $this->translator->trans('manager.distribution.status.opened', []);
                break;
            case 'closed':
                $trans = $this->translator->trans('manager.distribution.status.closed', []);
                break;
            case 'ended':
                $trans = $this->translator->trans('manager.distribution.status.ended', []);
                break;
            default:
                throw new Exception('Unknown distribution status');
            break;
        };
        return $trans;
    }

    public function getDistributionInProgress()
    {
        return $this->distributionRepository->findDistributionInProgress();
        
    }
}
