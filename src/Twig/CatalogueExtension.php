<?php

namespace App\Twig;

use App\Entity\Catalogue;
use App\Entity\Product;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class CatalogueExtension extends AbstractExtension
{
    private $CatalogueConfigReader;
    private $translator;

    public function __construct(CatalogueConfigReader $CatalogueConfigReader, TranslatorInterface $translator)
    {
        $this->CatalogueConfigReader = $CatalogueConfigReader;
        $this->translator = $translator;
    }
    
    public function getFilters()
    {
        return [
            new TwigFilter('catalogueType', [$this, 'findCatalogueName']),
            new TwigFilter('getCatalogueStatus', [$this, 'getCatalogueStatus']),
            new TwigFilter('getCurrentDistributionLinks', [$this, 'getCurrentDistributionLinks']),
            new TwigFilter('getLinkedDistribution', [$this, 'getLinkedDistribution']),
        ];
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('keysMapping', [$this, 'keysMapping']),
            new TwigFunction('getCatalogueNames', [$this, 'getCatalogueNames']),
        ];
    }

    public function findCatalogueName($type)
    {
        return $this->CatalogueConfigReader->findCatalogueName($type);
    }

    public function keysMapping($type)
    {
        return $this->CatalogueConfigReader->getCatalogueKeysMapping($type);
    }

    public function getCatalogueNames(array $catalogueNames, $product, ?int $filter=null): array
    {
        if (!in_array($product->getItem()->getCatalogueId()->getName(), $catalogueNames)) {
            if(!isset($filter) || ($product->getOrderOrigin() === $filter)){
                $catalogueNames[] = $product->getItem()->getCatalogueId()->getName();
            }
        }
        return $catalogueNames;
    }

    public function getCatalogueStatus(int $status)
    {
        if($status === Catalogue::STATUS_ONLINE){
            return $this->translator->trans('manager.catalogue.status.online', []);
        } else{
            return $this->translator->trans('manager.catalogue.status.archived', []);
        }   
    }

    public function getLinkedDistribution(Catalogue $catalogue, ?bool $searchCurrentDistribution = true)
    {
        $listDate = [];
        foreach ($catalogue->getDistributions() as $distribution) {
            if(!$searchCurrentDistribution || $distribution->isDistributionInProgress()){
                $listDate[$distribution->getId()] = $distribution->getDeliveryDate();
            }
        }
        return $listDate;
    }
}
