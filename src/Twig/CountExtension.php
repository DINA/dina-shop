<?php

namespace App\Twig;

use App\Repository\BasketRepository;
use App\Repository\OrderRepository;
use App\Repository\SharingProposalRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CountExtension extends AbstractExtension
{
    private $sharingProposalRepository;
    private $basketRepository;
    private $orderRepository;

    public function __construct(SharingProposalRepository $sharingProposalRepository, 
                                                    BasketRepository $basketRepository,
                                                    OrderRepository $orderRepository)
    {
        $this->sharingProposalRepository = $sharingProposalRepository;
        $this->basketRepository = $basketRepository;
        $this->orderRepository = $orderRepository;
    }
    public function getFunctions()
    {
        return [
            new TwigFunction('countSharingProposalItems', [$this->sharingProposalRepository, 'countItems']),
            new TwigFunction('countBasketItems', [$this->basketRepository, 'countItems']),
            new TwigFunction('countOrdertItems', [$this->orderRepository, 'countItems']),
        ];
    }

}
