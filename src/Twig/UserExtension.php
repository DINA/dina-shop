<?php

namespace App\Twig;

use App\Entity\User;
use App\Repository\BasketRepository;
use App\Repository\OrderRepository;
use App\Repository\SharingProposalRepository;
use Exception;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class UserExtension extends AbstractExtension
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('hasOnlyUserRole', [$this, 'hasOnlyUserRole']),
        ];
    }
    
    public function getFilters()
    {
        return [
            new TwigFilter('getUserStatus', [$this, 'getUserStatus']),
        ];
    }

    public function hasOnlyUserRole (User $user): bool
    {
        $roles = $user->getRoles();
        return ((1 === count($roles)) && ($roles['0'] === 'ROLE_USER'))? true : false;
    }

    public function getUserStatus(int $status)
    {
        switch ($status) {
            case User::STATUS_ACTIVE:
                $trans = $this->translator->trans('admin.user.status.active', []);
                break;
            case User::STATUS_INACTIVE:
                $trans = $this->translator->trans('admin.user.status.inactive', []);
                break;
            case User::STATUS_WAITING_APPROVAL:
                $trans = $this->translator->trans('admin.user.status.waiting', []);
                break;
            case User::STATUS_DELETED:
                $trans = $this->translator->trans('admin.user.status.deleted', []);
                break;
            default:
                throw new Exception('Unknown user status');
                break;
        };
        return $trans;
    }
}
