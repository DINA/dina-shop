<?php

namespace App\Twig;

use App\Controller\Shop\ShopItemController;
use App\Repository\BasketRepository;
use App\Repository\OrderRepository;
use App\Repository\SharingProposalRepository;
use App\Service\ProductManager\Flag;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class ItemExtension extends AbstractExtension
{
    use Flag;

    public function getFilters()
    {
        return [
            new TwigFilter('getOriginFlag', [$this, 'getCountryFlag']),
        ];
    }
}