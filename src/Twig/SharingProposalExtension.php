<?php

namespace App\Twig;

use App\Entity\SharedProduct;
use App\Entity\SharingProposal;
use Exception;
use Symfony\Component\Form\FormView;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class SharingProposalExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('sortSharingProposal', ['App\Entity\SharingProposal','sortSharingProposal']),
        ];
    }
}