<?php

namespace App\Security\Voter;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class UserProfileVoter extends Voter
{
    protected function supports($attribute, $subject)
    {
        return in_array($attribute, ['SHOW_PROFILE', 'SHOW_ORDER', 'EDIT_PROFILE', 'EDIT_ADMIN_PROFILE'])
            && $subject instanceof \App\Entity\User;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /** @var \App\Entity\User $user */
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'SHOW_PROFILE':
            case 'SHOW_ORDER':
                return ( $user->getId() === $subject->getId())? true : false;
            break;
            case 'EDIT_PROFILE':
                /* Super users can't edit themselves their profile, only admin can */
                return ($user->hasRole(['ROLE_WORKER', 'ROLE_COORDINATOR', 'ROLE_MANAGER']))? false : true;
            break;
            case 'EDIT_ADMIN_PROFILE':
                /* The coordinator can't change the super users profiles, only members */
                return (($user->hasRole(['ROLE_COORDINATOR']) && 
                    (($subject->hasRole(['ROLE_WORKER', 'ROLE_COORDINATOR', 'ROLE_MANAGER', 'ROLE_ADMIN']))
                    || ($subject->getStatus() == User::STATUS_DELETED))
                    )) ? false : true;
                break;

        }

        return false;
    }
}
