<?php

namespace App\Repository;

use App\Entity\SharingProposal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SharingProposal|null find($id, $lockMode = null, $lockVersion = null)
 * @method SharingProposal|null findOneBy(array $criteria, array $orderBy = null)
 * @method SharingProposal[]    findAll()
 * @method SharingProposal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SharingProposalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SharingProposal::class);
    }

    public function countItems(int $distribId): int
    {
        return $this->createQueryBuilder('s')
        ->select('count(item)')
        ->join('s.sharedProducts', 'item')
        ->join('s.Distribution', 'distribution')
        ->where('distribution.id = :id')
        ->setParameter('id',$distribId)
        ->getQuery()
        ->getSingleScalarResult();
    }

    // /**
    //  * @return SharingProposal[] Returns an array of SharingProposal objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SharingProposal
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
