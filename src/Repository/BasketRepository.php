<?php

namespace App\Repository;

use App\Entity\Basket;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

/**
 * @method Basket|null find($id, $lockMode = null, $lockVersion = null)
 * @method Basket|null findOneBy(array $criteria, array $orderBy = null)
 * @method Basket[]    findAll()
 * @method Basket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BasketRepository extends ServiceEntityRepository
{
    private $security;

    public function __construct(ManagerRegistry$registry, Security $security)
    {
        parent::__construct($registry, Basket::class);
        $this->security = $security;
    }

    public function countItems(?string $filter=null): int
    {
        $q = $this->createQueryBuilder('b')
            ->select('count(product)')
            ->join('b.user', 'user')
            ->join('b.product', 'product')
            ->andWhere('user.id = :user')
            ->setParameter('user', $this->security->getUser()->getId());

        if('shared' === $filter){
            $q->andWhere('product.relatedSharedProduct IS NOT null');
        } elseif('individual' === $filter){
            $q->andWhere('product.relatedSharedProduct IS null');
        }

        return $q->getQuery()->getSingleScalarResult();
    }

    // /**
    //  * @return Basket[] Returns an array of Basket objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Basket
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
