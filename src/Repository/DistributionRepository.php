<?php

namespace App\Repository;

use App\Entity\Distribution;
use App\Service\DistributionStatusCheck\DistributionStatusCheck;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Distribution|null find($id, $lockMode = null, $lockVersion = null)
 * @method Distribution|null findOneBy(array $criteria, array $orderBy = null)
 * @method Distribution[]    findAll()
 * @method Distribution[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DistributionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Distribution::class);
    }

    public function findAll()
    {
        return $this->findBy(array(), array('id' => 'DESC'));
    }

    public function findAllQuery(): Query
    {
        return $this->createQueryBuilder('d')
            ->addOrderBy('d.id', 'DESC')
            ->getQuery();
    }

    public function findDistributionInProgress(): ?Distribution
    {
       return   $this->findDistributionByStatus([
            Distribution::STATUSES_NAMES[Distribution::STATUS_SHAREINPROGRESS], 
            Distribution::STATUSES_NAMES[Distribution::STATUS_SHAREDONE], 
            Distribution::STATUSES_NAMES[Distribution::STATUS_OPENED],
            Distribution::STATUSES_NAMES[Distribution::STATUS_CLOSED]])->getOneOrNullResult();
    }

    public function findDistributionInNewState(): ?Distribution
    {
        return $this->findDistributionByStatus(['new'])->getOneOrNullResult();
    }

    public function findDistributionInShareState(): ?Distribution
    {
        return $this->findDistributionByStatus(['shareinprogress', 'sharedone'])->getOneOrNullResult();
    }

    public function findDistributionInShareProgressState(): ?Distribution
    {
        return $this->findDistributionByStatus(['shareinprogress'])->getOneOrNullResult();
    }

    public function findDistributionInOpenState(): ?Distribution
    {
        return $this->findDistributionByStatus(['opened'])->getOneOrNullResult();
    }

    private function findDistributionByStatus(array $statuses, ?int $exceptId = null): Query
    {
        $qb = $this->createQueryBuilder('d')
            ->where('d.status IN (:statuses)')
            ->setParameter('statuses', $statuses)
            ;

        if($exceptId){
            $qb->andWhere('d.id != :id')
            ->setParameter('id', $exceptId);
        }

        return $qb->getQuery();
    }

    // /**
    //  * @return Distribution[] Returns an array of Distribution objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Distribution
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
