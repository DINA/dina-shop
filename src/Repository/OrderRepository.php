<?php

namespace App\Repository;

use App\Entity\Distribution;
use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    private $security;

    public function __construct(ManagerRegistry $registry, Security $security)
    {
        parent::__construct($registry, Order::class);
        $this->security = $security;
    }

    public function countItems(?string $filter = null): int
    {
        $q = $this->createQueryBuilder('o')
            ->select('count(product)')
            ->join('o.product', 'product')
            ->join('o.distribution', 'distribution')
            ->join('o.user', 'user')
            ->where('distribution.status = :status')
            ->setParameter('status', Distribution::ALLOWED_STATUSES[Distribution::STATUS_OPENED])
            ->andWhere('user.id = :user')
            ->setParameter('user', $this->security->getUser()->getId());

        if ('shared' === $filter) {
            $q->andWhere('product.relatedSharedProduct IS NOT null');
        } elseif ('individual' === $filter) {
            $q->andWhere('product.relatedSharedProduct IS null');
        }

        return $q->getQuery()->getSingleScalarResult();
    }

    // /**
    //  * @return Order[] Returns an array of Order objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Order
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
