<?php

namespace App\Repository;

use App\Entity\Catalogue;
use App\Entity\Distribution;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Flex\Unpack\Result;

/**
 * @method Catalogue|null find($id, $lockMode = null, $lockVersion = null)
 * @method Catalogue|null findOneBy(array $criteria, array $orderBy = null)
 * @method Catalogue[]    findAll()
 * @method Catalogue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CatalogueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Catalogue::class);
    }

    public function findAllQuery(): Query
    {
        return $this->createQueryBuilder('c')
            ->orderBy('c.status', 'DESC')
            ->addOrderBy('c.id','DESC')
            ->getQuery();
    }

    public function findExtraOnline(string $type): ?Catalogue
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.extra = true')
            ->andWhere('c.status = :status')
            ->setParameter('status', Catalogue::STATUS_ONLINE)
            ->andWhere('c.type = :type')
            ->setParameter('type', $type)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllExtraOnline(): ?array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.extra = true')
            ->andWhere('c.status = :status')
            ->setParameter('status', Catalogue::STATUS_ONLINE)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findAllExtraOnlineByDistribution(?Distribution $distribution): ?array
    {
        if(!$distribution){
            return null;
        }
        return $this->createQueryBuilder('c')
            ->andWhere('c.extra = true')
            ->andWhere('c.status = :status')
            ->setParameter('status', Catalogue::STATUS_ONLINE)
            ->join('c.distributions', 'distribution')
            ->andWhere('distribution.id = :id')
            ->setParameter('id', $distribution->getId())
            ->getQuery()
            ->getResult();
    }

    public function findAllAutoArchiveOnlineByDistribution(?Distribution $distribution): ?array
    {
        if (!$distribution) {
            return null;
        }
        return $this->createQueryBuilder('c')
            ->andWhere('c.autoArchive = true')
            ->andWhere('c.status = :status')
            ->setParameter('status', Catalogue::STATUS_ONLINE)
            ->join('c.distributions', 'distribution')
            ->andWhere('distribution.id = :id')
            ->setParameter('id', $distribution->getId())
            ->getQuery()
            ->getResult();
    }

    public function findAllByDistribution(?Distribution $distribution): ?array
    {
        if (!$distribution) {
            return null;
        }
        return $this->createQueryBuilder('c')
            ->join('c.distributions', 'distribution')
            ->andWhere('distribution.id = :id')
            ->setParameter('id', $distribution->getId())
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Catalogue[] Returns an array of Catalogue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Catalogue
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
