<?php

namespace App\Repository;

use App\Entity\Bookmark;
use App\Entity\Catalogue;
use App\Entity\CatalogueSearch;
use App\Entity\Distribution;
use App\Entity\Item;
use App\Entity\SuggestedItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Item::class);
    }

    public function findByCatalogue(?QueryBuilder $builder=null, Catalogue $catalogue =null, ?array $order=null): QueryBuilder
    {
        $qb = ($builder)? $builder->select('i')->from(Item::class, 'i') : $this->createQueryBuilder('i');
       
        if (!$catalogue) {
            return $qb;
        }
        /* Default order */
        if (empty($order)) {
            $order = ['column' => 'id', 'dir' => 'ASC'];
        }

        return $qb  
            ->leftJoin('i.catalogueId', 'cat')
            ->addSelect('cat')
            ->where('cat.id = :catId')
            ->setparameter('catId', $catalogue->getId())
            ->orderBy('i.' . $order['column'], $order['dir'])
            ;
    }

    public function findAllOnlineExtraItems($builder, ?array $order = null): QueryBuilder
    {
        /* Default order */
        if (empty($order)) {
            $order = ['column' => 'id', 'dir' => 'ASC'];
        }

        return $builder
            ->select('i')
            ->from(Item::class, 'i')
            ->leftJoin('i.catalogueId', 'cat')
            ->addSelect('cat')
            ->where('cat.extra = true')
            ->andWhere('cat.status = :status')
            ->setParameter('status', Catalogue::STATUS_ONLINE)
            ->orderBy('i.' . $order['column'], $order['dir'])
            ;
    }

    public function countByCatalogue(Catalogue $catalogue, ?array $params=null): int
    {
        $key = $params['search']['value'] ?? null;

        $qb = $this->createQueryBuilder('i');
        $qb
            ->leftJoin('i.catalogueId', 'cat')
            ->where('cat.id = :catId')
            ->select('count(i.id)')
            ->setparameter('catId', $catalogue->getId())
            ->orderBy('i.id', 'ASC');
        if ($key) {
            $keys = str_replace(' ', '%', $key);
            $qb
                ->andWhere('i.reference LIKE :key OR i.designation LIKE :key')
                ->setParameter('key', '%' . $keys . '%');
        }
        
        return $qb->getQuery()->getSingleScalarResult();
    }

    public function findByCriterias(?QueryBuilder $builder=null, CatalogueSearch $search, ?array $order = null, ?array $range = null, ?bool $count = false): QueryBuilder
    {

        $builder = $this->findByCatalogue($builder, $search->getCatalogue(), $order);

        if (!empty($search->getOrigins())) {
            $this->queryOrigins($builder, $search->getOrigins());
        }

        if (!empty($search->getFamilies())) {
            $this->queryFamilies($builder, $search->getFamilies());
        }

        if (!empty($search->getBrands())) {
            $this->queryBrands($builder, $search->getBrands());
        }

        if (!empty($search->getSearchText())) {
            $this->querySearchText($builder, $search->getSearchText());
        }

        if (!$count && isset($range['start']) && isset($range['length'])) {
            $builder
                ->setFirstResult($range['start'])
                ->setMaxResults($range['length']);
        } elseif ($count) {
            $builder->select('count(i.id)');
        }

        return $builder;
    }

    public function findforEquivalentItem($catalogues, Item $oldItem): ?Item
    {
        return $this->createQueryBuilder('i')
            ->leftJoin('i.catalogueId', 'cat')
            ->leftJoin('cat.producerId', 'producer')
            ->where('cat.id IN (:catIds)')
            ->setParameter('catIds', $catalogues)
            ->andWhere('i.reference = :reference')
            ->setParameter('reference', $oldItem->getReference())
            ->andWhere('i.designation = :designation')
            ->setparameter('designation', $oldItem->getDesignation())
            ->andWhere('producer.name = :name')
            ->setParameter('name', $oldItem->getCatalogueId()->getProducerId()->getName())
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findforBookmarkedItem($catalogues, Bookmark $bookmark): ?Item
    {
        $query = $this->findforSpecificItem($catalogues, $bookmark);
        try{
            $item = $query->getOneOrNullResult();
        } catch(NonUniqueResultException $e){
            $items = $query->getResult(); /* Get list */
            /* Several identicals items are allowed only for the same producer (example: one item in catalogue frais and the other in mercuriale viande ) */
            $producers = array_map(function($item){
                return $item->getCatalogueId()->getProducerId()->getName();
            }, $items);
            $sameProducer = (1 === count(array_unique($producers)))? true : false;
            if($sameProducer){
                $item = $items[0]; /* Use arbitrary the first item returned */
            } else{
                throw new NonUniqueResultException('Same item for differents producers not allowed');
            }
        }
        return $item;
    }

    public function findforSuggestedItem($catalogues, SuggestedItem $suggestedItem): ?Item
    {
        return $this->findforSpecificItem($catalogues, $suggestedItem)->getOneOrNullResult();
    }

    /**
     * findforSpecificItem
     * Find an item based on the catalogue, reference and designation
     * Since November 2021, strictly equal designation is no more a criteria to
     * avoid multiple "almost same" bookmarks in the user bookmark list
     *
     * @param  mixed $catalogues
     * @param  mixed $specificItem
     * @return Query
     */
    private function findforSpecificItem($catalogues, $specificItem): Query
    {
        return $this->createQueryBuilder('i')
            ->leftJoin('i.catalogueId', 'cat')
            ->leftJoin('cat.producerId', 'producer')
            ->where('cat.id IN (:catIds)')
            ->setParameter('catIds', $catalogues)
            ->andWhere('i.reference = :reference')
            ->setParameter('reference', $specificItem->getReference())
            ->andWhere('producer.name = :name')
            ->setParameter('name', $specificItem->getProducerName())
            ->getQuery();
    }

    public function listFamilies($catalogue): QueryBuilder
    {
        return $this->listChoices($catalogue)
            ->groupBy('i.family');
    }

    public function listOrigins($catalogue): QueryBuilder
    {
        return $this->listChoices($catalogue)
            ->groupBy('i.origin');
    }

    public function listBrands($catalogue): QueryBuilder
    {
        return $this->listChoices($catalogue)
            ->groupBy('i.brand');
    }

    private function listChoices($catalogSearch): QueryBuilder
    {
        if (is_object($catalogSearch['catalogue'])) {
            $idCat = $catalogSearch['catalogue']->getId();
        } else {
            $idCat = $catalogSearch['catalogue'];
        }
        $qb = $this->createQueryBuilder('i')
            ->leftJoin('i.catalogueId', 'cat')
            ->where('cat.id = :catId')
            ->setparameter('catId', $idCat);

        /* quand on change de catalogue on veut reconstruire l'index des 3 filtres et ne pas reprendre les valeus précédemment sélectionnées

        if (!empty($catalogSearch['origins'])) {
            $this->queryOrigins($qb, $catalogSearch['origins']);
        }

        if (!empty($catalogSearch['families'])) {
            $this->queryFamilies($qb, $catalogSearch['families']);
        }

        if (!empty($catalogSearch['brands'])) {
            $this->queryBrands($qb, $catalogSearch['brands']);
        }
 */
        //if (!empty($catalogSearch['searchText'])) {
        //    $this->querySearchText($qb,  $catalogSearch['searchText']);
        //}

        return $qb;
    }

    private function queryOrigins(QueryBuilder $query, $key): QueryBuilder
    {
        return $query
            ->andWhere('i.origin IN (:origins)')
            ->setParameter('origins', $key);
    }

    private function queryFamilies(QueryBuilder $query, $key): QueryBuilder
    {
        return $query
            ->andWhere('i.family IN (:families)')
            ->setParameter('families', $key);
    }

    private function queryBrands(QueryBuilder $query, $key): QueryBuilder
    {
        return $query
            ->andWhere('i.brand IN (:brands)')
            ->setParameter('brands', $key);
    }

    private function querySearchText(QueryBuilder $query, $key): QueryBuilder
    {
        $keys = str_replace(' ', '%', $key);
        return $query
            ->andWhere('i.reference LIKE :key OR i.designation LIKE :key')
            ->setParameter('key', '%' . $keys . '%');
    }

    // /**
    //  * @return Item[] Returns an array of Item objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Item
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
