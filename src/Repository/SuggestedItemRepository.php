<?php

namespace App\Repository;

use App\Entity\Distribution;
use App\Entity\SuggestedItem;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SuggestedItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method SuggestedItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method SuggestedItem[]    findAll()
 * @method SuggestedItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SuggestedItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SuggestedItem::class);
    }

    public function findByDistributionAndUser(Distribution $distribution, User $user, SuggestedItem $suggestedItem)
    {
        return $this->createQueryBuilder('s')
            ->join('s.distribution', 'distribution')
            ->andWhere('distribution.id = :distributionId')
            ->setParameter('distributionId', $distribution->getId())
            ->join('s.users', 'user')
            ->andWhere('user.id = :userId')
            ->setParameter('userId', $user->getId())
            ->andWhere('s.id = :suggestedItemId')
            ->setParameter('suggestedItemId', $suggestedItem->getId())
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllByDistributionAndUser(Distribution $distribution, User $user)
    {
        return $this->createQueryBuilder('s')
            ->join('s.distribution', 'distribution')
            ->andWhere('distribution.id = :distributionId')
            ->setParameter('distributionId', $distribution->getId())
            ->join('s.users', 'user')
            ->andWhere('user.id = :userId')
            ->setParameter('userId', $user->getId())
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return SuggestedItem[] Returns an array of SuggestedItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SuggestedItem
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
