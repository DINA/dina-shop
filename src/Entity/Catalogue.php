<?php

namespace App\Entity;

use App\Repository\CatalogueRepository;
use App\Service\CatalogueImport\CatalogueImport;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use App\Validator\Constraints\Catalogue as CatalogueConstraint;
use App\Validator\Constraints\CatalogueWorker as CatalogueWorkerConstraint;

/**
 * @ORM\Entity(repositoryClass=CatalogueRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 * @CatalogueConstraint()
 * @CatalogueWorkerConstraint(groups={"worker"})
 */
class Catalogue
{
    const STATUS_ARCHIVED = 0;
    const STATUS_ONLINE = 1;

    const STATUS_NAMES = ['archived', 'online'];

    const CAT_STATUS = [
        self::STATUS_ARCHIVED => self::STATUS_NAMES[self::STATUS_ARCHIVED],
        self::STATUS_ONLINE => self::STATUS_NAMES[self::STATUS_ONLINE]
    ];
   
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Producer::class, inversedBy="catalogues")
     * @ORM\JoinColumn(onDelete="SET NULL") 
     */
    private $producerId;

    /**
     * @ORM\Column(type="integer")
     * CAT_STATUS type
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $type;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(max=1024)
     */
    private $comment;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $spreadsheet;

    /**
     *  @Assert\File(
     *     mimeTypes = {"application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.oasis.opendocument.spreadsheet", "application/octet-stream"},
     *     mimeTypesMessage = "form.catalogue.spreadsheetFile"
     * )
     * @Vich\UploadableField(mapping="catalogue_sreadsheet", fileNameProperty="spreadsheet", originalName="spreadsheetName")
     * @var File | null
     */
    private $spreadsheetFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $spreadsheetName;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Type("\DateTime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\Type("\DateTime")
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=Item::class, mappedBy="catalogueId", orphanRemoval=true, cascade={"persist"})
     */
    private $items;

    /**
     * @ORM\ManyToMany(targetEntity=Distribution::class, mappedBy="Catalogues")
     */
    private $distributions;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $extra = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $autoArchive = false;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $format;
    

    private $dropboxFile;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->setOnlineStatus();
        $this->items = new ArrayCollection();
        $this->distributions = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProducerId(): ?Producer
    {
        return $this->producerId;
    }

    public function setProducerId(?Producer $producerId): self
    {
        $this->producerId = $producerId;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAt(): self
    {
        $this->updatedAt = new \Datetime();

        return $this;
    }

    public function getStatusName(): string
    {
        return self::CAT_STATUS[$this->getStatus()];
    }

    public function isStatus(int $status): bool
    {
        return ($this->getStatus() === $status)? true : false;
    }

    public function setOnlineStatus(): self
    {
        $this->setStatus(array_search('online', self::CAT_STATUS));
        return $this;
    }

    public function setArchivedStatus(): self
    {
        $this->setStatus(array_search('archived', self::CAT_STATUS));
        return $this;
    }

    public static function getStatusValue(string $status): string
    {
        return array_search($status, self::CAT_STATUS);
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setCatalogueId($this);
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getCatalogueId() === $this) {
                $item->setCatalogueId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Distribution[]
     */
    public function getDistributions(): Collection
    {
        return $this->distributions;
    }

    public function addDistribution(Distribution $distribution): self
    {
        if (!$this->distributions->contains($distribution)) {
            $this->distributions[] = $distribution;
            $distribution->addCatalogue($this);
        }

        return $this;
    }

    public function removeDistribution(Distribution $distribution): self
    {
        if ($this->distributions->contains($distribution)) {
            $this->distributions->removeElement($distribution);
            $distribution->removeCatalogue($this);
        }

        return $this;
    }


    /**
     * Get the value of spreadsheet
     *
     * @return  string
     */ 
    public function getSpreadsheet()
    {
        return $this->spreadsheet;
    }

    /**
     * Set the value of spreadsheet
     *
     * @param  string  $spreadsheet
     *
     * @return  self
     */ 
    public function setSpreadsheet(?string $spreadsheet)
    {
        $this->spreadsheet = $spreadsheet;

        return $this;
    }

    /**
     * Get | null
     *
     * @return  File
     */ 
    public function getSpreadsheetFile()
    {
        return $this->spreadsheetFile;
    }

    /**
     * Set | null
     *
     * @param  File  $spreadsheetFile  | null
     *
     * @return  self
     */ 
    public function setSpreadsheetFile(File $spreadsheetFile = null)
    {
        $this->spreadsheetFile = $spreadsheetFile;

        if ($spreadsheetFile) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * Get the value of spreadsheetName
     *
     * @return  string|null
     */ 
    public function getSpreadsheetName()
    {
        return $this->spreadsheetName;
    }

    /**
     * Set the value of spreadsheetName
     *
     * @param  string|null  $spreadsheetName
     *
     * @return  self
     */ 
    public function setSpreadsheetName(?string $spreadsheetName)
    {
        $this->spreadsheetName = $spreadsheetName;

        return $this;
    }

    public function getExtra(): ?bool
    {
        return $this->extra;
    }

    public function setExtra(?bool $extra): self
    {
        $this->extra = $extra;

        return $this;
    }

    public function getAutoArchive(): ?bool
    {
        return $this->autoArchive;
    }

    public function setAutoArchive(?bool $autoArchive): self
    {
        $this->autoArchive = $autoArchive;

        return $this;
    }

    public function getFormat(): ?string
    {
        return $this->format;
    }

    public function setFormat(string $format): self
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get the value of dropboxFile
     */ 
    public function getDropboxFile(): ?DropboxFile
    {
        return $this->dropboxFile;
    }

    /**
     * Set the value of dropboxFile
     *
     * @return  self
     */ 
    public function setDropboxFile(DropboxFile $dropboxFile)
    {
        $this->dropboxFile = $dropboxFile;
        /* Because dropbox is not a doctrine entity, we neet to update this field to force the CatalogueListener processing */
        $this->setUpdatedAt(new \Datetime());

        return $this;
    }
}
