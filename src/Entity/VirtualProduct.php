<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\MappedSuperclass;

/**
 * @MappedSuperclass
 */
abstract class VirtualProduct
{
    const PRODUCTUNIT_UNDEFINED = 0;
    const PRODUCTUNIT_KG = 1;
    const PRODUCTUNIT_PARCEL = 2;
    const PRODUCTUNIT_PIECE = 3;

    const PRODUCTUNIT = [
        self::PRODUCTUNIT_KG => 'kg',
        self::PRODUCTUNIT_PARCEL => "parcel", 
        self::PRODUCTUNIT_PIECE => "piece",
    ];

    const ORDERORIGIN_SHARE = 0;
    const ORDERORIGIN_INDIVIDUAL = 1;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $productUnit = self::PRODUCTUNIT_PARCEL ;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $orderOrigin = self::ORDERORIGIN_INDIVIDUAL;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProductUnit(): int
    {
        return $this->productUnit;
    }

    public function getProductUnitString(): string
    {
        return self::PRODUCTUNIT[$this->productUnit];
    }

    public function setProductUnit(?int $productUnit): self
    {
        if(isset($productUnit)){
            $this->productUnit = $productUnit;
        }

        return $this;
    }

    public function getOrderOrigin(): ?int
    {
        return $this->orderOrigin;
    }

    public function setOrderOrigin(int $orderOrigin): self
    {
        $this->orderOrigin = $orderOrigin;

        return $this;
    }
}
