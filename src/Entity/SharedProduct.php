<?php

namespace App\Entity;

use App\Repository\SharedProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass=SharedProductRepository::class)
 */
class SharedProduct extends VirtualProduct
{
    const SUBTYPE_UNDEFINED = 0;
    const SUBTYPE_WHEIGHT = 1;
    const SUBTYPE_SHARE = 2;
    const SUBTYPE_CHEESE = 3;

    const SUBTYPE = [
        self::SUBTYPE_WHEIGHT => 'to weigh',
        self::SUBTYPE_SHARE => 'to share',
        self::SUBTYPE_CHEESE => "cheese",
    ];
    /* const SUBTYPE2 = [
        'default' => self::SUBTYPE_UNDEFINED,
        "cheese" => self::SUBTYPE_CHEESE,
    ]; */

    /**
     * @ORM\ManyToOne(targetEntity=Item::class, inversedBy="sharedProducts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $item;


    /**
     * @ORM\ManyToOne(targetEntity=SharingProposal::class, inversedBy="sharedProducts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sharingProposal;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     * @Assert\Range(
     *      min = 1,
     *      max = 99,
     * )
     */
    private $pieceNumber;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $subType;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isCheese;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $packaging;
    /* Packaging is added only to peform a validation on the string, in order to avoid errors when excel spreadsheet will be generated */

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max = 255,
     *      allowEmptyString = true
     * )
     */
    private $comment;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="relatedSharedProduct", orphanRemoval=true)
     */
    private $individualProducts;

    public function __construct()
    {
        $this->individualProducts = new ArrayCollection();
    }

    
    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): self
    {
        $this->item = $item;

        return $this;
    }


    public function getSharingProposal(): ?SharingProposal
    {
        return $this->sharingProposal;
    }

    public function setSharingProposal(?SharingProposal $sharingProposal): self
    {
        $this->sharingProposal = $sharingProposal;

        return $this;
    }

    public function getPieceNumber(): ?int
    {
        return $this->pieceNumber;
    }

    public function setPieceNumber(?int $pieceNumber): self
    {
        $this->pieceNumber = $pieceNumber;

        return $this;
    }

    public function getPackaging(): ?string
    {
        return $this->packaging;
    }

    public function setPackaging(?string $packaging): self
    {
        $this->packaging = $packaging;

        return $this;
    }

    /**
     * @Assert\Callback
     */
    public function isFormValid(ExecutionContextInterface $context)
    {
        if ((($this->getProductUnit() != Product::PRODUCTUNIT_PIECE)) && (null !== ($this->getPieceNumber()))) {  
            $context
                ->buildViolation('form.sharing_proposal.not_a_piece') 
                ->atPath('pieceNumber')
                ->addViolation()
            ;
            $context
                ->buildViolation('form.sharing_proposal.not_a_piece_full')
                ->setParameter('{{ designation }}', $this->getItem()->getDesignation())
                ->addViolation();
        } elseif ($this->getIsCheese() && ($this->getProductUnit() != Product::PRODUCTUNIT_KG)) {
            $context
                ->buildViolation('form.sharing_proposal.cheese')
                ->atPath('productUnit')
                ->addViolation();
            $context
                ->buildViolation('form.sharing_proposal.cheese_full')
                ->setParameter('{{ designation }}', $this->getItem()->getDesignation())
                ->addViolation();
        } elseif((($this->getProductUnit() == Product::PRODUCTUNIT_PIECE)) && (!$this->getPieceNumber()))
        {
            $context
                ->buildViolation('form.sharing_proposal.piece_missing') 
                ->atPath('pieceNumber')
                ->addViolation()
                ;
            $context
                ->buildViolation('form.sharing_proposal.piece_missing_full')
                ->setParameter('{{ designation }}', $this->getItem()->getDesignation())
                ->addViolation()
            ;
        } elseif ( $this->getProductUnit() == Product::PRODUCTUNIT_KG) {
            /* format OK: 2 - 2kg - 2kgs - env 2.5kg - env 2,5 - etc... */
            if (!preg_match('#(^(env)?[ ]?([0-9]?[0-9]([.|,][0-9][0-9]?)?)[ ]*(kg[s]?|[K]{1})?)$#i', $this->getItem()->getPackaging())) {
                $context
                ->buildViolation( 'form.sharing_proposal.packaging')
                ->setParameter('{{ designation }}', $this->getItem()->getDesignation())
                ->setParameter('{{ packaging }}', $this->getItem()->getPackaging())
                ->addViolation();
            }
        }
    }

    public function getSubType(): ?string
    {
        return $this->subType;
    }

    public function setSubType(?string $subType): self
    {
        $this->subType = $subType;

        return $this;
    }

    public function getIsCheese(): ?bool
    {
        return $this->isCheese;
    }

    public function setIsCheese(?bool $isCheese): self
    {
        $this->isCheese = $isCheese;

        return $this;
    }

    public function processSubType(){
        if($this->productUnit == self::PRODUCTUNIT_KG){
            if($this->isCheese){
                $this->subType = self::SUBTYPE_CHEESE;
            } else{
                $this->subType = self::SUBTYPE_WHEIGHT;
            }
        } else{
            $this->subType = self::SUBTYPE_SHARE;
        }
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getIndividualProducts(): Collection
    {
        return $this->individualProducts;
    }

    public function addIndividualProduct(Product $individualProduct): self
    {
        if (!$this->individualProducts->contains($individualProduct)) {
            $this->individualProducts[] = $individualProduct;
            $individualProduct->setRelatedSharedProduct($this);
        }

        return $this;
    }

    public function removeIndividualProduct(Product $individualProduct): self
    {
        if ($this->individualProducts->contains($individualProduct)) {
            $this->individualProducts->removeElement($individualProduct);
            // set the owning side to null (unless already changed)
            if ($individualProduct->getRelatedSharedProduct() === $this) {
                $individualProduct->setRelatedSharedProduct(null);
            }
        }

        return $this;
    }
}
