<?php

namespace App\Entity;

use App\Repository\DistributionRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints\Distribution as DistributionConstraint;

/**
 * @ORM\Entity(repositoryClass=DistributionRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @DistributionConstraint(groups={"worker"})
 */
class Distribution
{
    const STATUS_NEW = 0;
    const STATUS_SHAREINPROGRESS = 1;
    const STATUS_SHAREDONE = 2;
    const STATUS_OPENED =3;
    const STATUS_CLOSED = 4;
    const STATUS_ENDED = 5;

    const STATUSES_NAMES = ['new', 'shareinprogress', 'sharedone', 'opened', 'closed', 'ended'];

    const ALLOWED_STATUSES = [
        self::STATUS_NEW => self::STATUSES_NAMES[self::STATUS_NEW],
        self::STATUS_SHAREINPROGRESS =>self::STATUSES_NAMES[self::STATUS_SHAREINPROGRESS],
        self::STATUS_SHAREDONE =>self::STATUSES_NAMES[self::STATUS_SHAREDONE],
        self::STATUS_OPENED =>self::STATUSES_NAMES[self::STATUS_OPENED],
        self::STATUS_CLOSED =>self::STATUSES_NAMES[self::STATUS_CLOSED],
        self::STATUS_ENDED => self::STATUSES_NAMES[self::STATUS_ENDED],
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     * @Assert\Choice(choices=Distribution::STATUSES_NAMES, message="Status is not a valid status.")
     */
    private $status = 'new';

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Type("\DateTime")
     */
    private $deliveryDate;

    /**
     * @ORM\ManyToMany(targetEntity=Catalogue::class, inversedBy="distributions")
     */
    private $Catalogues;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Type("\DateTime")
     */
    private $CreatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\Type("\DateTime")
     */
    private $UpdatedAt;

    /**
     * @ORM\OneToOne(targetEntity=SharingProposal::class, mappedBy="Distribution", cascade={"persist", "remove"})
     */
    private $sharingProposal;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="distribution", orphanRemoval=true)
     */
    private $orders;

    /**
     * @ORM\OneToMany(targetEntity=SuggestedItem::class, mappedBy="distribution")
     */
    private $suggestedItems;

    //private $workflowRegistry;

    public function __construct()
    {
        $this->Catalogues = new ArrayCollection();
        $this->CreatedAt = new DateTime();
        $this->deliveryDate = new DateTime();
        $this->orders = new ArrayCollection();
        $this->suggestedItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDeliveryDate(): ?\DateTimeInterface
    {
        return $this->deliveryDate;
    }

    public function setDeliveryDate(\DateTimeInterface $deliveryDate): self
    {
        $this->deliveryDate = $deliveryDate;

        return $this;
    }

    /**
     * @return Collection|Catalogue[]
     */
    public function getCatalogues(): Collection
    {
        return $this->Catalogues;
    }

    public function addCatalogue(Catalogue $catalogue): self
    {
        if (!$this->Catalogues->contains($catalogue)) {
            $this->Catalogues[] = $catalogue;
        }

        return $this;
    }

    public function removeCatalogue(Catalogue $catalogue): self
    {
        if ($this->Catalogues->contains($catalogue)) {
            $this->Catalogues->removeElement($catalogue);
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->CreatedAt;
    }

    public function setCreatedAt(\DateTimeInterface $CreatedAt): self
    {
        $this->CreatedAt = $CreatedAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->UpdatedAt;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAt(): self
    {
        $this->UpdatedAt = new \Datetime();

        return $this;
    }

    static public function getStatusIndex(string $status): int
    {
        $status = self::checkStatusKey($status);
        return  array_search($status, self::ALLOWED_STATUSES);
    }

    static public function getNextStatus(string $currentStatus): string
    {
        $value = self::getStatusIndex($currentStatus);
        return ($value === max(array_keys(self::ALLOWED_STATUSES))) ? self::ALLOWED_STATUSES[$value] :  self::ALLOWED_STATUSES[$value + 1];
    }

    static public function getPreviousStatus(string $currentStatus): string
    {
        $value = self::getStatusIndex($currentStatus);
        return ($value === min(array_keys(self::ALLOWED_STATUSES))) ? self::ALLOWED_STATUSES[$value] :  self::ALLOWED_STATUSES[$value - 1];
    }

    /**
     * checkStatusKey
     *
     * @param  mixed $status
     * @return string | false
     */
    static public function checkStatusKey(string $status): string
    {
        if (false === in_array($status, self::ALLOWED_STATUSES)) {
            throw new Exception('Invalid distribution status value');
        } else {
            return $status;
        }
    }

    public function isDistributionInProgress(): bool
    {
        $distributionStatus = Distribution::getStatusIndex($this->getStatus());
        return (($distributionStatus > Distribution::STATUS_NEW) && ($distributionStatus < Distribution::STATUS_ENDED))? true : false;
    }

    public function isDistributionInNewState(Distribution $distribution): bool
    {
        return ('new' === $distribution->getStatus()) ? true : false;
    }

    public function isDistributionInOpenState(Distribution $distribution): bool
    {
        return ('opened' === $distribution->getStatus()) ? true : false;
    }

    public function isDistributionInEndedState(Distribution $distribution): bool
    {
        return ('ended' === $distribution->getStatus()) ? true : false;
    }

    public function isDistributionInClosedState(Distribution $distribution): bool
    {
        return ('closed' === $distribution->getStatus()) ? true : false;
    }

    public function getSharingProposal(): ?SharingProposal
    {
        return $this->sharingProposal;
    }

    public function setSharingProposal(SharingProposal $sharingProposal): self
    {
        $this->sharingProposal = $sharingProposal;

        // set the owning side of the relation if necessary
        if ($sharingProposal->getDistribution() !== $this) {
            $sharingProposal->setDistribution($this);
        }

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setDistribution($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->contains($order)) {
            $this->orders->removeElement($order);
            // set the owning side to null (unless already changed)
            if ($order->getDistribution() === $this) {
                $order->setDistribution(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SuggestedItem[]
     */
    public function getSuggestedItems(): Collection
    {
        return $this->suggestedItems;
    }

    public function addSuggestedItem(SuggestedItem $suggestedItem): self
    {
        if (!$this->suggestedItems->contains($suggestedItem)) {
            $this->suggestedItems[] = $suggestedItem;
            $suggestedItem->setDistribution($this);
        }

        return $this;
    }

    public function removeSuggestedItem(SuggestedItem $suggestedItem): self
    {
        if ($this->suggestedItems->contains($suggestedItem)) {
            $this->suggestedItems->removeElement($suggestedItem);
            // set the owning side to null (unless already changed)
            if ($suggestedItem->getDistribution() === $this) {
                $suggestedItem->setDistribution(null);
            }
        }

        return $this;
    }
}