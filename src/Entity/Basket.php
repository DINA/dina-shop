<?php

namespace App\Entity;

use App\Repository\BasketRepository;
use App\Service\ProductManager\Price;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints\Basket as BasketConstraint;


/**
 * @ORM\Entity(repositoryClass=BasketRepository::class)
 * @BasketConstraint()
 */
class Basket
{
    use Price;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="basket", cascade={"persist"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="basket", cascade={"persist"})
     * @Assert\Valid()
     */
    private $product;

    /**
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $price = 0;

    public function __construct()
    {
        $this->product = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProduct(): Collection
    {
        return $this->product;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->product->contains($product)) {
            $this->product[] = $product;
            $product->setBasket($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->product->contains($product)) {
            $this->product->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getBasket() === $this) {
                $product->setBasket(null);
            }
        }

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->getTotalPrice();

    }

    public function getVATPrice(): ?string
    {
        return $this->getTotalVATPrice();
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }
}
