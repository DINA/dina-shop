<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use App\Service\ProductManager\Price;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
    use Price;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="orders")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="linkedOrder")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity=Distribution::class, inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $distribution;

    /**
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $price;

    public function __construct()
    {
        $this->product = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProduct(): Collection
    {
        return $this->product;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->product->contains($product)) {
            $this->product[] = $product;
            $product->setLinkedOrder($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->product->contains($product)) {
            $this->product->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getLinkedOrder() === $this) {
                $product->setLinkedOrder(null);
            }
        }

        return $this;
    }

    public function getDistribution(): ?Distribution
    {
        return $this->distribution;
    }

    public function setDistribution(?Distribution $distribution): self
    {
        $this->distribution = $distribution;

        return $this;
    }


    public function getPrice(): ?string
    {
        return $this->getTotalPrice();
    }

    public function getVATPrice(): ?string
    {
        return $this->getTotalVATPrice();
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }
}
