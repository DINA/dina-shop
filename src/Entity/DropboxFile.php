<?php

namespace App\Entity;


class DropboxFile
{
    
    /**
     * dropboxFile Name
     */
    private $dropboxFileName;

    /**
     * dropboxFile Link
     */
    private $dropboxFileLink;

    /**
     * Get the value of dropboxFile
     */
    public function getDropboxFileName()
    {
        return $this->dropboxFileName;
    }

    /**
     * Set the value of dropboxFile
     *
     * @return  self
     */
    public function setDropboxFileName($dropboxFileName)
    {
        $this->dropboxFileName = $dropboxFileName;

        if ($dropboxFileName) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * Get the value of dropboxFileLink
     */
    public function getDropboxFileLink()
    {
        return $this->dropboxFileLink;
    }

    /**
     * Set the value of dropboxFileLink
     *
     * @return  self
     */
    public function setDropboxFileLink($dropboxFileLink)
    {
        $this->dropboxFileLink = $dropboxFileLink;

        return $this;
    }
}