<?php

namespace App\Entity;

use App\Repository\SharingProposalRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Form\FormView;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=SharingProposalRepository::class)
 */
class SharingProposal
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=Distribution::class, inversedBy="sharingProposal", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $Distribution;

    /**
     * @ORM\OneToMany(targetEntity=Item::class, mappedBy="sharingProposal")
     */
    private $Item;

    /**
     * @ORM\OneToMany(targetEntity=SharedProduct::class, mappedBy="sharingProposal", orphanRemoval=true)
     * @Assert\Valid()
     */
    private $sharedProducts;
 
    public function __construct()
    {
        $this->Item = new ArrayCollection();
        $this->sharedProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDistribution(): ?Distribution
    {
        return $this->Distribution;
    }

    public function setDistribution(Distribution $Distribution): self
    {
        $this->Distribution = $Distribution;

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItem(): Collection
    {
        return $this->Item;
    }

    public function addItem(Item $item): self
    {
        if (!$this->Item->contains($item)) {
            $this->Item[] = $item;
            $item->setSharingProposal($this);
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->Item->contains($item)) {
            $this->Item->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getSharingProposal() === $this) {
                $item->setSharingProposal(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SharedProduct[]
     */
    public function getSharedProducts(): Collection
    {
        return $this->sharedProducts;
    }

    public function addSharedProduct(SharedProduct $sharedProduct): self
    {
        if (!$this->sharedProducts->contains($sharedProduct)) {
            $this->sharedProducts[] = $sharedProduct;
            $sharedProduct->setSharingProposal($this);
        }

        return $this;
    }

    public function removeSharedProduct(SharedProduct $sharedProduct): self
    {
        if ($this->sharedProducts->contains($sharedProduct)) {
            $this->sharedProducts->removeElement($sharedProduct);
            // set the owning side to null (unless already changed)
            if ($sharedProduct->getSharingProposal() === $this) {
                $sharedProduct->setSharingProposal(null);
            }
        }

        return $this;
    }

    /**
     * sortSharingProposal
     * Allow to sort sharing proposal by "à peser, à partager, fromage"
     *
     * @param  mixed $array
     * @return array or ArrayCollection
     */
    public static function sortSharingProposal($array): ?array
    {
        if ($array instanceof \Traversable) {
            $array = iterator_to_array($array);
        }

        if(!is_null($array)){
            uasort($array, array('self', 'cmp'));
        }


        return $array;
    }

    
    /**
     * cmp
     * comparison function to sort sharing proposal
     *
     * @param  mixed $a
     * @param  mixed $b
     * @return void
     */
    static function cmp($a, $b)
    {
        /* SharedProduct or Formview? */
        if ($a instanceof FormView) {
            $A = $a->vars['value'];
            $B = $b->vars['value'];
        } elseif ($a instanceof SharedProduct) {
            $A = $a;
            $B = $b;
        } else {
            throw new Exception("Unexpected instance for sorting");
        }

        switch($A->getItem()->getCatalogueId()->getType()){
            case 'RelaisVert_Catalogue_Sec':
                switch ($B->getItem()->getCatalogueId()->getType()) {
                    case 'RelaisVert_Catalogue_Sec':
                        return 0;
                    case 'RelaisVert_Catalogue_Frais':
                        return 1;
                    case 'RelaisVert_Mercuriale_FruitsLegumes':
                        return 1;
                    default:
                        return 1;
                }
                break;
            case 'RelaisVert_Catalogue_Frais':
                 switch($B->getItem()->getCatalogueId()->getType()){
                    case 'RelaisVert_Catalogue_Sec':
                        return -1;
                    case 'RelaisVert_Catalogue_Frais':
                        return 0;
                    case 'RelaisVert_Mercuriale_FruitsLegumes':
                        return 1;
                    default:
                        return 1;
                }
                break;
            case 'RelaisVert_Mercuriale_FruitsLegumes':
                switch ($B->getItem()->getCatalogueId()->getType()) {
                    case 'RelaisVert_Catalogue_Sec':
                        return -1;
                    case 'RelaisVert_Catalogue_Frais':
                        return -1;
                    case 'RelaisVert_Mercuriale_FruitsLegumes':
                        return 0;
                    default:
                        return 1;
                }
                break;
            default:
                return -1;
            break;
        }

        return 0;
    }
    
    /**
     * cmp_old
     * obsolete function to order "à peser, à partager, fromage". 
     *
     * @param  mixed $a
     * @param  mixed $b
     * @return void
     */
    static function cmp_old($a, $b)
    {
        /* SharedProduct or Formview? */
        if ($a instanceof FormView) {
            $A = $a->vars['value'];
            $B = $b->vars['value'];
        } elseif ($a instanceof SharedProduct) {
            $A = $a;
            $B = $b;
        } else {
            throw new Exception("Unexpected instance for sorting");
        }

        /* Put cheese at the end */
        if ($A->getIsCheese() < $B->getIsCheese()) {
            return -1;
        } elseif ($A->getIsCheese() > $B->getIsCheese()) {
            return 1;
            /* Then put "à peser" first then "à partager" in 2nd position */
        } elseif (($A->getProductUnit() < $B->getProductUnit())) {
            return -1;
        } elseif (($A->getProductUnit() > $B->getProductUnit())) {
            return 1;
            /* Sort products by catalogue name */
        } elseif (($A->getItem()->getCatalogueId()->getName() < $B->getItem()->getCatalogueId()->getName())) {
            return -1;
        } elseif (($A->getItem()->getCatalogueId()->getName() > $B->getItem()->getCatalogueId()->getName())) {
            return 1;
        }

        return 0;
    }
}
