<?php

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;



class CatalogueSearch
{
   
    private $catalogue;

    private $origin;

    private $searchText;

    //-------------------------------------

    private $origins;

    private $families;

    private $brands;


    public function __construct()
    {
        $this->origins = new ArrayCollection();
        $this->families = new ArrayCollection();
        $this->brands = new ArrayCollection();
    }

      //-------------------------------------

    /**
     * Get the value of catalogue
     */ 
    public function getCatalogue()
    {
        return $this->catalogue;
    }

    /**
     * Set the value of catalogue
     *
     * @return  self
     */ 
    public function setCatalogue($catalogue)
    {
        $this->catalogue = $catalogue;

        return $this;
    }

    // ---------------------------------------------------

    /**
     * Get the value of origins
     */ 
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * Set the value of origins
     *
     * @return  self
     */ 
    public function setOrigin($item)
    {
        if($item){
        $this->origin = $item->getOrigin();
        }
   
        return $this;
    }

    // ---------------------------------------------------

    public function getOrigins()
    {
        $output = null;
        foreach($this->origins as $origin){
            $output[] = $origin;
        }
        return $output;
    }

    /**
     * Set the value of origins
     *
     * @return  self
     */
    public function setOrigins($items)
    {
        foreach($items as $item){
            if ($item && ($item instanceof Item)) {
                $this->origins[] = $item->getOrigin();
            }
            elseif($item){
                $this->origins[] = $item;
            }
        }
       
       return $this;
    }

    public function getFamilies()
    {
        $output = null;
        foreach ($this->families as $family) {
            $output[] = $family;
        }
        return $output;
    }

    /**
     * Set the value of families
     *
     * @return  self
     */
    public function setFamilies($items)
    {
        foreach ($items as $item) {
            if ($item && ($item instanceof Item)) {
                $this->families[] = $item->getFamily();
            } elseif ($item) {
                $this->families[] = $item;
            }
        }

        return $this;
    }

    public function getBrands()
    {
        $output = null;
        foreach ($this->brands as $brand) {
            $output[] = $brand;
        }
        return $output;
    }

    /**
     * Set the value of families
     *
     * @return  self
     */
    public function setBrands($items)
    {
        foreach ($items as $item) {
            if ($item && ($item instanceof Item)) {
                $this->brands[] = $item->getBrand();
            } elseif ($item) {
                $this->brands[] = $item;
            }
        }

        return $this;
    }

    /**
     * Get the value of searchText
     */ 
    public function getSearchText()
    {
        return $this->searchText;
    }

    /**
     * Set the value of searchText
     *
     * @return  self
     */ 
    public function setSearchText($searchText)
    {
        $this->searchText = $searchText;

        return $this;
    }
}
