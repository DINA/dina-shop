<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product extends VirtualProduct
{
    /**
     * @ORM\ManyToOne(targetEntity=Item::class, inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     */
    private $item;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $quantityInt = 1;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2, nullable=true)
     */
    private $quantityDec = 1;

    /**
     * @ORM\ManyToOne(targetEntity=Basket::class, inversedBy="product")
     * @ORM\JoinColumn(onDelete="SET NULL") 
     */
    private $basket;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="product")
     */
    private $linkedOrder;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $preOrder;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $packaging;
    /* Packaging is added only to peform a validation on the string, in order to avoid errors when excel spreadsheet will be generated */

    /**
     * @ORM\ManyToOne(targetEntity=SharedProduct::class, inversedBy="individualProducts")
     */
    private $relatedSharedProduct;
    

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function getQuantityInt(): ?int
    {
        return $this->quantityInt;
    }

    public function setQuantityInt(?int $quantityInt): self
    {
        $this->quantityInt = $quantityInt;

        return $this;
    }

    public function getQuantityDec(): ?string
    {
        return $this->quantityDec;
    }

    /**
     * @Assert\Callback
     */
    public function isValid(ExecutionContextInterface $context)
    {
       /* Test also OK with a comma ',' */
       if(strstr($this->quantityDec, '.')){
            $val = floatval($this->quantityDec);
       } else{
            $val = intval($this->quantityDec);
       }

       if($val<=0){
            $context
                ->buildViolation('form.product.quantity_positive') 
                ->atPath('quantityDec')
                ->addViolation()
            ;
            $context
                ->buildViolation('form.product.quantity_positive_full')
                ->setParameter('{{ designation }}', $this->getItem()->getDesignation())
                ->addViolation();
       }
        elseif(is_float($val) && ($this->productUnit != self::PRODUCTUNIT_KG)){
            $context
                ->buildViolation('form.product.quantity') 
                ->atPath('quantityDec')
                ->addViolation()
            ;
            $context
                ->buildViolation('form.product.quantity_full')
                ->setParameter('{{ designation }}', $this->getItem()->getDesignation())
                ->addViolation();
        }
        elseif((self::PRODUCTUNIT_PIECE === $this->getProductUnit()) && ($val < $this->getItem()->getMinPacking())){
            $context
                ->buildViolation('form.product.min_order')
                ->setParameter('{{ value }}',$this->getItem()->getMinPacking())
                ->atPath('quantityDec')
                ->addViolation()
            ;
            $context
                ->buildViolation('form.product.min_order_full')
                ->setParameter('{{ designation }}', $this->getItem()->getDesignation())
                ->setParameter('{{ value }}', $this->getItem()->getMinPacking())
                ->addViolation();
        } elseif ( ($this->getProductUnit() == Product::PRODUCTUNIT_PARCEL) && ($this->getItem()->getCatalogueId()->getType() === 'RelaisVert_Mercuriale_FruitsLegumes')) {
           /* Ici je ne peux pas filter car on peut avoir quelque chode de type:
                - "env 2,5kg" (si on achète une meule complète)
                - 14 kg (une cagette entière)
                - 1 COL
                - 12 PCE
                donc dans le doute je ne fais rien mais je laisse le code si j'ai besoin un jour de valider le conditionnement
            */
        }
        
    }

    public function setQuantityDec(?string $quantityDec): self
    {
        if(isset($quantityDec)){
            $this->quantityDec = $quantityDec;
        }

        return $this;
    }

    public function getBasket(): ?Basket
    {
        return $this->basket;
    }

    public function setBasket(?Basket $basket): self
    {
        $this->basket = $basket;

        return $this;
    }

    public function getLinkedOrder(): ?Order
    {
        return $this->linkedOrder;
    }

    public function setLinkedOrder(?Order $linkedOrder): self
    {
        $this->linkedOrder = $linkedOrder;

        return $this;
    }

    public function getPreOrder(): ?bool
    {
        return $this->preOrder;
    }

    public function setPreOrder(?bool $preOrder): self
    {
        $this->preOrder = $preOrder;

        return $this;
    }

    public function getPackaging(): ?string
    {
        return $this->packaging;
    }

    public function setPackaging(?string $packaging): self
    {
        $this->packaging = $packaging;

        return $this;
    }

    public function getRelatedSharedProduct(): ?SharedProduct
    {
        return $this->relatedSharedProduct;
    }

    public function setRelatedSharedProduct(?SharedProduct $relatedSharedProduct): self
    {
        $this->relatedSharedProduct = $relatedSharedProduct;

        return $this;
    }
}
