<?php

namespace App\Entity;

use App\Repository\ItemRepository;
use ArrayAccess;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ItemRepository::class)
 * @ORM\Table(name="item", indexes={
 *     @ORM\Index(name="idx_reference", columns={"catalogue_id_id","reference"}),
 *     @ORM\Index(name="idx_designation", columns={"catalogue_id_id","designation"}),
 * })
 */
class Item
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Catalogue::class, inversedBy="items", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $catalogueId;

    /**
     * @ORM\Column(type="string", length=32)
     * @Assert\NotBlank
     */
    private $reference;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=4, max=255)
     */
    private $designation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $brand;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $packaging;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $inspection;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $origin;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $label;

    /**
     * @ORM\Column(type="decimal", precision=6, scale=2)
     */
    private $unitaryPrice;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=2)
     */
    private $vat;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $family;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $packing;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $minPacking; 

    /**
     * @ORM\Column(type="decimal", precision=6, scale=2, nullable=true)
     */
    private $unpackedPrice;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $calibre_categ;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $ed;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customField1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customField2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customField3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customField4;

    /**
     * @ORM\ManyToOne(targetEntity=SharingProposal::class, inversedBy="Item")
     * @ORM\JoinColumn(onDelete="SET NULL") 
     */
    private $sharingProposal;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="item", orphanRemoval=true)
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity=SharedProduct::class, mappedBy="item", orphanRemoval=true)
     */
    private $sharedProducts;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $EAN13;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $SKU; //Stock keeping unit ou UVC en français (voire UV)


    public function __construct()
    {
        $this->setVat(5.5);
        $this->products = new ArrayCollection();
        $this->sharedProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCatalogueId(): ?Catalogue
    {
        return $this->catalogueId;
    }

    public function setCatalogueId(?Catalogue $catalogueId): self
    {
        $this->catalogueId = $catalogueId;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(?string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getPackaging(): ?string
    {
        return $this->packaging;
    }

    public function setPackaging(?string $packaging): self
    {
        $this->packaging = $packaging;

        return $this;
    }

    public function getInspection(): ?string
    {
        return $this->inspection;
    }

    public function setInspection(?string $inspection): self
    {
        $this->inspection = $inspection;

        return $this;
    }

    public function getOrigin(): ?string
    {
        return $this->origin;
    }

    public function setOrigin(?string $origin): self
    {
        $this->origin = $origin;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getUnitaryPrice(): ?string
    {
        return $this->unitaryPrice;
    }

    public function setUnitaryPrice(string $unitaryPrice): self
    {
        $this->unitaryPrice = $unitaryPrice;

        return $this;
    }

    public function getVat(): ?string
    {
        return $this->vat;
    }

    public function setVat(string $vat): self
    {
        $this->vat = $vat;

        return $this;
    }

    public function getFamily(): ?string
    {
        return $this->family;
    }

    public function setFamily(?string $family): self
    {
        $this->family = $family;

        return $this;
    }

    public function getPacking(): ?int
    {
        return $this->packing;
    }

    public function setPacking(?int $packing): self
    {
        $this->packing = $packing;

        return $this;
    }

    public function getMinPacking(): ?int
    {
        return $this->minPacking;
    }

    public function setMinPacking(?int $minPacking): self
    {
        $this->minPacking = $minPacking;

        return $this;
    }

    public function getUnpackedPrice(): ?string
    {
        return $this->unpackedPrice;
    }

    public function setUnpackedPrice(?string $unpackedPrice): self
    {
        $this->unpackedPrice = $unpackedPrice;

        return $this;
    }

    public function getCalibreCateg(): ?string
    {
        return $this->calibre_categ;
    }

    public function setCalibreCateg(?string $calibre_categ): self
    {
        $this->calibre_categ = $calibre_categ;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getEd(): ?string
    {
        return $this->ed;
    }

    public function setEd(?string $ed): self
    {
        $this->ed = $ed;

        return $this;
    }

    public function getCustomField1(): ?string
    {
        return $this->customField1;
    }

    public function setCustomField1(?string $customField1): self
    {
        $this->customField1 = $customField1;

        return $this;
    }

    public function getCustomField2(): ?string
    {
        return $this->customField2;
    }

    public function setCustomField2(?string $customField2): self
    {
        $this->customField2 = $customField2;

        return $this;
    }

    public function getCustomField3(): ?string
    {
        return $this->customField3;
    }

    public function setCustomField3(?string $customField3): self
    {
        $this->customField3 = $customField3;

        return $this;
    }

    public function getCustomField4(): ?string
    {
        return $this->customField4;
    }

    public function setCustomField4(?string $customField4): self
    {
        $this->customField4 = $customField4;

        return $this;
    }

    public function getSharingProposal(): ?SharingProposal
    {
        return $this->sharingProposal;
    }

    public function setSharingProposal(?SharingProposal $sharingProposal): self
    {
        $this->sharingProposal = $sharingProposal;

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function getEAN13(): ?string
    {
        return $this->EAN13;
    }

    public function setEAN13(?string $EAN13): self
    {
        $this->EAN13 = $EAN13;

        return $this;
    }

    public function getSKU(): ?string
    {
        return $this->SKU;
    }

    public function setSKU(?string $SKU): self
    {
        $this->SKU = $SKU;

        return $this;
    }
}
