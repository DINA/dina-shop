<?php

namespace App\Widgets;

use Twig\Environment;

class ImportCatalogueWidget implements WidgetInterface
{

    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function render(): string
    {
        return $this->twig->render(
            'widgets/importCatalogue.html.twig',[
            ]);
    }

}
