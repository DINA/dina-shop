<?php

namespace App\Widgets;

use App\Repository\ProducerRepository;
use Twig\Environment;

class NewDistributionWidget implements WidgetInterface
{

    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function render(): string
    {
        return $this->twig->render(
            'widgets/newDistribution.html.twig',[
            ]);
    }

}
