<?php

namespace App\Widgets;

interface WidgetInterface
{
    public function render(): string;

}
