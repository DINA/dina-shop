<?php

namespace App\Widgets;

use App\Repository\DistributionRepository;
use Twig\Environment;

class ViewDistributionWidget implements WidgetInterface
{

    private $distributionRepository;
    private $twig;

    public function __construct(Environment $twig, DistributionRepository $distributionRepository)
    {
        $this->distributionRepository = $distributionRepository;
        $this->twig = $twig;
    }

    public function render(): string
    {
        $distribution = $this->distributionRepository->findDistributionInProgress();

        if($distribution){
            return $this->twig->render(
                'widgets/viewDistribution.html.twig',[
                    'distribution' => $distribution,
             ]);
         } else{
             return '';
         }
    }

}
