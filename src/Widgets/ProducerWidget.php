<?php

namespace App\Widgets;

use App\Repository\ProducerRepository;
use Twig\Environment;

class ProducerWidget implements WidgetInterface
{

    private $producerRepository;
    private $twig;

    public function __construct(Environment $twig, ProducerRepository $producerRepository)
    {
        $this->producerRepository = $producerRepository;
        $this->twig = $twig;
    }

    public function render(): string
    {
        $count =  $this->producerRepository->createQueryBuilder('p')->select('count(p.id)')
        ->getQuery()
        ->getSingleScalarResult();

        return $this->twig->render(
            'widgets/producer.html.twig',[
                'count' => $count,
            ]);
    }

}
