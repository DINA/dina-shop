<?php

namespace App\Controller\Manager;

use App\Entity\Distribution;
use App\Entity\Order;
use App\Entity\User;
use App\Form\DistributionEditType;
use App\Form\DistributionType;
use App\Repository\DistributionRepository;
use App\Repository\UserRepository;
use App\Service\Response\RedirectBackResponse;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use LogicException;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Workflow\Registry;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/manager/distribution")
 */
class DistributionController extends AbstractController
{
    use RedirectBackResponse;

    private $workflowRegistry;
    private $distributionRepository;
    private $translator;
    private $security;

    public function __construct(Registry $workflowRegistry, DistributionRepository $distributionRepository, TranslatorInterface $translator, Security $security)
    {
        $this->workflowRegistry = $workflowRegistry;
        $this->distributionRepository = $distributionRepository;
        $this->translator = $translator;
        $this->security = $security;
    }

    /**
     * @Route("/", name="distribution_index", methods={"GET"})
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        $page = $request->query->getInt('page', 1);
        if ($page < 1) {
            throw $this->createNotFoundException('Page "' . $page . '" inexistante.');
        }

        $query = $this->distributionRepository->findAllQuery();
        $distributions = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('manager/distribution/index.html.twig', [
            'distributions' => $distributions
        ]);
    }

    /**
     * @Route("/new", name="distribution_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        if (!$this->distributionRepository->findDistributionInNewState()) {

            $distribution = new Distribution();
            $form = $this->createForm(DistributionType::class, $distribution);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($distribution);
                $entityManager->flush();
                $this->addFlash('success', $this->translator->trans(
                    'manager.distribution.new.flash_success',
                    []
                ));
                return $this->redirectToRoute('distribution_index');
            }
        } else {
            $this->addFlash('danger', $this->translator->trans(
                'manager.distribution.new.flash_error',
                []
            ));
            return $this->redirectToRoute('distribution_index');
        }

        return $this->render('manager/distribution/new.html.twig', [
            'distribution' => $distribution,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="distribution_show", methods={"GET"})
     */
    public function show(Distribution $distribution): Response
    {
        return $this->render('manager/distribution/show.html.twig', [
            'distribution' => $distribution,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="distribution_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Distribution $distribution): Response
    {
        /*  -  interdire d'éditer des champs d'une distribition en fonction de son propre état
        * status > 'open' :  ne doit plus être éditable
        * status == 'open' : doit seulement pouvoir éditer la date
        * status < 'open' : peut tout éditer 
        TODO: à refaire en utilisant les validation groups
        */
        if (Distribution::getStatusIndex($distribution->getStatus()) < Distribution::STATUS_OPENED) {
            $form = $this->createForm(DistributionType::class, $distribution);
        } elseif (Distribution::getStatusIndex($distribution->getStatus()) == Distribution::STATUS_OPENED) {
            $form = $this->createForm(DistributionEditType::class, $distribution);
        } else {
            $this->addFlash('danger', $this->translator->trans(
                'manager.distribution.edit.flash_error',
                []
            ));
            return $this->redirectToRoute('distribution_index');
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', $this->translator->trans(
                'common.flash_message.edit.success',
                []
            ));
            return $this->redirectToRoute('distribution_index');
        }

        return $this->render('manager/distribution/edit.html.twig', [
            'distribution' => $distribution,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="distribution_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Distribution $distribution): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        if ($this->isCsrfTokenValid('delete' . $distribution->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            if ($distribution->isDistributionInEndedState($distribution)) {
                foreach ($distribution->getOrders() as $order) {
                    $products = $order->getProduct();
                    foreach ($products as $product) {
                        $entityManager->remove($product);
                    }
                    $entityManager->remove($order);
                }
                $entityManager->remove($distribution);
                $entityManager->flush();
                $this->addFlash('success', $this->translator->trans(
                    'manager.distribution.delete.flash_success',
                    []
                ));;
            } elseif($distribution->isDistributionInNewState($distribution)) {
                if($distribution->getSuggestedItems()->isEmpty()){

                    $entityManager->remove($distribution);
                    $entityManager->flush();
                    $this->addFlash('success', $this->translator->trans(
                        'manager.distribution.delete.flash_success',
                        []
                    ));;
                } else{
                    $this->addFlash('danger', $this->translator->trans(
                        'manager.distribution.delete.flash_error',
                        []
                    ));
                }
            } else {
                $this->addFlash('danger', $this->translator->trans(
                    'manager.distribution.delete.flash_error',
                    []
                ));
            }
        }

        return $this->redirectToRoute('distribution_index');
    }

    /**
     * @Route("/{id}/next", name="distribution_next", methods={"GET","POST"})
     */
    public function goToNextState(Request $request, Distribution $distribution)
    {
        $workflow = $this->workflowRegistry->get($distribution);

        if ($workflow->can($distribution, 'share')) {
            $workflow->apply($distribution, 'share');
            $this->addFlash('success',$this->translator->trans(
                'manager.distribution.next_state.shareinprogress',
                []
            ));
        } elseif ($workflow->can($distribution, 'review')) {
            $workflow->apply($distribution, 'review');
            $this->addFlash('success', $this->translator->trans(
                'manager.distribution.next_state.sharedone',
                []
            ));
        } elseif ($workflow->can($distribution, 'open')) {
            $workflow->apply($distribution, 'open');
            $this->addFlash('success', $this->translator->trans(
                'manager.distribution.next_state.open',
                []
            ));
        } elseif ($workflow->can($distribution, 'end')) {
            $workflow->apply($distribution, 'end');
            $this->addFlash('success', $this->translator->trans(
                'manager.distribution.next_state.ended',
                []
            ));
        } elseif ($workflow->can($distribution, 'close')) {
            $workflow->apply($distribution, 'close');
            $this->addFlash('success', $this->translator->trans(
                'manager.distribution.next_state.closed',
                []
            ));
        } else{
            $blockersList = $workflow->buildTransitionBlockerList($distribution, 'share');
            foreach($blockersList as $blocker){
                $this->addFlash('danger', $this->translator->trans($blocker->getMessage(),
                    []));
            break;
            }
        }

        $this->getDoctrine()->getManager()->flush();

        return new RedirectResponse($this->redirectBack($request));
    }

    /**
     * @Route("/{id}/previous", name="distribution_previous", methods={"GET","POST"})
     */
    public function goToPreviousState(Request $request, Distribution $distribution)
    {
        $workflow = $this->workflowRegistry->get($distribution);

        if ($workflow->can($distribution, 'reject')) {
            $workflow->apply($distribution, 'reject');
        }

        $isAdmin = $this->security->getUser()->hasRole('ROLE_ADMIN');
        if($isAdmin){
            if ($workflow->can($distribution, 'reshare')) {
                $workflow->apply($distribution, 'reshare');
            }
            if ($workflow->can($distribution, 'reopen')) {
                $workflow->apply($distribution, 'reopen');
            }
        }

        $this->getDoctrine()->getManager()->flush();

        return new RedirectResponse($this->redirectBack($request));
    }

    public function menuCurrentDistribution(DistributionRepository $distributionRepository): Response
    {
        return $this->render('manager/distribution/_current.html.twig', [
            'distribution' => $distributionRepository->findDistributionInProgress(),
        ]);
    }
}
