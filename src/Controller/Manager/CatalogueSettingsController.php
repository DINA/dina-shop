<?php

namespace App\Controller\Manager;

use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CatalogueSettingsController extends AbstractController
{
    /**
     * @Route("/manager/catalogue_settings", name="catalogue_settings")
     */
    public function index(CatalogueConfigReader $catalogueConfigReader)
    {
        return $this->render('manager/catalogue_settings/index.html.twig', [
            'settings' => $catalogueConfigReader->getCataloguesDefinition(),
        ]);
    }
}
