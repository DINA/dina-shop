<?php

namespace App\Controller\Manager;

use App\Controller\ItemController as ControllerItemController;
use App\Entity\Catalogue;
use App\Entity\CatalogueSearch;
use App\Entity\Item;
use App\Form\CatalogueImportType;
use App\Form\CatalogueSearchType;
use App\Form\ItemType;
use App\Repository\CatalogueRepository;
use App\Service\Spreadsheet\Spreadsheet;
use App\Repository\ItemRepository;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use App\Service\CatalogueImport\CatalogueImport;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Omines\DataTablesBundle\DataTableFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/manager/item")
 */
class ItemController extends ControllerItemController /* AbstractController */
{
    private $catalogueConfigReader;

    public function __construct(TranslatorInterface $translator, CatalogueConfigReader $catalogueConfigReader)
    {
        parent::__construct(
            [
                'index' => 'manager/item/index.html.twig',
                'new' => 'manager/item/new.html.twig',
                'edit' => 'manager/item/edit.html.twig',
                'show' => 'manager/item/show.html.twig',
                'actions' => 'manager/item/_actions.html.twig',
            ],
           'manager_item_index',
            CatalogueSearchType::class,
            $translator
        );
        $this->catalogueConfigReader = $catalogueConfigReader;
    }

    /**
     * @Route("/", name="manager_item_index", methods={"GET","POST"})
     */
    public function index(Request $request, ItemRepository $itemRepository, CatalogueRepository $catalogueRepository,CatalogueConfigReader $catalogueConfigReader, DataTableFactory $dataTableFactory, PaginatorInterface $paginator): Response
    {
        return parent::index($request, $itemRepository, $catalogueRepository, $catalogueConfigReader, $dataTableFactory, $paginator);
    }

    /**
     * @Route("/new/{cat_id}", name="item_new", methods={"GET","POST"})
     * @ParamConverter("catalogue", options={"mapping": {"cat_id": "id"}})
     */
    public function new(Request $request, Catalogue $catalogue): Response
    {
        return parent::new($request,$catalogue);
    }

    /**
     * @Route("/{id}", name="item_show", requirements={"id" = "\d+"}, methods={"GET"})
     */
    public function show(Item $item): Response
    {
        return parent::show($item);
    }

    /**
     * @Route("/{id}/edit", name="item_edit", requirements={"id" = "\d+"}, methods={"GET","POST"})
     */
    public function edit(Request $request, Item $item): Response
    {
        return parent::edit($request,$item);
    }

    /**
     * @Route("/{id}", name="item_delete", requirements={"id" = "\d+"}, methods={"DELETE"})
     */
    public function delete(Request $request, Item $item): Response
    {
        return parent::delete($request,$item);
    }

    /**
     * @Route("/import/{cat_id}", name="item_import", methods={"GET", "POST"})
     * @ParamConverter("catalogue", options={"mapping": {"cat_id": "id"}})
     */
    public function import(Request $request, CatalogueImport $catalogueImport, Catalogue $catalogue): Response
    {
        /* Very bad idea to put this method here, it would have been clever to put it in he catalogue controller because this is 
        almost the same than the CatalogueController:new.... Too lazy this evening to change it.... 
         */
        /* Add worker validation group, because in this case a file is mandatory */
        $form = $this->createForm(CatalogueImportType::class, $catalogue, ['validation_groups' => ['Default', 'worker']]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            try {
                $catalogue->setFormat($this->catalogueConfigReader->findCatalogueFormat($catalogue->getType()));
                $entityManager->persist($catalogue);
                $entityManager->flush();
                $this->addFlash('success', $this->translator->trans(
                    'manager.item.import.flash_success',
                    []
                ));
            } catch (Exception $e) {
                if (Spreadsheet::MISSING_KEYS === $e->getCode()) {
                    $message = 'manager.catalogue.new.reason.missing_key';
                } elseif (Spreadsheet::NO_REF_KEYS === $e->getCode()) {
                    $message = 'manager.catalogue.new.reason.no_keys';
                } else {
                    $message = 'manager.catalogue.new.reason.other';
                }
                $this->addFlash('danger', $this->translator->trans(
                    'manager.catalogue.new.flash_error',
                    [
                        '%code%' => $e->getCode(),
                        'error' => $this->translator->trans($message, ['%columns%' => $e->getMessage()]),
                    ],
                ));
            }
            return $this->redirectToRoute('manager_item_index', [
                'catalogue' => $catalogue->getId()
            ]);
        }

        return $this->render('manager/item/import.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
