<?php

namespace App\Controller\Manager;

use App\Repository\CatalogueRepository;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use App\Widgets\WidgetInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/manager")
 */
class DashboardController extends AbstractController
{
    /** @var WidgetInterface[] */
    private $widgets;

    public function __construct(array $widgets)
    {
        $this->widgets = $widgets;
    }
    /**
     * @Route("/", name="dashboard", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $widgets = array_reduce($this->widgets, function (string $html,  $widget) {
            return $html . $widget->render();
        }, '');
        return $this->render('manager/dashboard.html.twig', [
            'widgets' => $widgets
        ]);
    }
}