<?php

namespace App\Controller\Manager;

use App\Entity\Catalogue;
use App\Form\CatalogueEditType;
use App\Form\CatalogueType;
use App\Repository\CatalogueRepository;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use App\Service\Spreadsheet\Spreadsheet;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/manager/catalogue")
 */
class CatalogueController extends AbstractController
{
    private $catalogueConfigReader;
    private $translator;

    public function __construct(CatalogueConfigReader $catalogueConfigReader, TranslatorInterface $translator)
    {
        $this->catalogueConfigReader = $catalogueConfigReader;
        $this->translator = $translator;
    }
    /**
     * @Route("/", name="catalogue_index", methods={"GET"})
     */
    public function index(Request $request, PaginatorInterface $paginator, CatalogueRepository $catalogueRepository): Response
    {
        $page = $request->query->getInt('page', 1);
        if ($page < 1) {
            throw $this->createNotFoundException('Page "' . $page . '" inexistante.');
        }

        $query = $catalogueRepository->findAllQuery();
        $catalogues = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('manager/catalogue/index.html.twig', [
            'catalogues' => $catalogues,
            'cataloguesTypes' => $this->catalogueConfigReader->getCataloguesNames()
        ]);
    }

    /**
     * @Route("/new", name="catalogue_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $catalogue = new Catalogue();
        $form = $this->createForm(CatalogueType::class, $catalogue);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            try {
                $catalogue->setFormat($this->catalogueConfigReader->findCatalogueFormat($catalogue->getType()));
                $entityManager->persist($catalogue);
                $entityManager->flush();
                $this->addFlash('success', $this->translator->trans(
                    'manager.catalogue.new.flash_success',
                    []
                ));
            } catch (Exception $e) {
                if(Spreadsheet::MISSING_KEYS === $e->getCode()){
                    $message = 'manager.catalogue.new.reason.missing_key';
                } elseif(Spreadsheet::NO_REF_KEYS === $e->getCode()) {
                    $message = 'manager.catalogue.new.reason.no_keys';
                } else{
                    $message = 'manager.catalogue.new.reason.other';
                }
                $this->addFlash('danger', $this->translator->trans(
                    'manager.catalogue.new.flash_error',[
                        '%code%' => $e->getCode(),
                        'error' => $this->translator->trans($message, ['%columns%' => $e->getMessage()]),
                    ],
                ));
            }
            return $this->redirectToRoute('catalogue_index');
        }

        return $this->render('manager/catalogue/new.html.twig', [
            'catalogue' => $catalogue,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="catalogue_show", methods={"GET"})
     */
    public function show(Catalogue $catalogue): Response
    {
        return $this->render('manager/catalogue/show.html.twig', [
            'catalogue' => $catalogue,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="catalogue_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Catalogue $catalogue): Response
    {
        $form = $this->createForm(CatalogueEditType::class, $catalogue);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var EntityManagerInterface */
            $em = $this->getDoctrine()->getManager();

            /* Dirty patch; if the catalogue were already archived, edition is not allowed,
                otherwise this is a "normal" edition
             */
            $oldCatalogue = $em->getUnitOfWork()->getOriginalEntityData($catalogue);
            if($oldCatalogue['status'] == Catalogue::STATUS_ARCHIVED){
                $this->addFlash('danger', $this->translator->trans(
                'manager.catalogue.edit.flash_error_archived',
                ));
                return $this->redirectToRoute('catalogue_index');
            }

            try {
                $em->beginTransaction(); // suspend auto-commit
                $em->lock($catalogue, \Doctrine\DBAL\LockMode::PESSIMISTIC_READ);
                $em->flush();
                $em->getConnection()->commit();
                $this->addFlash('success', $this->translator->trans(
                    'common.flash_message.edit.success',
                    []
                ));
            }  catch(Exception $e){
                $this->addFlash('danger', $this->translator->trans(
                    'manager.catalogue.edit.flash_error',
                    ['error' => $e->getMessage(),],
                ));
            }
            return $this->redirectToRoute('catalogue_index');
        } 

        return $this->render('manager/catalogue/edit.html.twig', [
            'catalogue' => $catalogue,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="catalogue_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Catalogue $catalogue): Response
    {
        if ($this->isCsrfTokenValid('delete' . $catalogue->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            try{
                $distributions = $catalogue->getDistributions();
                foreach($distributions as $distribution){
                    $entityManager->remove($distribution);
                }
                $entityManager->remove($catalogue);
                $entityManager->flush();
                $this->addFlash('success', $this->translator->trans(
                    'manager.catalogue.delete.flash_success',
                    []
                ));
            } catch(ForeignKeyConstraintViolationException $e){
                $this->addFlash('danger', $this->translator->trans(
                    'manager.catalogue.delete.flash_error',[],
                ));
            } catch(Exception $e){
                $this->addFlash('danger', $this->translator->trans(
                    'manager.catalogue.delete.flash_error_other',
                    ['error' => $e->getMessage(),],
                ));
            }
           
        }

        return $this->redirectToRoute('catalogue_index');
    }

    /**
     * @Route("/{id}/archive", name="catalogue_archive", methods={"POST"})
     */
    public function archive(Request $request, Catalogue $catalogue): Response
    {
        if ($this->isCsrfTokenValid('archive' . $catalogue->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $catalogue->setStatus(Catalogue::STATUS_ARCHIVED);
            try {
                $entityManager->flush();
                $this->addFlash('success', $this->translator->trans(
                    'manager.catalogue.archive.flash_success',
                    []
                ));
            } catch (Exception $e) {
                $this->addFlash('danger', $this->translator->trans(
                    'manager.catalogue.archive.flash_error',
                    ['error' => $e->getMessage(),],
                ));
            }
        }

        return $this->redirectToRoute('catalogue_index');
    }

    /**
     * @Route("/{id}/download", name="catalogue_download", methods={"GET"})
     */
    public function download(Request $request, Catalogue $catalogue, ParameterBagInterface $params): Response
    {
        $filepath = $this->getParameter('vich_uploader.mappings')['catalogue_sreadsheet']['upload_destination'] . '/' . $catalogue->getSpreadsheet();

        if (file_exists($filepath)) {
            $file = new File($filepath);
            return $this->file($file, $catalogue->getSpreadsheetName());
        } else {
            throw $this->createNotFoundException('The catalogue file does not exist');
        }
    }
}
