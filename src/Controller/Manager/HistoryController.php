<?php

namespace App\Controller\Manager;

use App\Repository\LogRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/manager/history")
 */
class HistoryController extends AbstractController
{

    /**
     * @Route("/", name="history_index", methods={"GET"})
     */
    public function index(Request $request, LogRepository $logRepository): Response
    {
        $entries = $logRepository->findBy([],['createdAt' => 'DESC'], 10, 0 );
        $number = count($entries);
        return $this->render('manager/log/index.html.twig', [
            'entries' => $entries,
            'number' => $number,
        ]);
    }
}