<?php

namespace App\Controller\Shop;

use App\Entity\Basket;
use App\Entity\Bookmark;
use App\Entity\Distribution;
use App\Entity\Item;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\User;
use App\Form\BasketType;
use App\Repository\BookmarkRepository;
use App\Repository\DistributionRepository;
use App\Repository\ItemRepository;
use App\Service\ProductManager\BookmarkManager;
use App\Service\ProductManager\ProductManager;
use App\Service\Response\RedirectBackResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\RedirectController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/shop/bookmark")
 */
class BookmarkController extends AbstractController
{
    use RedirectBackResponse;

    private $distributionRepository;
    private $productManager;
    private $itemRepository;
    private $bookmarkRepository;
    private $translator;

    public function __construct(DistributionRepository $distributionRepository, ProductManager $productManager, ItemRepository $itemRepository, BookmarkRepository $bookmarkRepository, TranslatorInterface $translator )
    {
        $this->distributionRepository = $distributionRepository;
        $this->productManager = $productManager;
        $this->itemRepository = $itemRepository;
        $this->bookmarkRepository = $bookmarkRepository;
        $this->translator = $translator;
    }


    /**
     * @Route("/", name="shop_bookmark_index", methods={"GET", "POST"})
     */
    public function index(Request $request): Response
    {
        return $this->render(
            'shop/bookmark/index.html.twig',[
                'bookmarks' => $this->getUser()->getBookmarks()
        ]);
    }

    /**
     * @Route("/{id}/addbookmark", name="shop_bookmark_add", requirements={"id" = "\d+"}, methods={"POST"})
     */
    public function addBookmark(Request $request, Item $item, BookmarkManager $bookmarkManager)
    {
        /** @var \App\Entity\User $user */
        $user = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        
        $bookmark = $bookmarkManager->getBookmarkFromItem($item);
        $userBookmark = ($bookmark)? $this->bookmarkRepository->findByUser($user, $bookmark) : null;

        /* if no bookmark at all or bookrmark not registered for the user */
        if((!$bookmark) || ($bookmark && !$userBookmark)){
            /* no bookmark at all */
            if(!$bookmark){
                $bookmark = $bookmarkManager->createBookmarkFromItem($item);
                $user->addBookmark($bookmark);
                $entityManager->persist($bookmark);
            } else{ /* not registered for the user */
                $user->addBookmark($bookmark);
            }
            $entityManager->persist($user);
            $entityManager->flush();

            if ($request->isXmlHttpRequest()) {
                $jsonData = array(
                    'error' => 0,
                    'id' => $item->getId(),
                    'designation' => $item->getDesignation(),
                    'disabled' => ($this->distributionRepository->findDistributionInOpenState())? false: true,
                    'flash' => array(
                        'alert' => 'success',
                        'message' => $this->translator->trans(
                            'shop.bookmarks.flash_add_success',
                            []
                        )
                    )
                );
                return new JsonResponse($jsonData);
            }

            $this->addFlash('success', $this->translator->trans(
                'shop.bookmarks.flash_add_success',
                []
            ));
        } else{
            if ($request->isXmlHttpRequest()) {
                $jsonData = array(
                    'error' => 1,
                    'flash' => array(
                        'alert' => 'warning',
                        'message' => $this->translator->trans(
                            'shop.bookmarks.flash_add_warning',
                            []
                        )
                    )
                );
                return new JsonResponse($jsonData);
            }
            $this->addFlash('warning', $this->translator->trans(
                'shop.bookmarks.flash_add_warning',
                []
            ));
        }

        return new RedirectResponse($this->redirectBack($request));
    }

    /**
     * @Route("/{id}/removebookmark", name="shop_bookmark_remove", requirements={"id" = "\d+"}, methods={"POST"})
     */
    public function remove(Request $request,  Bookmark $bookmark)
    {
        if ($request->isXmlHttpRequest()) {
            /** @var \App\Entity\User $user */
            $user = $this->getUser();

            
            $user->removeBookmark($bookmark);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($bookmark);
            $entityManager->persist($user);
            $entityManager->flush();

            $jsonData = array(
                'error' => 0,
                'url' => null,
                'flash' => array(
                    'alert' => 'warning',
                    'message' => $this->translator->trans(
                        'shop.bookmarks.flash_remove_success',
                        []
                    )
                )
            );

            return new JsonResponse($jsonData);
        }
    }

    public function menuBookmarks(): Response
    {
        return $this->render(
            'shop/bookmark/_list.html.twig',[
                'bookmarks' => $this->getUser()->getBookmarks()
            ]);
    }
}
