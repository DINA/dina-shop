<?php

namespace App\Controller\Shop;

use App\Entity\Basket;
use App\Entity\Bookmark;
use App\Entity\Distribution;
use App\Entity\Item;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\SharedProduct;
use App\Entity\User;
use App\Form\BasketType;
use App\Repository\BasketRepository;
use App\Repository\DistributionRepository;
use App\Repository\ItemRepository;
use App\Service\ProductManager\BookmarkManager;
use App\Service\ProductManager\ProductManager;
use App\Service\Response\RedirectBackResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\RedirectController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/shop/basket")
 */
class BasketController extends AbstractController
{
    use RedirectBackResponse;

    const MAX_INDIVIDUAL_ITEMS = 23;

    /** @var \App\Entity\Distribution $currentDistribution */
    private $currentDistribution;
    private $distributionRepository;
    private $productManager;
    private $bookmarkManager;
    private $basketRepository;
    private $translator;

    public function __construct(
        DistributionRepository $distributionRepository,
        ProductManager $productManager,
        BookmarkManager $bookmarkManager,
        BasketRepository $basketRepository,
        TranslatorInterface $translator
    ) {
        $this->distributionRepository = $distributionRepository;
        $this->productManager = $productManager;
        $this->bookmarkManager = $bookmarkManager;
        $this->currentDistribution = $this->distributionRepository->findDistributionInOpenState();
        $this->basketRepository = $basketRepository;
        $this->translator = $translator;
    }


    /**
     * @Route("/", name="basket_index", methods={"GET", "POST"})
     */
    public function index(Request $request): Response
    {
        if (!$this->currentDistribution) {
            return $this->render('shop/no_distribution.html.twig');
        }

        /** @var \App\Entity\User $user */
        $user = $this->getUser();

        if (!($basket = $user->getBasket())) {
            $basket = new Basket();
            $user->setBasket($basket);
        }

        $form = $this->createForm(BasketType::class, $basket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();

            if ($form->get('Order')->isClicked()) {

                $currentOrder = $this->getCurrentOrder($user);

                foreach ($basket->getProduct() as $product) {
                    /* Update order if product already ordered */
                    if (!$this->productManager->updateProductFromBasketToOrder($currentOrder, $product)) {
                        /* If not, add the product */
                        $currentOrder->addProduct($product);
                    }
                    $basket->removeProduct($product);
                }
                $currentOrder->setPrice($currentOrder->getPrice());

                $entityManager->persist($currentOrder);
                $entityManager->persist($basket);
                $entityManager->flush();
                $this->addFlash('success', $this->translator->trans(
                    'shop.basket_order.order_OK',
                ));
                return $this->redirectToRoute('order_show', ['id' => $currentOrder->getId()]);
            }

            if ($form->get('Refresh')->isClicked()) {
                $currentOrder = $this->getCurrentOrder($user);
                $currentOrder->setPrice($currentOrder->getPrice());
                $entityManager->persist($currentOrder);
                $entityManager->persist($basket);
                $entityManager->flush();
                $this->addFlash('success', $this->translator->trans(
                    'shop.basket_order.save_basket',
                ));
            }

            /* regenerate the form to not display the removed product */
            return $this->redirectToRoute('basket_index');
        }

        return $this->render(
            'shop/basket/index.html.twig',
            [
                'form' => $form->createView(),
                'basket' => $basket
            ]
        );
    }

    /**
     * @Route("/{id}/add", name="basket_item_add", requirements={"id" = "\d+"}, methods={"POST"})
     */
    public function add(Request $request, Item $item)
    {
        /** @var \App\Entity\User $user */
        $user = $this->getUser();

        if (!($basket = $user->getBasket())) {
            $basket = new Basket();
            $user->setBasket($basket);
        }

        $nbIndividualItems = $this->basketRepository->countItems('individual');
        $remains = self::MAX_INDIVIDUAL_ITEMS - $nbIndividualItems;

        /* If enough place in basket */
        if($remains > 0){
            // Create a product from individual order if it doesn't exists, otherwise add +1 to the quantity
            $product = $this->productManager->getProductFromItem($basket, $item);
            if (!$product) {
                $product = $this->productManager->createProductFromItem($item, $this->currentDistribution);
                $basket->addProduct($product);
            }

            $itemDesignation = $product->getItem()->getDesignation();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->persist($basket);
            $entityManager->flush();

            $nbIndividualItems = $this->basketRepository->countItems('individual');
            $remains = self::MAX_INDIVIDUAL_ITEMS - $nbIndividualItems;

            $flashResponse = array(
                'alert' => 'success',
                'message' => $this->translator->trans(
                    'shop.basket_order.flash_add_success',
                    ['item' => $itemDesignation,]
                )
            );

            if ($nbIndividualItems >= 20) {
                $flashAlertResponse = array(
                    'alert' => 'warning',
                    'message' => $this->translator->trans(
                        'shop.basket_order.flash_alert_warning',
                        ['nb' => $remains,]
                    )
                );
            } else{
                $flashAlertResponse = [];
            }
        } 
        /* No more place in basket */
        else{
            $flashResponse = array(
                'alert' => 'danger',
                'message' => $this->translator->trans(
                    'shop.basket_order.flash_add_full',
                )
            );
            $flashAlertResponse = [];
        }

        /* Send Response */
        if ($request->isXmlHttpRequest()) {
            $jsonData = array(
                'error' => 0,
                'nbItems' => $this->basketRepository->countItems(),
                'nbSharedItems' => $this->basketRepository->countItems('shared'),
                'nbIndividualItems' => $nbIndividualItems,
                'flash' => $flashResponse,
                'flashAlert' => $flashAlertResponse,
            );
            return new JsonResponse($jsonData);
        }

        $this->addFlash('success', $this->translator->trans(
            'shop.basket_order.flash_add_success',
            ['item' => $itemDesignation,],
        ));

        return new RedirectResponse($this->redirectBack($request));
    }

    /**
     * @Route("/{id}/remove", name="basket_item_remove", requirements={"id" = "\d+"}, methods={"DELETE"})
     */
    public function remove(Request $request,  Product $product)
    {
        if ($request->isXmlHttpRequest()) {

            if (!$this->isCsrfTokenValid('remove_product_token', $request->headers->get('x-remove-product-token'))) {
                throw $this->createAccessDeniedException();
            }

            /** @var \App\Entity\User $user */
            $user = $this->getUser();

            $basket = $user->getBasket();

            /* 
            /!\ WARNING: This is not the right method!
            Because product are CollectionType, it should only removed of the DOM through JS script. 
            Then the product would have been deleted ONLY ONCE the form would have been submitted (order or save buttons). This is why we have an orphan_removal option in entity and an allow_delete option in the form.
            But this is not the behavior we want here, we want the product to be deleted right now, without an additionnal click on a submit button. 
            So, we remove manually the product here from database and the JS script is in charge to refresh the page to generate a clean up-to-date form.
             */
            $basket->removeProduct($product);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($product);
            $entityManager->persist($basket);
            $entityManager->flush();

            $jsonData = array(
                'error' => 0,
                'url' => $this->generateUrl('basket_index'),
                'flash' => array(
                    'alert' => 'warning',
                    'message' => $this->translator->trans(
                        'shop.basket_order.flash_remove_success',
                        ['item' => $product->getItem()->getDesignation(),]
                    )
                )
            );

            return new JsonResponse($jsonData);
        }
    }

    /**
     * @Route("/{id}/add/previousorder", name="basket_add_from_order", requirements={"id" = "\d+"}, methods={"POST"})
     */
    public function addAllItemsFromPreviousOrder(Request $request, Order $order)
    {
        $oldProducts = $order->getProduct();
        foreach ($oldProducts as $oldProduct) {
            $item = $this->productManager->searchForEquivalentItem($this->currentDistribution, $oldProduct->getItem());
            if ($item) {
                $this->add($request, $item);
            }
        }
        return $this->index($request);
    }

    /**
     * @Route("/{id}/add/previousorder", name="basket_item_add_from_order", requirements={"id" = "\d+"}, methods={"POST"})
     */
    public function addFromPreviousOrder(Request $request, Item $oldItem, Distribution $distribution = null)
    {
        $distribution = $distribution ?? $this->distributionRepository->findDistributionInOpenState();
        $item = $this->productManager->searchForEquivalentItem($distribution, $oldItem);

        if ($item) {
            return $this->render('shop/basket/_addBasket.html.twig', [
                'row' => $item,
            ]);
        } else {
            return new Response();
        }
    }

    /**
     * @Route("/{id}/add/bookmark", name="basket_item_add_from_bookmark", requirements={"id" = "\d+"}, methods={"POST"})
     */
    public function addFromBookmark(Request $request, Bookmark $bookmark)
    {
        $item = $this->bookmarkManager->searchForItemFromBookmark($this->currentDistribution, $bookmark);

        if ($item) {
            return $this->render('shop/basket/_addBasketFromBookmark.html.twig', [
                'row' => $item,
            ]);
        } else {
            return new Response();
        }
    }

    /**
     * @Route("/{id}/add/sharedproduct", name="basket_item_add_from_sharedProduct", requirements={"id" = "\d+"}, methods={"POST"})
     */
    public function addFromSahredProduct(Request $request, SharedProduct $sharedProduct)
    {
        /** @var \App\Entity\User $user */
        $user = $this->getUser();

        if (!($basket = $user->getBasket())) {
            $basket = new Basket();
            $user->setBasket($basket);
        }

        $product = $this->productManager->getProductFromSharedProduct($basket, $sharedProduct);
        if (!$product) {
            $product = $this->productManager->createProductFromSharedProduct($sharedProduct);
            $basket->addProduct($product);
        }
        $itemDesignation = $product->getItem()->getDesignation();

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($product);
        $entityManager->persist($basket);
        $entityManager->flush();


        if ($request->isXmlHttpRequest()) {
            $jsonData = array(
                'error' => 0,
                'nbSharedItems' => $this->basketRepository->countItems('shared'),
                'nbIndividualItems' => $this->basketRepository->countItems('individual'),
                'flash' => array(
                    'alert' => 'success',
                    'message' => $this->translator->trans(
                        'shop.basket_order.flash_add_success',
                        ['item' => $itemDesignation,]
                    )
                )
            );
            return new JsonResponse($jsonData);
        }


        $this->addFlash('success', $this->translator->trans(
            'shop.basket_order.flash_add_success',
            ['item' => $itemDesignation,],
        ));

        return new RedirectResponse($this->redirectBack($request));
    }

    public function getCurrentOrder(User $user): Order
    {
        //Get all orders for the current distribution
        $distribution = $this->currentDistribution;
        $orders = $distribution->getOrders();

        //Search for an existing order for the user
        $currentOrder = null;
        foreach ($orders as $order) {
            if ($order->getUser() == $user) {
                $currentOrder = $order;
                break;
            }
        }
        // Create it if not found
        if (!$currentOrder) {
            $currentOrder = new Order();
            $user->addOrder($currentOrder);
            $distribution->addOrder($currentOrder);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->persist($distribution);

        return $currentOrder;
    }

    /**
     * @Route("/countbasketitems", name="basket_count_items", methods={"GET"})
     */
    public function countBasketItems(Request $request)
    {
        /** @var \App\Entity\User $user */
        $user = $this->getUser();
        $basket = $user->getBasket();


        if ($request->isXmlHttpRequest()) {
            $jsonData = array(
                'nbItems' => $this->basketRepository->countItems()
            );
            return new JsonResponse($jsonData);
        }
    }

    /**
     * @Route("/{id}/update_product", name="basket_item_update_product", requirements={"id" = "\d+"}, methods={"POST"})
     */
    public function update_product(Request $request, Product $product)
    {
        if ($request->isXmlHttpRequest()) {
            $params = $request->request->all();
            if (isset($params['unit']) && !empty($params['unit'])) {
                $product->setProductUnit($params['unit']);
            }
            if (isset($params['quantity']) && !empty($params['quantity'])) {
                $product->setQuantityDec($params['quantity']);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            $user = $this->getUser();
            $basket = $user->getBasket();

            //$currentOrder = $this->getCurrentOrder($user);
            //$currentOrder->setPrice($currentOrder->getPrice());

            $jsonData = array(
                'error' => 0,
                'price' => array(
                    'price' => $basket->getPrice(),
                    'VATprice' =>$basket->getVATPrice(),
                ),
            );

            return new JsonResponse($jsonData);
        }
    }
}
