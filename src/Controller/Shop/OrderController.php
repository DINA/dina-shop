<?php

namespace App\Controller\Shop;

use App\Entity\Basket;
use App\Entity\Item;
use App\Entity\Order;
use App\Entity\Product;
use App\Form\BasketType;
use App\Form\OrderType;
use App\Form\ProductType;
use App\Repository\DistributionRepository;
use App\Service\Mail\UserNotification;
use App\Service\ProductManager\ProductManager;
use Doctrine\Common\Collections\Criteria;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/shop/order")
 */
class OrderController extends AbstractController
{

    private $distributionRepository;
    private $productManager;
    private $translator;

    public function __construct(DistributionRepository $distributionRepository, ProductManager $productManager, TranslatorInterface $translator)
    {
        $this->distributionRepository = $distributionRepository;
        $this->productManager = $productManager;
        $this->translator = $translator;
    }


    /**
     * @Route("/", name="order_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        /** @var \App\Entity\User $user */
        $user = $this->getUser();      
        
        $criteria = Criteria::create()->orderBy(['id' => 'DESC']);
        $orders = $user->getOrders()->matching($criteria);
        
        if (!$orders) {
            throw new AccessDeniedException("Forbidden access");
        }

        return $this->render(
            'shop/order/index.html.twig',
            [
                'orders' => $orders,
            ]
        );
    }


    /**
     * @Route("/show/{id?null}", name="order_show", methods={"GET"})
     */
    public function show( Order $order = null): Response
    {
        $user = $this->getUser();
        $distribution = $this->distributionRepository->findDistributionInOpenState();
        if(!$order){
            $orders = ($distribution)? $distribution->getOrders() : [];
            //Search for an existing order for the user
            foreach ($orders as $userOrder) {
                if ($userOrder->getUser() == $user) {
                    $order = $userOrder;
                    break;
                }   
            }
        }

        if ($order) {
            $this->denyAccessUnlessGranted('SHOW_ORDER', $order->getUser());
        }

        return $this->render(
            'shop/order/show.html.twig',
            [
                'order' => $order,
                'distribution' => $distribution,
            ]
        );
    }

    /**
     * @Route("/{id}/edit", name="order_edit", methods={"POST"})
     */
    public function edit(Request $request, Order $order): Response
    {
        if ($this->isCsrfTokenValid('edit' . $order->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();

            $user = $this->getUser();
            if (!($basket = $user->getBasket())) {
                $basket = new Basket();
                $user->setBasket($basket);
            }

            foreach ($order->getProduct() as $product) {
                $basket->addProduct($product);
                $order->removeProduct($product);
            }

            $entityManager->persist($basket);
            $entityManager->remove($order);
            $entityManager->flush();
            $this->addFlash('warning', $this->translator->trans(
                'shop.basket_order.order_edit',
            ));
        }
        return $this->redirectToRoute('basket_index');
    }

    /**
     * @Route("/{id}/managerEdit", name="order_manager_edit", methods={"GET", "POST"})
     */
    public function managerEdit(Request $request, Order $order, UserNotification $userNotification): Response
    {
        $form = $this->createForm(OrderType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $order->setPrice($order->getPrice());
            $entityManager->persist($order);
            $entityManager->flush();
            $userNotification->editOrderNotification($order);
            return $this->redirectToRoute(
                'order_manager_edit',
                ['id' => $order->getId(),]);
           
        }

        return $this->render(
            'shop/order/managerShow.html.twig',[
                'order' => $order,
                'form' => $form->createView(),
            ]);
    }

    /**
     * @Route("/{id}/delete", name="order_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Order $order): Response
    {
        if ($this->isCsrfTokenValid('delete' . $order->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();

            foreach ($order->getProduct() as $product) {
                $order->removeProduct($product);
            }

            $entityManager->remove($order);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home');
    }
    

    
}
