<?php

namespace App\Controller\Shop;

use App\Controller\ItemController;
use App\Entity\Catalogue;
use App\Entity\CatalogueSearch;
use App\Entity\Item;
use App\Entity\SharingProposal;
use App\Form\CatalogueEditType;
use App\Form\CatalogueImportType;
use App\Form\CatalogueSearchShopType;
use App\Form\CatalogueSearchType;
use App\Form\ItemAltProducerType;
use App\Form\ItemRVCatSecFraisType;
use App\Form\ItemRVMercFrLgType;
use App\Form\ItemRVMercViandeType;
use App\Repository\CatalogueRepository;
use App\Repository\DistributionRepository;
use App\Repository\ItemRepository;
use App\Repository\SharingProposalRepository;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use App\Service\CatalogueImport\CatalogueImport;
use App\Service\Exception\NoDistributionException;
use App\Service\ProductManager\BookmarkManager;
use App\Service\Response\RedirectBackResponse;
use App\Service\Spreadsheet\CatalogueSpreadsheetReader;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Omines\DataTablesBundle\Adapter\ArrayAdapter;
use Omines\DataTablesBundle\Adapter\Doctrine\ORM\QueryBuilderProcessorInterface;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\TwigColumn;
use Omines\DataTablesBundle\DataTableFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/shop/item")
 */
class ShopItemController extends ItemController/* AbstractController */
{
    use RedirectBackResponse;

    private $distributionRepository;
    private $dataTableFactory;
    private $catalogueRepository;
    private $itemRepository;
    private $catalogueConfigReader;

    public function __construct(DistributionRepository $distributionRepository, TranslatorInterface $translator, DataTableFactory $dataTableFactory, CatalogueRepository $catalogueRepository, ItemRepository $itemRepository, CatalogueConfigReader $catalogueConfigReader)
    {
        $this->distributionRepository = $distributionRepository;
        $this->dataTableFactory = $dataTableFactory;
        $this->catalogueRepository = $catalogueRepository;
        $this->itemRepository = $itemRepository;
        $this->catalogueConfigReader = $catalogueConfigReader;
        parent::__construct(
            ['index' => 'shop/item/index.html.twig',
            'actions' => 'shop/item/_actions.html.twig',
            ],
            'shop_item_index',
            CatalogueSearchShopType::class,
            $translator
        );
    }

    /**
     * @Route("/", name="shop_item_index", methods={"GET","POST"})
     */
    public function index(Request $request, ItemRepository $itemRepository, CatalogueRepository $catalogueRepository, CatalogueConfigReader $catalogueConfigReader, DataTableFactory $dataTableFactory, PaginatorInterface $paginator): Response
    {
        return parent::index($request, $itemRepository, $catalogueRepository, $catalogueConfigReader, $dataTableFactory, $paginator);
    }

    /**
     * @Route("/sharing", name="shop_item_sharing_index", methods={"GET"})
     */
    public function sharingIndex(Request $request, SharingProposalRepository $sharingProposalRepository, PaginatorInterface $paginator): Response
    {
        $distribution = $this->distributionRepository->findDistributionInProgress();
        if (!$distribution || !$distribution->isDistributionInOpenState($distribution)) {
            return $this->render('shop/no_distribution.html.twig',[
                'suggestionsAllowed' => ($distribution)? true : false,
            ]);
        }

        $sharedProducts =  ($distribution) ? (($distribution->getSharingProposal()) ? $distribution->getSharingProposal()->getSharedProducts() : null) : null;

        return $this->render('shop/item/sharingIndex.html.twig', [
            'sharedProducts' => $sharedProducts,
            'distribution' => $distribution
        ]);
    }

    /**
     * extraDistributionStep
     *
     * @return mixed Distribution|Response
     */
    protected function extraDistributionStep()
    {
        $distribution = $this->distributionRepository->findDistributionInProgress();
        if (!$distribution) {
            return $this->render('shop/no_distribution.html.twig', [
                'suggestionsAllowed' => false,
            ]);
        }
        return $distribution;
    }
}
