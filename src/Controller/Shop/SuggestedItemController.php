<?php

namespace App\Controller\Shop;

use App\Entity\Basket;
use App\Entity\Bookmark;
use App\Entity\Distribution;
use App\Entity\Item;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\SuggestedItem;
use App\Entity\User;
use App\Form\BasketType;
use App\Repository\BookmarkRepository;
use App\Repository\DistributionRepository;
use App\Repository\ItemRepository;
use App\Repository\SuggestedItemRepository;
use App\Service\ProductManager\BookmarkManager;
use App\Service\ProductManager\ProductManager;
use App\Service\ProductManager\SuggestedItemManager;
use App\Service\Response\RedirectBackResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\RedirectController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/shop/sugggesteditems")
 */
class SuggestedItemController extends AbstractController
{
    use RedirectBackResponse;

    private $distributionRepository;
    private $itemRepository;
    private $suggestedItemRepository;
    private $bookmarkManager;
    private $translator;

    public function __construct(DistributionRepository $distributionRepository, ItemRepository $itemRepository, SuggestedItemRepository $suggestedItemRepository, BookmarkManager $bookmarkManager, TranslatorInterface $translator)
    {
        $this->distributionRepository = $distributionRepository;
        $this->itemRepository = $itemRepository;
        $this->suggestedItemRepository = $suggestedItemRepository;
        $this->bookmarkManager = $bookmarkManager;
        $this->translator = $translator;
        $this->currentDistribution = $this->distributionRepository->findDistributionInOpenState();
    }


    /**
     * @Route("/", name="suggested_items_index", methods={"GET", "POST"})
     */
    public function index(Request $request): Response
    {
        $currentDistribution = $this->distributionRepository->findDistributionInShareState();
        $distribution = ($currentDistribution) ?  $currentDistribution : $this->distributionRepository->findDistributionInNewState();
        $suggestedItems = $this->suggestedItemRepository->findAllByDistributionAndUser($distribution, $this->getUser()) ;

        return $this->render(
            'shop/suggested_item/index.html.twig',
            [
                'suggestedItems' => $suggestedItems
            ]
        );
    }

    /**
     * @Route("/{id}/addsuggesteditem", name="suggesteditem_add", requirements={"id" = "\d+"}, methods={"POST"})
     */
    public function addSuggestedItem(Request $request, Item $item, SuggestedItemManager $suggestedItemManager)
    {
        /** @var \App\Entity\User $user */
        $user = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();

        $currentDistribution = $this->distributionRepository->findDistributionInShareState();

        $distribution = ($currentDistribution) ?  $currentDistribution : $this->distributionRepository->findDistributionInNewState();

        if ($distribution) {
            $suggestedItem = $suggestedItemManager->getSuggestedItemFromItem($item);
            $distributionSuggestedItem = ($suggestedItem) ? $this->suggestedItemRepository->findByDistributionAndUser($distribution, $user, $suggestedItem) : null;

            /* if no suggested Item at all or suggested item not registered for the user */
            if ((!$suggestedItem) || ($suggestedItem && !$distributionSuggestedItem)) {
                /* no suggestedItem at all */
                if (!$suggestedItem) {
                    $suggestedItem = $suggestedItemManager->createSuggestedItemFromItem($item);
                    $suggestedItem->addUser($user);
                    $distribution->addSuggestedItem($suggestedItem);
                } else { /* not suggestedItem for this distribution */
                    $suggestedItem->addUser($user);
                    $distribution->addSuggestedItem($suggestedItem);
                }
                $entityManager->persist($suggestedItem);
                $entityManager->persist($distribution);
                $entityManager->flush();

                if ($request->isXmlHttpRequest()) {
                    $jsonData = array(
                        'error' => 0,
                        'id' => $suggestedItem->getId(),
                        'designation' => $suggestedItem->getDesignation(),
                        'flash' => array(
                            'alert' => 'success',
                            'message' => $this->translator->trans(
                                'worker.suggested_items.flash_add_success',
                                []
                            )
                        )
                    );
                    return new JsonResponse($jsonData);
                }

                $this->addFlash('success', $this->translator->trans(
                    'worker.suggested_items.flash_add_success',
                    []
                ));
            } elseif ($distributionSuggestedItem) {
                if ($request->isXmlHttpRequest()) {
                    $jsonData = array(
                        'error' => 1,
                        'flash' => array(
                            'alert' => 'warning',
                            'message' => $this->translator->trans(
                                'worker.suggested_items.flash_warning',
                                []
                            )
                        )
                    );
                    return new JsonResponse($jsonData);
                }
                $this->addFlash('warning', $this->translator->trans(
                    'worker.suggested_items.flash_warning',
                    []
                ));
            }
        } else {
            if ($request->isXmlHttpRequest()) {
                $jsonData = array(
                    'error' => 2,
                    'flash' => array(
                        'alert' => 'error',
                        'message' => $this->translator->trans(
                            'worker.suggested_items.flash_error',
                            []
                        )
                    )
                );
                return new JsonResponse($jsonData);
            }
            $this->addFlash('danger', $this->translator->trans(
                'worker.suggested_items.flash_error',
                []
            ));
        }

        return new RedirectResponse($this->redirectBack($request));
    }

    /**
     * @Route("/{id}/removesuggesteditem", name="suggesteditem_remove", requirements={"id" = "\d+"}, methods={"POST"})
     */
    public function remove(Request $request,  SuggestedItem $suggestedItem)
    {
        /** @var \App\Entity\User $user */
        $user = $this->getUser();

        $currentDistribution = $this->distributionRepository->findDistributionInProgress();
        $distribution = ($currentDistribution) ?  $currentDistribution : $this->distributionRepository->findDistributionInNewState();

        $entityManager = $this->getDoctrine()->getManager();
        if ($distribution) {
            /* Remove from user */
            $user->removeSuggestedItem($suggestedItem);
            /* If no user suggest this item, remove from distribution and delete it */
            if ($suggestedItem->getUsers()->isEmpty()) {
                $distribution->removeSuggestedItem($suggestedItem);
                $entityManager->remove($suggestedItem);
            }
            $entityManager->flush();
            if ($request->isXmlHttpRequest()) {
                $jsonData = array(
                    'error' => 0,
                    'url' => null,
                    'flash' => array(
                        'alert' => 'warning',
                        'message' => $this->translator->trans(
                            'worker.suggested_items.flash_remove_success',
                            []
                        )
                    )
                );
                return new JsonResponse($jsonData);
            } else {
                $this->addFlash('success', $this->translator->trans(
                    'worker.suggested_items.flash_remove_success',
                    []
                ));
            }
        } else {
            if ($request->isXmlHttpRequest()) {
                $jsonData = array(
                    'error' => 0,
                    'url' => null,
                    'flash' => array(
                        'alert' => 'warning',
                        'message' => $this->translator->trans(
                            'worker.suggested_items.flash_error',
                            []
                        )
                    )
                );
                return new JsonResponse($jsonData);
            } else {
                $this->addFlash('danger', $this->translator->trans(
                    'worker.suggested_items.flash_error',
                    []
                ));
            }
        }

        return new RedirectResponse($this->redirectBack($request));
    }

    /**
     * @Route("/{id}/add/bookmark", name="suggesteditem_add_from_bookmark", requirements={"id" = "\d+"}, methods={"POST"})
     */
    public function addFromBookmark(Request $request, Bookmark $bookmark)
    {
        $item = $this->bookmarkManager->searchForItemFromBookmark($this->currentDistribution, $bookmark);

        if ($item) {
            return $this->render('shop/suggested_item/_addSuggestedItemFromBookmark.html.twig', [
                'row' => $item,
            ]);
        } else {
            return new Response();
        }
    }
}
