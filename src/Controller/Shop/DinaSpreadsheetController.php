<?php

namespace App\Controller\Shop;

use App\Entity\Distribution;
use App\Entity\Order;
use App\Service\Date\LocaleDate;
use App\Service\DinaSpreadsheet\DinaSpreadsheet;
use App\Service\Mail\AssoNotification;
use App\Service\Response\RedirectBackResponse;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Mailer\MailerInterface;

/**
 * @Route("/shop/dina")
 */
class DinaSpreadsheetController extends AbstractController
{
    use RedirectBackResponse;
    use LocaleDate;

    private $dinaSpreadsheet;
    private $assoNotification;
    private $params;

    public function __construct(DinaSpreadsheet $dinaSpreadsheet, AssoNotification $assoNotification, ParameterBagInterface $params)
    {
        $this->dinaSpreadsheet = $dinaSpreadsheet;
        $this->assoNotification = $assoNotification;
        $this->params = $params;
    }

    /**
     * @Route("/distribution/{id}/sharingproposal", name="gen_sharingproposal_dinaspreadsheet", methods={"GET","POST"})
     */
    public function genSharingPropsalDinaSpreadsheet(Request $request, Distribution $distribution): Response
    {
        $filepath = $this->dinaSpreadsheet->generateSharingProposal($distribution->getSharingProposal());
        $localeDate = $this->setLongFormatLocale($distribution->getDeliveryDate(), $this->params->get('locale'));
        return $this->downloadFile($filepath, 'Proposition de partage - ' . $localeDate, '.xlsx');
    }

    /**
     * @Route("/distribution/{id}/mail", name="mail_sharingproposal_dinaspreadsheet", methods={"GET","POST"})
     */
    public function mailSharingPropsalDinaSpreadsheet(Request $request, Distribution $distribution): Response
    {
        $filepath = $this->dinaSpreadsheet->generateSharingProposal($distribution->getSharingProposal());
        $this->assoNotification->mailOneFile($filepath, $distribution, $this->params->get('locale'));
        return new RedirectResponse($this->redirectBack($request));
    }

    /**
     * @Route("/{id}", name="gen_single_dinaspreadsheet", methods={"GET","POST"})
     */
    public function genSingleDinaSpreadsheet(Request $request, Order $order): Response
    {
        $filepath = $this->dinaSpreadsheet->generateSingle($order);
        $localeDate = $this->setLongFormatLocale($order->getDistribution()->getDeliveryDate(), $this->params->get('locale'));
        return $this->downloadFile($filepath, $order->getUser()->getUsername() . ' - commande - ' . $localeDate, '.xlsx');
    }

    /**
     * @Route("/distribution/{id}", name="gen_bulk_dinaspreadsheet", methods={"GET","POST"})
     */
    public function downloadBulkDinaSpreadsheet(Request $request, Distribution $distribution): Response
    {
        if (!$distribution->getOrders()->isEmpty()) {
            $folder = $this->genBulkDinaSpreadsheet($request, $distribution);
            $filepath =  $this->dinaSpreadsheet->zip($distribution);
            return $this->downloadFile($filepath, 'BondeCommande_' . $distribution->getDeliveryDate()->format('Ymd'), '.zip');
        } else{
            return new RedirectResponse($this->redirectBack($request));
        }
    }

    /**
     * @Route("/distribution/{id}/mail_orders", name="mail_bulk_dinaspreadsheet", methods={"GET","POST"})
     */
    public function mailBulkDinaSpreadsheet(Request $request, Distribution $distribution): Response
    {
        if(!$distribution->getOrders()->isEmpty()){
            $folder = $this->dinaSpreadsheet->generateBulk($distribution);
            $this->assoNotification->mailFiles($folder, $distribution, $this->params->get('locale'));
        }
        return new RedirectResponse($this->redirectBack($request));
    }

    private function genBulkDinaSpreadsheet(Request $request, Distribution $distribution): string
    {
        if (!$distribution->isDistributionInClosedState($distribution)) {
            $this->flash->error('Vous devez clore les commandes avant de générer les bons');
            return new RedirectResponse($this->redirectBack($request));
        }
        return $this->dinaSpreadsheet->generateBulk($distribution);
    }

    private function downloadFile(string $filepath, string $fileName, string $fileExtension)
    {
        if (file_exists($filepath)) {
            $response =  new BinaryFileResponse($filepath);
            $response->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                $fileName . $fileExtension
            );
            $response->deleteFileAfterSend(true);
            return $response;
        } else {
            throw $this->createNotFoundException('The file does not exist');
        }
    }
}