<?php

namespace App\Controller;

use App\Entity\Catalogue;
use App\Entity\CatalogueSearch;
use App\Entity\Item;
use App\Form\CatalogueType;
use App\Form\ExtraCatalogueType;
use App\Form\ItemType;
use App\Repository\CatalogueRepository;
use App\Repository\DistributionRepository;
use App\Repository\ItemRepository;
use App\Repository\ProducerRepository;
use App\Repository\ProductRepository;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\TwigColumn;
use Omines\DataTablesBundle\DataTableFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/extra")
 */
class ExtraItemController extends ItemController
{
    public function __construct(TranslatorInterface $translator)
    {
        parent::__construct(
            [
                'index' => 'extra_item/new_item.html.twig',
                'new' => 'extra_item/new_item.html.twig',
                'edit' => 'extra_item/edit.html.twig',
                'show' => 'extra_item/show.html.twig',
            ],
            'extra_item',
            CatalogueSearchType::class,
            $translator
        );
    }

    /**
     * @Route("/item", name="extra_item")
     */
    public function indexAll(Request $request, CatalogueRepository $catalogueRepository, ItemRepository $itemRepository, DistributionRepository $distributionRepository,CatalogueConfigReader $catalogueConfigReader, DataTableFactory $dataTableFactory)
    {
        /** @var \App\Entity\User $user */
        $user = $this->getUser();
        if ($user->hasRole('ROLE_WORKER')) {
            $template = 'worker/extra_item/sharingIndex.html.twig';
            $this->template['actions'] = 'worker/extra_item/_actions.html.twig';
            $distribution = $distributionRepository->findDistributionInShareProgressState();
            if (!$distribution) {
                return $this->render('worker/no_distribution.html.twig');
            }
        } else{
            $template = 'shop/extra_item/index.html.twig';
            $this->template['actions'] = 'shop/extra_item/_actions.html.twig';
            $distribution = $distributionRepository->findDistributionInOpenState();
            if (!$distribution) {
                return $this->render('shop/no_distribution.html.twig');
            }
        }

        /* Combines all columns for all catalogues */
        $catalogues = $catalogueRepository->findAllExtraOnlineByDistribution($distribution);
        if ($catalogues) {
            $mapping = [];
            foreach($catalogues as $catalogue){
                $localMapping =  $catalogueConfigReader->getCatalogueKeysMapping($catalogue->getType());
                $mapping = array_merge($mapping,$localMapping);
            }
        }
        /* if no article at all, create some dummies coloumn */
        if(!isset($mapping)){
            $mapping = [
                'reference' => 'Référence',
                'designation' => 'Désignation',
                'unitaryPrice' => 'Prix unitaire',
            ];
        }

        $params = $request->request->all();
        $order = [];
        /* Get column to sort */
        if (isset($params['order'])) {
            $order['dir'] = $params['order'][0]['dir'];
            $order['column']  = self::DT_BUNDLE_COLUMNS_ORDER[$params['order'][0]['column']];
        }

       /* Create datatable */
        $table = $dataTableFactory->create();
        $this->dtAddColumns($table,  $catalogueConfigReader, null, $mapping);
        $table->add('Catalogue',  TextColumn::class, [
            'label' => 'Catalogue', 'render' => function ($value, $context) {
                return  $context->getCatalogueId()->getName();
            }
        ]);
        
        $table->createAdapter(ORMAdapter::class, [
            'entity' => Item::class,
            'query' => function (QueryBuilder $builder) use ($itemRepository, $order) {
                return $itemRepository->findAllOnlineExtraItems($builder, $order);
            },
        ])->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        return $this->render($template, [
            'distribution' => $distribution,
            'datatable' => $table ?? null
        ]);
    }
    
    /**
     * @Route("/catalogue_new", name="extra_catalogue_new", methods={"GET","POST"})
     */
    public function newCatalogue(Request $request, 
                        ProducerRepository $producerRepository, 
                        CatalogueRepository $catalogueRepository, 
                        CatalogueConfigReader $catalogueConfigReader,
                        DistributionRepository $distributionRepository
                        ): Response
    {
        $catalogue = new Catalogue();
        $distribution = $distributionRepository->findDistributionInProgress();

        $form = $this->createForm(ExtraCatalogueType::class, $catalogue, ['distribution' => $distribution]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            try {
                $selectedCatalogue = $catalogueRepository->findExtraOnline($catalogue->getType());
                if(!$selectedCatalogue){
                    //Extra catalogue not yet created, do it.
                    $catalogue->setName('Extra ' . $catalogueConfigReader->getCataloguesNames()[$catalogue->getType()].' #'.$distribution->getId());
                    $catalogue->setComment('Auto generated extra catalogue');
                    $catalogue->setExtra(true);
                    $catalogue->setAutoArchive(true);
                    $catalogue->setFormat($catalogueConfigReader->findCatalogueFormat($catalogue->getType()));
                    $entityManager->persist($catalogue);
                    $distribution->addCatalogue($catalogue);
                    $entityManager->persist($distribution);
                    $entityManager->flush();
                    $selectedCatalogue = $catalogue;
                }
                return $this->redirectToRoute('extra_item_new', ['cat_id' => $selectedCatalogue->getId()]);
            } catch (Exception $e) {
                $this->addFlash('danger', $this->translator->trans(
                    'extra_items.new.flash_error',
                    ['error' => $e->getMessage(),],
                ));
            }
        }

        return $this->render('extra_item/new_catalogue.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/item_new/{cat_id}", name="extra_item_new", methods={"GET","POST"})
     * @ParamConverter("catalogue", options={"mapping": {"cat_id": "id"}})
     */
    public function itemNew(Request $request, Catalogue $catalogue): Response
    {
        return parent::new($request,$catalogue );
    }

    /**
     * @Route("/{id}", name="extra_item_show", requirements={"id" = "\d+"}, methods={"GET"})
     */
    public function show(Item $item): Response
    {
        return parent::show($item);
    }

    /**
     * @Route("/{id}/edit", name="extra_item_edit", requirements={"id" = "\d+"}, methods={"GET","POST"})
     */
    public function edit(Request $request, Item $item): Response
    {
        return parent::edit($request, $item);
    }

    /**
     * @Route("/{id}", name="extra_item_delete", requirements={"id" = "\d+"}, methods={"DELETE"})
     */
    public function delete(Request $request, Item $item): Response
    {
        return parent::delete($request, $item);
    }
}
