<?php

namespace App\Controller;

use App\Entity\Catalogue;
use App\Entity\CatalogueSearch;
use App\Entity\Distribution;
use App\Entity\Item;
use App\Form\CatalogueImportType;
use App\Form\CatalogueSearchType;
use App\Form\ItemType;
use App\Repository\CatalogueRepository;
use App\Repository\ItemRepository;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use App\Service\CatalogueImport\CatalogueImport;
use App\Service\ProductManager\Flag;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\TwigColumn;
use Omines\DataTablesBundle\DataTableFactory;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class ItemController extends AbstractController
{
    use Flag;

    const DT_BUNDLE_COLUMNS_ORDER = ['empty','details','buttons', 'reference','designation'];

    protected $template;
    private $redirectRoute;
    private $catalogueSearchTypeClass;
    protected $translator;  

    public function __construct(array $template, string $redirectRoute, string $catalogueSearchTypeClass, TranslatorInterface $translator )
    {
        $this->template = $template;
        $this->redirectRoute = $redirectRoute;
        $this->catalogueSearchTypeClass = $catalogueSearchTypeClass;
        $this->translator = $translator;
    }

    public function index(Request $request, ItemRepository $itemRepository, CatalogueRepository $catalogueRepository, CatalogueConfigReader $catalogueConfigReader , DataTableFactory $dataTableFactory, PaginatorInterface $paginator): Response
    {
        $distribution = $this->extraDistributionStep();
        if ($distribution instanceof Response) {
            return $distribution;
        }

        $catId = $request->query->get('catalogue') ?? null;
        $selectedCatalogue = new CatalogueSearch();
        $form = $this->createForm($this->catalogueSearchTypeClass, $selectedCatalogue, ['distribution' => $distribution]);
        $form->handleRequest($request);

        if (!($form->isSubmitted() && $form->isValid()) && $catId) {
            //Validation error but a catalogue is selected, reset all fields!
            return $this->redirectToRoute($this->redirectRoute, ['catalogue' => $catId]);
        }

        if ($request->isXmlHttpRequest() || ($form->isSubmitted() && $form->isValid())) {

            $catId = $request->request->get('catalogue') ?? $request->query->get('catalogue') ?? null;
            $params = $request->request->all();
            $searchCriterias = new CatalogueSearch();
            $catalogue = $catalogueRepository->find($catId);
            $searchCriterias->setCatalogue($catalogue);
            $order = [];

            /* Get params */
            if (isset($params['searchText'])) {
                $searchCriterias->setSearchText($params['searchText']);
            }
            if (isset($params['origins'])) {
                $searchCriterias->setOrigins(explode(',',$params['origins'][0]));
            }
            if (isset($params['families'])) {
                $searchCriterias->setFamilies(explode(',', $params['families'][0]));
            }
            if (isset($params['brands'])) {
                $searchCriterias->setBrands(explode(',', $params['brands'][0]));
            }
            /* Get column to sort */
            if (isset($params['order'])) {
                $order['dir'] = $params['order'][0]['dir'];
                $order['column']  = self::DT_BUNDLE_COLUMNS_ORDER[$params['order'][0]['column']];
            }

            /* Create datatable */
            $table = $dataTableFactory->create();
            $this->dtAddColumns($table,  $catalogueConfigReader,  $catalogue);
            $table->createAdapter(ORMAdapter::class, [
                'entity' => Item::class,
                'query' => function (QueryBuilder $builder) use ($searchCriterias, $itemRepository, $order) {
                    return $itemRepository->findByCriterias($builder, $searchCriterias, $order);
                },
            ])->handleRequest($request);

            if ($table->isCallback()) {
                return $table->getResponse();
            }
        }

        return $this->render($this->template['index'], [
            'form' => $form->createView(),
            'catalogue' => $selectedCatalogue->getCatalogue(),
            'distribution' => $distribution,
            'datatable' => $table ?? null
        ]);
    }

    public function new(Request $request, Catalogue $catalogue): Response
    {
        $item = new Item();
        $form = $this->createForm(ItemType::class, $item, ['catalogueType' => $catalogue->getType()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $item->setCatalogueId($catalogue);
            $entityManager->persist($item);
            $entityManager->flush();
            $this->addFlash('success', $this->translator->trans(
                'manager.item.new.flash_success',
                []
            ));

            return $this->redirectToRoute($this->redirectRoute, ['catalogue' => $catalogue->getId()]);
        }

        return $this->render($this->template['new'], [
            'item' => $item,
            'form' => $form->createView(),
        ]);
    }


    public function show(Item $item): Response
    {
        return $this->render($this->template['show'], [
            'item' => $item,
        ]);
    }

    public function edit(Request $request, Item $item): Response
    {
        $form = $this->createForm(ItemType::class, $item, ['catalogueType' => $item->getCatalogueId()->getType()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var EntityManagerInterface */
            $em = $this->getDoctrine()->getManager();
            try {
                $em->beginTransaction(); // suspend auto-commit
                $em->lock($item, \Doctrine\DBAL\LockMode::PESSIMISTIC_READ);
                $em->flush();
                $em->getConnection()->commit();
                $this->addFlash('success', $this->translator->trans(
                    'common.flash_message.edit.success',
                    []
                ));
            } catch (Exception $e) {
                $this->addFlash('danger', $this->translator->trans(
                    'manager.item.edit.flash_error',
                    ['error' => $e->getMessage(),],
                ));
            }
            return $this->redirectToRoute($this->redirectRoute, ['catalogue' => $item->getCatalogueId()->getId()]);
        }

        return $this->render($this->template['edit'], [
            'item' => $item,
            'form' => $form->createView(),
        ]);
    }

    public function delete(Request $request, Item $item): Response
    {
        if ($this->isCsrfTokenValid('delete' . $item->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            //Gros workakound dégueu: voir ItemListener.php 
            if ($item->getCatalogueId()->isStatus(Catalogue::STATUS_ARCHIVED)) {
                $this->addFlash('danger', 'Can\'t delete item from an archived catalogue');
            } else {
                try {
                    $entityManager->remove($item);
                    $entityManager->flush();
                    $this->addFlash('success', $this->translator->trans(
                        'manager.item.delete.flash_success',[],
                    ));
                } catch (Exception $e) {
                    $this->addFlash('danger', $this->translator->trans(
                        'manager.item.delete.flash_error',
                        ['error' => $e->getMessage(),],
                    ));
                }
            }
        }

        return $this->redirectToRoute($this->redirectRoute, ['catalogue' => $item->getCatalogueId()->getId()]);
    }

    /**
     * extraDistributionStep
     *
     * @return mixed Distribution|Response
     */
    protected function extraDistributionStep()
    {
        return null;
    }

    protected function dtAddColumns($table, CatalogueConfigReader $catalogueConfigReader, ?Catalogue $catalogue=null, ?array $mapping=null)
    {
        /* If there is no specific catalogue, it means we want to display all extra items and the mapping have been already computed  */
        if($catalogue){
            $mapping =  $catalogueConfigReader->getCatalogueKeysMapping($catalogue->getType());
        }

        $table
            ->add('showdetails',  TextColumn::class, ['label' => 'Détails', 'className' => 'details-control'])
            ->add('details',  TextColumn::class, ['visible' => false, 'className' => 'details', 'render' => function ($value, $context) use ($mapping) {
                $strings[] =  $context->getOrigin() ? sprintf('<li> %s: %s </li>', $mapping['origin'], $context->getOrigin()) : null;
                $strings[] =  $context->getLabel() ? sprintf('<li> %s: %s </li>', $mapping['label'], $context->getLabel()) : null;
                $strings[] =  $context->getInspection() ? sprintf('<li> %s: %s </li>', $mapping['inspection'], $context->getInspection()) : null;
                $strings[] =  $context->getVat() ? sprintf('<li> %s: %s </li>', $this->translator->trans('common.listing.vat', []), $context->getVat()) : null;
                $strings[] =  $context->getUnpackedPrice() ? sprintf('<li> %s: %s </li>', $mapping['unpackedPrice'], $context->getUnpackedPrice()) : null;
                $strings[] =  $context->getEd() ? sprintf('<li> %s: %s </li>', $mapping['ed'], $context->getEd()) : null;
                $strings[] =  $context->getEAN13() ? sprintf('<li> %s: %s </li>', $mapping['EAN13'], $context->getEAN13()) : null;
                $strings[] =  $context->getSKU() ? sprintf('<li> %s: %s </li>', $mapping['SKU'], $context->getSKU()) : null;
                return sprintf('<ul>%s</ul>', implode('', $strings));
            }])
            ->add('buttons', TwigColumn::class, [
                'label' => 'Actions',
                'className' => 'buttons align-middle',
                'template' => $this->template['actions'],
            ])
            ->add('reference', TextColumn::class, ['label' => $mapping['reference']])
            ->add('designation', TextColumn::class, ['label' => $mapping['designation']])
            /* /!\ Do not change previous order, see DT_BUNDLE_COLUMNS_ORDER */
            ->add('unitaryPrice', TextColumn::class, ['label' => $mapping['unitaryPrice'], 'orderable' => false, 'render' => function ($value, $context) {
                return sprintf('%s€', $context->getUnitaryPrice());
            }]);
        if (isset($mapping['origin'])) {
            $table->add('origin', TextColumn::class, ['label' => '<span class="oi oi-globe"></span>', 'className' => 'text-center', 'orderable' => false, 'render' => function ($value, $context) {
                return $this->getCountryFlag($context->getOrigin(), $context->getCatalogueId()->getFormat());
            }]);
        }

        if (isset($mapping['brand'])) {
            $table->add('brand', TextColumn::class, ['label' => $mapping['brand'], 'orderable' => false]);
        }

        if (isset($mapping['family'])) {
            $table->add(
                'family',
                TextColumn::class,
                ['label' => $mapping['family'], 'orderable' => false]
            );
        }

        if (isset($mapping['packaging'])) {
            $table->add('packaging', TextColumn::class, ['label' => $mapping['packaging'], 'orderable' => false]);
        }

        if (isset($mapping['packing'])) {
            $table->add('packing', TextColumn::class, ['label' => $mapping['packing'], 'orderable' => false]);
        }

        if (isset($mapping['minPacking'])) {
            $table->add('minPacking', TextColumn::class, ['label' => $mapping['minPacking'], 'orderable' => false]);
        }

        if (isset($mapping['calibre_categ'])) {
            $table->add('calibre_categ', TextColumn::class, ['label' => $mapping['calibre_categ'], 'orderable' => false]);
        }
        if (isset($mapping['comment'])) {
            $table->add('comment', TextColumn::class, ['label' => $mapping['comment'], 'orderable' => false]);
        }
        if (isset($mapping['customField1'])) {
            $table->add('customField1', TextColumn::class, ['label' => $mapping['customField1'], 'orderable' => false]);
        }
        if (isset($mapping['customField2'])) {
            $table->add('customField2', TextColumn::class, ['label' => $mapping['customField2'], 'orderable' => false]);
        }
        if (isset($mapping['customField3'])) {
            $table->add('customField3', TextColumn::class, ['label' => $mapping['customField3'], 'orderable' => false]);
        }
        if (isset($mapping['customField4'])) {
            $table->add('customField4', TextColumn::class, ['label' => $mapping['customField4'], 'orderable' => false]);
        }
    }
}
