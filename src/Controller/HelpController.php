<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HelpController extends AbstractController
{
    /**
     * @Route("/help", name="help")
     */
    public function index()
    {
        return $this->render('help/index.html.twig', [
        ]);
    }

    /**
     * @Route("/help/shop", name="help_shop")
     */
    public function help_shop()
    {
        return $this->render('help/shop.html.twig', [
        ]);
    }

    /**
     * @Route("/help/shop/general/menu", name="help_shop_general_menu")
     */
    public function help_shop_general_menu()
    {
        return $this->render('help/shop/general/menu.html.twig', []);
    }

    /**
     * @Route("/help/shop/general/presentation", name="help_shop_general_presentation")
     */
    public function help_shop_general_presentation()
    {
        return $this->render('help/shop/general/presentation.html.twig', []);
    }

    /**
     * @Route("/help/shop/general/errors", name="help_shop_general_errors")
     */
    public function help_shop_general_errors()
    {
        return $this->render('help/shop/general/errors.html.twig', []);
    }

    /**
     * @Route("/help/shop/basket/interface", name="help_shop_basket_interface")
     */
    public function help_shop_basket_interface()
    {
        return $this->render('help/shop/basket/interface.html.twig', []);
    }

    /**
     * @Route("/help/shop/basket/errors", name="help_shop_basket_errors")
     */
    public function help_shop_basket_errors()
    {
        return $this->render('help/shop/basket/errors.html.twig', []);
    }

    /**
     * @Route("/help/shop/order/actions", name="help_shop_order_actions")
     */
    public function help_shop_order_actions()
    {
        return $this->render('help/shop/order/actions.html.twig', []);
    }

    /**
     * @Route("/help/shop/suggest/add", name="help_shop_suggest_add")
     */
    public function help_shop_suggest_add()
    {
        return $this->render('help/shop/suggestion/add.html.twig', []);
    }

    /**
     * @Route("/help/shop/suggest/remove", name="help_shop_suggest_remove")
     */
    public function help_shop_suggest_remove()
    {
        return $this->render('help/shop/suggestion/remove.html.twig', []);
    }

    /**
     * @Route("/help/shop/bookmark/add", name="help_shop_bookmark_add")
     */
    public function help_shop_bookmark_add()
    {
        return $this->render('help/shop/bookmark/add.html.twig', []);
    }

    /**
     * @Route("/help/shop/bookmark/remove", name="help_shop_bookmark_remove")
     */
    public function help_shop_bookmark_remove()
    {
        return $this->render('help/shop/bookmark/remove.html.twig', []);
    }

    /**
     * @Route("/help/shop/bookmark/order", name="help_shop_bookmark_order")
     */
    public function help_shop_bookmark_order()
    {
        return $this->render('help/shop/bookmark/order.html.twig', []);
    }

    /**
     * @Route("/help/shop/extra/add", name="help_shop_extra_add")
     */
    public function help_shop_extra_add()
    {
        return $this->render('help/shop/extra/add.html.twig', []);
    }

    /**
     * @Route("/help/worker", name="help_worker")
     */
    public function help_worker()
    {
        return $this->render('help/worker.html.twig', []);
    }

    /**
     * @Route("/help/worker/general/menu", name="help_worker_general_menu")
     */
    public function help_worker_general_menu()
    {
        return $this->render('help/worker/general/menu.html.twig', []);
    }

    /**
     * @Route("/help/worker/general/presentation", name="help_worker_general_presentation")
     */
    public function help_worker_general_presentation()
    {
        return $this->render('help/worker/general/presentation.html.twig', []);
    }

    /**
     * @Route("/help/worker/general/errors", name="help_worker_general_errors")
     */
    public function help_worker_general_errors()
    {
        return $this->render('help/worker/general/errors.html.twig', []);
    }

    /**
     * @Route("/help/worker/selection/suggestion", name="help_worker_selection_suggestion")
     */
    public function help_worker_selection_suggestion()
    {
        return $this->render('help/worker/selection/suggestion.html.twig', []);
    }

    /**
     * @Route("/help/worker/catalogue/list", name="help_worker_catalogue_list")
     */
    public function help_worker_catalogue_list()
    {
        return $this->render('help/worker/catalogues/list.html.twig', []);
    }

    /**
     * @Route("/help/worker/catalogue/import", name="help_worker_catalogue_import")
     */
    public function help_worker_catalogue_import()
    {
        return $this->render('help/worker/catalogues/import.html.twig', []);
    }

    /**
     * @Route("/help/worker/sharingproposal/interface", name="help_worker_sharingproposal_interface")
     */
    public function help_worker_sharingproposal_interface()
    {
        return $this->render('help/worker/sharing_proposal/interface.html.twig', []);
    }

    /**
     * @Route("/help/worker/sharingproposal/errors", name="help_worker_sharingproposal_erros")
     */
    public function help_worker_sharingproposal_erros()
    {
        return $this->render('help/worker/sharing_proposal/errors.html.twig', []);
    }

    /**
     * @Route("/help/worker/sharingproposal/finish", name="help_worker_sharingproposal_finish")
     */
    public function help_worker_sharingproposal_finish()
    {
        return $this->render('help/worker/sharing_proposal/finish.html.twig', []);
    }

    /**
     * @Route("/help/manager", name="help_manager")
     */
    public function help_manager()
    {
        return $this->render('help/manager.html.twig', []);
    }

    /**
     * @Route("/help/manager/general/menu", name="help_manager_general_menu")
     */
    public function help_manager_general_menu()
    {
        return $this->render('help/manager/general/menu.html.twig', []);
    }

    /**
     * @Route("/help/manager/general/presentation", name="help_manager_general_presentation")
     */
    public function help_manager_general_presentation()
    {
        return $this->render('help/manager/general/presentation.html.twig', []);
    }

    /**
     * @Route("/help/manager/catalogue/add", name="help_manager_catalogue_add")
     */
    public function help_manager_catalogue_add()
    {
        return $this->render('help/manager/catalogues/add.html.twig', []);
    }

    /**
     * @Route("/help/manager/catalogue/import", name="help_manager_catalogue_import")
     */
    public function help_manager_catalogue_import()
    {
        return $this->render('help/manager/catalogues/import.html.twig', []);
    }

    /**
     * @Route("/help/manager/catalogue/extra", name="help_manager_catalogue_extra")
     */
    public function help_manager_catalogue_extra()
    {
        return $this->render('help/manager/catalogues/extra.html.twig', []);
    }

    /**
     * @Route("/help/manager/catalogue/error", name="help_manager_catalogue_error")
     */
    public function help_manager_catalogue_error()
    {
        return $this->render('help/manager/catalogues/error.html.twig', []);
    }

    /**
     * @Route("/help/manager/catalogue/archive", name="help_manager_catalogue_archive")
     */
    public function help_manager_catalogue_archive()
    {
        return $this->render('help/manager/catalogues/archive.html.twig', []);
    }

    /**
     * @Route("/help/manager/item/import", name="help_manager_item_import")
     */
    public function help_manager_item_import()
    {
        return $this->render('help/manager/items/import.html.twig', []);
    }

    /**
     * @Route("/help/manager/item/add", name="help_manager_item_add")
     */
    public function help_manager_item_add()
    {
        return $this->render('help/manager/items/add.html.twig', []);
    }

    /**
     * @Route("/help/manager/item/edit", name="help_manager_item_edit")
     */
    public function help_manager_item_edit()
    {
        return $this->render('help/manager/items/edit.html.twig', []);
    }

    /**
     * @Route("/help/manager/user/management", name="help_manager_user_management")
     */
    public function help_manager_user_management()
    {
        return $this->render('help/manager/users/management.html.twig', []);
    }

    /**
     * @Route("/help/manager/distribution/cycle", name="help_manager_distribution_cycle")
     */
    public function help_manager_distribution_cycle()
    {
        return $this->render('help/manager/distributions/cycle.html.twig', []);
    }

    /**
     * @Route("/help/manager/distribution/new", name="help_manager_distribution_new")
     */
    public function help_manager_distribution_new()
    {
        return $this->render('help/manager/distributions/new.html.twig', []);
    }

    /**
     * @Route("/help/manager/distribution/catalogues", name="help_manager_distribution_catalogues")
     */
    public function help_manager_distribution_catalogues()
    {
        return $this->render('help/manager/distributions/catalogues.html.twig', []);
    }

    /**
     * @Route("/help/manager/distribution/steps", name="help_manager_distribution_steps")
     */
    public function help_manager_distribution_steps()
    {
        return $this->render('help/manager/distributions/steps.html.twig', []);
    }

    /**
     * @Route("/help/manager/distribution/sharing-proposal", name="help_manager_distribution_sharingproposal")
     */
    public function help_manager_distribution_shareingproposal()
    {
        return $this->render('help/manager/distributions/sharing_proposal.html.twig', []);
    }

    /**
     * @Route("/help/manager/distribution/orders", name="help_manager_distribution_orders")
     */
    public function help_manager_distribution_orders()
    {
        return $this->render('help/manager/distributions/orders.html.twig', []);
    }

    /**
     * @Route("/help/admin", name="help_admin")
     */
    public function help_admin()
    {
        return $this->render('help/admin.html.twig', []);
    }

    /**
     * @Route("/help/admin/conf", name="help_admin_conf")
     */
    public function help_admin_conf()
    {
        return $this->render('help/admin/conf.html.twig', []);
    }

    /**
     * @Route("/help/user", name="help_user")
     */
    public function help_user()
    {
        return $this->render('help/user.html.twig', []);
    }

    /**
     * @Route("/help/user/credentials/edit", name="help_user_credentials_edit")
     */
    public function help_user_credentials_edit()
    {
        return $this->render('help/user/credentials/edit.html.twig', []);
    }

    /**
     * @Route("/help/user/credentials/lost", name="help_user_credentials_lost")
     */
    public function help_user_credentials_lost()
    {
        return $this->render('help/user/credentials/lost.html.twig', []);
    }
}
