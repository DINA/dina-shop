<?php

namespace App\Controller\Worker;

use App\Entity\Distribution;
use App\Form\DistributionWorkerEditType;
use App\Repository\DistributionRepository;
use App\Service\Response\RedirectBackResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/worker/distribution")
 */
class DistributionController extends AbstractController
{
    use RedirectBackResponse;

    private $distributionRepository;
    private $translator;

    public function __construct(DistributionRepository $distributionRepository, TranslatorInterface $translator)
    {
        $this->distributionRepository = $distributionRepository;
        $this->translator = $translator;
    }


    /**
     * @Route("/{id}/edit", name="worker_distribution_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Distribution $distribution): Response
    {
        $form = $this->createForm(DistributionWorkerEditType::class, $distribution);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', $this->translator->trans(
                'common.flash_message.edit.success',
                []
            ));
            return $this->redirectToRoute('worker_catalogue_index');
        }

        return $this->render('worker/distribution/edit.html.twig', [
            'distribution' => $distribution,
            'form' => $form->createView(),
        ]);
    }

}
