<?php

namespace App\Controller\Worker;

use App\Controller\ItemController as ControllerItemController;
use App\Entity\CatalogueSearch;
use App\Entity\Item;
use App\Form\CatalogueSearchShopType;
use App\Form\ItemAltProducerType;
use App\Form\ItemRVCatSecFraisType;
use App\Form\ItemRVMercFrLgType;
use App\Form\ItemRVMercViandeType;
use App\Repository\CatalogueRepository;
use App\Repository\DistributionRepository;
use App\Repository\ItemRepository;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Omines\DataTablesBundle\DataTableFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/worker/item")
 */
class ItemController extends ControllerItemController/* AbstractController */
{
    private $distributionRepository;

    public function __construct(DistributionRepository $distributionRepository, TranslatorInterface $translator)
    {
        $this->distributionRepository = $distributionRepository;
        parent::__construct(
            ['index' => 'worker/item/index.html.twig',
            'edit' => 'worker/item/edit.html.twig',
            'show' => 'worker/item/show.html.twig',
            'actions' => 'worker/item/_actions.html.twig',
            ],
            'worker_item_edit_index',
            CatalogueSearchShopType::class,
            $translator
        );
    }

    /**
     * @Route("/", name="worker_item_index", methods={"GET","POST"})
     * @Route("/edit", name="worker_item_edit_index", methods={"GET","POST"})
     */
    public function index(Request $request, ItemRepository $itemRepository, CatalogueRepository $catalogueRepository, CatalogueConfigReader $catalogueConfigReader, DataTableFactory $dataTableFactory, PaginatorInterface $paginator): Response
    {
        return parent::index($request, $itemRepository, $catalogueRepository, $catalogueConfigReader, $dataTableFactory, $paginator);
    }

    /**
     * @Route("/{id}", name="worker_item_show", requirements={"id" = "\d+"}, methods={"GET"})
     */
    public function show(Item $item): Response
    {
        return parent::show($item);
    }

    /**
     * @Route("/{id}/edit", name="worker_item_edit", requirements={"id" = "\d+"}, methods={"GET","POST"})
     */
    public function edit(Request $request, Item $item): Response
    {
        return parent::edit($request, $item);
    }
    
    /**
     * extraDistributionStep
     *
     * @return mixed Distribution|Response
     */
    protected function extraDistributionStep()
    {
        $distribution = $this->distributionRepository->findDistributionInShareState();
        if (!$distribution) {
            return $this->render('worker/no_distribution.html.twig');
        }
        return $distribution;
    }
}
