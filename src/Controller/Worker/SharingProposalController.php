<?php

namespace App\Controller\Worker;

use App\Entity\Catalogue;
use App\Entity\CatalogueSearch;
use App\Entity\Distribution;
use App\Entity\Item;
use App\Entity\SharedProduct;
use App\Entity\SharingProposal;
use App\Entity\SuggestedItem;
use App\Form\CatalogueEditType;
use App\Form\CatalogueImportType;
use App\Form\CatalogueSearchShopType;
use App\Form\CatalogueSearchType;
use App\Form\ItemAltProducerType;
use App\Form\ItemRVCatSecFraisType;
use App\Form\ItemRVMercFrLgType;
use App\Form\ItemRVMercViandeType;
use App\Form\SharingProposalType;
use App\Repository\DistributionRepository;
use App\Repository\ItemRepository;
use App\Repository\SharingProposalRepository;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use App\Service\CatalogueImport\CatalogueImport;
use App\Service\ProductManager\ProductManager;
use App\Service\ProductManager\SuggestedItemManager;
use App\Service\Response\RedirectBackResponse;
use App\Service\Spreadsheet\CatalogueSpreadsheetReader;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/worker/sharingproposal")
 */
class SharingProposalController extends AbstractController
{
    use RedirectBackResponse;

    private $distributionRepository;
    private $suggestedItemManager;
    private $productManager;
    private $sharingProposalRepository;
    private $itemRepository;
    private $translator;

    public function __construct(
        DistributionRepository $distributionRepository,
        SuggestedItemManager $suggestedItemManager,
        ProductManager $productManager,
        SharingProposalRepository $sharingProposalRepository,
        ItemRepository $itemRepository,
        TranslatorInterface $translator
    ) {
        $this->distributionRepository = $distributionRepository;
        $this->suggestedItemManager = $suggestedItemManager;
        $this->productManager = $productManager;
        $this->sharingProposalRepository = $sharingProposalRepository;
        $this->itemRepository = $itemRepository;
        $this->translator = $translator;
    }


    /**
     * @Route("/", name="sharing_proposal_index", methods={"GET", "POST"})
     */
    public function index(Request $request): Response
    {
        $result = $this->list($request);
        if (is_array($result)) {
            return $this->render(
                'worker/sharingproposal/index.html.twig',
                $this->list($request)
            );
        } else { //In case there is a route redirection
            return $result;
        }
    }

    /**
     * managerIndex
     * This function allows to load the dashboard twig template when manager request it
     *
     * @param  mixed $request
     * @return Response
     */
    /**
     * @Route("/managerindex", name="sharing_proposal_manager_index", methods={"GET", "POST"})
     */
    public function managerIndex(Request $request): Response
    {
        $result = $this->list($request);
        if (is_array($result)) {
            return $this->render(
                'worker/sharingproposal/managerIndex.html.twig',
                $result
            );
        } else { //In case there is a route redirection
            return $result;
        }
    }

    /**
     * @Route("/{id}/add", name="sharing_proposal_item_add", requirements={"id" = "\d+"}, methods={"POST"})
     */
    public function add(Request $request, Item $item)
    {
        $distribution = $this->distributionRepository->findDistributionInShareProgressState();
        if (!$distribution) {
            return $this->render('worker/no_distribution.html.twig');
        }
        if ($distribution->getSharingProposal()) {
            $sharingProposal = $distribution->getSharingProposal();
        } else {
            $sharingProposal = new SharingProposal();
            $distribution->setSharingProposal($sharingProposal);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $product = $this->productManager->getSharedProductFromItem($sharingProposal, $item);
        if (!$product) {
            $product = $this->productManager->createSharedProductFromItem($item);
            $entityManager->persist($product);
        }

        $sharingProposal->addSharedProduct($product);
        $entityManager->persist($distribution);
        $entityManager->flush();

        $itemDesignation = $product->getItem()->getDesignation();
        if ($request->isXmlHttpRequest()) {
            $jsonData = array(
                'error' => 0,
                'nbItems' => $this->sharingProposalRepository->countItems($distribution->getId()),
                'flash' => array(
                    'alert' => 'success',
                    'message' => $this->translator->trans(
                        'worker.sharing_proposal.flash_add_success',
                        ['item' => $itemDesignation,]
                    )
                )
            );
            return new JsonResponse($jsonData);
        }

        $this->addFlash('success', $this->translator->trans(
            'worker.sharing_proposal.flash_add_success',
            ['item' => $itemDesignation]
        ));

        return new RedirectResponse($this->redirectBack($request));
    }


    /**
     * @Route("/{id}/remove", name="sharing_proposal_item_remove", requirements={"id" = "\d+"}, methods={"DELETE"})
     */
    public function remove(Request $request, SharedProduct $sharedProduct)
    {
        if ($request->isXmlHttpRequest()) {

            if (!$this->isCsrfTokenValid('remove_product_token', $request->headers->get('x-remove-product-token'))) {
                throw $this->createAccessDeniedException();
            }

            $distribution = $this->distributionRepository->findDistributionInShareProgressState();
            if ($distribution->getSharingProposal()) {
                $sharingProposal = $distribution->getSharingProposal();
            } else {
                throw new Exception('Distribution must exist to remove an item');
            }

            /* See comment in BasketController in remove() method */
            $sharingProposal->removeSharedProduct($sharedProduct);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sharingProposal);
            $entityManager->flush();

            $jsonData = array(
                'error' => 0,
                'url' => $this->generateUrl('sharing_proposal_index'),
                'flash' => array(
                    'alert' => 'success',
                    'message' => $this->translator->trans(
                        'shop.basket_order.flash_remove_success',
                        ['item' => $sharedProduct->getItem()->getDesignation(),]
                    )
                )
            );

            return new JsonResponse($jsonData);
        }
    }

    /**
     * @Route("/{id}/update_product", name="sharing_proposal_update_product", requirements={"id" = "\d+"}, methods={"POST"})
     */
    public function update_product(Request $request, SharedProduct $product)
    {
        if ($request->isXmlHttpRequest()) {
            $params = $request->request->all();
            if (isset($params['unit']) && !empty($params['unit'])) {
                $product->setProductUnit($params['unit']);
            }
            if (isset($params['quantity']) && !empty($params['quantity'])) {
                $product->setPieceNumber($params['quantity']);
            }
            if (isset($params['cheese']) && !empty($params['cheese'])) {
                $product->setIsCheese(filter_var($params['cheese'], FILTER_VALIDATE_BOOLEAN));
            }
            if (isset($params['comment']) && !empty($params['comment'])) {
                $product->setComment($params['comment']);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            $jsonData = array(
                'error' => 0,
            );

            return new JsonResponse($jsonData);
        }
    }

    /**
     * @Route("/{id}/add/suggesteditem", name="sharing_proposal_item_add_from_suggested_item", requirements={"id" = "\d+"}, methods={"POST"})
     */
    public function addFromSuggestedItem(Request $request, SuggestedItem $suggestedItem)
    {
        try{
            $distribution = $this->distributionRepository->findDistributionInShareProgressState();
            $item = $this->suggestedItemManager->searchForItemFromSuggestedItem($distribution, $suggestedItem);
        } catch (Exception $e){
            //Probablement un produit en doublon dans 2 catalogues
            return new Response();
        }

        if ($item) {
            return $this->render('worker/sharingproposal/_addSharingProposal.html.twig', [
                'row' => $item,
                'distribution' => $distribution,
            ]);
        } else {
            return new Response();
        }
    }

    /**
     * @Route("/add/{id}/all", name="sharing_proposal_all_item_add", requirements={"id" = "\d+"}, methods={"POST"})
     */
    public function addAll(Request $request, Catalogue $catalogue)
    {
        $distribution = $this->distributionRepository->findDistributionInShareProgressState();
        if (!$distribution) {
            return $this->render('worker/no_distribution.html.twig');
        }
        if ($distribution->getSharingProposal()) {
            $sharingProposal = $distribution->getSharingProposal();
        } else {
            $sharingProposal = new SharingProposal();
            $distribution->setSharingProposal($sharingProposal);
        }

        $entityManager = $this->getDoctrine()->getManager();

        $items = $this->itemRepository->findByCatalogue(null, $catalogue)->getQuery()->getResult();
        foreach ($items as $item) {
            $this->add($request, $item);
        }
        $entityManager->persist($distribution);
        $entityManager->flush();

        if ($request->isXmlHttpRequest()) {
            $jsonData = array(
                'error' => 0,
                'nbItems' => $this->sharingProposalRepository->countItems($distribution->getId()),
                'flash' => array(
                    'alert' => 'success',
                    'message' => $this->translator->trans(
                        'worker.sharing_proposal.flash_all_add_success'
                    )
                )
            );
            return new JsonResponse($jsonData);
        }

        $this->addFlash('success', $this->translator->trans(
            'worker.sharing_proposal.flash_all_add_success'
        ));

        return new RedirectResponse($this->redirectBack($request));
    }
    
    /**
     * @Route("/{id}/add/allsuggesteditem", name="sharing_proposal_item_add_all_from_suggested_item", requirements={"id" = "\d+"}, methods={"POST"})
     */
    public function addAllSuggestedItems(Request $request, Distribution $distribution)
    {
        $suggestedItems = $distribution->getSuggestedItems();
        foreach ($suggestedItems as $suggestedItem) {
            $item = $this->suggestedItemManager->searchForItemFromSuggestedItem($distribution, $suggestedItem);
            if ($item) {
                $this->add($request, $item);
            }
        }

        if ($request->isXmlHttpRequest()) {
            $jsonData = array(
                'error' => 0,
                'nbItems' => $this->sharingProposalRepository->countItems($distribution->getId()),
                'flash' => array(
                    'alert' => 'success',
                    'message' => $this->translator->trans(
                        'worker.sharing_proposal.flash_all_add_success'
                    )
                )
            );
            return new JsonResponse($jsonData);
        }

        return $this->index($request);
    }


    /**
     * displayListing
     * this function is called to render directly the items in the distribution "show" page
     *
     * @param  mixed $request
     * @return Response
     */
    public function displayListing(Request $request, Distribution $distribution): Response
    {
        return $this->render('worker/sharingproposal/_listing.html.twig', $this->list($request, $distribution));
    }


    /**
     * list
     *
     * @param  mixed $request
     * @return array|Response
     */
    private function list(Request $request, ?Distribution $distribution=null) //No return type, answer can be an array or a response
    {
        $distribution = $distribution?? $this->distributionRepository->findDistributionInProgress();
        if (!$distribution) {
            return $this->render('worker/no_distribution.html.twig');
        }

        if ($distribution->getSharingProposal()) {
            $sharingProposal = $distribution->getSharingProposal();
        } else {
            $sharingProposal = new SharingProposal();
            $sharingProposal->setDistribution($distribution);
        }

        $form = $this->createForm(SharingProposalType::class, $sharingProposal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $entityManager = $this->getDoctrine()->getManager();

            if ($form->get('submitProposition')->isClicked()) {
                foreach ($sharingProposal->getSharedProducts() as $product) {
                    $product->processSubType();
                    $entityManager->persist($product);
                    $entityManager->flush();
                }
                return $this->redirectToRoute('distribution_next', ['id' => $distribution->getId()]);
            }

            $entityManager->persist($sharingProposal);
            $entityManager->flush();
            $this->addFlash('success', $this->translator->trans(
                'worker.sharing_proposal.check',
            ));
            /* Workaround here: regenerate the form to not display the removed product */
            return $this->redirectToRoute('sharing_proposal_index');
        }

        return [
            'distribution' => $distribution,
            'form' => isset($form)? $form->createView() : null,
            'sharingProposal' => $sharingProposal?? null
        ];
    }
}
