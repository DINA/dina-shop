<?php

namespace App\Controller\Worker;

use App\Entity\Basket;
use App\Entity\Bookmark;
use App\Entity\Distribution;
use App\Entity\Item;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\SuggestedItem;
use App\Entity\User;
use App\Form\BasketType;
use App\Repository\BookmarkRepository;
use App\Repository\DistributionRepository;
use App\Repository\ItemRepository;
use App\Repository\SuggestedItemRepository;
use App\Service\ProductManager\BookmarkManager;
use App\Service\ProductManager\ProductManager;
use App\Service\ProductManager\SuggestedItemManager;
use App\Service\Response\RedirectBackResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\RedirectController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @Route("/worker/sugggesteditems")
 */
class SuggestedItemController extends AbstractController
{
    use RedirectBackResponse;

    private $distributionRepository;
    private $itemRepository;
    private $suggestedItemRepository;

    public function __construct(DistributionRepository $distributionRepository, ItemRepository $itemRepository, SuggestedItemRepository $suggestedItemRepository)
    {
        $this->distributionRepository = $distributionRepository;
        $this->itemRepository = $itemRepository;
        $this->suggestedItemRepository = $suggestedItemRepository;
    }


    /**
     * @Route("/", name="worker_suggested_items_index", methods={"GET", "POST"})
     */
    public function index(Request $request): Response
    {
        $distribution = $this->distributionRepository->findDistributionInProgress();
        if (!$distribution) {
            return $this->render('worker/no_distribution.html.twig');
        }
        return $this->render(
            'worker/suggested_item/index.html.twig',[
                'distribution' => $distribution,
                'suggestedItems' => isset($distribution)? $distribution->getSuggestedItems() : null
        ]);
    }

    /**
     * @Route("/managerIndex/{id}", name="worker_manager_suggested_items_index", requirements={"id" = "\d+"}, methods={"GET", "POST"})
     */
    public function managerIndex(Request $request, Distribution $distribution): Response
    {
        return $this->render(
            'worker/suggested_item/managerIndex.html.twig',
            [
                'distribution' => $distribution,
                'suggestedItems' => isset($distribution) ? $distribution->getSuggestedItems() : null
            ]
        );
    }
    

    /**
     * areSuggestedItems
     *
     * @return Response
     */
    public function areSuggestedItems(): Response
    {
        $distribution = $this->distributionRepository->findDistributionInProgress();

        return $this->render('worker/item/modal.html.twig', [
            'suggestedItems' => isset($distribution) ? $distribution->getSuggestedItems() : null
        ]);
    }
}
