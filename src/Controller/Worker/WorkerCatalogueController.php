<?php

namespace App\Controller\Worker;

use App\Entity\Catalogue;
use App\Entity\Distribution;
use App\Form\CatalogueEditType;
use App\Form\CatalogueType;
use App\Form\CatalogueWorkerEditType;
use App\Form\DistributionType;
use App\Form\DistributionWorkerEditType;
use App\Repository\CatalogueRepository;
use App\Repository\DistributionRepository;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use App\Service\Spreadsheet\Spreadsheet;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/worker/catalogue")
 */
class WorkerCatalogueController extends AbstractController
{
    private $catalogueConfigReader;
    private $translator;
    private $distributionRepository;

    public function __construct(CatalogueConfigReader $catalogueConfigReader, DistributionRepository $distributionRepository, TranslatorInterface $translator)
    {
        $this->catalogueConfigReader = $catalogueConfigReader;
        $this->translator = $translator;
        $this->distributionRepository = $distributionRepository;
    }
    /**
     * @Route("/", name="worker_catalogue_index", methods={"GET"})
     */
    public function index(Request $request, PaginatorInterface $paginator, CatalogueRepository $catalogueRepository): Response
    {
        $distribution = $this->distributionRepository->findDistributionInShareState();
        if (!$distribution) {
            return $this->render('worker/no_distribution.html.twig');
        }

        $allCatalogueTypes = $this->catalogueConfigReader->getCataloguesDefinition();
        /* we want only the mercuriale and the DINA producers */
        $expectedCatalogues = $this->catalogueConfigReader->filterBy($allCatalogueTypes ,[
                'key' => 'RelaisVert_Mercuriale_FruitsLegumes',
                'format' => 'Format_DINA_Producer',
            ]
        );
        /* Get names of the catalogues */
        $expectedCataloguesNames = $this->catalogueConfigReader->getCataloguesNames($expectedCatalogues);
       
        /* Retrieve the catalogues already present, but we are interested only by RelaisVert_Mercuriale_FruitsLegumes et DINA producers */
        $providedCatalogues = array_filter($distribution->getCatalogues()->toArray(), function($catalogue) use($allCatalogueTypes){
            return (($catalogue->getType() === 'RelaisVert_Mercuriale_FruitsLegumes') || 
            ($allCatalogueTypes[$catalogue->getType()]->getFormat() === 'Format_DINA_Producer'))? true : false;
        });

        /* It's easier to work with pure array than entities, so retrieve pure arrays */
        $providedCatalogues = $this->catalogueConfigReader->getJsonCatalogueDefinitionFomCatalogueEntities($providedCatalogues);
        $providedCataloguesNames = $this->catalogueConfigReader->getCataloguesNames($providedCatalogues);

        $missingCatalogues = array_diff_assoc($expectedCataloguesNames, $providedCataloguesNames);
       
        return $this->render('worker/catalogue/index.html.twig', [
            'distribution' => $distribution,
            'catalogues' => $distribution->getCatalogues()->toArray(),
            'cataloguesTypes' => $this->catalogueConfigReader->getCataloguesNames(),
            'missingCatalogues' => $missingCatalogues,
        ]);
    }

    /**
     * @Route("/new", name="worker_catalogue_new", methods={"GET","POST"})
     */
    public function new(Request $request/* , ValidatorInterface $validator */): Response
    {
        //TODO: refactoriser les controllers catalogues
        $catalogue = new Catalogue();
        $form = $this->createForm(CatalogueType::class, $catalogue, ['validation_groups' => ['Default', 'worker']]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            try {
                $distribution = $this->distributionRepository->findDistributionInShareState();
                if(!$distribution){
                    throw new Exception('Distribution is not in share state');
                }
                $catalogue->setFormat($this->catalogueConfigReader->findCatalogueFormat($catalogue->getType()));
                $entityManager->persist($catalogue);
                $distribution->addCatalogue($catalogue);
                $entityManager->flush();
                $this->addFlash('success', $this->translator->trans(
                    'manager.catalogue.new.flash_success',
                    []
                ));
            } catch (Exception $e) {
                if(Spreadsheet::MISSING_KEYS === $e->getCode()){
                    $message = 'manager.catalogue.new.reason.missing_key';
                } elseif(Spreadsheet::NO_REF_KEYS === $e->getCode()) {
                    $message = 'manager.catalogue.new.reason.no_keys';
                } else{
                    $message = 'manager.catalogue.new.reason.other';
                }
                $this->addFlash('danger', $this->translator->trans(
                    'manager.catalogue.new.flash_error',[
                        '%code%' => $e->getCode(),
                        'error' => $this->translator->trans($message, ['%columns%' => $e->getMessage()]),
                    ],
                ));
            }
            return $this->redirectToRoute('worker_catalogue_index');
        }

        return $this->render('worker/catalogue/new.html.twig', [
            'catalogue' => $catalogue,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="worker_catalogue_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Catalogue $catalogue): Response
    {
        $form = $this->createForm(CatalogueWorkerEditType::class, $catalogue);
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var EntityManagerInterface */
            $em = $this->getDoctrine()->getManager();
            try {
                $em->beginTransaction(); // suspend auto-commit
                $em->lock($catalogue, \Doctrine\DBAL\LockMode::PESSIMISTIC_READ);
                $em->flush();
                $em->getConnection()->commit();
                $this->addFlash('success', $this->translator->trans(
                    'common.flash_message.edit.success',
                    []
                ));
            }  catch(Exception $e){
                $this->addFlash('danger', $this->translator->trans(
                    'manager.catalogue.edit.flash_error',
                    ['error' => $e->getMessage(),],
                ));
            }
            return $this->redirectToRoute('worker_catalogue_index');
        }
    
        return $this->render('worker/catalogue/edit.html.twig', [
            'catalogue' => $catalogue,
            'form' => $form->createView(),
        ]);
    }


}
