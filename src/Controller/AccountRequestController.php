<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\AccountRequestType;
use App\Form\UserRequestType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AccountRequestController extends AbstractController
{
    /**
     * @Route("/account_request", name="account_request")
     */
    public function index(Request$request, UserPasswordEncoderInterface $passwordEncoder, MailerInterface $mailer)
    {
        $user = new User();
        $form = $this->createForm(AccountRequestType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success','Your request have been logged, you will receive an email soon');

            $email = (new TemplatedEmail())
            ->from($user->getEmail())
            ->to('dina@mail.com')
            ->subject('A new user has request an account')
            ->htmlTemplate('emails/account_request.html.twig')
            ->context([
                'user' => $user,
                'id' => $user->getId(),
            ]);

            $mailer->send($email);

            return $this->redirectToRoute('home');
           
        }

        return $this->render('user/account_request.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}