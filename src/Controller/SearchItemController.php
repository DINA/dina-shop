<?php

namespace App\Controller;

use App\Entity\Catalogue;
use App\Entity\CatalogueSearch;
use App\Repository\ItemRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchItemController extends AbstractController
{
    private $itemRepository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    /**
     * @Route("/search/item-names/{id}", name="autocomplete_name_ajax", requirements={"id" = "\d+"}, methods={"GET"})
     */
    public function autocomplete(Request $request, Catalogue $catalogue)
    {
        if ($request->isXmlHttpRequest()) {
            $params = $request->query->all();


            $searchCriterias = new CatalogueSearch();
            $searchCriterias->setCatalogue($catalogue);
            if (isset($params['searchText'])) {
                $searchCriterias->setSearchText($params['searchText']);
            }
            if (isset($params['origins']) && !empty($params['origins'])) {
                $origins = explode(',', $params['origins']);
                $searchCriterias->setOrigins($origins);
            }
            if (isset($params['families']) && !empty($params['families'])) {
                $families = explode(',', $params['families']);
                $searchCriterias->setFamilies($families);
            }
            if (isset($params['brands']) && !empty($params['brands'])) {
                $brands = explode(',', $params['brands']);
                $searchCriterias->setBrands($brands);
            }

            $query = $this->itemRepository->findByCriterias(null, $searchCriterias)->getQuery();
            $items = $query->getArrayResult();
            $names = array_map(function ($item) {
                return $item['designation'];
            }, $items);

            return new JsonResponse($names);
        }
    }
}
