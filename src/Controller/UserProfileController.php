<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserEditType;
use App\Form\UserProfileEditType;
use App\Service\Mail\UserNotification;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/userprofile")
 * 
 */
class UserProfileController extends AbstractController
{
    private $passwordEncoder;
    private $userNotification;
    private $translator;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, UserNotification $userNotification, TranslatorInterface $translator)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->userNotification = $userNotification;
        $this->translator = $translator;
    }

    /**
     * @Route("/{id}", name="user_show_profile", methods={"GET"})
     */
    public function show(User $user): Response
    {
        $this->denyAccessUnlessGranted('SHOW_PROFILE', $user);

        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit_profile", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $this->denyAccessUnlessGranted('EDIT_PROFILE', $user);
        
        $form = $this->createForm(UserProfileEditType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($user->getPlainPassword()) {
                $password = $this->passwordEncoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($password);
                $this->userNotification->resetPasswordNotification($user);
            } else{
                $this->addFlash('danger', $this->translator->trans(
                    'user.flash_error',
                    []
                ));
                return $this->render('user/edit.html.twig', [
                    'user' => $user,
                    'form' => $form->createView(),
                ]);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', $this->translator->trans(
                'common.flash_message.edit.success',
                []
            ));

            return $this->redirectToRoute('user_show_profile', ['id' => $user->getId()]);
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

}