<?php

namespace App\Controller\Admin;

use App\Entity\Producer;
use App\Form\ProducerType;
use App\Repository\ProducerRepository;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/producer")
 */
class ProducerController extends AbstractController
{
    private $catalogueConfigReader;
    private $translator;
    
    public function __construct(CatalogueConfigReader $catalogueConfigReader, TranslatorInterface $translator )
    {
        $this->catalogueConfigReader = $catalogueConfigReader;
        $this->translator = $translator;
    }

    /**
     * @Route("/", name="producer_index", methods={"GET"})
     */
    public function index(ProducerRepository $producerRepository): Response
    {
        return $this->render('admin/producer/index.html.twig', [
            'producers' => $producerRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="producer_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $producer = new Producer($this->catalogueConfigReader);
        $form = $this->createForm(ProducerType::class, $producer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($producer);
            $entityManager->flush();
            $this->addFlash('success', $this->translator->trans(
                'admin.producer.new.flash_success',[]));
            return $this->redirectToRoute('producer_index');
        }

        return $this->render('admin/producer/new.html.twig', [
            'producer' => $producer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="producer_show", methods={"GET"})
     */
    public function show(Producer $producer): Response
    {
        return $this->render('admin/producer/show.html.twig', [
            'producer' => $producer,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="producer_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Producer $producer): Response
    {
        $form = $this->createForm(ProducerType::class, $producer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', $this->translator->trans(
                'common.flash_message.edit.success',[]));
            return $this->redirectToRoute('producer_index');
        }

        return $this->render('admin/producer/edit.html.twig', [
            'producer' => $producer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="producer_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Producer $producer): Response
    {
        if ($this->isCsrfTokenValid('delete'.$producer->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            if($producer->getCode() !== 'RV'){
                $entityManager->remove($producer);
                $entityManager->flush();
                $this->addFlash('success', $this->translator->trans(
                    'admin.producer.delete.flash_success',
                    []
                ));
            } else{
                $this->addFlash('danger', $this->translator->trans(
                    'admin.producer.delete.flash_RV_error',
                    []
                ));
            }
        }

        return $this->redirectToRoute('producer_index');
    }
}
