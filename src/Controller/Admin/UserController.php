<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\UserEditType;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Service\Mail\UserNotification;
use App\Service\Response\RedirectBackResponse;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/user")
 */
class UserController extends AbstractController
{
    private $passwordEncoder;
    private $userNotification;
    private $translator;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, UserNotification $userNotification, TranslatorInterface $translator)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->userNotification = $userNotification;
        $this->translator = $translator;
    }

    /**
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('admin/user/index.html.twig', [
            'users' => $userRepository->findBy(
                array(),
                array(
                    'status' => 'ASC',
                    'username' => 'ASC',
                    'id' => 'ASC'
                )
            ),
        ]);
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $this->passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', $this->translator->trans(
                'admin.user.new.flash_success',
                []
            ));
            $this->userNotification->newUserNotification($user);

            return $this->redirectToRoute('user_index');
        }

        return $this->render('admin/user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        return $this->render('admin/user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $this->denyAccessUnlessGranted('EDIT_ADMIN_PROFILE', $user);
        $form = $this->createForm(UserEditType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($user->getPlainPassword()) {
                $password = $this->passwordEncoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($password);
                $this->userNotification->resetPasswordNotification($user);
            } else {
                $this->userNotification->editUserNotification($user);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', $this->translator->trans(
                'common.flash_message.edit.success',
                []
            ));

            return $this->redirectToRoute('user_index');
        }

        return $this->render('admin/user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            if (!$user->hasRole(['ROLE_MANAGER', 'ROLE_ADMIN', 'ROLE_COORDINATOR', 'ROLE_WORKER'])) {
                $entityManager = $this->getDoctrine()->getManager();
                $this->userNotification->deleteUserNotification($user);
                /* Do not delete the user to not alterate the distribution history  */
                $bookmarks = $user->getBookmarks();
                $bookmarks->map(function ($bookmark) use ($user) {
                    $user->removeBookmark($bookmark);
                });
                /* Set deleted status */
                $user->setStatus(User::STATUS_DELETED);
                $user->eraseCredentials();
                $entityManager->persist($user);
                $entityManager->flush();
                $this->addFlash('success', $this->translator->trans(
                    'admin.user.delete.flash_success',
                    []
                ));
            } else {
                $this->addFlash('danger', $this->translator->trans(
                    'admin.user.delete.flash_error',
                    []
                ));
            }
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * @Route("/{id}/approve", name="user_approve", methods={"POST"})
     */
    public function approve(Request $request, User $user): Response
    {
        $user->setStatus(User::STATUS_ACTIVE);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        $this->userNotification->approveUserNotification($user);

        $this->addFlash('success', 'User  approved');
        return $this->redirectToRoute('user_index');
    }
}
