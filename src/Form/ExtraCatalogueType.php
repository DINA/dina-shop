<?php

namespace App\Form;

use App\Entity\Catalogue;
use App\Entity\Distribution;
use App\Entity\Producer;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExtraCatalogueType extends AbstractType
{
    private $catalogueConfigReader; 

    public function __construct(CatalogueConfigReader $catalogueConfigReader)
    {
        $this->catalogueConfigReader = $catalogueConfigReader;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('name')
            ->remove('comment')
            ->remove('producerId')
            ->remove('spreadsheetFile')
            ->remove('dropboxFile')
            ->remove('type')
            ->remove('autoArchive')
            ->add('type',  ChoiceType::class, [
                'translation_domain' => 'forms',
                'help' => 'extra_catalogue.help',
                'choices' => $this->getChoices($options['distribution'])
            ])
        ;
    }

    public function getParent()
    {
        return CatalogueType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Catalogue::class,
            'distribution' => null,
        ]);
    }

    private function getChoices(Distribution $distribution)
    {
        $choices = $this->catalogueConfigReader->getCataloguesNames();
        foreach ($distribution->getCatalogues() as $catalogue){
            $output[$choices[$catalogue->getType()]] = $catalogue->getType();
        }
        return $output;
    }

}
