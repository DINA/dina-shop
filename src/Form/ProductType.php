<?php

namespace App\Form;

use App\Entity\Item;
use App\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class ProductType extends AbstractType
{
    private $router;
    private $csrfTokenManager;

    public function __construct(UrlGeneratorInterface $router, CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->router = $router;
        $this->csrfTokenManager = $csrfTokenManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('productUnit', ChoiceType::class, [
                'label' => false,
                'placeholder' => 'product.placeholder',
                'translation_domain' => 'forms',
            ])
            /*
            Remove button comment: 
                Remove can't be a submitType otherwise the "Enter key" (at keyboard, after have entered a quantity) will validate first the delete button instead of other submitbuttons. 
                So it must be a ButtonType but in this case we need to have Javascript like explained here: https://symfony.com/doc/current/form/form_collections.html#allowing-tags-to-be-removed
                See also here: https://symfonycasts.com/screencast/collections/collection-allow-delete and here:  http://geekscience.info/2016/11/formulaires-sf-partie-3.html   
            */    
            ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA,   function (FormEvent $event) {
            $product = $event->getData();
            if (null === $product) {
                return;
            }

            $token = $this->csrfTokenManager->getToken('remove_product_token')->getValue();


            /* Old obsolete rule. Keep it in case of....
            $preOrderDisabled = (($product->getItem()->getCatalogueId()->getType() === 'RelaisVert_Catalogue_Frais')
                            || ($product->getItem()->getCatalogueId()->getType() === 'RelaisVert_Mercuriale_Viande')) ? false : true; */
            $preOrderDisabled = true;

            $event->getForm()->add('packaging', TextType::class, [
                'label'    => false,
                'data' => $product->getItem()->getPackaging(),
                'disabled' => true,
                'attr' => [
                    'class' => 'form-packaging',
                ],
            ]);

            $event->getForm()->add('productUnit', ChoiceType::class, [
                'choices' => $this->getChoices($product),
                'disabled' => ($product->getOrderOrigin() == Product::ORDERORIGIN_SHARE) ? true : false,
                'label' => false,
                'placeholder' => 'product.placeholder',
                'translation_domain' => 'forms',
                'attr' => [
                    'class' => 'select-unit',
                    'data-product-id' => $product->getId(),
                    'data-update-url' =>  $this->router->generate('basket_item_update_product', ['id' => $product->getId()]),
                ],
            ]);

            if ($product->getProductUnit() === Product::PRODUCTUNIT_KG) {
                $event->getForm()->add('quantityDec', NumberType::class, [
                    'label' => false,
                    'scale' => 2,
                    'html5' => true,
                    'attr' => [
                        'class' => 'quantity-input',
                        'data-product-id' => $product->getId(),
                        'data-update-url' =>  $this->router->generate('basket_item_update_product', ['id' => $product->getId()]),
                        'step' => 'any',
                        'min' => '0',
                        'max' => '99',
                    ],
                ]);
            } else {

                $event->getForm()->add('quantityDec', IntegerType::class, [
                    'required' => false,
                    'label' => false,
                    'attr' => [
                        'class' => 'quantity-input',
                        'data-product-id' => $product->getId(),
                        'data-update-url' =>  $this->router->generate('basket_item_update_product', ['id' => $product->getId()]),
                        'step' => 'any',
                        'min' => '0',
                        'max' => '99',
                    ],
                ]);
            }

            $event->getForm()->add('preOrder', CheckboxType::class, [
                'label'    => false,
                'required' => false,
                'disabled' => $preOrderDisabled,
            ]);
            $event->getForm()->add('Remove', ButtonType::class, [
                'label' => 'Remove',
                'attr' => [
                    'class' => 'btn btn-danger form-delete-button',
                    'type' => "button", //To prevent submission in form
                    'role' => "button",
                    'data-product-id' => $product->getId(),
                    'data-remove-url' =>  $this->router->generate('basket_item_remove', ['id' => $product->getId()]),
                    'data-delete-token' => $token
                ],
                'translation_domain' => 'forms',
                'label' => false,
            ]);
        });

        $builder->addEventListener(FormEvents::POST_SUBMIT,   function (FormEvent $event) {
            /** @var Product product */
            $product = $event->getData();
            if (null === $product) {
                return;
            }

            $form = $event->getForm(); //->getParent();
            //$form->getParent()->add('test', EmailType::class);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
            'method' => 'get',
            //'allow_extra_fields' => true,
            'attr' => ['id' => 'task-form']
        ]);
    }

    private function getChoices(Product $product)
    {
        if ($product->getOrderOrigin() == Product::ORDERORIGIN_INDIVIDUAL) {
            $minPacking = $product->getItem()->getMinPacking();
            $choices = Product::PRODUCTUNIT;
            $output = [];
            foreach ($choices as $k => $v) {
                if (($k == Product::PRODUCTUNIT_PIECE) && (!$minPacking)) {
                    /* If minpacking null, so it is not possible to order pieces */
                    continue;
                } elseif ($k == Product::PRODUCTUNIT_KG) {
                    /* not possible to have KG in individual order */
                    continue;
                } else {
                    $output[$v] = $k;
                }
            }
            return $output;
        } else {
            //Keep the units set sharing proposal
            $output[Product::PRODUCTUNIT[$product->getProductUnit()]] = $product->getProductUnit();
            return $output;
        }
    }
}
