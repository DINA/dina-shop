<?php

namespace App\Form\Type;

use App\Entity\DropboxFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DropboxType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dropboxFileName', TextType::class, [
                'translation_domain' => 'forms',
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'catalogue.placeholder_dropbox',
                    'readonly' => true,
                ],
                'help' => 'catalogue.help_dropbox',
                ])
            ->add('dropboxFileLink', HiddenType::class, [
                'required' => false,
                ])
            ;
    }

    public function getBlockPrefix()
    {
        return 'dropboxFile';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DropboxFile::class,
        ]);
    }
}