<?php

namespace App\Form;

use App\Entity\Catalogue;
use App\Entity\Producer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class CatalogueEditType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('status')
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA,    function (FormEvent $event) { 
               
                $catalogue = $event->getData();
                if (null === $catalogue) {
                    return; 
                }

                //Remove status box once catalogue is archived (cannot put online again)
                if (Catalogue::STATUS_ONLINE == $catalogue->getStatus())  {
                    $event->getForm() ->add('status',  ChoiceType::class, [
                        'choices' => $this->getChoices(),
                        'label_format' => 'catalogue.%name%',
                        'translation_domain' => 'forms',
                    ]);
                } else {  
                    /* To prevent error when form is displayed */
                    $event->getForm()->add('status', HiddenType::class);
                }

                /*  Disable Autoarchive option choice for extra catalogues  */
                $formArchiveOptions = [
                    'label_attr' => [
                        'class' => 'switch-custom',
                    ],
                    'label_format' => 'catalogue.%name%',
                    'translation_domain' => 'forms',
                    'required' => false,
                    'help' => 'catalogue.help_archive',
                ];
                $formArchiveOptions['disabled'] = ($catalogue->getExtra())? true : false;

                $event->getForm()->add('autoArchive',  CheckboxType::class, $formArchiveOptions);
            }
        );
    }

    public function getParent()
    {
        return CatalogueType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Catalogue::class,
        ]);
    }

    private function getChoices()
    {
        $choices = Catalogue::CAT_STATUS;
        $output = [];
        foreach ($choices as $k => $v) {
            $translated_text = ($v === 'online')? $this->translator->trans('manager.catalogue.status.online', []) : $this->translator->trans('manager.catalogue.status.archived', []);
            $output[$translated_text] = $k;
        }
        return $output;
    }

}
