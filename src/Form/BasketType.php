<?php

namespace App\Form;

use App\Entity\Basket;
use App\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BasketType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('product', CollectionType::class, [
            'entry_type' => ProductType::class,
            'entry_options' => ['label' => false],
            'label' => false,
            'by_reference' => false,
            'allow_delete' => true,
            ])
            ->add('Order', SubmitType::class, [
                'label' => 'Order',
                'attr' => ['class' => 'btn btn-primary mx-1'],
                'translation_domain' => 'forms',
                'label' => 'product.submit',
            ])
            ->add('Refresh', SubmitType::class, [
                'label' => 'Refresh Price',
                'attr' => ['class' => 'btn btn-info mx-1'],
                'translation_domain' => 'forms',
                'label' => 'product.price',
            ])
        ;                   
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Basket::class,
            //"allow_extra_fields" => true,
        ]);
    }

    private function getChoices()
    {
        $choices = Product::PRODUCTUNIT;
        $output = [];
        foreach ($choices as $k => $v) {
            $output[$v] = $k;
        }
        return $output;
    }

    public function getBlockPrefix() //Permet d'avoir des paramètres propres dans l'URL sans trucs du genre ?[maxprice = xxxx]
    {
        return '';
    }
}
