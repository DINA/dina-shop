<?php

namespace App\Form;

use App\Entity\Catalogue;
use App\Entity\Producer;
use App\Entity\Product;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductManagerEditType extends AbstractType
{
    private $catalogueConfigReader; 

    public function __construct(CatalogueConfigReader $catalogueConfigReader)
    {
        $this->catalogueConfigReader = $catalogueConfigReader;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('Remove',SubmitType::class, [
                'label' => 'Remove',
                'validation_groups' => false, /* To remove all validation */
                'attr' => ['class' => 'btn btn-secondary'],
                'translation_domain' => 'forms',
                'label' => 'product.remove',
            ]);
        ;

    }

    public function getParent()
    {
        return ProductType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }

    

}
