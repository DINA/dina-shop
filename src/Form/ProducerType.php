<?php

namespace App\Form;

use App\Entity\Producer;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProducerType extends AbstractType
{
    private $catalogueConfigReader;

    public function __construct(CatalogueConfigReader $catalogueConfigReader)
    {
        $this->catalogueConfigReader = $catalogueConfigReader;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'label_format' => 'producer.%name%',
                'translation_domain' => 'forms',
            ])
            ->add('code', ChoiceType::class, [
                'choices' => $this->getChoices(),
                'label_format' => 'producer.%name%',
                'translation_domain' => 'forms',
                'help' => 'producer.help_code',
                
            ])   
            ->add('phone', TextType::class,[
                'label_format' => 'producer.%name%',
                'translation_domain' => 'forms',
                'required' => false,
                'help' => 'producer.help_phone',
            ])
            ->add('email', EmailType::class,[
                'label_format' => 'producer.%name%',
                'translation_domain' => 'forms',
                'required' => false,
                'help' => 'producer.help_mail',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Producer::class,
        ]);
    }

    private function getChoices()
    {
        $output = [];
        $choices = $this->catalogueConfigReader->getCataloguesCodes();
        foreach ($choices as $k => $v) {
            $output[$v] = $v;
        }
        return $output;
    }
}
