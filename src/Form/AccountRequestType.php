<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class AccountRequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label_format' => 'user.%name%',
                'translation_domain' => 'forms',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter your email',
                    ]),
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'translation_domain' => 'forms',
                'first_options' => [
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Please enter a password',
                        ]),
                        new Length([
                            'min' => 6,
                            'minMessage' => 'form.profile.min_password',
                            'max' => 255,
                        ]),
                    ],
                    'label' => 'user.password',
                    'attr' => array('placeholder' => 'user.placeholder_password')
                ],
                'second_options' => [
                    'label' => 'user.repeat_password',
                    'attr' => array('placeholder' => 'user.placeholder_repeat_password')
                ],
                'invalid_message' => 'form.profile.check_repeated_password',
            ])
            ->add('username', TextType::class,[
                'label_format' => 'user.%name%',
                'translation_domain' => 'forms',
                'help' => 'user.help_username',
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
