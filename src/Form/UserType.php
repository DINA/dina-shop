<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder  
            ->add('status',  ChoiceType::class, [
                        'choices' => $this->getChoices(),
                        'translation_domain' => 'forms',
                        'label' => 'user.status.Status',
                        'help' => 'user.status.help',
                    ])
            ->add('username', TextType::class, [
                'label_format' => 'user.%name%',
                'translation_domain' => 'forms',
                'help' => 'user.help_username',
            ])
        ;
    }

    public function getParent()
    {
        return AccountRequestType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

    private function getChoices()
    {
        $choices = User::STATUSES;
        $output = [];
        foreach ($choices as $k => $v) {
            if($k == User::STATUS_WAITING_APPROVAL){
                continue;
            }
            $output[$v] = $k;
        }
        return $output;
    }
}
