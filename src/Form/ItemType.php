<?php

namespace App\Form;

use App\Entity\Catalogue;
use App\Entity\Item;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemType extends AbstractType
{
    private $catalogueConfigReader;

    public function __construct(CatalogueConfigReader $catalogueConfigReader)
    {
        $this->catalogueConfigReader = $catalogueConfigReader;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //Form is depending on catalogue definition
        $mapping = $this->catalogueConfigReader->getCatalogueKeysMapping($options['catalogueType']);

        foreach ($mapping as $key => $value) {
            if (isset($value)) {
                switch($key){
                    case 'reference':
                    case 'designation': 
                    case 'vat':
                        $class = TextType::class;
                        $required = true;
                        break;
                    case 'unitaryPrice':
                        $class = MoneyType::class;
                        $required = true;
                        break;
                    default:
                        $class = TextType::class;
                        $required = false;
                        break;
                };
                $builder->add($key, $class, [
                    'translation_domain' => 'forms',
                    'label_format' => 'item.%name%',
                    'required' => $required,
                    ]);
            }
        }
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Item::class,
            'catalogueType' => null,
        ]);
    }
}
