<?php

namespace App\Form;

use App\Entity\Catalogue;
use App\Entity\DropboxFile;
use App\Entity\Producer;
use App\Form\Type\CatalogueFileType;
use App\Form\Type\DropboxType;
use App\Repository\ProducerRepository;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CatalogueType extends AbstractType
{
    private $catalogueConfigReader;
    private $producerRepository;

    public function __construct(CatalogueConfigReader $catalogueConfigReader, ProducerRepository $producerRepository)
    {
        $this->catalogueConfigReader = $catalogueConfigReader;
        $this->producerRepository = $producerRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'label_format' => 'catalogue.%name%',
                'translation_domain' => 'forms',
                'attr' => ['placeholder' => 'catalogue.placeholder_name'],
            ])
            ->add('status', HiddenType::class)
            ->add('type',  ChoiceType::class, [
                'choices' => $this->getChoices($options),
                'choice_attr' => function ($choice, $key, $value) {
                    return $this->getChoicesAttr($choice, $key, $value);
                },
                'label_format' => 'catalogue.%name%',
                'translation_domain' => 'forms',
            ])
            ->add('autoArchive',  CheckboxType::class, [
                'label_attr' => ['class' => 'switch-custom'],
                'label_format' => 'catalogue.%name%',
                'translation_domain' => 'forms',
                'required' => false,
                'help' => 'catalogue.help_archive',
            ])
            ->add('spreadsheetFile', FileType::class, [
                'label_format' => 'catalogue.%name%',
                'translation_domain' => 'forms',
                //'label' => 'Goods catalogue',
                'required' => false,
                'label_attr'=> ['data-browse' => 'Parcourir'],
                'attr' => ['placeholder' => 'catalogue.placeholder_upload',],
                'help' => 'catalogue.help_spreadsheet',
            ])
            ->add('comment', TextType::class, [
                'label_format' => 'catalogue.%name%',
                'translation_domain' => 'forms',
                'attr' => ['placeholder' => 'catalogue.placeholder_comment'],
                'required' => false
            ])
            ->add('dropboxFile',DropboxType::class, [
                'label' => false,
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Catalogue::class,
        ]);
    }

    //TODO: à faire plus tard: probablement mal construit, la traduction cherche à traduire chaque champ proposé dans ce choix (voir translation dans la toolbar symfony)
    private function getChoices($options)
    {
        $output = [];
        /* If the validation group is worker, this is as we had a CatalogueWorkerType, the choices are limited */
        if(!empty($options['validation_groups']) && in_array('worker',$options['validation_groups'])){
            $allCatalogueTypes = $this->getCataloguesFromAvailableProducers(false);
            /* We want only the Mercuriale and the Dina Producers */
            $choices = $this->catalogueConfigReader->filterBy($allCatalogueTypes, [
                'key' => 'RelaisVert_Mercuriale_FruitsLegumes',
                'format' => 'Format_DINA_Producer',
            ]);
            foreach ($choices as $k => $v) {
                $output[$v->getName()] = $k;
            }
        } else{
            $choices = $this->getCataloguesFromAvailableProducers();
            foreach ($choices as $k => $v) {
                $output[$v] = $k;
            }
        }
        return $output;
    }

    private function getChoicesAttr($choice, $key, $value): array
    {
        $format = $this->catalogueConfigReader->findCatalogueFormat($value);
        return ['data-format' => $format];
    }

    /**
     * getCataloguesFromAvailableProducers
     *
     * @param  bool $onlyNames
     *                  true: only names
     *                  false: full catalogue definitions
     * @return array
     */
    private function getCataloguesFromAvailableProducers(?bool $onlyNames=true): array
    {
        $producers = $this->producerRepository->findAll();
        $catalogues = $this->catalogueConfigReader->getCataloguesDefinition();
        $choices = [];
        /* Only catalogues of current producers are displayed
                We don't want to diplay a catalogue still present in cataloguedef but where the producer is no more in database
             */
        foreach ($catalogues as $key => $catalogue) {
            foreach ($producers as $producer) {
                if ($catalogue->getCode() === $producer->getCode()) {
                    if($onlyNames){
                        $choices[$key] = $catalogue->getName();
                    } else{
                        $choices[$key] = $catalogue;
                    }
                }
            }
        }
        return $choices;
    }

}
