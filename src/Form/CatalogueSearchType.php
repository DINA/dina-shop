<?php

namespace App\Form;

use App\Entity\Catalogue;
use App\Entity\CatalogueSearch;
use App\Entity\Item;
use App\Repository\CatalogueRepository;
use App\Repository\ItemRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CatalogueSearchType extends AbstractType
{


    private $repository;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('catalogue', EntityType::class, [
                'class' => Catalogue::class,
                'choice_label' => 'name',
                'translation_domain' => 'forms',
                'label_format' => 'catalogue_search.%name%',
                'help' => 'catalogue_search.help_catalogue',
                //Workaround un peu crado avec désactivation du querybuilder pour pourvoir faire des recherches dans des catalogues archivés
                //'group_by' => function ($choice, $key, $value) {
                //    if ($choice->getStatus() === Catalogue::STATUS_ONLINE ) {
                //        return 'Online';
                //    }
                //    return 'Archived';
                //},
                'query_builder' => function (CatalogueRepository $repository ) {
                    return  $repository->createQueryBuilder('c')
                        ->where('c.status = :status')
                        ->setParameter('status', Catalogue::getStatusValue('online'));
                }
            ])
            ;

        // Add some search fields depending on the catalogue
        $builder->addEventListener(FormEvents::PRE_SUBMIT,   function (FormEvent $event) {
            $catalogSearch = $event->getData();
            if (null === $catalogSearch) {
                return;
            }
            if ($catalogSearch['catalogue']) {
                $event->getForm()->add('origins', EntityType::class, [
                    'class'   => Item::class,
                    'translation_domain' => 'forms',
                    'label_format' => 'catalogue_search.%name%',
                    'help' => 'catalogue_search.help_origins',
                    'choice_label' => 'origin',
                    'choice_value' => function (Item $item) {
                        return $item->getOrigin();
                    },
                    'placeholder' => 'Choose an origin',
                    'required' => false,
                    'multiple' => true,
                    //query_builer mandatory to perform a GROUP BY on origin
                    'query_builder' => function (ItemRepository $repository) use ($catalogSearch) {
                        return  $repository->listOrigins($catalogSearch);
                    }
                ]);

                $event->getForm()->add('families', EntityType::class, [
                    'class'   => Item::class,
                    'translation_domain' => 'forms',
                    'label_format' => 'catalogue_search.%name%',
                    'help' => 'catalogue_search.help_families',
                    'choice_label' => 'family',
                    'choice_value' => function (Item $item) {
                        return $item->getFamily();
                    },
                    'placeholder' => 'Choose a family',
                    'required' => false,
                    'multiple' => true,
                    //query_builer mandatory to perform a GROUP BY on origin
                    'query_builder' => function (ItemRepository $repository) use ($catalogSearch) {
                        return $repository->listFamilies($catalogSearch);
                    }
                ]);

                $event->getForm()->add('brands', EntityType::class, [
                    'class'   => Item::class,
                    'translation_domain' => 'forms',
                    'label_format' => 'catalogue_search.%name%',
                    'help' => 'catalogue_search.help_brands',
                    'choice_label' => 'brand',
                    'choice_value' => function (Item $item) {
                        return $item->getBrand();
                    },
                    'placeholder' => 'Choose a brand',
                    'required' => false,
                    'multiple' => true,
                    //query_builer mandatory to perform a GROUP BY on origin
                    'query_builder' => function (ItemRepository $repository) use ($catalogSearch) {
                        return $repository->listBrands($catalogSearch);
                    }
                ]);

                $event->getForm()->add('searchText', TextType::class, [
                    'translation_domain' => 'forms',
                    'attr' => array(
                        'placeholder' => 'catalogue_search.placeholder_search_text',
                    ),
                    'label_format' => 'catalogue_search.%name%',
                    'required' => false,
                    'label' => false,
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CatalogueSearch::class, 
            'method' => 'get', //pour avoir le formulaire en GET, permet de partage ses recherches en filant l'URL
            'csrf_protection' => false, // non pertinent ici
            "allow_extra_fields" => true,
            'distribution' => null,
        ]);
    }

    public function getBlockPrefix() //Permet d'avoir des paramètres propres dans l'URL sans trucs du genre ?[maxprice = xxxx]
    {
        return '';
    }
}
