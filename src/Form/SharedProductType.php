<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\SharedProduct;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class SharedProductType extends AbstractType
{
    private $router;
    private $csrfTokenManager;
    private $catalogueConfigReader;

    public function __construct(UrlGeneratorInterface $router, CsrfTokenManagerInterface $csrfTokenManager, CatalogueConfigReader $catalogueConfigReader)
    {
        $this->router = $router;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->catalogueConfigReader = $catalogueConfigReader;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('subType', ChoiceType::class, [
                'choices' => SharedProduct::SUBTYPE,
                'label' => false,
            ])
            ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA,   function (FormEvent $event) {

            /** @var \App\Entity\SharedProduct $product */
            $product = $event->getData();
            if (null === $product) {
                return;
            }

            $token = $this->csrfTokenManager->getToken('remove_product_token')->getValue();

            /* Check if the producer is allowed to sell cheese for sharing proposal */
            $otherConfig = $this->catalogueConfigReader->findCatalogueOthers($product->getItem()->getCatalogueId()->getType());
            $cheeseDisabled = (isset($otherConfig['sellCheeseAllowed']) && ($otherConfig['sellCheeseAllowed'] === true))? false : true;

            $event->getForm()->add('isCheese', CheckboxType::class, [
                'label'    => false,
                'required' => false,
                'disabled' => $cheeseDisabled,
                'attr' => [
                    'class' => 'select-cheese',
                    'data-update-url' =>  $this->router->generate('sharing_proposal_update_product', ['id' => $product->getId()]),
                ],
            ]);

            $event->getForm()->add('productUnit', ChoiceType::class, [
                'choices' => $this->getChoices($product),
                'label' => false,
                'attr' => [
                    'class' => 'select-unit',
                    'data-update-url' =>  $this->router->generate('sharing_proposal_update_product', ['id' => $product->getId()]),
                ],
            ]);
            $event->getForm()->add('packaging', TextType::class, [
                'label'    => false,
                'data' => $product->getItem()->getPackaging(),
                'disabled' => true,
                'attr' => [
                    'class' => 'packaging-input',
                ],
            ]);
            $event->getForm()->add('pieceNumber', IntegerType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'min' => '1',
                    'class' => 'piece-input',
                    'data-update-url' =>  $this->router->generate('sharing_proposal_update_product', ['id' => $product->getId()]),
                ],
        
            ]);
            $event->getForm()->add('Remove', ButtonType::class, [
                'label' => 'Remove',
                'attr' => [
                    'class' => 'btn btn-danger form-delete-button',
                    'type' => "button",
                    'role' => "button",
                    'data-product-id' => $product->getId(),
                    'data-remove-url' =>  $this->router->generate('sharing_proposal_item_remove', ['id' => $product->getId()]),
                    'data-delete-token' => $token
                ],
                'translation_domain' => 'forms',
                'label' => false,
            ]);
            $event->getForm()->add('comment', TextType::class, [
                'translation_domain' => 'forms',
                'attr' => [
                    'placeholder' => 'sharing_proposal.placeholder',
                    'data-update-url' => $this->router->generate('sharing_proposal_update_product', ['id' => $product->getId()]),
                ],
                'label' => false,
                'required' => false,
            ]);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SharedProduct::class,
            'method' => 'get',
            'attr' => ['id' => 'task-form'],
        ]);
    }

    private function getChoices(SharedProduct $product)
    {
        $choices = SharedProduct::PRODUCTUNIT;
        $output = [];
        foreach ($choices as $k => $v) {
            if ($k == SharedProduct::PRODUCTUNIT_PARCEL) {
                /* not possible to have PARCEL in sharin proposal (kg ou pc only) */
                continue;
            } else {
                $output[$v] = $k;
            }
        }
        return $output;
    }
}
