<?php

namespace App\Form;

use App\Entity\Catalogue;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CatalogueImportType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('name')
            ->remove('status')
            ->remove('comment')
            ->remove('producerId')
            ->remove('type')
            ->remove('autoArchive')
            ->add('type',  TextType::class, [
                'label_format' => 'catalogue.%name%',
                'translation_domain' => 'forms',
                'disabled' => true
            ])
        ;
    }

    public function getParent()
    {
        return CatalogueType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Catalogue::class,
        ]);
    }
}
