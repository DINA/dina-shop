<?php

namespace App\Form;

use App\Entity\Basket;
use App\Entity\Order;
use App\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('product', CollectionType::class, [
            'entry_type' => ProductManagerEditType::class,
            'entry_options' => ['label' => false],
            'label' => false,
            ])
            ->add('Confirm', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary mx-1'],
                'label' => 'common.buttons.confirm',
            ])
        ;                   
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);
    }

    public function getBlockPrefix() //Permet d'avoir des paramètres propres dans l'URL sans trucs du genre ?[maxprice = xxxx]
    {
        return '';
    }
}
