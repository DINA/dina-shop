<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserEditType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder  
            ->add('status',  ChoiceType::class, [
                        'choices' => $this->getChoices(),
                        'translation_domain' => 'forms',
                        'label' => 'user.status.Status',
                    ])
            ->remove('plainPassword')
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'translation_domain' => 'forms',
                'required' => false,
                'first_options' => [
                    'constraints' => [
                        new Length([
                            'min' => 6,
                            'minMessage' => 'form.profile.min_password',
                            'max' => 255,
                        ]),
                    ],
                    'label' => 'user.password',
                    'attr' => array('placeholder' => 'user.placeholder_keep_blank')
                ],
                'second_options' => [
                    'label' => 'user.repeat_password',
                    'attr' => array('placeholder' => 'user.placeholder_keep_blank')
                ],
                'invalid_message' => 'form.profile.check_repeated_password',
            ])
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                $user = $event->getData();
                if (null === $user) {
                    return;
                }

                if ($user->hasRole(['ROLE_WORKER', 'ROLE_COORDINATOR', 'ROLE_MANAGER', 'ROLE_ADMIN'])){
                    $event->getForm()->remove('status');
                    $event->getForm()->add('status',  ChoiceType::class, [
                        'choices' => $this->getChoices(),
                        'translation_domain' => 'forms',
                        'label' => 'user.status.Status',
                        'disabled' => true,
                        ]);
                    }
                }
        );
    }

    public function getParent()
    {
        return AccountRequestType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

    private function getChoices()
    {
        $choices = User::STATUSES;
        $output = [];
        foreach ($choices as $k => $v) {
            if($k == User::STATUS_WAITING_APPROVAL){
                continue;
            }
            $output[$v] = $k;

            //$translated_text = ($v === 'Active') ? $this->translator->trans('admin.user.status.active', []) : $this->translator->trans('admin.user.status.inactive', []);
            //$output[$translated_text] = $k;
        }
        return $output;
    }
}
