<?php

namespace App\Form;

use App\Entity\Catalogue;
use App\Entity\Distribution;
use App\Repository\CatalogueRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DistributionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('deliveryDate', DateTimeType::class, [
                'label_format' => 'distribution.%name%',
                'translation_domain' => 'forms',
                'widget' => 'single_text',
                // prevents rendering it as type="date", to avoid HTML5 date pickers
                'html5' => false,
                'attr' => ['class' => 'js-datepicker'],
            ])
            ->add('Catalogues', EntityType::class, [
                'class' => Catalogue::class,
                'label_format' => 'distribution.%name%',
                'translation_domain' => 'forms',
                'choice_label' => 'name',
                'help' => 'distribution.help_catalogues',
                'multiple' => true,
                'required' => false,
                'query_builder' => function (CatalogueRepository $repository) {
                    return  $repository->createQueryBuilder('c')
                        ->where('c.status = :status')
                        ->setParameter('status', Catalogue::getStatusValue('online'));
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Distribution::class,
        ]);
    }
}
