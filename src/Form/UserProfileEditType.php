<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserProfileEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('plainPassword')
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'required' => false,
                'translation_domain' => 'forms',
                'first_options' => [
                    'constraints' => [
                        new Length([
                            'min' => 6,
                            'minMessage' => 'form.profile.min_password',
                            'max' => 255,
                        ]),
                    ],
                    'label' => 'user.password',
                    'attr' => array('placeholder' => 'user.placeholder_password')
                ],
                'second_options' => [
                    'label' => 'user.repeat_password',
                    'attr' => array('placeholder' => 'user.placeholder_repeat_password')
                ],
                'invalid_message' => 'form.profile.check_repeated_password',
            ]);

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                $user = $event->getData();
                if (null === $user) {
                    return;
                }

                $event->getForm()->remove('email');
                $event->getForm()->remove('username');
                $event->getForm()->add('email', EmailType::class, [
                    'label_format' => 'user.%name%',
                    'translation_domain' => 'forms',
                    'disabled' => true,
                ]);
                $event->getForm()->add('username', TextType::class, [
                    'label_format' => 'user.%name%',
                    'translation_domain' => 'forms',
                    'disabled' => true,
                ]);
            }
        );
    }

    public function getParent()
    {
        return AccountRequestType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
