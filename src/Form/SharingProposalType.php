<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\SharingProposal;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolver;

class SharingProposalType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sharedProducts', CollectionType::class, [
            'entry_type' => SharedProductType::class,
            'entry_options' => ['label' => false],
            'label' => false,
            'allow_delete' => true,
            'by_reference' => false,
        ])
            ->add('submitProposition', SubmitType::class, [
                'label' => 'Submit Proposition',
                'translation_domain' => 'forms',
                'label' => 'sharing_proposal.submit',
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Save',
                'translation_domain' => 'forms',
                'label' => 'sharing_proposal.save',
            ])
        ;                   
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SharingProposal::class,
        ]);
    }

    //private function getChoices()
    //{
    //    $choices = Product::PRODUCTUNIT;
    //    $output = [];
    //    foreach ($choices as $k => $v) {
    //        $output[$v] = $k;
    //    }
    //    return $output;
    //}

    public function getBlockPrefix() //Permet d'avoir des paramètres propres dans l'URL sans trucs du genre ?[maxprice = xxxx]
    {
        return '';
    }
}
