<?php
namespace App\DoctrineListener;

use App\Entity\Catalogue;
use App\Service\CatalogueImport\CatalogueImport;
use App\Service\DistributionStatusCheck\DistributionStatusCheck;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Exception;

class ImportItemsListener
{
    private $catalogueImport;
    private $distributionStatusCheck;

    public function __construct(CatalogueImport $catalogueImport, DistributionStatusCheck $distributionStatusCheck)
    {
        $this->catalogueImport = $catalogueImport;
        $this->distributionStatusCheck = $distributionStatusCheck;
    }

    /**
     * postPersist
     * Import when a new catalogue is created
     *
     * @param  mixed $args
     * @return void
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Catalogue) {
            return;
        }

        /* Upload items if file exist only */
        if($entity->getSpreadsheetFile() || ($entity->getDropboxFile() && $entity->getDropboxFile()->getDropboxFileLink())){
            $this->catalogueImport->import($entity);
        }
    }
    
    /**
     * postUpdate
     * Import from articles
     *
     * @param  mixed $args
     * @return void
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Catalogue) {
            return;
        }

        /* Upload items if file exist only */
        if ($entity->getSpreadsheetFile() || ($entity->getDropboxFile()) && $entity->getDropboxFile()->getDropboxFileLink()) {
            if($entity->isStatus(Catalogue::STATUS_ARCHIVED)){
                throw new Exception('Can\'t import items into an archived catalogue');
            } elseif($list = $this->distributionStatusCheck->checkLinkedDistribution($entity)) {
                $str =  implode(',', $list);
                throw new Exception('Catalogue ' . $entity->getName() . ' is already used by distribution: ' . $str . ' ; can\'t import new items');
            } else{
                $this->catalogueImport->update($entity);
            }
        }
    }
}
