<?php
namespace App\DoctrineListener;

use App\Entity\Catalogue;
use App\Repository\ProducerRepository;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use App\Service\CatalogueImport\CatalogueImport;
use App\Service\DistributionStatusCheck\DistributionStatusCheck;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class CatalogueListener
{
    private $distributionStatusCheck;
    private $entityManager;
    private $producerRepository;
    private $catalogueConfigReader;
    private $params;

    public function __construct(DistributionStatusCheck $distributionStatusCheck, 
                                EntityManagerInterface $entityManager, 
                                ProducerRepository $producerRepository,
                                CatalogueConfigReader $catalogueConfigReader,
                                ParameterBagInterface $params
    )
    {
        $this->distributionStatusCheck = $distributionStatusCheck;
        $this->entityManager = $entityManager;
        $this->producerRepository = $producerRepository;
        $this->catalogueConfigReader = $catalogueConfigReader;
        $this->params = $params;
    }

    /**
     * prePersist
     *
     * @param  mixed $args
     * @return void
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Catalogue) {
            return;
        }

        /* This test is already performed in the CatalogueValidator but now we have to link the producer with the catalogue */
        $catCode = $this->catalogueConfigReader->findCatalogueCode($entity->getType());
        /** @var \App\Entity\Producer $producer */
        $producer = $this->producerRepository->findOneBy(['code' => $catCode]);
        if($producer){
            $entity->setProducerId($producer);
        }else{
            throw new Exception('Unable to find the producer associated ot this catalogue code:' . $catCode);
        }

        /* If a dropbox link is provided, download the file */
        if ($entity->getDropboxFile() && $entity->getDropboxFile()->getDropboxFileLink()) {
            $this->dropboxDownloadFile($entity);
        }
    }

    /**
     * preUpdate
     *
     * @param  mixed $args
     * @return void
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Catalogue) {
            return;
        }

        /* Delete catalogue? */
        if (($entity->isStatus(Catalogue::STATUS_ARCHIVED))  && ($list = $this->distributionStatusCheck->checkLinkedDistribution($entity))) {
            $str =  implode(',', $list);
            throw new Exception('Catalogue ' . $entity->getName() . ' is already used by distribution: ' . $str . ' ; can\'t archive it');
        }
        /* Edit catalogue: If a dropbox link is provided, download the file */
        elseif ($entity->getDropboxFile() && $entity->getDropboxFile()->getDropboxFileLink()) {
            $this->dropboxDownloadFile($entity);
        }
        /* Archive catalogue: delete associated file to keep space on the server */
        elseif ($entity->isStatus(Catalogue::STATUS_ARCHIVED)) {
            if ($entity->getSpreadsheet()) {
                $filepath = $this->params->get('vich_uploader.mappings')['catalogue_sreadsheet']['upload_destination'] . '/' . $entity->getSpreadsheet();
                if (file_exists($filepath)) {
                    unlink($filepath);
                    $entity->setSpreadsheet('');
                    $entity->setSpreadsheetName('');
                }
            }
        }
    }

    /**
     * postUpdate
     *
     * @param  mixed $args
     * @return void
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Catalogue) {
            return;
        }
        
        /* Delete all items never ordered to save place in database
        * Keep all ordered items to have an order history for users */
        if ($entity->isStatus(Catalogue::STATUS_ARCHIVED)){
            foreach($entity->getItems() as $item){
                if($item->getProducts()->isEmpty())
                {
                    //Never ordered, now check it is not a part of a sharing proposal:
                    if(!$item->getSharingProposal()){
                        $entity->removeItem($item);
                    } 
                }
            }
            
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }
    }

    /**
     * preRemove
     *
     * @param  mixed $args
     * @return void
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Catalogue) {
            return;
        }

        if ($entity->isStatus(Catalogue::STATUS_ONLINE)) {
            throw new Exception('Can\'t delete an online catalogue');
        } elseif ($list = $this->distributionStatusCheck->checkLinkedDistribution($entity)) {
            $str =  implode(',', $list);
            throw new Exception('Catalogue ' . $entity->getName() . ' is already used by distribution: ' . $str . ' ; can\'t delete it');
        }
    }

    private function dropboxDownloadFile(Catalogue $catalogue): void
    {
        $url = $catalogue->getDropboxFile()->getDropboxFileLink();
        $fileName = uniqid('', true) . '_' . $catalogue->getDropboxFile()->getDropboxFileName();

        $filepath = $this->params->get('vich_uploader.mappings')['catalogue_sreadsheet']['upload_destination'] . '/' . $fileName;
        // Use file_get_contents() function to get the file 
        // from url and use file_put_contents() function to 
        // save the file by using base name 
        if (file_put_contents($filepath, file_get_contents($url))) {
            $catalogue->setSpreadsheet($fileName);
            $catalogue->setSpreadsheetName($catalogue->getDropboxFile()->getDropboxFileName());
        } else {
            throw new Exception('Error downloading from DropBox');
        }
    }
}
