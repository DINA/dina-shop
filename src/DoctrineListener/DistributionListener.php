<?php
namespace App\DoctrineListener;

use App\Entity\Catalogue;
use App\Entity\Distribution;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;

class DistributionListener
{
    private $entityManager;

    public function __construct( EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * postUpdate
     *
     * @param  mixed $args
     * @return void
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Distribution) {
            return;
        }

        /* Delete all concerned shared products when a catalogue is removed from the distribution */

        $sharingProposal = $entity->getSharingProposal();
        if($sharingProposal){
            $sharedProducts = $sharingProposal->getSharedProducts();
            
            /** @var \App\Entity\Collection|Catalogue[] $catalogues */
            $catalogues = $entity->getCatalogues();
            $deleted = $catalogues->getDeleteDiff();
            foreach($deleted as $catalogue){
                foreach($sharedProducts as $sharedProduct){
                    if($sharedProduct->getItem()->getCatalogueId() == $catalogue){
                        $sharingProposal->removeSharedProduct($sharedProduct);
                    }
                }
            }
            $this->entityManager->persist($sharingProposal);
            $this->entityManager->flush();
        }
    }

}
