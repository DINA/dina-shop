<?php
namespace App\DoctrineListener;

use App\Entity\Catalogue;
use App\Entity\Item;
use App\Service\CatalogueImport\CatalogueImport;
use App\Service\DistributionStatusCheck\DistributionStatusCheck;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Exception;

class ItemListener
{
    private $distributionStatusCheck;

    public function __construct(DistributionStatusCheck $distributionStatusCheck)
    {
        $this->distributionStatusCheck = $distributionStatusCheck;
    }


    /**
     * prePersist
     *
     * @param  mixed $args
     * @return void
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Item) {
            return;
        }

        if ($entity->getCatalogueId()->isStatus(Catalogue::STATUS_ARCHIVED)) {
            throw new Exception('Can\'t add item into an archived catalogue');
        } 
    }

    /**
     * preUpdate
     *
     * @param  mixed $args
     * @return void
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Item) {
            return;
        }

        if ($entity->getCatalogueId()->isStatus(Catalogue::STATUS_ARCHIVED)) {
            throw new Exception('Can\'t edit item from an archived catalogue');
        }
    }

    /**
     * preRemove
     *
     * @param  mixed $args
     * @return void
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Item) {
            return;
        }

        //Gros workaround dégueulasse, on va tester le statut du catalogue uniquement dans le controlleur pour éviter de se modre la queue:
        // on ne peut supprimer un cataloque QUE s'il est archived, mais on ne peut pas supprimé un article d'un catalogue archived donc on interdit la suppression du catalogue archived....
        // On teste aussi que le catalogue n'est pas un catalogue extra car dans ce cas on autorise la suppression d'article (pour corriger les boulettes des membres)
        /* if ($entity->getCatalogueId()->isStatus(Catalogue::STATUS_ARCHIVED)) {
            throw new Exception('Can\'t delete item from an archived catalogue');
        } else */
        //Test on $entity->getCatalogueId() to prevent an exception when items are deleted by CatalogueLisener::postEvent

        $catalogue = $entity->getCatalogueId();

        if(($catalogue) && (!$catalogue->getExtra()) && ($list = $this->distributionStatusCheck->checkLinkedDistribution($catalogue))){
            $str =  implode(',', $list);
            throw new Exception('Catalogue ' .$entity->getCatalogueId()->getName() . ' is used by distribution: ' . $str . ' ; can\'t delete item');
        }
     
    }
    
}
