<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('admin');
        $user->setEmail('admin@mail.com');
        $user->setStatus(User::STATUS_ACTIVE);
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'admin'
        ));
        $user->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('partage');
        $user->setEmail('partage@mail.com');
        $user->setStatus(User::STATUS_ACTIVE);
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'password'
        ));
        $user->setRoles(['ROLE_WORKER']);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('commande');
        $user->setEmail('commande@mail.com');
        $user->setStatus(User::STATUS_ACTIVE);
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'password'
        ));
        $user->setRoles(['ROLE_MANAGER']);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('coordinateur');
        $user->setEmail('coordinateur@mail.com');
        $user->setStatus(User::STATUS_ACTIVE);
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'password'
        ));
        $user->setRoles(['ROLE_COORDINATOR']);
        $manager->persist($user);
        
        $user = new User();
        $user->setUsername('test');
        $user->setEmail('test@mail.com');
        $user->setStatus(User::STATUS_INACTIVE);
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'user'
        ));
        $user->setRoles(['ROLE_USER']);
        $manager->persist($user);

        $manager->flush();
    }
}
