<?php

namespace App\DataFixtures;

use App\Entity\Producer;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    private $catalogueConfigReader;

    public function __construct(CatalogueConfigReader $catalogueConfigReader)
    {
        $this->catalogueConfigReader = $catalogueConfigReader;
    }

    public function load(ObjectManager $manager): void
    {
        $producer = new Producer($this->catalogueConfigReader);
        $producer->setName('Relais Vert');
        $producer->setCode('RV');
        $manager->persist($producer);

        $producer = new Producer($this->catalogueConfigReader);
        $producer->setName('Coline');
        $producer->setCode('COL');
        $manager->persist($producer);

        $producer = new Producer($this->catalogueConfigReader);
        $producer->setName('Laurence');
        $producer->setCode('LAU');
        $manager->persist($producer);

        $producer = new Producer($this->catalogueConfigReader);
        $producer->setName('Katia');
        $producer->setCode('ROSSI');
        $manager->persist($producer);

        $producer = new Producer($this->catalogueConfigReader);
        $producer->setName('Millésime 86');
        $producer->setCode('M86');
        $manager->persist($producer);

        $producer = new Producer($this->catalogueConfigReader);
        $producer->setName('Laboratoire Combe d\'Ase');
        $producer->setCode('LCA');
        $manager->persist($producer);

        $manager->flush();
    }
}
