<?php

namespace App\Service\ProductManager;

use App\Entity\Basket;
use App\Entity\Bookmark;
use App\Entity\Catalogue;
use App\Entity\Distribution;
use App\Entity\Item;
use App\Entity\Product;
use App\Repository\BookmarkRepository;
use App\Repository\ItemRepository;
use App\Repository\ProductRepository;
use App\Repository\SharingProposalRepository;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;

class BookmarkManager extends VirtualItemManager
{
    protected $repository;
    protected $class = Bookmark::class;
    private $itemRepository;

    public function __construct(BookmarkRepository $bookmarkRepository, ItemRepository $itemRepository)
    {
        $this->repository = $bookmarkRepository;
        $this->itemRepository = $itemRepository;
        parent::__construct();
    }

    public function createBookmarkFromItem(Item $item): Bookmark
    {
        return $this->createVirtualItemFromItem($item);
    }

    public function getBookmarkFromItem(Item $item): ?Bookmark
    {
        return $this->getVirtualItemFromItem($item);
    }

    public function searchForItemFromBookmark(?Distribution $currentDistribution, Bookmark $bookmark): ?Item
    {
        if ($currentDistribution) {
            $catalogues = $currentDistribution->getCatalogues();
            $newItem =  $this->itemRepository->findforBookmarkedItem($catalogues, $bookmark);
        }
        return ($currentDistribution) ? $newItem : null;
    }
}