<?php

namespace App\Service\ProductManager;

use App\Entity\Basket;
use App\Entity\Catalogue;
use App\Entity\Distribution;
use App\Entity\Item;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\SharedProduct;
use App\Entity\SharingProposal;
use App\Repository\ItemRepository;
use App\Repository\ProductRepository;
use App\Repository\SharingProposalRepository;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;

class ProductManager
{
    const MAX_PIECES=25;

    private $sharingProposalRepository;
    private $productRepository;
    private $catalogueConfigReader;
    private $itemRepository;

    public function __construct(SharingProposalRepository $sharingProposalRepository, ProductRepository $productRepository, CatalogueConfigReader $catalogueConfigReader, ItemRepository $itemRepository)
    {
        $this->sharingProposalRepository = $sharingProposalRepository;
        $this->productRepository = $productRepository;
        $this->catalogueConfigReader = $catalogueConfigReader;
        $this->itemRepository = $itemRepository;
    }
    
    /**
     * createProductFromItem
     *
     * @param  mixed $item
     * @param  mixed $currentDistribution
     * @return Product
     */
    public function createProductFromItem(Item $item, Distribution $currentDistribution): Product
    {
        $product = new Product();

        $product->setItem($item);

        $orderOrigin = ($item->getSharingProposal())? Product::ORDERORIGIN_SHARE : Product::ORDERORIGIN_INDIVIDUAL;
        $product->setOrderOrigin($orderOrigin);

        $product->setPackaging($product->getItem()->getPackaging());

        //Do not pre-select unit of item allow pieces order
        $productUnit = ($item->getMinPacking())? Product::PRODUCTUNIT_UNDEFINED :Product::PRODUCTUNIT_PARCEL;
        $product->setProductUnit($productUnit);

        $catType = $item->getCatalogueId()->getType();
        $product->setPreOrder($this->catalogueConfigReader->findCataloguePreOrder($catType));
        if ('RelaisVert_Mercuriale_Viande' === $catType) {
            /* Get RelaisVert_Catalogue_Frais in the current  */
            $catFrais = $currentDistribution->getCatalogues()->filter(function($catalogue){
                return ($catalogue->getType() === 'RelaisVert_Catalogue_Frais')? true : false;
            });

            /* Chez Relais Vert la règle de la précommande est simple:
            - un produit dans le catalogue frais est toujours en stock chez eux => pas de précommande
            - si un produit de la mercuriale viande est AUSSI présent dans le catalogue frais => pas de précommande
            - si un produit de la mercuriale viande n'est PAS présent dans le catalogue frais => précommande
            Si catalogue frais pas présent dans la distribution, dans le doute on précommande...
            */
            if($catFrais->isEmpty() || (!$this->isInThisCatalogue($product->getItem(), $catFrais->current()))){
                $product->setPreOrder(true);
            }
        }

        return $product;
    }
    
    /**
     * createSharedProductFromItem
     *
     * @param  mixed $item
     * @return SharedProduct
     */
    public function createSharedProductFromItem(Item $item): SharedProduct
    {
        $product = new SharedProduct();

        $product->setItem($item);

        $orderOrigin =Product::ORDERORIGIN_SHARE;
        $product->setOrderOrigin($orderOrigin);

        $product->setPackaging($product->getItem()->getPackaging());

        $catType = $item->getCatalogueId()->getType();
        if ('RelaisVert_Mercuriale_FruitsLegumes' === $catType) {
            /* Preset "kg' unit only if item comes from RelaisVert_Mercuriale_FruitsLegumes and has a 'kg' string in packaging */
            $productUnit = (stristr($item->getPackaging(), 'KG') ? Product::PRODUCTUNIT_KG : Product::PRODUCTUNIT_PIECE);
        } elseif(('RelaisVert_Catalogue_Frais' === $catType) && ($product->getItem()->getFamily() === 'Fromages')) {
            /* /!\ The string "Fromages" comes from Relais Vert catalogue */
            /* Preset "kg' unit only if item has a 'kg' string in packaging like "COMTÉ POINTE AOP	FROMAGERIE PETITE	2,5	kg"*/
            if(stristr($item->getPackaging(), 'KG')){
                $productUnit =  Product::PRODUCTUNIT_KG;
                $product->setIsCheese(true);
            } else{ /* Other cheeses: MOZZARELLA DE BUFFLONNE	AMBROSI		pc  12 */
                $productUnit =  Product::PRODUCTUNIT_PIECE;
            }
        } elseif ($item->getCatalogueId()->getFormat() === 'Format_DINA_Producer') {
            $productUnit = (stristr($item->getSKU(), 'KG') ? Product::PRODUCTUNIT_KG : Product::PRODUCTUNIT_PIECE);
        } else {
            $productUnit =  Product::PRODUCTUNIT_PIECE;
        }
        $product->setProductUnit($productUnit);

        /* Preset Pieces number */
        if (($product->getProductUnit() == Product::PRODUCTUNIT_PIECE)) {
            /* Get current packing if already set */
            if ($product->getItem()->getPacking()) {
                $pieceNumber = $product->getItem()->getPacking();
            } else {
                $packaging = $product->getItem()->getPackaging();
                if (stristr($packaging, 'PCE')) {
                    $pieceNumber = trim(preg_filter('(pce|Pce|PCE)', '', $packaging));
                } elseif (stristr($packaging, 'COL')) {
                    $pieceNumberDesignation = $pieceNumberCalibreCateg = null;
                    //Search for: 'space', 'X' character followed by 1,2 or 3 digits, then a 'space' again
                    if (preg_match('( [X]([0-9]{1,3})( |$))', $product->getItem()->getDesignation(), $result)) {
                        $pieceNumberDesignation = $result[1];
                    }
                    //Search for: 'X' character followed by 'space'(facultative) then 1 or 2 digits
                    if(preg_match('#^x[ ]?([0-9]{1,2})#i', $product->getItem()->getCalibreCateg(), $result)){
                        $pieceNumberCalibreCateg = $result[1];
                    }

                    /* Both numbers match, 100% trusted */
                    if($pieceNumberDesignation === $pieceNumberCalibreCateg){
                        $pieceNumber = $pieceNumberDesignation;
                    } 
                    /* Both numbers set but no match, 0% trusted */
                    elseif (isset($pieceNumberDesignation) && isset($pieceNumberCalibreCateg) && ($pieceNumberDesignation !== $pieceNumberCalibreCateg)) {
                        $pieceNumber = null;
                    }
                    /* Only pieceNumberDesignation found, limit to 25 pieces
                        ex: CRESSON BARQ, 8 X100 GR CAT II must be 8, not 100
                     */
                    elseif($pieceNumberDesignation && !isset($pieceNumberCalibreCateg)){
                        $pieceNumber = ($pieceNumberDesignation <= self::MAX_PIECES)? $pieceNumberDesignation : null;
                    }
                    /* Only pieceNumberDesignation found, limit to 25 pieces
                     */
                    elseif (!isset($pieceNumberDesignation) && $pieceNumberCalibreCateg) {
                        $pieceNumber = ($pieceNumberCalibreCateg <= self::MAX_PIECES)? $pieceNumberCalibreCateg : null;
                    }
                }
                /* For DINA producers, set default to 1 if no value */
                elseif($item->getCatalogueId()->getFormat() === 'Format_DINA_Producer'){
                    $pieceNumber = 1;
                }
            }
            $product->setPieceNumber($pieceNumber ?? null);
        } 

        return $product;
    }

    public function createProductFromSharedProduct(SharedProduct $sharedProduct): Product
    {
        $product = new Product();

        $product->setRelatedSharedProduct($sharedProduct);
        /* Set Item again to ease acces instead of getRelatedSharedProduct->getItem() */
        $product->setItem($sharedProduct->getItem());
        /* Idem, furthermore it will always set to ORDERORIGIN_SHARE */
        $product->setOrderOrigin($sharedProduct->getOrderOrigin());
        /* Idem */
        $product->setProductUnit($sharedProduct->getProductUnit());

        return $product;
    }

    /**
     * getProductFromItem
     * Search a product in the basket and add +1 to quantity if found
     *
     * @param  mixed $basket
     * @param  mixed $item
     * @return Product
     */
    public function getProductFromItem(Basket $basket, Item $item): ?Product
    {
        foreach ($basket->getProduct() as $product) {
            /* Check also the origin, a product can be ordered individually AND from sharing proposal */
            if(($product->getItem() == $item)  && ($product->getOrderOrigin() === Product::ORDERORIGIN_INDIVIDUAL)){
                $product->setQuantityDec($product->getQuantityDec()+1);
                return $product;
            }
        }
        return null;
    }
    
    /**
     * updateProductFromBasketToOrder
     * Search a product in the order and add to quantity in the basket if found
     *
     * @param  mixed $order
     * @param  mixed $product
     * @return Product
     */
    public function updateProductFromBasketToOrder(Order $order, Product $product): ?Product
    {
        foreach ($order->getProduct() as $existingProduct) {
            /* Test also that the origin is the same, otherwise this is not the same product */
            if (($existingProduct->getItem() == $product->getItem()) && ($existingProduct->getOrderOrigin() === $product->getOrderOrigin())) {
                $existingProduct->setQuantityDec($existingProduct->getQuantityDec() + $product->getQuantityDec());
                return $existingProduct;
            }
        }
        return null;
    }

    /**
     * getProductFromSharedProduct
     * Search a sharedProduct in the basket and add +1 to quantity if found
     *
     * @param  mixed $basket
     * @param  mixed $sharedProduct
     * @return Product
     */
    public function getProductFromSharedProduct(Basket $basket, SharedProduct $sharedProduct): ?Product
    {
        foreach ($basket->getProduct() as $product) {
            /* Check also the origin, a product can be ordered individually AND from sharing proposal
                example: I order 1 (full) comté (3kg - a gift for an english friend) AND I order personnally 500g for my personnal use
             */
            if (($product->getItem() == $sharedProduct->getItem()) && ($product->getOrderOrigin() === Product::ORDERORIGIN_SHARE)) {
                $product->setQuantityDec($product->getQuantityDec() + 1);
                return $product;
            }
        }
        return null;
    }
    
    /**
     * getSharedProductFromItem
     *
     * @param  mixed $sharingProposal
     * @param  mixed $item
     * @return SharedProduct
     */
    public function getSharedProductFromItem(SharingProposal $sharingProposal=null, Item $item): ?SharedProduct
    {
        if($sharingProposal){
            foreach ($sharingProposal->getSharedProducts() as $product) {
                if ($product->getItem() == $item) {
                    return $product;
                }
            }
        }
        return null;
    }
    
    /**
     * searchForEquivalentItem
     *
     * @param  mixed $currentDistribution
     * @param  mixed $oldItem
     * @return Item
     */
    public function searchForEquivalentItem(?Distribution $currentDistribution, Item $oldItem): ?Item
    {
        if ($currentDistribution) {
            $catalogues = $currentDistribution->getCatalogues();
            $newItem =  $this->itemRepository->findforEquivalentItem($catalogues, $oldItem);
        }
        return ($currentDistribution) ? $newItem : null;
    }
    
    /**
     * isInThisCatalogue
     * Search if an item is present in the given catalogue
     *
     * @param  mixed $item
     * @param  mixed $catalogue
     * @return bool
     */
    private function isInThisCatalogue(Item $item, Catalogue $catalogue): bool
    {
        return $this->itemRepository->findOneBy([
            'reference' => $item->getReference(),
            'catalogueId' => $catalogue->getId()
        ]) ? true : false;
    }
    
    /**
     * isMeetFamily
     * obsolete function
     *
     * @param  mixed $family
     * @return bool
     */
    private function isMeetFamily(string $family): bool
    {
        /* /!\ Theses strings comes from Relais Vert catalogue */
        return preg_match('#^abats|boeuf|canard|charcuterie|dinde|pintade|porc|poulet|veau$#i', $family);
    }
}