<?php


namespace App\Service\ProductManager;

use App\Entity\Product;
use Exception;

trait Flag
{
    public function getCountryFlag(string $origin = null, ?string $catalogueFormat = null): string
    {
        /* Automatically add PACA flag for local producers */
        if(isset($catalogueFormat) && ($catalogueFormat === 'Format_DINA_Producer')){
            return '<img class="flag-icon" src="/img/paca.svg" width="20px" alt="world-logo" title="' . $origin . '">';
        }
        /* If not local producer and no origin, add world flag */
        if (!isset($origin)) {
            return '<img class="flag-icon" src="/img/globe.png" width="20px" alt="world-logo" title="Inconnu">';
        }

        /* Otherwise get flag */
        if (stristr($origin, 'france')) {
            return '<span class="flag-icon flag-icon-fr" title="' . $origin . '"></span>';
        } elseif (stristr($origin, 'paca')) { /* Set PACA after france, more accurate */
            return '<img class="flag-icon" src="/img/paca.svg" width="20px" alt="world-logo" title="' . $origin . '">';
        } elseif (stristr($origin, 'espagne')) {
            return '<span class="flag-icon flag-icon-es" title="' . $origin . '"></span>';
        } elseif (stristr($origin, 'italie')) {
            return '<span class="flag-icon flag-icon-it" title="' . $origin . '"></span>';
        } 
        /* Europe */ elseif (
            stristr($origin, 'union europeenne') || stristr($origin, 'union européenne') || stristr($origin, "UNION EUROPÉENNE")
            || stristr($origin, 'europe')
            || stristr($origin, 'allemagne')
            || stristr($origin, 'autriche')
            || stristr($origin, 'belgique')
            || stristr($origin, 'bulgarie')
            || stristr($origin, 'chypre')
            || stristr($origin, 'croatie')
            || stristr($origin, 'danemark')
            || stristr($origin, 'estonie')
            || stristr($origin, 'finlande')
            || stristr($origin, 'grece') || stristr($origin, 'grèce')
            || stristr($origin, 'hongrie')
            || stristr($origin, 'irlande')
            || stristr($origin, 'lettonie')
            || stristr($origin, 'lituanie')
            || stristr($origin, 'luxembourg')
            || stristr($origin, 'malte')
            || stristr($origin, 'pays bas') || stristr($origin, 'pays-bas')
            || stristr($origin, 'pologne')
            || stristr($origin, 'portugal')
            || stristr($origin, 'republique tcheque') || stristr($origin, 'république tchèque')
            || stristr($origin, 'roumanie')
            || stristr($origin, 'slovaquie')
            || stristr($origin, 'slovenie') || stristr($origin, 'slovénie')
            || stristr($origin, 'suede') || stristr($origin, 'suède')
        ) {
            return '<span class="flag-icon flag-icon-eu" title="' . $origin . '"></span>';
        }
        /* World */ else {
            return '<img class="flag-icon" src="/img/globe.png" width="20px" alt="world-logo" title="' . $origin . '">';
        }
    }
}