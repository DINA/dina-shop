<?php

namespace App\Service\ProductManager;

use App\Entity\Basket;
use App\Entity\Bookmark;
use App\Entity\Catalogue;
use App\Entity\Item;
use App\Entity\Product;
use App\Repository\BookmarkRepository;
use App\Repository\ProductRepository;
use App\Repository\SharingProposalRepository;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;

abstract class VirtualItemManager
{
    protected $repository;
    protected $class;

    public function __construct()
    {
        if ($this->repository === null) {
            throw new \Exception("Class " . get_class($this) . " doesn't have repository");
        }
        if ($this->class === null) {
            throw new \Exception("La class " . get_class($this) . " doesn't have attribute \$class");
        }
    }

    public function createVirtualItemFromItem(Item $item)
    {
        $vItem = new $this->class;

        $vItem->setProducerName($item->getCatalogueId()->getProducerId()->getName());
        $vItem->setReference($item->getReference());
        $vItem->setDesignation($item->getDesignation());

        return $vItem;
    }

    public function getVirtualItemFromItem(Item $item)
    {
        $vItem = $this->repository->findBy([
            'producerName' => $item->getCatalogueId()->getProducerId()->getName(),
            'reference' => $item->getReference(),
            'designation' =>$item->getDesignation()
        ]);
        return $vItem[0]?? null;
        
    }
}