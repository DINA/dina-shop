<?php

namespace App\Service\ProductManager;

use App\Entity\Distribution;
use App\Entity\Item;
use App\Entity\SuggestedItem;
use App\Repository\ItemRepository;
use App\Repository\SuggestedItemRepository;

class SuggestedItemManager extends VirtualItemManager
{
    protected $repository;
    protected $class = SuggestedItem::class;
    private $itemRepository;


    public function __construct(SuggestedItemRepository $suggestedItemRepository, ItemRepository $itemRepository)
    {
        $this->repository = $suggestedItemRepository;
        $this->itemRepository = $itemRepository;
        parent::__construct();
    }

    public function createSuggestedItemFromItem(Item $item): SuggestedItem
    {
        return $this->createVirtualItemFromItem($item);
    }

    public function getSuggestedItemFromItem(Item $item): ?SuggestedItem
    {
       return $this->getVirtualItemFromItem($item);
    }

    public function searchForItemFromSuggestedItem(?Distribution $currentDistribution, SuggestedItem $suggestedItem): ?Item
    {
        if ($currentDistribution) {
            $catalogues = $currentDistribution->getCatalogues();
            $newItem =  $this->itemRepository->findforSuggestedItem($catalogues, $suggestedItem);
        }
        return ($currentDistribution) ? $newItem : null;
    }

}