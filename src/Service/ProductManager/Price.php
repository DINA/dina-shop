<?php


namespace App\Service\ProductManager;

use App\Entity\Product;
use Exception;

trait Price
{
    public function getTotalPrice(): ?string
    {
        $price =  array_reduce($this->product->getValues(), function ($total, Product $product) {
            return $this->computeCustomerPrice($product) + $total;
        }, 0);
        return $price;
    }

    public function getTotalVATPrice(): ?string
    {
        $price =  array_reduce($this->product->getValues(), function ($total, Product $product) {
            return $this->computeCustomerPrice($product, true) + $total;
        }, 0);
        return $price;
    }

    /**
     * displayUnitaryPrice
     * Issue #10
     *
     * @param  mixed $product
     * @return string
     */
    public function getUnitaryPrice($product): ?float
    {
        switch ($product->getProductUnit()) {
            case Product::PRODUCTUNIT_KG:
            case Product::PRODUCTUNIT_PARCEL:
                $price =  $product->getItem()->getUnitaryPrice();
                break;
            case Product::PRODUCTUNIT_PIECE:
                if (false !== stristr($product->getItem()->getPackaging(), "COL")) {
                    /* If the packaging is COL, we need do divide by the number of pieces 
                        ex: SAFC => to divide
                    */
                    $price =  $product->getItem()->getUnitaryPrice() / $product->getPieceNumber();
                } else {
                    /* do not divide: 
                        Lessive 2L 38935
                        Dattes 40749
                     */
                    $price =  $product->getItem()->getUnitaryPrice();
                }
                break;
            default:
                throw new Exception('Unknown Type: ' . $product->getProductUnit());
                break;
        }
        return $price;
    }

    private function computeCustomerPrice(Product $product, ?bool $withVAT = false): ?string
    {
        switch ($product->getProductUnit()) {
            case Product::PRODUCTUNIT_KG:
                $price = $product->getQuantityDec() * $product->getItem()->getUnitaryPrice();
                break;
            case Product::PRODUCTUNIT_PARCEL:
                $wheightKg = $this->extractWeight($product);
                $price = $product->getQuantityDec() * $product->getItem()->getUnitaryPrice() * ($wheightKg?? 1) * ($product->getItem()->getPacking() ?? 1);
                break;
            case Product::PRODUCTUNIT_PIECE:
                if ($product->getOrderOrigin() === Product::ORDERORIGIN_SHARE) {
                    if(false !== stristr($product->getItem()->getPackaging(),"COL")){
                        $price = $product->getQuantityDec() * $product->getItem()->getUnitaryPrice() / $product->getRelatedSharedProduct()->getPieceNumber();
                    } else{
                        $price = $product->getQuantityDec() * $product->getItem()->getUnitaryPrice() ;
                    }
                } else {
                    $wheightKg = $this->extractWeight($product);
                    $price = $product->getQuantityDec() * $product->getItem()->getUnpackedPrice() * ($wheightKg ?? 1);
                }
                break;
            case Product::PRODUCTUNIT_UNDEFINED:
                $price = 0;
                break;
            default:
                throw new Exception('Unknown Type: ' . $product->getProductUnit());
                break;
        }
        return ($withVAT) ? $price * (($product->getItem()->getVat() + 100) / 100) : $price;
    }


    /**
     * extractWeight
     * If unitary price is given per kg and not per parcel / piece, we must calculate the right price for the parcel / piece
     * Can manage:
     *      env 2-4KG
    *       env 100g-180g
    *       120g-200g
     *
     * @param  mixed $product
     * @return int or null if no wheight
     */
    private function extractWeight(Product $product): ?float
    {
        $catalogueType = $product->getItem()->getCatalogueId()->getType();
        
        if ((($catalogueType === 'RelaisVert_Mercuriale_FruitsLegumes') && (stristr($product->getItem()->getPackaging(), 'KG')))
        || (('RelaisVert_Catalogue_Frais' === $catalogueType) )
        || ('RelaisVert_Mercuriale_Viande' === $catalogueType)
        ) {
            /* Multiply the unitary price by number of kg in the parcel
                        Search for env 2,5kg (example)
                     */
            if (preg_match('#(^(env)?[ ]?([0-9]?[0-9]([.|,][0-9][0-9]?)?)[ ]*(kg[s]?|[K]{1})?)$#i', $product->getItem()->getPackaging(), $result)) {
                $weight = $result[3];
            }

            elseif (preg_match(
            '#(^(env)?[ ]?([0-9]?[0-9]([.|,][0-9][0-9]?)?)[-|\/]*([0-9]?[0-9]([.|,][0-9][0-9]?)?) ?(kg[s]?|[K]{1})?)$#i', $product->getItem()->getPackaging(), $result)) {
                $weight = (floatval($result[3]) + floatval($result[5])) / 2; //Give average
            }
            /* Search for env 250g (example) */ 
            elseif (preg_match(
                '#(^(env)[ ]?([0-9]{0,2}[0-9])[ ]*(g))$#i', $product->getItem()->getPackaging(), $result
            )) {
                $weight = '0.' . $result[3];
            }
            /* Search for env 100g-180g or 100g-180g or 100-180g or 100/180g  (example) */ 
            elseif (preg_match(
                '#(^(env)?[ ]?([0-9]{0,2}[0-9])g?[ ]*[-|\/][ ]*([0-9]{0,2}[0-9]) ?g)$#i', $product->getItem()->getPackaging(), $result
            )) {
                $weight = (floatval($result[3]) + floatval($result[4])) / 2 / 1000; //Give average and convert in kg
            }

            if(isset($weight)){
                $weight = str_replace(",", ".", $weight); // replace ',' with '.'
                return floatval($weight);
            }
        }
        return null;
    }
}
