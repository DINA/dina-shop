<?php


namespace App\Service\Response;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;

class RedirectForSuperUser
{
    private $security;
    private $router;

    public function __construct(RouterInterface $router, Security $security)
    {
        $this->security = $security;
        $this->router = $router;
    }

    public function redirect($event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $requestedUri = $event->getRequest()->getRequestUri();
        /** @var \App\Entity\User $user */
        $user = $this->security->getUser();

        if((strstr($requestedUri, '/shop/') && !strstr($requestedUri, '/help/shop/') ) && 
        ((!strstr($requestedUri, '/shop/order/')) && (!strstr($requestedUri, '/shop/dina/')) )
        ){
            if ($user && $user->hasRole('ROLE_MANAGER')){
                $response = new RedirectResponse($this->router->generate('dashboard'), 301);
                $event->setResponse($response);
            } elseif ($user && $user->hasRole('ROLE_WORKER')) {
                $response = new RedirectResponse($this->router->generate('worker_catalogue_index'), 301);
                $event->setResponse($response);
            }
            elseif ($user && $user->hasRole('ROLE_ADMIN')) {
                $response = new RedirectResponse($this->router->generate('dashboard'), 301);
                $event->setResponse($response);
            }
            elseif ($user && $user->hasRole('ROLE_COORDINATOR')) {
                $response = new RedirectResponse($this->router->generate('catalogue_index'), 301);
                $event->setResponse($response);
            }
        }
    }
}