<?php


namespace App\Service\Response;

use App\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class RedirectForUser
{
    private $security;
    private $router;
    /** @var Session */
    private $session;
    private $translator;

    public function __construct(RouterInterface $router, Security $security, SessionInterface $session, TranslatorInterface $translator)
    {
        $this->security = $security;
        $this->router = $router;
        $this->session = $session;
        $this->translator = $translator;
    }

    public function redirect($event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $requestedUri = $event->getRequest()->getRequestUri();
        /** @var \App\Entity\User $user */
        $user = $this->security->getUser();

        if(strstr($requestedUri, '/shop/')){
            if ($user && ( User::STATUS_ACTIVE != $user->getStatus())){
                $this->session->getFlashBag()->add('danger', $this->translator->trans( 'user.flash_connect_error',[]));
                $response = new RedirectResponse($this->router->generate('home'), 301);
                $event->setResponse($response);
            }
        }
    }
}