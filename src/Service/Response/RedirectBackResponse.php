<?php


namespace App\Service\Response;


use Symfony\Component\HttpFoundation\Request;

trait RedirectBackResponse
{
    public function redirectBack(Request $request)
    {
        $referer = $request->headers->get('referer');
        return $url = ($referer == NULL)? '/' :$referer;
    }
}
