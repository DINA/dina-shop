<?php

namespace App\Service\Mail;

use App\Entity\Distribution;
use App\Entity\Order;
use App\Entity\User;
use App\Service\Date\LocaleDate;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class AssoNotification
{
    use LocaleDate;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    private $dinaAsso;
    private $noreply;
    private $translator;

    public function __construct(MailerInterface $mailer, TranslatorInterface $translator, string $dinaAsso, string $noreply)
    {
        $this->mailer = $mailer;
        $this->dinaAsso = $dinaAsso;
        $this->noreply = $noreply;
        $this->translator = $translator;
    }

    public function mailOneFile(string $filePath, Distribution $distribution, string $locale='en')
    {
        if (file_exists($filePath)) {
            $localeDate = $this->setLongFormatLocale($distribution->getDeliveryDate(), $locale);
            $email = (new TemplatedEmail())
                ->from($this->noreply)
                ->to($this->dinaAsso)
                ->subject($this->translator->trans('send_sharing_proposal.subject', ['%name%' => $localeDate], 'emails'))
                ->htmlTemplate('emails/send_sharing_proposal.html.twig')
                ->attachFromPath($filePath)
                ->context([
                    /*  'user' => $user,
                    'id' => $user->getId(), */]);  

            $this->mailer->send($email);
        } else {
            throw new NotFoundHttpException('This file does not exist');
        }
    }

    public function mailFiles(string $folderpath, Distribution $distribution, string $locale = 'en')
    {
        if (file_exists($folderpath)) {
            $localeDate = $this->setLongFormatLocale($distribution->getDeliveryDate(), $locale);
            $email = (new TemplatedEmail())
                ->from($this->noreply)
                ->to($this->dinaAsso)
                ->subject($this->translator->trans('send_spreadsheets.subject', ['%name%' => $localeDate], 'emails'))
                ->htmlTemplate('emails/send_spreadsheets.html.twig')
                ->context([
                    /*  'user' => $user,
                    'id' => $user->getId(), */]);

            /* Attach each spreadsheet to the mail */
            $handle = opendir($folderpath);
            while (false !== $f = readdir($handle)) {
                if ($f != '.' && $f != '..') {
                    $filePath = "$folderpath/$f";
                    $email->attachFromPath($filePath);
                }
            }
            closedir($handle);

            $this->mailer->send($email);
        } else {
            throw new NotFoundHttpException('The directory does not exist');
        }
    }
}
