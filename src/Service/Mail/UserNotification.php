<?php

namespace App\Service\Mail;

use App\Entity\Order;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class UserNotification
{

    private $userRepository;
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    private $dinaMail;
    private $noreply;
    private $translator;

    public function __construct(UserRepository $userRepository, TranslatorInterface $translator, MailerInterface $mailer, string $dinaMail, string $noreply)
    {
        $this->userRepository = $userRepository;
        $this->mailer = $mailer;
        $this->dinaMail = $dinaMail;
        $this->noreply = $noreply;
        $this->translator = $translator;
    }

    public function editOrderNotification(Order $order)
    {
        $user = $order->getUser();  

        $email = (new TemplatedEmail())
            ->from(new Address($this->noreply, 'Dina Website'))
            ->to($user->getEmail())
            ->subject($this->translator->trans('edit_order.subject', [], 'emails'))
            ->htmlTemplate('emails/editOrder.html.twig')
            ->context([
                'user' => $user,
                'order' => $order
            ]);

        $this->mailer->send($email);
    }

    public function resetPasswordNotification(User $user)
    {
        $email = (new TemplatedEmail())
            ->from(new Address($this->noreply, 'Dina Website'))
            ->to($user->getEmail())
            ->subject($this->translator->trans('user.password.subject', [], 'emails'))
            ->htmlTemplate('emails/user/password_reset.html.twig')
            ->context([
                'user' => $user,
            ]);

        $this->mailer->send($email);
    }

    public function newUserNotification(User $user)
    {
        $email = (new TemplatedEmail())
            ->from(new Address($this->noreply, 'Dina Website'))
            ->to($user->getEmail())
            ->subject($this->translator->trans('user.new.subject', [], 'emails'))
            ->htmlTemplate('emails/user/newUser.html.twig')
            ->context([
                'user' => $user,
            ]);

        $this->mailer->send($email);
    }

    public function deleteUserNotification(User $user)
    {
        $email = (new TemplatedEmail())
            ->from(new Address($this->noreply, 'Dina Website'))
            ->to($user->getEmail())
            ->subject($this->translator->trans('user.delete.subject', [], 'emails'))
            ->htmlTemplate('emails/user/deleteUser.html.twig')
            ->context([
                'user' => $user,
            ]);

        $this->mailer->send($email);
    }

    public function editUserNotification(User $user)
    {
        $email = (new TemplatedEmail())
            ->from(new Address($this->noreply, 'Dina Website'))
            ->to($user->getEmail())
            ->subject($this->translator->trans('user.edit.subject', [], 'emails'))
            ->htmlTemplate('emails/user/editUser.html.twig')
            ->context([
                'user' => $user,
            ]);

        $this->mailer->send($email);
    }

    public function approveUserNotification(User $user)
    {
        $email = (new TemplatedEmail())
            ->from(new Address($this->noreply, 'Dina Website'))
            ->to($user->getEmail())
            ->subject($this->translator->trans('user.approve.subject', [], 'emails'))
            ->htmlTemplate('emails/user/approveUser.html.twig')
            ->context([
                'user' => $user,
            ]);

        $this->mailer->send($email);
    }

    public function opendDistributionNotification()
    {
        $users = $this->userRepository->getActiveUsers();
        foreach($users as $user){
            $email = (new TemplatedEmail())
            ->from(new Address($this->noreply, 'Dina Website'))
            ->to($user->getEmail())
            ->subject($this->translator->trans('user.distribution.open.subject', [], 'emails'))
            ->htmlTemplate('emails/user/distribution_opened.html.twig')
           ;
                
            $this->mailer->send($email);
        }
    }

    public function blockDistributionNotification()
    {
        $users = $this->userRepository->getActiveUsers();
        foreach ($users as $user) {
            $email = (new TemplatedEmail())
                ->from(new Address($this->noreply, 'Dina Website'))
                ->to($user->getEmail())
                ->subject($this->translator->trans('user.distribution.block.subject', [], 'emails'))
                ->htmlTemplate('emails/user/distribution_blocked.html.twig');

            $this->mailer->send($email);
        }
    }
}
