<?php

namespace App\Service\JsonReader\CatalogueDefinitions;

use ArrayAccess;

class Keys implements ArrayAccess
{
    private $strictComparison;
    private $mapping;
    private $alternativeMapping;

    public function offsetExists($offset)
    {
        return property_exists($this, $offset);
    }

    public function offsetGet($offset)
    {
        return $this->$offset;
    }

    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->$offset);
    }

    /**
     * Get the value of strictComparison
     */ 
    public function getStrictComparison()
    {
        return $this->strictComparison;
    }

    /**
     * Set the value of strictComparison
     *
     * @return  self
     */ 
    public function setStrictComparison($strictComparison)
    {
        $this->strictComparison = $strictComparison;

        return $this;
    }

    /**
     * Get the value of mapping
     */ 
    public function getMapping()
    {
        return $this->mapping;
    }

    /**
     * Set the value of mapping
     *
     * @return  self
     */ 
    public function setMapping($mapping)
    {
        $this->mapping = $mapping;

        return $this;
    }

    /**
     * Get the value of alternativeMapping
     */ 
    public function getAlternativeMapping()
    {
        return $this->alternativeMapping;
    }

    /**
     * Set the value of alternativeMapping
     *
     * @return  self
     */ 
    public function setAlternativeMapping($alternativeMapping)
    {
        $this->alternativeMapping = $alternativeMapping;

        return $this;
    }
}