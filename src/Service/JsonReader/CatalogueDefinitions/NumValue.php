<?php

namespace App\Service\JsonReader\CatalogueDefinitions;

use ArrayAccess;

class NumValue implements ArrayAccess
{
    private $integer;
    private $float;

    public function offsetExists($offset)
    {
        return property_exists($this, $offset);
    }

    public function offsetGet($offset)
    {
        return $this->$offset;
    }

    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->$offset);
    }

    /**
     * Get the value of integer
     */ 
    public function getInteger()
    {
        return $this->integer;
    }

    /**
     * Set the value of integer
     *
     * @return  self
     */ 
    public function setInteger($integer)
    {
        $this->integer = $integer;

        return $this;
    }

    /**
     * Get the value of float
     */ 
    public function getFloat()
    {
        return $this->float;
    }

    /**
     * Set the value of float
     *
     * @return  self
     */ 
    public function setFloat($float)
    {
        $this->float = $float;

        return $this;
    }
}