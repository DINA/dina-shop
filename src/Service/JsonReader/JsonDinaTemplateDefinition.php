<?php

namespace App\Service\JsonReader;

use ArrayAccess;

class JsonDinaTemplateDefinition implements ArrayAccess
{
    private $offsetLine;
    private $OrderKeysMapping;
    private $ArticleKeysMapping;


    public function offsetExists($offset)
    {
        return property_exists($this, $offset);
    }

    public function offsetGet($offset)
    {
        return $this->$offset;
    }

    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->$offset);
    }


    /**
     * Get the value of offsetLine
     */ 
    public function getOffsetLine()
    {
        return $this->offsetLine;
    }

    /**
     * Set the value of offsetLine
     *
     * @return  self
     */ 
    public function setOffsetLine($offsetLine)
    {
        $this->offsetLine = $offsetLine;

        return $this;
    }

    /**
     * Get the value of OrderKeysMapping
     */ 
    public function getOrderKeysMapping()
    {
        return $this->OrderKeysMapping;
    }

    /**
     * Set the value of OrderKeysMapping
     *
     * @return  self
     */ 
    public function setOrderKeysMapping($OrderKeysMapping)
    {
        $this->OrderKeysMapping = $OrderKeysMapping;

        return $this;
    }

    /**
     * Get the value of ArticleKeysMapping
     */ 
    public function getArticleKeysMapping()
    {
        return $this->ArticleKeysMapping;
    }

    /**
     * Set the value of ArticleKeysMapping
     *
     * @return  self
     */ 
    public function setArticleKeysMapping($ArticleKeysMapping)
    {
        $this->ArticleKeysMapping = $ArticleKeysMapping;

        return $this;
    }
}