<?php

namespace App\Service\JsonReader;

use Exception;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

abstract class JsonReader
{

    protected $JsonPath;
    protected $serializer;
    protected $jsonObject;

    /**
     * decodeJSON
     *
     * @param  mixed $parseRules
     * @return array|JsonCatalogueDefinition //all the catalogues types defined in the json file or a specific catalogue
     */
    public function decodeJSON(?string $arrayToRead = null)
    {
        $arraySelected = null;
        $json = file_get_contents($this->JsonPath);
        $jsonObject = $this->serializer->deserialize($json, $this->jsonObject, 'json', [
            AbstractNormalizer::ALLOW_EXTRA_ATTRIBUTES => true,
        ]);
        if (!is_null($arrayToRead)) {
            $arraySelected = $jsonObject[$arrayToRead];
            if (is_null($arraySelected)) {
                throw new Exception('Requested JSON array not found');
            }
        }
        return $arraySelected ?? $jsonObject;
    }
             
    /**
     * readJSON
     * obsolete since Serializer
     *
     * @return array
     */
    protected function readJSON(): array
    {
        $content = file_get_contents($this->JsonPath);
        $json = json_decode($content, true);
        if (is_null($json)) {
            throw new Exception('JSON file read error');
        }
        return $json;   
    }
}
