<?php

namespace App\Service\JsonReader;

use App\Service\JsonReader\CatalogueDefinitions\Keys;
use App\Service\JsonReader\CatalogueDefinitions\NumValue;
use ArrayAccess;

class JsonCatalogueDefinition implements ArrayAccess
{
    private $name;
    private $code;
    private $format;
    private $preOrder;
    private $referenceLineAttributes;
    private $keys;
    private $numericValuesToConvert;
    private $other;


    public function offsetExists($offset)
    {
        return property_exists($this, $offset);
    }

    public function offsetGet($offset)
    {
        return $this->$offset;
    }

    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->$offset);
    }


    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of code
     */ 
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of code
     *
     * @return  self
     */ 
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get the value of format
     */ 
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set the value of format
     *
     * @return  self
     */ 
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get the value of preOrder
     */ 
    public function getPreOrder()
    {
        return $this->preOrder;
    }

    /**
     * Set the value of preOrder
     *
     * @return  self
     */ 
    public function setPreOrder($preOrder)
    {
        $this->preOrder = $preOrder;

        return $this;
    }

    /**
     * Get the value of keys
     */ 
    public function getKeys(): Keys
    {
        return $this->keys;
    }

    /**
     * Set the value of keys
     *
     * @return  self
     */ 
    public function setKeys(Keys $keys)
    {
        $this->keys = $keys;

        return $this;
    }

    /**
     * Get the value of numericValuesToConvert
     */ 
    public function getNumericValuesToConvert(): NumValue
    {
        return $this->numericValuesToConvert;
    }

    /**
     * Set the value of numericValuesToConvert
     *
     * @return  self
     */ 
    public function setNumericValuesToConvert(NumValue $numericValuesToConvert)
    {
        $this->numericValuesToConvert = $numericValuesToConvert;

        return $this;
    }

    /**
     * Get the value of referenceLineAttributes
     */ 
    public function getReferenceLineAttributes()
    {
        return $this->referenceLineAttributes;
    }

    /**
     * Set the value of referenceLineAttributes
     *
     * @return  self
     */ 
    public function setReferenceLineAttributes($referenceLineAttributes)
    {
        $this->referenceLineAttributes = $referenceLineAttributes;

        return $this;
    }

    /**
     * Get the value of other
     */ 
    public function getOther()
    {
        return $this->other;
    }

    /**
     * Set the value of other
     *
     * @return  self
     */ 
    public function setOther($other)
    {
        $this->other = $other;

        return $this;
    }
}