<?php

namespace App\Service\DistributionStatusCheck;

use App\Entity\Catalogue;
use App\Entity\Distribution;
use App\Repository\DistributionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;



class DistributionStatusCheck
{
    private $entityManager;
    private $distributionRepository;
    
    public function __construct(EntityManagerInterface $entityManager, DistributionRepository $distributionRepository)
    {
        $this->entityManager = $entityManager;
        $this->distributionRepository = $distributionRepository;
    }

    public function checkLinkedDistribution(Catalogue $catalogue, ?bool $inProgress = true): ?array
    {
        $distributions = $catalogue->getDistributions();
        $list = [];
        foreach ($distributions as $distribution) {
            if(!$inProgress || $distribution->isDistributionInProgress())
                $list[] = $distribution->getId();
        }
        return $list;
    }
}
