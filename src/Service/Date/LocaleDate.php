<?php

namespace App\Service\Date;

use DateTime;

trait LocaleDate
{
    public function setLongFormatLocale(DateTime $date, string $locale='en'): string
    {
        if('fr' === $locale){
            setlocale(LC_TIME, "fr_FR");
        }
        /* Seems to be an bug with strftime, force to encode with utf8 to manage months with accent (ex: décembre) */
        return utf8_encode(strftime("%A %d %B %G", $date->getTimestamp()));
    }

    public function setShortFormatLocale(DateTime $date, string $locale = 'en'): string
    {
        if ('fr' === $locale) {
            setlocale(LC_TIME, "fr_FR");
        }
        /* Seems to be an bug with strftime, force to encode with utf8 to manage months with accent (ex: décembre) */
        return utf8_encode(strftime("%d %B", $date->getTimestamp()));
    }
}