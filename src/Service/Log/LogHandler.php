<?php

namespace App\Service\Log;

use App\Entity\Log;
use App\Repository\LogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class LogHandler
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;
    private $security;
    private $logRepository;

    /**
     * MonologDBHandler constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Security $security, LogRepository $logRepository)
    {
        $this->em = $em;
        $this->security = $security;
        $this->logRepository = $logRepository;
    }

    /**
     * Called when writing to our database
     * @param array $record
     */
    public function write(array $record)
    {
        $logEntry = new Log();
        $logEntry->setMessage($record['message']);
        $logEntry->setLevel($record['level']);
        $logEntry->setContext($record['context']);
        $logEntry->setAction($record['action']);
        $user = $this->security->getUser();
        $logEntry->setUser( ($user)? $user->getUsername() : 'nobody');

        $this->em->persist($logEntry);
        $this->em->flush();
        $this->cleanup();
    }

    private function cleanup()
    {
        $entries = $this->logRepository->olderThan2Months();
        foreach($entries as $entry){
            $this->em->remove($entry);
        }
        $this->em->flush();
    }
}