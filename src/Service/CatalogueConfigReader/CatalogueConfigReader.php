<?php

namespace App\Service\CatalogueConfigReader;

use App\Service\JsonReader\JsonReader;
use Symfony\Component\Serializer\SerializerInterface;

class CatalogueConfigReader extends JsonReader
{
    private $definitions = null;
    protected $serializer;
    protected $jsonObject;

    public function __construct(SerializerInterface $serializer)
    {
        $this->JsonPath =  dirname(dirname(__DIR__ )). '/Config/cataloguesDefinitions.json';
        $this->serializer = $serializer;
        $this->jsonObject = 'App\Service\JsonReader\JsonCatalogueDefinition[]';
    }

    /**
     * getCataloguesNames
     * Return an associative array key => names as values:
     *      "RelaisVert_Mercuriale_Viande" => "Relais Vert - Mercuriale Viande"
     *      "M86" => "Millésime 86"
     *      "LCA" => "LCA Huiles Essentielles"
     *      ...
     *
     * @param  mixed $entries
     * @return array
     */
    public function getCataloguesNames(?array $entries=null): array
    {
        return $this->getCatalogueValue('name', $entries);
    }

    /**
     * getCataloguesCodes
     * Return an associative array key => code as values:
     *
     * @return array
     */
    public function getCataloguesCodes(?array $entries = null): array
    {
        return $this->getCatalogueValue('code', $entries);
    }

    /**
     * getCataloguesFormats
     * Return an associative array key => format as values:
     *
     * @return array
     */
    public function getCataloguesFormats(?array $entries = null): array
    {
        return $this->getCatalogueValue('format', $entries);
    }

    /**
     * getCataloguesPreOrders
     * Return an associative array key => preOrder as values:
     *
     * @return array
     */
    public function getCataloguesPreOrders(?array $entries = null): array
    {
        return $this->getCatalogueValue('preOrder', $entries);
    }

    /**
     * getCataloguesOthers
     * Return an associative array key
     *
     * @return array
     */
    public function getCataloguesOthers(?array $entries = null): ?array
    {
        return $this->getCatalogueValue('other', $entries);
    }

    /**
     * getCatalogueKeysMapping
     * return like: 
     *      "reference" => "Reference"
     *      "designation" => "Designation"
     *      "brand" => "Marque"
     *      "packaging" => "Conditionnement"
     *      "inspection" => "Contrôle"
     *
     * @param  mixed $type
     * @return void
     */
    public function getCatalogueKeysMapping(string $type)
    {
        $definitions = $this->getCataloguesDefinition();
        return $definitions[$type]->getKeys()->getMapping();
    }
    
    /**
     * findCatalogueName
     *
     * @param  mixed $type
     * @return string
     */
    public function findCatalogueName(string $type): string
    {
        return $this->getCataloguesNames()[$type];
    }
    
    /**
     * findCatalogueCode
     *
     * @param  mixed $type
     * @return string
     */
    public function findCatalogueCode(string $type): string
    {
        return $this->getCataloguesCodes()[$type];
    }
    
    /**
     * findCatalogueFormat
     *
     * @param  mixed $type
     * @return string
     */
    public function findCatalogueFormat(string $type): string
    {
        return $this->getCataloguesFormats()[$type];
    }
    
    /**
     * findCataloguePreOrder
     *
     * @param  mixed $type
     * @return string
     */
    public function findCataloguePreOrder(string $type): string
    {
        return $this->getCataloguesPreOrders()[$type];
    }

    /**
     * findCatalogueOther
     *
     * @param  mixed $type
     * @return string
     */
    public function findCatalogueOthers(string $type): ?Array
    {
        return $this->getCataloguesOthers()[$type];
    }

    /**
     * getCataloguesDefinition
     *
     * @return array JsonCatalogueDefinition[]
     */
    public function getCataloguesDefinition(): array
    {
        $this->definitions = (!$this->definitions)? $this->decodeJSON() : $this->definitions;
        return $this->definitions;
    }

    /**
     * getJsonCatalogueDefinitionFomCatalogueEntities
     *
     * @param  \App\Entity\Catalogue[] $cataloguesEntities
     * @return array JsonCatalogueDefinition[]
     */
    public function getJsonCatalogueDefinitionFomCatalogueEntities($cataloguesEntities): array
    {
        $jsonArray = [];
        foreach($cataloguesEntities as $catalogueEntity){
            $jsonArray[$catalogueEntity->getType()] = $this->getCataloguesDefinition()[$catalogueEntity->getType()];
        }
        return $jsonArray;
    }


    /**
     * filterBy
     * Filter an array of JsonCatalogueDefinition[] by the key of the catalogue or an attribute
     * Exemple:
     *      'key' => 'RelaisVert_Mercuriale_FruitsLegumes',
     *      'format' => 'Format_DINA_Producer',
     *      'code' => 'LCA',
     *
     * @param  mixed $catalogues
     * @param  mixed $filter
     * @return array JsonCatalogueDefinition[]
     */
    public function filterBy(array $catalogues, array $filter): array
    {
        return array_filter($catalogues, function ($catalogueTypeContent, $catalogueTypeKey) use ($filter) {
            foreach($filter as $paramKey => $paramValue){
                if($paramKey == 'key'){
                    if (isset($filter['key']) && ($catalogueTypeKey === $filter['key'])) {
                        return  true;
                    }else{
                        continue;
                    }
                }
                if (isset($filter[$paramKey]) && ($catalogueTypeContent[$paramKey] === $filter[$paramKey])) {
                    return  true;
                }
            }
        }, ARRAY_FILTER_USE_BOTH);
    }

    private function getCatalogueValue(string $key, ?array $entries=null): array
    {
        $keys = [];
        $catalogues = $entries?? $this->getCataloguesDefinition();
        foreach ($catalogues as $catalogue => $value) {
            $keys[$catalogue] = $value[$key];
        }
        return $keys;
    }
}
