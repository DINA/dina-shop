<?php

namespace App\Service\DinaConfigReader;

use App\Service\JsonReader\JsonReader;
use Symfony\Component\Serializer\SerializerInterface;

class DinaConfigReader extends JsonReader
{
    protected $serializer;
    protected $jsonObject;

    public function __construct(SerializerInterface $serializer)
    {
        $this->JsonPath =  dirname(dirname(__DIR__)) . '/Config/DinaTemplate.json';
        $this->serializer = $serializer;
        $this->jsonObject = 'App\Service\JsonReader\JsonDinaTemplateDefinition[]';
    }
}