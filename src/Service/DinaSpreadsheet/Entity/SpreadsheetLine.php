<?php


namespace App\Service\DinaSpreadsheet\Entity;

class SpreadsheetLine
{
    private $lineNumber; //n° de ligne dans le feuile de calcul
    private $data = [];


    /**
     * Get the value of lineNumber
     */ 
    public function getLineNumber()
    {
        return $this->lineNumber;
    }

    /**
     * Set the value of lineNumber
     *
     * @return  self
     */ 
    public function setLineNumber($lineNumber)
    {
        $this->lineNumber = $lineNumber;

        return $this;
    }

    /**
     * Get the value of data
     */ 
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set the value of data
     *
     * @return  self
     */ 
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    public function addData($data)
    {
        $this->data = array_merge($this->data,$data);

        return $this;
    }
}
