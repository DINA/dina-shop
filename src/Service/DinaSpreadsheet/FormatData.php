<?php

namespace App\Service\DinaSpreadsheet;

use App\Entity\Order;
use App\Entity\Product;
use App\Entity\SharedProduct;
use App\Entity\SharingProposal;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use App\Service\DinaSpreadsheet\Entity\SpreadsheetLine;
use App\Service\JsonReader\JsonDinaTemplateDefinition;
use App\Service\ProductManager\Price;
use Exception;

class FormatData
{
    use Price;

    //Special Array for DINA spreadsheet
    const PRODUCTUNITDINA = [
        Product::PRODUCTUNIT_KG => 'kg',
        Product::PRODUCTUNIT_PARCEL => 'undefined',
        Product::PRODUCTUNIT_PIECE => 'pc'
    ];

    const ERROR_STRING = "/!\ ERROR /!\\";


    private $shareLineIndex;
    private $individualLineIndex;
    private $catalogueConfigReader;



    public function __construct(CatalogueConfigReader $catalogueConfigReader )
    {
        $this->catalogueConfigReader = $catalogueConfigReader;
    } 

    public function formatSheet(array $fileMapping, ?SharingProposal $sharingProposal=null, ?Order $order=null)
    {
        $this->shareLineIndex = 0;
        $this->individualLineIndex = 0;
        $spreadsheetLines = [];

        if($order){
            foreach ($order->getProduct() as $product) {
                $catFormat = $this->catalogueConfigReader->findCatalogueFormat($product->getItem()->getCatalogueId()->getType());
                $spreadsheetLine = new SpreadsheetLine();
                $sharedProduct = $this->searchForSharedProduct($product, $sharingProposal);
                if ($product->getOrderOrigin() === Product::ORDERORIGIN_SHARE) {
                    /* Generate the line in "sharing proposal format", but only if the product is ordered from sharing proposal */
                    $this->setSpreadsheetLine($spreadsheetLine, $fileMapping, $sharedProduct, $catFormat);
                }
                /* Add the whole individual command or only the quantity if sharedProduct (line already built with previous call )*/
                $spreadsheetLines[] = $this->setSpreadsheetLine($spreadsheetLine, $fileMapping, $product, $catFormat);
            }
        } else if ($sharingProposal){
            foreach ($sharingProposal->sortSharingProposal($sharingProposal->getSharedProducts()) as $sharedProduct) {
                $catFormat = $this->catalogueConfigReader->findCatalogueFormat($sharedProduct->getItem()->getCatalogueId()->getType());
                $spreadsheetLine = new SpreadsheetLine();
                $spreadsheetLines[] = $this->setSpreadsheetLine($spreadsheetLine, $fileMapping, $sharedProduct, $catFormat);
            }
        } else{
            throw new Exception('neither order nor sharing proposal to process');
        }
        return $spreadsheetLines;
    }

    private function setSpreadsheetLine(SpreadsheetLine $spreadsheetLine, array $fileMapping, $product, string $catFormat): SpreadsheetLine
    {
        $mapping = $this->getMappingforRow($fileMapping, $product, $catFormat);
        $data = $this->formatDataforRow($product, $mapping, $catFormat);
        $spreadsheetLine->addData($data);
        $this->index($product, $mapping, $spreadsheetLine);

        return $spreadsheetLine;
    }

    private function searchForSharedProduct(Product $product, SharingProposal $sharingProposal): ?SharedProduct
    {
        foreach($sharingProposal->getSharedProducts() as $sharedProduct){
            if (($sharedProduct->getItem()->getReference() === $product->getItem()->getReference())
                && ($sharedProduct->getItem()->getDesignation() === $product->getItem()->getDesignation())){
                    return $sharedProduct;
                }
        }
        return null;
    }


    /**
     * getMappingforRow
     *
     * @param  mixed $mapping
     * @param  Product|SharedProduct $product
     * @param  mixed $catFormat
     * @return void
     */
    private function getMappingforRow(array $mapping, $product, string $catFormat) : JsonDinaTemplateDefinition 
    {
        if($product instanceof SharedProduct){
            if (Product::ORDERORIGIN_SHARE === $product->getOrderOrigin()) {
                return $mapping['SharingProposition'];
            } 
        } else{
            if (Product::ORDERORIGIN_SHARE === $product->getOrderOrigin()) {
                return $mapping['IndividualSharingProposition'];
            } else if (Product::ORDERORIGIN_INDIVIDUAL === $product->getOrderOrigin()) {
                switch ($catFormat) {
                    case 'Format_RelaisVert_Catalogue_SecFrais':
                    case 'Format_RelaisVert_Mercuriale_FruitsLegumes':
                    case 'Format_RelaisVert_Mercuriale_Viande':
                        return $mapping['IndividualCommand_RelaisVert'];
                        break;
                    case 'Format_Alternative_Producer':
                    case 'Format_DINA_Producer':
                        return $mapping['IndividualCommand_Alternate_Producer'];
                        break;
                    default:
                        throw new Exception("TODO: à faire $catFormat");
                    break;
                };
            } else {
                throw new Exception("Unknown command type");
            }
        }
    }

    /**
     * formatDataforRow
     *
     * @param  Product|SharedProduct $product
     * @param  mixed $mapping
     * @param  mixed $catFormat
     * @return void
     */
    private function formatDataforRow($product, JsonDinaTemplateDefinition $mapping, $catFormat)
    {
        $data = [];
        if($mapping['ArticleKeysMapping']){
            foreach( $mapping['ArticleKeysMapping']as $key => $value){
                $method = 'get' . ucfirst($key);
                if (method_exists($product->getItem(), $method)) {
                    $data[$value] = $product->getItem()->$method();
                } elseif (method_exists($product, $method)) {
                    $data[$value] = $product->$method();
                } else {
                    $data[$value] = $this->$key($product, $catFormat);
                }
            }
        }
        foreach($mapping['OrderKeysMapping'] as $key => $value){
            $columns = explode(',', $value);
            foreach($columns as $column){
                $method = 'get'.ucfirst($key);
                if(method_exists($product->getItem(),$method)){
                    $data[$column] = $product->getItem()->$method();
                 } elseif (method_exists($product, $method)) {
                    $data[$column] = $product->$method();
                } else {
                    $data[$column] = $this->$key($product, $catFormat, $column);
                }
            }
        }
        return $data;
    }

    private function index( $product, JsonDinaTemplateDefinition $mapping, SpreadsheetLine $spreadsheetLine)
    {
        if($product instanceof Product)
        {
            if(Product::ORDERORIGIN_INDIVIDUAL === $product->getOrderOrigin()){
                $spreadsheetLine->setLineNumber($mapping['offsetLine']['first']+($this->individualLineIndex++));      
            }
        } else{
            $spreadsheetLine->setLineNumber($mapping['offsetLine']['first'] + ($this->shareLineIndex++));
        }
    }

    
    /**
     * setCommentString: column 'M'
     * common for both sections
     *
     * @param  mixed $product
     * @param  mixed $catFormat
     * @return string
     */
    private function setCommentString($product, string $catFormat): ?string
    {
        $str = null;
        //On met le code pour tous les types de producteurs sauf Relais vert
        if (('Format_Alternative_Producer' == $catFormat) || ('Format_DINA_Producer' == $catFormat)) {
            $code = $this->catalogueConfigReader->findCatalogueCode($product->getItem()->getCatalogueId()->getType());
            $str = $code;
        }
        //Si l'utilisateur a coché Précommande
        if (($product instanceof Product) && $product->getPreOrder()) {
            $str = $str.' Précommande';
        }
        //Pour la PP aucun doute possible on suit la config définie
        if (($product instanceof SharedProduct) && $this->catalogueConfigReader->findCataloguePreOrder($product->getItem()->getCatalogueId()->getType())) {
            $str = $str . ' Précommande';
        }
        //On rajoute le commentaire s'il y est.
        if (($product instanceof SharedProduct) && $product->getComment()) {
            $str = $str .' '. $product->getComment();
        }
        return $str?? null;
    }

    /**
     * setSubTypeString: column 'Q'
     * is called only for the sharing proposal generation
     *
     * @param  mixed $product
     * @return void
     */
    private function setSubTypeString(SharedProduct $product)
    {
        switch($product->getSubType()){
            case SharedProduct::SUBTYPE_SHARE:
                return 'A partager';
            break;
            case SharedProduct::SUBTYPE_WHEIGHT:
                return 'A peser';
            break;
            case SharedProduct::SUBTYPE_CHEESE:
                return 'Fromage';
            break;
            case SharedProduct::SUBTYPE_UNDEFINED:
            default:
                throw new Exception('Unknown subtype');
            break;
        }
    }

    /**
     * setIntegerPackingForSharingProposal: column 'H'
     * is called only for the sharing proposal generation
     *
     * @param  mixed $product
     * @param  mixed $catFormat
     * @return string
     */
    private function setIntegerPackingForSharingProposal(SharedProduct $product, $catFormat): ?string
    {
        if(Product::PRODUCTUNIT_PIECE == $product->getProductUnit()){
            return $product->getPieceNumber();
        }
        return $product->getItem()->getPacking();
    }

    /**
     * setIntegerPackagingForSharingProposal: column 'D'
     * is called only for the sharing proposal generation
     * 
     * managed cases (colonne poids):
     *  Réf	    Désignation	                    Marque	            Poids	Kg/pc
     *  11372	COMTÉ POINTE AOP	            FROMAGERIE PETITE	 2,5	kg
     *  BABO	BASILIC BOUQUET X10 CAT II			                        pc
     *  CALA	CAROTTE LAVEE CAT II		                         12 	kg
     *  BABI	BANANE REP DOM EQUITABLE CAT II		                 18,50 	kg
     *  BABO	BASILIC BOUQUET X10 CAT II			                        pc
     *  BABO	BASILIC BOUQUET X10 CAT II			                        pc 
     *  TOM	    Tomates mélange variétés	    COL	                 1	    kg
     *  OIG	    Oignon jaune et rouge mélangé	COL	                 1	    kg
     *  16126	MOZZARELLA DE BUFFLONNE	AMBROSI	                    125g	pc
     *  38381	LESSIVE POUDRE ZÉRO	ECOVER	                        1,2kg	pc
     *  26801	CHOCOLAT NOIR DESSERT PÂTISSIER 58%	KAOKA	        200g	pc
     *
     * @param  mixed $product
     * @param  mixed $catFormat
     * @return string
     */
    private function setIntegerPackagingForSharingProposal( $product, $catFormat): ?string
    {
        if(Product::PRODUCTUNIT_KG == $product->getProductUnit()){
            return $this->filterPackaging($product) ?? self::ERROR_STRING;
        } else{  
            /* Nothing to write for Mercuriale, but write packaging for others */
              return ($catFormat == 'Format_RelaisVert_Mercuriale_FruitsLegumes')? null :
            $product->getItem()->getPackaging();
        }
    }

    private function setUnitaryPriceForSharingProposal($product, $catFormat): string
    {
        return number_format($this->getUnitaryPrice($product), 2);
    }

    /**
     * setLabelString: column 'G'
     * is called only for the sharing proposal generation
     *
     * @param  mixed $product
     * @param  mixed $catFormat
     * @return string
     */
    private function setLabelString($product, string $catFormat): ?string
    {
        switch ($catFormat) {
            case 'Format_Alternative_Producer':
            case 'Format_RelaisVert_Catalogue_SecFrais':
                $value = $product->getItem()->getLabel();
                break;
            case 'Format_RelaisVert_Mercuriale_FruitsLegumes':
                $value = $product->getItem()->getInspection();
                break;
            default:
                $value = null;
                break;
        }
        return $value;
    }

    /**
     * setProductUnitDinaString: columns 'E,L'
     * is called only for the sharing proposal generation
     *
     * @param  mixed $product
     * @return string
     */
    private function setProductUnitDinaString($product): string
    {
        return self::PRODUCTUNITDINA[$product->getProductUnit()];
    }

    /************************** Individual section ***********************/

    /**
     * setKgString: columns 'E'
     * is called only for the individual command
     *
     * @param  mixed $product
     * @param  mixed $catFormat
     * @return string
     */
    private function setKgString(Product $product, string $catFormat): ?string
    {
        //return ($catFormat == 'Format_RelaisVert_Mercuriale_FruitsLegumes')? 'kg' : $product->getItem()->getInspection();
        if ($catFormat == 'Format_RelaisVert_Mercuriale_FruitsLegumes') {
            if (product::PRODUCTUNIT_PARCEL == $product->getProductUnit()) {
                $str = ($this->filterPackaging($product)) ? 'kg' : null;
            } else {
                /* Impossible case */
                $str = self::ERROR_STRING;
            }
        } else {
            $str = $product->getItem()->getInspection(); //j'ai un doute, pas packagign?
        }
        return $str;
    }

    /**
     * setPackaging: columns 'D'
     * is called only for the individual command
     * 
     *  Commande individuelle		Marque	Poids
     *  POEL	POMME ELSTAR CAT II		    13 	
     *  CORPO	CORIANDRE POT X6	
     * 
     * @param  mixed $product
     * @param  mixed $catFormat
     * @return string
     */
    private function setPackaging( $product, string $catFormat): ?string
    {
        if ($catFormat == 'Format_RelaisVert_Mercuriale_FruitsLegumes') {
            return $this->filterPackaging($product) ?? null;
        } else{
            return $product->getItem()->getPackaging();
        }
    }

    /**
     * setParcelQuantityString: column 'K'
     * is called only for the individual command
     *
     * @param  mixed $product
     * @param  mixed $catFormat
     * @param  mixed $column
     * @return string
     */
    private function setParcelQuantityString(Product $product, string $catFormat, string $column): ?string
    {
        if ($column == "K") {
            return (Product::PRODUCTUNIT_PIECE === $product->getProductUnit()) ? null : $product->getQuantityDec();
        } else {
            throw new Exception('Unknown column for parcel quantity');
        }
    }

    /**
     * setPieceQuantityString: column 'L'
     * is called only for the individual command
     *
     * @param  mixed $product
     * @param  mixed $catFormat
     * @param  mixed $column
     * @return string
     */
    private function setPieceQuantityString(Product $product, string $catFormat, string $column): ?string
    {
        if ($column == "L") {
            return (Product::PRODUCTUNIT_PIECE === $product->getProductUnit()) ? $product->getQuantityDec() : null;
        } else {
            throw new Exception('Unknown column for piece quantity');
        }
    }
    
    /**
     * filterPackaging
     * keep only the numeric value
     *
     * @param  mixed $product
     * @return string
     */
    /* Si on renvoie une valeur, c'est que c'est produit au kg OU c'est un colis de "sous type" kg (ex: une cagette de bananes) */
    private function filterPackaging($product): ?string
    {
        preg_match('#(^(env)?[ ]?([0-9]?[0-9]([.|,][0-9][0-9]?)?)[ ]*(kg[s]?|[K]{1})?)$#i', $product->getItem()->getPackaging(), $results);
        return (!empty($results))? $results[3] : null;
    }

    private function setNull()
    {
        return null;
    }

}