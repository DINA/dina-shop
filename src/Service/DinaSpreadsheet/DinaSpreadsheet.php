<?php


namespace App\Service\DinaSpreadsheet;

use App\Entity\Distribution;
use App\Entity\Order;
use App\Entity\SharingProposal;
use App\Entity\User;
use App\Service\Date\LocaleDate;
use App\Service\DinaConfigReader\DinaConfigReader;
use App\Service\HZip\HZip;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class DinaSpreadsheet
{
    use LocaleDate;

    private $params;
    private $dinaSpreadsheetRules;
    private $formatData;
    private $formatSpreadsheet;

    public function __construct(
        ParameterBagInterface $params,
        DinaConfigReader $dinaSpreadsheetRules,
        FormatData $formatData,
        FormatSpreadsheet $formatSpreadsheet
    ) {
        $this->params = $params;
        $this->dinaSpreadsheetRules = $dinaSpreadsheetRules;
        $this->formatData = $formatData;
        $this->formatSpreadsheet = $formatSpreadsheet;
    }

    public function generateSharingProposal(SharingProposal $sharingProposal): string
    {
        $datas = $this->formatSharingProposal($sharingProposal);
        return $this->generateSharingProposalForm($datas, $sharingProposal->getDistribution());
    }

    public function generateSingle(Order $order): string
    {
        $datas = $this->formatOrderFormForUser($order);
        return$this->generateOrderFormForUser($datas, $order->getUser());
    }
     
    public function generateBulk(Distribution $distribution): string
    {    
       foreach ($distribution->getOrders()  as $order) {
           $datas = $this->formatOrderFormForUser($order);
           $this->generateOrderFormForDistribution($datas, $order->getUser(), $distribution);
       }
        return dirname(dirname(dirname(__DIR__))) . '/' . $this->params->get('uploadPath').'/'.$distribution->getId();
    }

    public function zip(Distribution $distribution)
    {
        $uploadPath = dirname(dirname(dirname(__DIR__))) . '/' . $this->params->get('uploadPath');
        $distributionId = $distribution->getId();
        $date = $distribution->getDeliveryDate()->format('Ymd');
        $filepath = "$uploadPath/$date-$distributionId.zip";
        HZip::zipDir("$uploadPath/$distributionId", $filepath);
        return $filepath;
    }

    private function formatSharingProposal(SharingProposal $sharingProposal): ?array
    {
        $fileFormat = $this->dinaSpreadsheetRules->decodeJSON();
        return $this->formatData->formatSheet($fileFormat, $sharingProposal, null);
    }

    private function formatOrderFormForUser(Order $order): ?array
    {
        $fileFormat = $this->dinaSpreadsheetRules->decodeJSON();
        return $this->formatData->formatSheet($fileFormat, $order->getDistribution()->getSharingProposal(), $order);
    }

    /**
     * generateSharingProposalForm
     * generate the DINA form for verification purpose by the user itself
     *
     * @param  mixed $datas
     * @param  mixed $user
     * @return void
     */
    private function generateSharingProposalForm(array $datas, Distribution $distribution): string
    {
        $localeDate = $this->setLongFormatLocale($distribution->getDeliveryDate(), $this->params->get('locale'));
        $filename = 'Proposition de partage - ' . $localeDate;
        return $this->formatSpreadsheet->commit($datas, $filename);
    }

    /**
     * generateOrderFormForUser
     * generate the DINA form for verification purpose by the user itself
     *
     * @param  mixed $datas
     * @param  mixed $user
     * @return void
     */
    private function generateOrderFormForUser(array $datas, User $user): string
    {
        $filename = 'VerificationCommande_' . $user->getUsername();
        return $this->formatSpreadsheet->commit($datas, $filename);
    }

    /**
     * generateOrderFormForDistribution
     * generate the DINA forms for the DINA software: only storage
     *
     * @param  mixed $datas
     * @param  mixed $user
     * @return void
     */
    private function generateOrderFormForDistribution(array $datas, User $user, Distribution $distribution)
    {
        $localeDate = $this->setLongFormatLocale($distribution->getDeliveryDate(),$this->params->get('locale'));
        $filename = $distribution->getId() . '/'.$user->getUsername().' - commande - '.$localeDate;
        $this->formatSpreadsheet->commit($datas, $filename);
    }
}
