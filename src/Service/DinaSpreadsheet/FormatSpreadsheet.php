<?php

namespace App\Service\DinaSpreadsheet;

use App\Service\DinaSpreadsheet\Entity\SpreadsheetLine;
use App\Service\Spreadsheet\SpreadsheetInterface;
use Symfony\Component\HttpFoundation\File\Stream;

class FormatSpreadsheet
{
    private $XlsWriter;
    private $uploadPath;
    private $outputFileFormat;
    private $templateFilePath;
   
    public function __construct($uploadPath, $outputFileFormat, $templateFilePath , SpreadsheetInterface $XlsWriter)
    {
        $this->XlsWriter = $XlsWriter;
        $this->uploadPath =  $uploadPath;
        $this->outputFileFormat = $outputFileFormat;
        $this->templateFilePath = __DIR__ .'/'.$templateFilePath;
    }

    public function commit(array $lines, string $nameFileToWrite)
    {
        $this->initSettings($this->templateFilePath);

        foreach ($lines as $line) {
            $this->writeLine($line);
        }

        $dirname = dirname(dirname(dirname(__DIR__))) .'/'. $this->uploadPath . '/'.explode('/',$nameFileToWrite)[0];
        if (!file_exists($dirname)) {
            mkdir($dirname, 0777, true);
        }

        $filepath = dirname(dirname(dirname(__DIR__))).'/'.$this->uploadPath . '/' . $nameFileToWrite . $this->outputFileFormat;

        $this->saveIntoFile($filepath);
        return $filepath;
    }


    /**
     * initSettings
     *
     * @param  mixed $pathFile
     * @return void
     */
    private function initSettings(string $templateFilePath)
    {
        $this->XlsWriter->initSettings($templateFilePath);
    }

    private function writeLine(SpreadsheetLine $spreadsheetLine)
    {
        foreach ($spreadsheetLine->getData() as $column => $value) {
            $this->writeCell($column . $spreadsheetLine->getLineNumber(), $value);
        }
    }

    private function writeCell(string $coord, $data)
    {
        $this->XlsWriter->writeCell($coord, $data);
    }

    private function saveIntoFile(string $pathFile)
    {
        $this->XlsWriter->saveIntoFile($pathFile);
    }
}
