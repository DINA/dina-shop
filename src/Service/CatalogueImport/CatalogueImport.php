<?php

namespace App\Service\CatalogueImport;

use App\Entity\Catalogue;
use App\Service\Spreadsheet\CatalogueSpreadsheetReader;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr;
use Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CatalogueImport
{
    private $entityManager;
    private $spreadsheet;
    private $params;

    public function __construct(EntityManagerInterface $entityManager, CatalogueSpreadsheetReader $spreadsheet, ParameterBagInterface $params)
    {
        $this->entityManager = $entityManager;
        $this->spreadsheet = $spreadsheet;
        $this->params = $params;
    }

    public function import(Catalogue $catalogue )
    {
        $catId = $catalogue->getId();
        $filepath = $this->params->get('vich_uploader.mappings')['catalogue_sreadsheet']['upload_destination'] . '/' . $catalogue->getSpreadsheet();
        $items = $this->spreadsheet->extract($filepath, $catalogue->getType());
        //Flush items per 1000 in database
        $batchSize = 1000;
        $i = 0;
        $this->entityManager->beginTransaction(); // suspend auto-commit
        try{
            
            foreach ($items as $item) {
                $i++;
                $item->setCatalogueId($catalogue);
                $this->entityManager->persist($item);
                if (($i % $batchSize) === 0) {
                    $this->entityManager->flush();
                    $this->entityManager->clear(); // Detaches all objects from Doctrine!
                    /* /!\ WARNING! clear()operation clear ALL entites get via the entitymanager, $catalogue included!
                    So we set-again the catalogue value.
                    See also http://www.petegore.fr/2015/01/quand-entitymanagerclear-nettoie-un-peu-trop-bien/
                    */
                    $catalogue = $this->entityManager->getRepository(Catalogue::class)
                    ->findOneBy(['id' => $catId]);
                }
            }
            //Flush remaining items
            $this->entityManager->flush();
            $this->entityManager->getConnection()->commit();
        } catch(Exception $e){
            $this->entityManager->getConnection()->rollBack();
            throw $e;
        }
    }

    public function update(Catalogue $catalogue)
    {
        $this->delete($catalogue);
        $this->import($catalogue);
    }

    private function delete(Catalogue $catalogue)
    {
        $items = $catalogue->getItems();
        foreach($items as $item){
            $this->entityManager->remove($item);
        }
        $this->entityManager->flush();
    }
}
