<?php

namespace App\Service\Spreadsheet;

use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as WriterXlsx;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\Stream;

class PhpOfficeSpreadsheet implements SpreadsheetInterface
{
    private $activeSheet;
    private $spreadSheet;
 
    /**
     * initSettings
     *
     * @param  mixed $pathFile
     * @return void
     */
    public function initSettings(string $pathFile)
    {
        set_time_limit ( 60 );  //ODS file is longuer than XLS to import, set limit time to 60s instead of 30 by default
        $this->spreadSheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($pathFile);
        // Get the first sheet in the workbook
        $this->activeSheet = $this->spreadSheet->getSheet(0);
    }

    public function highestColumnIndex(): int
    {
        return Coordinate::columnIndexFromString($this->activeSheet->getHighestColumn());
    }

    public function highestRowIndex(): int
    {
        return $this->activeSheet->getHighestRow();
    }

    public function fillSpreadSheetArray(): array
    {
        return $this->activeSheet->toArray();
    }

    public function getCellValueByColumnAndRow(int $row, int $column)
    {
        return $this->activeSheet->getCellByColumnAndRow($row , $column)->getValue();
    }

    public function saveIntoFile(string $pathFile)
    {
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($this->spreadSheet, 'Xlsx');      
        $writer->save($pathFile);
    }

    public function writeCell(string $coord, $data)
    {
        $this->activeSheet->getCell($coord)->setValue($data);
    }


}
