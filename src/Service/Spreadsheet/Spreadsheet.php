<?php

namespace App\Service\Spreadsheet;

use App\Entity\Item;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;
use Exception;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

abstract class Spreadsheet
{
    const NO_REF_KEYS = 0x3300; /* Arbitrary code */
    const MISSING_KEYS = 0x3301;

    private $XlsReader;
    private $spreadSheetArray;
    private $activeSheet;
    private $referencesRow = [];
    private $highestColumnIndex;
    private $highestRowIndex;
    private $exceptionMessage;

    protected $validityCheckKeys = [];
    /**
     * parseRulesArray
     *
     * @var JsonCatalogueDefinition
     */
    protected $parseRulesArray = [];
    protected $catalogueConfigReader;

        
    /**
     * __construct
     *
     * @param  mixed $XlsReader
     * @return void
     */
    public function __construct(SpreadsheetInterface $XlsReader, CatalogueConfigReader $catalogueConfigReader)
    {
        $this->XlsReader = $XlsReader;
        $this->catalogueConfigReader = $catalogueConfigReader;
    }
 
       
    /**
     * extract
     *
     * @param  mixed $pathFile
     * @param  mixed $parseRules
     * @return array
     */
    public function extract(string $pathFile, string $catalogueType): array
    {
        $missing = $coordinates = [];
        $this->parseRulesArray = $this->catalogueConfigReader->decodeJSON($catalogueType);
        $this->initSettings($pathFile);
        $this->spreadSheetArray = $this->XlsReader->fillSpreadSheetArray(); //Read all file content

        /* If keys match, check if all keys match with the columns */
        if($columns = $this->getColumnsName($this->parseRulesArray->getKeys()->getStrictComparison(), $this->parseRulesArray->getKeys()->getMapping())){
            $coordinates = $this->findCoordinates($this->parseRulesArray->getKeys()->getMapping(), $columns);
        } 
        /* elseif alternative keys are defined AND match, check if all keys match withthe columns */
        elseif($this->parseRulesArray->getKeys()->getAlternativeMapping()
            && ($columns = $this->getColumnsName($this->parseRulesArray->getKeys()->getStrictComparison(), $this->parseRulesArray->getKeys()->getAlternativeMapping()))
            ){
            $coordinates = $this->findCoordinates($this->parseRulesArray->getKeys()->getAlternativeMapping(), $columns);
        } 
        /* Reference keys not found */
        else{
            $missingString = '';
            /* get missing keys */
            foreach ($this->exceptionMessage as $message) {
                $missingString = $message.' | '.$missingString;
            }
            throw new Exception($missingString, self::NO_REF_KEYS);
        }
        
        /* Build items list to insert in DB */
        for ($i = $this->referencesRow[0]+1; $i < $this->highestRowIndex+1; $i ++) {
            $data = $this->spreadSheetArray[$i-1];
            //dump($data);
             $item = $this->fetchData($coordinates, $data);
             //Trim strings
             $this->trimItem($item);
             //Convert some strings to int/float
             $this->strToDecimal($item);
             if(!$this->isValid($i, $item)){
                continue;
             }
            $items[] = $this->hydrateItem($item);
             //$items[] = $item;
         }
         return $items;
    }
    
    /**
     * findCoordinates
     * Mandatory keys have been found in the sheet, now check all the keys to have a 100% match
     *
     * @return void
     */
    private function findCoordinates(array $mapping, array $columns): array
    {
        $coordinates = $this->matchColumns($mapping, $columns, $missing);
        if (is_null($coordinates)) {
            $missingString = '';
            /* get missing keys */
            foreach ($missing as $key => $value) {
                $missingString = '\'' . $mapping[$value] . '\' - ' . $missingString;
            }
            throw new Exception($missingString, self::MISSING_KEYS);
        }
        return $coordinates;
    }
    


    /**
     * initSettings
     *
     * @param  mixed $pathFile
     * @return void
     */
    private function initSettings(string $pathFile)
    {
        $this->XlsReader->initSettings($pathFile);
        $this->highestColumnIndex = $this->XlsReader->highestColumnIndex();
        $this->highestRowIndex = $this->XlsReader->highestRowIndex();
    }

    
    /**
     * isValid
     * row must not be a "reference row" and must contains all mandatory keys
     *
     * @param  mixed $row
     * @param  mixed $item
     * @param  mixed $keys
     * @return bool
     */
    private function isValid(int $row, array $item): bool
    {
        foreach($this->validityCheckKeys as $key){
            if(empty($item[$key])){
                return false;
            }
        }
        if(in_array($row, $this->referencesRow)){
            return false;
        }    
        return true;     
    }
    
    /**
     * trimItem
     *
     * @param  mixed $fields
     * @return void
     */
    private function trimItem(array &$fields)
    {
        foreach($fields as &$field){
            if(isset($field)){
                $field = trim($field);
            }
        }
    }


    private function strToDecimal(array &$fields)
    {
        foreach($fields as $key => &$value){
            if(in_array($key, $this->parseRulesArray['numericValuesToConvert']['float'])){
                $value = str_replace(",", ".", $value); // replace ',' with '.'
                $value = floatval($value);
            }
            if(in_array($key, $this->parseRulesArray['numericValuesToConvert']['integer'])){
                $value = intval($value);
            }
        }
    }


        
    /**
     * fetchData
     * fetch the requested datas from the array
     *
     * @param  mixed $coordinates
     * @param  mixed $data
     * @return array
     */

    private function fetchData(array $coordinates, array  $data): array
    {
        //$data = $this->spreadSheetArray[12];
        //dump($coordinates, $data);
        return array_map(function($coordinate)use($data){
            return (isset($coordinate))? $data[$coordinate] : null;
        }, $coordinates);
    }

        
    /**
     * matchColumns
     * Return the corresponding column position for each key of $match
     *
     * @param  mixed $match
     * @param  mixed $columns
     * @return array
     */
    private function matchColumns(array $match, array $columns, ?array &$missing): ?array
    {
        $coordinates = array_map(function($field) use ($columns){
                return (isset($field))? array_search(strtolower($field), array_map('strtolower', $columns)) : null;
        }, $match);
        $copy = $coordinates;
        /* Crapy code, to rewrite */
        $missing = array_reduce($coordinates, function($carry, $column) use (&$copy){
            if($column === false){
             $value = array_search(false,$copy, true);
              $carry[] = $value;
             unset($copy[$value]);
            }
            return $carry;
        });
        return in_array(false, $coordinates, true)? NULL : $coordinates;
    }
    
    /**
     * getColumnsName
     * Althought a sheet can have several times a fields definition, the only valid definition for the fields is the first declaration
     *
     * @return array
     */
    private function getColumnsName(bool $strictComparison, $mapping): ?array
    {
        //dump($this->findFields());
        try{
            $rawValues = array_values($this->spreadSheetArray[$this->findFields($mapping)[0]-1]); //-1 car le tableau commence à  0 contrairement à la feuille
        } catch(Exception $e){
            $this->exceptionMessage[] = $e->getMessage();
            return null;
        }
        return array_map(function($rawColumnName) use ($strictComparison){
            if(!$strictComparison){
                /* Remove any "month year" reference from the column title in the file */
                $columnWoDate = preg_replace('#(janvier|janv|février|fevrier|fev|mars|avril|avr|mai|juin|juillet|juill|août|aout|septembre|sept|octobre|oct|novembre|nov|décembre|decembre|dec) \d{4}#i', ' ', $rawColumnName);
                /* Remove 2020, 01/2020, 01/01/2020. Works with separators '/','.' and '-' */
                $columnWoDate = preg_replace('/(\d{0,2}[\.\/\-]*\d{0,2}[\.\/\-]*\d{4})/', ' ', $columnWoDate);
            }
            /* Trim the column in the file and replace any whitespace characters by a single space */
            $column = trim(preg_replace('/\s\s*/', ' ', $columnWoDate?? $rawColumnName));
            return $column;
        }, $rawValues);
    }

    
    /**
     * findFields
     * Determine the rows where fields are defined in the sheet
     * A sheet can have several fields definition
     * ex: Reference	Designation	Marque	Conditionnement	Contrôle	Origine	Label	Colisage	Prix Unitaire ...
     *
     * @return array
     */
    private function findFields(array $mapping): array
    {
        if(empty($this->referencesRow)){
            /* get attributes defined as mandatory */
            $referenceLineAttributes = $this->parseRulesArray->getReferenceLineAttributes();
            /* get the corresponding string */
            $referenceLineKeys = array_map(function($index) use($mapping){
                return $mapping[$index];
            }, $referenceLineAttributes);
            
            $this->findRows($referenceLineKeys);

            if(empty($this->referencesRow)){
                $missingString = '';
                /* get missing keys */
                foreach ($referenceLineKeys as $key => $value) {
                    $missingString = '\''.$value.'\' - '.$missingString;
                }
                throw new Exception($missingString, self::NO_REF_KEYS);
            }
        }
        return $this->referencesRow;
    }

    private function findRows(array $referenceLineKeys): void
    {
        for ($row = 1; $row < $this->highestRowIndex; $row++) {
            $buildRow = [];
            for ($col = 1; $col < $this->highestColumnIndex; $col++) {
                $value = $this->XlsReader->getCellValueByColumnAndRow($col, $row);
                //dump($value);
                $buildRow[] =  trim($value);
            }

            //If all mandatory keys are matched in the line
            $diff = array_diff($referenceLineKeys, $buildRow);
            //dump($buildRow, $diff);
            if (empty($diff)) {
                $this->referencesRow[] = $row;
            }
        }
    }

}