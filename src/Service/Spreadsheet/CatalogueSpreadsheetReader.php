<?php

namespace App\Service\Spreadsheet;

use App\Entity\Item;
use App\Service\CatalogueConfigReader\CatalogueConfigReader;

class CatalogueSpreadsheetReader extends Spreadsheet
{

    /**
     * validityCheckKeys
     * Defines which fields are mandatory to approve an entry
     *
     * @var array
     */
    protected $validityCheckKeys = [
        'designation', 'reference', 'unitaryPrice'
    ];
    protected $parseRulesArray = [];

    public function __construct(SpreadsheetInterface $XlsReader, CatalogueConfigReader $catalogueConfigReader)
    {
        $this->catalogueConfigReader = $catalogueConfigReader;
        parent::__construct($XlsReader,  $catalogueConfigReader);
    }


    public function getCataloguesTypes()
    {
        return $this->catalogueConfigReader->getCataloguesNames();
    }

    public function hydrateItem($data): Item
    {
        $item = new Item ();
        foreach ($data as $attribut => $value) {
            $method = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $attribut)));
            if (is_callable(array($item, $method))) {
                $item->$method($value);
            }
        }
        return $item;
    }
}
