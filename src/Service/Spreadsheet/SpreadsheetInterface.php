<?php

namespace App\Service\Spreadsheet;


interface SpreadsheetInterface
{
    public function initSettings(string $pathFile);
    public function highestColumnIndex(): int;
    public function highestRowIndex(): int;
    public function fillSpreadSheetArray(): array;
    public function getCellValueByColumnAndRow(int $row, int $column);
    public function saveIntoFile(string $pathFile);
    public function writeCell(string $coord, $data);
}