# cataloguesDefinitions.json

## Introduction

Le fichier `CataloguesDefinitions.json` sert de base de données concernant les formats de catalogues que sait gérer le site. Il est utilisé à la fois lors de l'importation des catalogues au format tableur (`.ods, .xls, .xlsx`) dans la base de donnée mais aussi pour indiquer quelles colonnes doivent être affichées dans les "vues" catalogues.

La classe `App\Service\CatalogueConfigReader\CatalogueConfigReader` sert de point d'entrée pour lire et extraire les informations de ce fichier; Les informations sont renvoyées sous la forme de l'objet `App\Service\JsonReader\JsonCatalogueDefinition` qui implémente la classe `ArrayAccess` pour pouvoir parcourir l'objet comme un tableau. 
**Important**: Dans les 1ères versions du site le fichier `.json` était décodé comme un simple tableau, la dé-sérialisation dans l'objet `JsonCatalogueDefinition` est apparue ultérieurement, d'où un code parfois "mixé" entre des appels de getters et une utilisation en tableau.

## Structure d'un catalogue

Exemple de format de catalogue:
```
"RelaisVert_Mercuriale_FruitsLegumes":{
        "name": "Relais Vert - Mercuriale Fruits & Legumes",
        "code": "RV",
        "format": "Format_RelaisVert_Mercuriale_FruitsLegumes",
        "preOrder": false,
        "referenceLineAttributes": [
            "reference",
            "designation"
        ],
        "keys": {
            "strictComparison": true,
            "mapping": {
                "reference": "ExRéférence",
                "designation": "ExDésignation",
                ...,
                "family": "ExSecteur"
            },
            "alternativeMapping": {
                "reference": "Ref",
                "designation": "Désignation",
               ...,
                "comment": "Remarque"
            }
        },
        "numericValuesToConvert":{
            "integer": [],
            "float": ["unitaryPrice"]
        }
    },
```
### La clé principale
La clé, dans cet exemple nommée `RelaisVert_Mercuriale_FruitsLegumes` défini le **type** de catalogue. Cette clé est utilisée par l'attribut `type` de la classe `App\Entity\Catalogue`. Elle est unique et permet de distinguer les différents types de catalogues existant.
**Important**: Ne pas modifier les clés existantes!

### name
Ce champ indique le nom "humainement lisible" de ce type de catalogue, tel qui sera proposé dans le menu déroulant lors de la création d'un catalogue. 
**Important**: Dans le code il ne faut pas confondre le `name` ici présenté avec l'attribut `name` de `App\Entity\Catalogue` qui lui définit le nom que donne l'utilisateur à son catalogue (ex: Catatalogue Sec Octobre 2020)
**Également très important**: Ce champ est utilisé lors de la validation d'un import de catalogue d'un producteur DINA. On vérifie que le nom du fichier tableur contient la valeur de `name`. Exemple: On vérifie que la chaîne "Coline" est présente dans le fichier "Suggestions de COline Léger". Les catalogues `Format_DINA_Producer` (voir champ `format`) étant idéntiques, on veut pouvoir contrôler que l'utilisateur n'importe pas les "suggestions de Laurence" dans le type "Coline".

### code
Pour éviter les erreurs de saisies, il existe un couplage très fort entre un producteur et ses catalogues. Ce champ `code` permet de lier le type de catalogue à un producteur particulier. Le choix du code est lié au code utilisé par l'automate.

### format
Ce champ était utilisé à l'origine uniqement pour aider au formatage des bons de commande des membres. Il est utile pour "regrouper" les catalogues par catégories et pouvoir en déduire certaines règles de formatage communes.  Ce champ est désormais également utilisé pour appliquer certaines règles de validation lors de la création d'un catalogue. 

Il existe 5 valeurs possible: 
- Format_RelaisVert_Catalogue_SecFrais: **Valeur Réservée, ne pas réutiliser**
- Format_RelaisVert_Mercuriale_FruitsLegumes: **Valeur Réservée, ne pas réutiliser**
- Format_RelaisVert_Mercuriale_Viande: **Valeur Réservée, ne pas réutiliser**
- Format_DINA_Producer: Utiliser cette valeur pour un producteur utilisant un catalogue au format "DINA" (ie: proposition de partage) comme Coline, Laurence ou Katia.
- Format_Alternative_Producer: Utiliser cette valeur pour un producteur avec un format de catalogue autre que DINA, comme LCA ou Millésime 86.

Toute autre valeur ne serait pas prise en compte sans modification du code source.

### preOrder
Permet de précocher la case "Précommande" dans les bons de commande des membres. À définir en fonction dès règles de commande du fournisseur.

### referenceLineAttributes
Ce tableau indique quels sont les mots-clés recherchés lors de l'importation d'un fichier tableur pour déterminer la "ligne de référence" qui contient les définitions des colonnes. Les mots-clés sont récupérés depuis l'objet `keys.mapping`. Voir la section "Parsage du catalogue" ci-dessous pour plus de détails.
Il est conseillé de laisser les valeurs `"referenceLineAttributes": ["reference","designation"]` pour tout les catalogues.

### keys
#### mapping
Cet objet indique dans quel attribut de l'entité `App\Entity\Item` mettre les valeurs des colonnes du fichier tableur. 
##### Les attributs disponibles
Les attributs disponibles sont les suivants: 
- `reference`: Référence 
- `designation`: Désignation
- `brand`:   Marque
- `packaging`: Conditionnement. **Attention** , dans certains catalogues le conditionnement est utilisé en lieu et place du colisage! 
- `inspection`: indique que l'article a été contrôle (en réalité, colonne utilisé par Relais Vert)
- `origin`: Pays ou région d'origine
- `label`: Label (Bio Solidaire, équitable, Demeter, etc...)
- `unitaryPrice`: Prix Unitaire Hors Taxe
- `vat`: TVA
- `family`: Famille de l'article (fromage, charcuterie, etc...)
- `packing`: Colisage, c'est à dire le nombre de pièces dans le colis
- `minPacking`: Colisage minimum, indique le nombre de pièces minimum à commander lors d'un décolisage
- `unpackedPrice`: Prix unitaire quand un article est décolisé
- `calibre_categ`: Champ un peu fourre tout utilisé dans la mercuriale fruits & légumes de Relais Vert. Indique parfois le nombre de pièces dans la cagette.
- `comment`: Commentaire
- `ed` : Date limite de consommation
- `EAN13`: Code barre
- `SKU`: "Stock keeping Unit" ou "UVC/UV" en français, indique le conditionnement du colis. Utilisé par le site pour les "producteurs DINA" (Coline, Laurence, ....) pour déterminer si le produit est à la pièce ou au Kg. Chez Relais Vert la valeur est quelque peu farfelue donc je ne l'utilise pas.
- `customField1`: Champ libre
- `customField2`: Champ libre
- `customField3`: Champ libre
- `customField4`: Champ libre

##### Note sur les attributs
3 attributs doivent êtres obligatoirement renseignés dans tous les cas:
- `reference`
- `designation`
- `unitaryPrice`

Les attributs suivants sont nécessaires pour certains formats de catalogues:
- `sku` est obligatoire pour les producteur DINA.
- `packaging` est obligatoire pour les catalogues Relais Vert
- `calibre_categ` est obligatoire pour `RelaisVert_Mercuriale_FruitsLegumes`

Il existe 3 attributs qui sont utilisés pour les filtres de recherche:
- `origin`: Filtre "Origine"
- `brand`: Filtre "Marque"
- `family`: Filtre "Famille"

Les autres attributs sont facultatifs et sont utilisés seulement à des fins d'affichage. Si un attribut n'est pas utile parce qu'il ne correspond à rien dans le catalogue du producteur, il suffit de ne pas le mentionner (ou de laisser sa valeur à `null`).

Enfin, il existe 4 attributs génériques qui peuvent être utilisés pour stocker des informations qui ne conviendraient pas aux autres atributs (exemple: vin rouge/blanc, etc...)
- `customField1`
- `customField2`
- `customField3`
- `customField4`

#### strictComparison
Certains catalogues comportent des dates dans leur intitulé de colonne (exemple: Références 2019). Si `strictComparison` est à `true`, la comparaison entre la valeur dans le fichier tableur et la valeur renseignée dans le .json est faite à l'identique, excepté les whitespaces qui sont retirés dans tous les cas. Cependant, cela peut être pénible de devoir modifier le fichier de configuration chaque année... Si le paramètres est à `false`, alors la comparaison se fait en supprimant les dates potentielles. Exemple: "Références 2019" devient "Références" lors de la comparaison.

#### alternativeMapping
Cet objet permet de renseigner un 2ème mapping pour un même type de catalogue. En pratique, cet objet n'est utilisé que pour la mercuriale de Relais Vert qui n'a pas le même format entre la version pdf et la version excel...

### numericValuesToConvert
Ce champ permet de renseigner les attributs qui doivent contenir des entiers ou des flottants. La conversion est faite lors de l'importation

## Ajouter un producteur et/ou un de format de catalogue

Les formats catalogues sont liés aux producteurs. **Un nouveau producteur entraîne systématiquement l'ajout d'un format de catalogue.**

### Ajouter un nouveau producteur avec un format de catalogue déjà existant
Ce cas concerne surtout l'ajout d'un nouveau producteur DINA. Il faut alors copier-coller le format déjà existant (prendre exemple sur Coline) et changer les champs suivants:
- Clé principale
- `name`: Renseigner un nom humainement parlant pour les membres
- `code`: Utiliser le même code que l'automate

### Ajouter un nouveau format de catalogue
Ce cas concerne l'ajout d'un producteur qui aurait un catalogue déjà existant. Dans ce cas, copier-coller un catalogue de format `Format_Alternative_Producer` et l'apdapter à ses besoins.

## Supprimer un format de catalogue
**/!\ Aucun test n'a été réalisé**. Celà ne devrait (en théorie) pas poser de problèmes à condition de bien archiver tous les catalogues de ce type au préalable.


# Analyse syntaxique des catalogues

L'analyse se fait en 2 temps:

## Recherche des lignes de référence
Une ligne de référence est une ligne qui contient le nom des colonnes du catalogue. Exemple: 
> "Reference | Designation | Marque | Conditionnement | Contrôle | Origine | etc..."

Pour déterminer qu'une ligne est une ligne de référence, il faut que toutes les valeurs renseignées dans `referenceLineAttributes` de `cataloguesDefinitions.json` soit trouvées sur une même ligne.

Cette recherche est effectuée par la fonction `findFields` de `App\Service\Spreadsheet\Spreadsheet.php` dans l'intégralité de la feuile tableur. On note alors le n° de ligne (dans la feuille tableaur) de la ligne de référence. **Important**: une ligne de référence peut être présente plusieurs fois par fichier, notamment dans le cas d'une conversion pdf -> xls.

Si aucune ligne de référence est trouvée, une exception est levée. Le fichier importé ne correspond probablement pas au type sélectionné.

## Match des colonnes

Une fois la ou les lignes de références touvées, il faut s'asurer que tous les champs renseignés dans l'objet `mapping` sont bien présents. C'est la méthode `findCoordinates` qui va:
- s'assurer que toutes les colonnes attendues sont bien présentes. Autrement une exception est levée
- construire une carte des coordonnées pour savoir que "Référence" est dans la colonne "C", "Prix Unitaire" dans la colonne "F", etc....
**/!\ Important**: On considère que la position des colonnes (dans le cas où il y aurait plusieurs lignes de référence) ne ne change pas d'une ligne de référence à l'autre. Exemple: Si la colonne "Référence" est dans la colonne "C", on considère qu'elle sera toujours à la colonne "C".

## Importation des valeurs
Une fois les coordonnées des colonnes obtenues, on commence à lire les données à partir de la 1ère ligne de référence trouvée et remplir nos objets `Item` jusqu'à la fin du fichier.


# DinaTemplate.json

Ce fichier peut être vu comme un fichier de grammaire qui décrit comment remplir les bons de commande des membres. Il est bien évidémment étroitement lié au fonctionnement de l'automate qui dicte les règles à suivre. La génération des bons de commande est effectuée par `App\Service\DinaSpreadsheet\FormatData`.

## Structure des objets

Les objets décrivent comment remplir les sections suivantes:
- `SharingProposition`:  "proposition de partage" excepté la quantité commandée par les membres.
- `IndividualSharingProposition`: quantités commandées par les membres dans la proposition de partage
- `IndividualCommand_RelaisVert`:  concerne les articles commandés chez Relais Vert 
- `IndividualCommand_Alternate_Producer`: concerne les articles commandés ailleurs que chez Relais vert (en fait, la seule différence avec `IndividualCommand_RelaisVert` est qu'on ne peut décoliser des articles)

Exemple:
```
"SharingProposition":{
        "offsetLine":{
            "first": 3,
            "last": 42
        },
        "OrderKeysMapping":{
            "setProductUnitDinaString": "E,L",
            "setSubTypeString": "Q",
            "setCommentString": "M"
        },
        "ArticleKeysMapping":{
            "reference": "A",
            "designation": "B",
            "brand": "C",
            "setIntegerPackagingForSharingProposal": "D",
            "origin": "F",
            "setLabelString": "G",
            "setIntegerPackingForSharingProposal": "H",
            "setUnitaryPriceForSharingProposal": "I",
            "vat": "J"
        }
    },
```

### offsetLine
Cet objet décrit où (quelle ligne du fichier) commence et où fini la section concernée.

### OrderKeysMapping
Cet objet dérit les informations à renseigner concernant la commande (quantité, commentaire, ...)
- Le champ (exemple `setProductUnitDinaString`) indique quelle méthode appeler dans `App\Service\DinaSpreadsheet\FormatData` pour renseigner la valeur attendue. 
- La valeur (exemple `E,L`) indique quelles sont les colonnes à remplir

### ArticleKeysMapping
Cet objet dérit les informations à renseigner concernant l'article commandé 
- Le champ indique quelle méthode appeler, avec 3 options possibles:
    - en priorité, l'attribut de l'objet `Item` (exemple: "reference": "A", appellera la méthode `getReference()`)
    - si la précédente méthode n'éxiste pas, la méthode de l'objet `Product`
    - si la précédente méthode n'éxiste pas, la méthode de la classe `FormatData`
- La valeur (exemple `A`) indique quelle est la colonne à remplir



