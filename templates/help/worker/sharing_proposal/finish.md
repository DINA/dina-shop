# Finaliser la proposition de partage

La distribution est désormais dan l'état "Paratage effectué" comme vous l'indique le bandeau vert en haut de l'écran:
![Finalisation](/media/help/worker/sharing_proposal/finish.png "Finalisation")

Vous devez ensuite:
- vérifier la feuille excel générée par le site: Bouton "vérifier la proposition"
- si vous êtes satisfait:
    - insérer cette proposition dans l'automate: (vous pouvez envoyer par mail la proposition sur l'adresse mail de DINA)
    - ouvrir la vente: Bouton "ouvrir la vente"
- Sinon vous pouvez modifier la proposition en cliquant sur "modifier"