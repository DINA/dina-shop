# Erreur sur le formulaire de la proposition de partage

Certaines erreurs peuvent vous empêcher de valider le formulaire de proposition. En voici la liste.

<video width="800" controls>
  <source src="/media/help/worker/general/errors.mkv">
</video>

## Erreurs de votre part

### Des pièces dans un produit au kilo
Vous ne pouvez pas mettre de pièces dans un produit au kg
![erreur1](/media/help/worker/sharing_proposal/errors/kg_piece.png "erreur1")

### Pas de pièces dans un produit à la pièce
Vous devez renseigner le champ pièce pour un produit à la pièce
![erreur2](/media/help/worker/sharing_proposal/errors/no_piece.png "erreur2")

### Trop de pièces
Vous ne pouvez pas mettre plus de 99 pièces pour un produit
![erreur3](/media/help/worker/sharing_proposal/errors/100pieces.png "erreur3")

### Du fromage à la pièce
Le champ formage ne peut être coché QUE sur un produit au kilo
![erreur4](/media/help/worker/sharing_proposal/errors/cheese_piece.png "erreur4")

## Erreurs catalogues
Certaines erreurs sont dûes directement au catalogue. Dans ce cas, **il vous faut modifier l'article dans le catalogue** (voir aide correspondante) avant de pouvoir valider le formulaire. Ceci est très important, sinon la génération automatique de la feuille excel sera entachée d'erreurs et provoquera des erreurs au niveau de l'automate.

## Mauvaise détection du type
Ici, la colonne conditionnement a été mal remplie, le site pense que les bananes sont des pièces et il met une erreur.
![erreur_cond1](/media/help/worker/sharing_proposal/errors/erreur_conditionnement1.png "erreur_cond1").
- Vous pouvez sélectionner à la main le champ `Unité` et le positionner sur `kg`, cela fonctionnera sans problèmes. Cependant, l'erreur réapparaitra tant que le catalogue n'a pas été corrigé.
- Il est donc préférable de corriger la valeur "conditionnement" en allant modifier le produit directement.

## Mauvais formatage 
Cette erreur est une variante de la précédente, mais cette fois il n'y a pas de contournement possible: **vous devez modifier le conditionnement de l'article** pour valider le formulaire. Pour cela se rendre dans `Gestion des Articles ↳ Édition des articles`, sélectionner le produit concerné et modifer son conditionnement. **La valeur doit être exprimée en kg et non en grammes**.
![erreur_cond2](/media/help/worker/sharing_proposal/errors/erreur_conditionnement2.png "erreur_cond2").

