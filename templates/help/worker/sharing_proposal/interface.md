# Formulaire de la proposition de partage

Le formulaire de la proposition de partage ce présente comme ceci:
![Formulaire](/media/help/worker/sharing_proposal/formulaire.png "Formulaire")
Il faut globalement le lire de la même manière que la feuille excel remplie à la main

## Les unités
Il faut choisir entre `pièce` et `kg`. Automatiquement le site essaie de déterminer l'unité à utiliser, mais vous devez néanmoins la vérifier avant de valider.
![Unité](/media/help/worker/sharing_proposal/units.png "Unité")

## Le nombre de pièces
Automatiquement le site essaie de déterminer le nombre de pièces, mais vous devez néanmoins la vérifier avant de valider (ex: 1ère ligne). Parfois la détection n'est pas possible, dans ce cas c'est à vous de renseigner la bonne valeur (ex: dernière ligne). 
Lorsqu'un produit est au kg, laissez vide le champ `pièces` (ex: ligne 2)
![Pièces](/media/help/worker/sharing_proposal/piece.png "Pièces")

## Fromage
Le site essaie de détecter automatiquemet si le produit est en un formage à partager. Si oui, il faut cocher la case `fromage`.
![Fromage](/media/help/worker/sharing_proposal/fromage.png "Fromage")

## Commentaire
Comme sur la feuille excel, il est possible de laisser un commentaire sur le produit désiré.
![Commentaire](/media/help/worker/sharing_proposal/commentaire.png "Commentaire")

## Le code producteur
Les codes producteurs comme `LAU`, `COL`, `ROSSI` sont ajoutés automatiquement par le site il n'y a rien à faire.

## Supprimer un article de la proposition de partage
Cliquez sur le bouton "poubelle"
![Poubelle](/media/help/worker/sharing_proposal/supprimer.png "Poubelle")

## Valider le formulaire
- le bouton "enregistrer les modifications" permet d'enregistrer votre travail. Cependant, **aucun enregistrement n'est possible tant que le formulaire signale une erreur**. Pensez à enregistrer régulièrement vos modifications! **À noter que l'enregistrement de la proposition est persistant**: cela signifie que vous pouvez fermer votre navigateur, changez d'ordinateur, finir votre proposition un autre jour,  vous retrouverez la proposition comme vous l'avez laissée!
- le bouton "soumettre la proposition" permet de soumettre la proposition et passe la distribution dans l'état "partage effectué" (cette action est réversible!). La soummission n'est pas possible tant que le formulaire signale une erreur.