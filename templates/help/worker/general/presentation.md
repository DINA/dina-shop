# Démonstration

## 1) L'importation des catalogues
La 1ère étape consiste à importer les catalogues manquants à la distribution.
**Important**: La mercuriale changeant très souvent, il est conseillé de l'importer dans le site au dernier moment.
<video width="800" controls>
  <source src="/media/help/worker/general/part1.webm" type="video/webm">
</video>

## 2) Les suggestions de partage et la sélection des artices
Aller voir si des membres ont suggéré des articles pour la proposition de partage, puis ajouter les articles à la proposition.
### Conseils
* Pour une navigation fluide et une proposition efficace, il est conseillé dans un premier temps d'ajouter tous les articles que vous souhaitez dans la proposition de partage sans vous soucier du type (pièce, kg, ...). Vous pourrez spécifier tout cela ultérieurement. 

<video width="800" controls>
  <source src="/media/help/worker/general/part2.webm" type="video/webm">
</video>

## 3) L'élaboration de la proposition
<video width="800" controls>
  <source src="/media/help/worker/general/part3.webm" type="video/webm">
</video>

## Les erreurs possibles
<video width="800" controls>
  <source src="/media/help/worker/general/errors.webm" type="video/webm">
</video>

## Articles Extra
<video width="800" controls>
  <source src="/media/help/worker/general/extra.webm" type="video/webm">
</video>
