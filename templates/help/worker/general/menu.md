# Interface

Le menu se présente de la façon suivante:

![Menu](/media/help/shop/general/menu.png "Menu")

- `DINA` est un lien cliquable qui retourne sur la page d'accueil
- `Sélection des articles`: C'est dans cette partie qu'à lieu l'élaboration de la proposition de partage
  - `Sélection des articles`: Page qui permet de filtrer et sélectionner les articles à mettre dans la proposition de partage
  - `Articles suggérés`: Cette page liste les articles suggérés par les membres pour la proposition de partage
- `Gestion des articles`: Cette section permet de gérer (ajouter / modifier) les articles présents dans les catalogues
  - `Édition des articles`: Cette page permet de consulter les articles dans un catalogue et des les modifier
  - `Articles Extra`: Cette page permet de rajouter des articles de manière temporaire(voir l'aide "Articles extra" dans la section"commande individuelle") pour les ajouter à la proposition de partage. Exemple: les articles en promotions de Relais Vert
- `Gestion des catalogues`: Cette section permet de gérer les catalogues de la distribution
  - `Liste des catalogues`: Cette page liste les catalogues présents et manquants pour la distribution en cours
  - `Importer un catalogue`: Cette page permet d'importer un catalogue pour la distribution en cours
- `Proposition de partage`: Cette page dresse l'inventaire des articles proposés et permet de soumettre la proposition de partage

## ÉCRAN "Pas de distribution au partage"
Vous pourrez peut être parfois apercevoir cet écran:

![partage](/media/help/worker/general/nodistribution.png "partage")

Ce message est affiché quand vous essayez d'ajouter des articles alors que la distributionn'est pas dans l'état "partage en cours". 
- Si la ditribution est dans l'état "partage effectué", cliquez sur le bouton "Modifier la proposition" pour ajouter ou modifier des produits
- Sinon, c'est que la proposition de partage n'a pas encore été lancée par le poste de commande