# Importer un catalogue

La procédure est identique à cette du poste de commande excepté:
- il est obligatoire de fournir un fichier à importer (création de catalogue vide interdite)
- vous ne pouvez importer que des catalogues de type "mercuriale" ou "producteur DINA"

Pour importer un catalogue, cliquez sur le bouton "import":
![Import](/media/help/worker/catalogue/import.png "Import")