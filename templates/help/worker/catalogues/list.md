# Liste des catalogues

## Liste des catalogues présents dans la distribution
La partie haute de la page présente les catalogues présents dans la distribution courante:
![Present](/media/help/worker/catalogue/list1.png "Present")

### (Dé)Lier un catalogue de la distribution
Une erreur a été commise, il manque un catalogue dans la distribution ou vous avez importé 2 fois le même catalogue (il apparait en doublon dans la liste des catalogues présents)! 
Vous pouvez corriger cela en en cliquand sur l'icône en forme de chaîne puis en (dé)sélectionnant les catalogues voulus.

<video width="800" controls muted autoplay>
  <source src="/media/help/worker/catalogue/link_catalogues.webm" type="video/webm">
</video>

## Liste des catalogues absents de la distribution
La partie basse de la page présente les catalogues potentiellement manquant dans la distribution courante:
![Absent](/media/help/worker/catalogue/list2.png "Absent")

- En rouge: la mercuriale fruits & legumes qui doit obligatoirement être ajoutée à chaque distribution
- En jaune: des producteurs détectés "potentiellement manquants". Certains producteurs ne livrent pas toute les semaines, il faut donc voir ces messages comme un signalement plutôt qu'une anomalie.

