# Articles suggérés

Les membres ont la possitiblité de suggérer des articles directement depuis le site. Pour consulter les articles suggérés, se rendre dans `Sélection des articles ↳ articles suggérés`

<video width="800" controls muted autoplay>
  <source src="/media/help/worker/selection/suggested_items.webm" type="video/webm">
</video>