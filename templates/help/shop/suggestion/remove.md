# Retirer un article de la suggestion de partage

Vous pouvez retirer une suggestion en allant dans le menu `Gestion des articles ↳ Suggestion d'articles`.
<video width="800" controls muted autoplay>
  <source src="/media/help/shop/suggestion/remove_suggestion.webm" type="video/webm">
</video>
