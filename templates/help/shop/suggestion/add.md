# Suggérer un article pour la prochaine proposition de partage

Vous pouvez suggérer un article pour la prochaine proposition de partage, en cliquant sur le bouton associé:
<video width="800" controls muted autoplay>
  <source src="/media/help/shop/suggestion/add_suggestion.webm" type="video/webm">
</video>

**Attention**, parfois en vous connectant vous pourrez voir ce message:
![Pas de commande](/media/help/shop/general/nodistribution.png "Pas de commande")

Cependant, vous pouvez suggérér à n'importe quel étape du cycle d'une distribution "en cours" en vous rendant dans le menu `Sélection des articles ↳ Sélection des articles`.

## Cas d'erreur
Toutefois, vous pouvez voir apparaître une erreur de cette sorte:

![erreur](/media/help/shop/suggestion/add_error.png "erreur")

Dans ce cas là, contacter immédiatement le responsable du poste de commande en lui demandant de créer la prochaine distribution dans le planning.