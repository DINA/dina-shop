# Vidéo de présentation d'une commande

## Vidéo
<video width="800" controls>
  <source src="/media/help/shop/general/order_presentation.webm" type="video/webm">
</video>

## Quelques conseils & informations importantes

* Pour une navigation fluide et une commande efficace, il est conseillé dans un premier temps d'ajouter tous vos articles dans le panier sans vous soucier des quantités ni du type (pièce, colis, ...). Vous pourrez spécifier tout cela ultérieurement dans le panier. Vous pouvez commencer par la proposition de partage, puis ensuite la commande individuelle.
* Pour commander plus rapidement:
  * N'hésitez pas à ajouter vos produits préférés en favoris pour les retrouver plus facilement.
  * Si vous commandez régulièrement la même chose, vous pouvez également repasser tout ou partie d'une ancienne commande depuis votre historique de commandes.
* Une fois dans le panier, vous pouvez spécifier les quantités et le colisage voulu. 
  * Vos modifications sont sauvegardées instantanément, mais il toujours nécessaire de valider son panier (voir bouton "vérifier" en bas de page) manuellement. Exemple: vous pouvez demander 1 tonne de compté (grosse raclette en perspective!), la valeur sera immédiatement enregistrée et vous pourrez continuez vos achats. Cependant quand vous essaierez de commander ou de vérifier le panier vous aurez une erreur de validation, "1000 kg" n'étant pas une quantité valide.
  * La commande ne sera validée que lorsque toutes les erreurs indiquées auront été corrigées.
  * ** /!\ TRÈS IMPORTANT, TOUJOURS VÉRIFIER VOTRE BON DE COMMANDE** avant de vous déconnecter. __C'est lui et lui seul qui fait foi__, le récapitulatif de la commande affiché sur le site est seulement une aide. 
* Votre panier est sauvegardé en base de donnée. Concrètemement, cela signifie qu'il n'y a pas de temps imparti pour passer sa commande, vous pouvez commencer un jour et finir le lendemain. De même, vous pouvez commencer sur un ordinateur et finir sur un autre ordinateur (ou tablette). Cependant, vous devez avoir validé votre commande avant la date impartie dans le règlement intérieur de DINA pour qu'elle soit prise en compte.
* L'affichage des produits étant "rustique" (format tableur), il est recommandé de passser commande sur un ordinateur. Une tablette peut convenir mais il peut y avoir quelques bugs d'affichage (une taille de 8" ou supérieure fonctionne correctement d'après les tests). En revanche la commmande n'est pas du tout optimisé pour mobile, la navigation risque d'être très difficile. 
* Bonne commande :-)