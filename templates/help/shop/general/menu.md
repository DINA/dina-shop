# Interface

Le menu se présente de la façon suivante:

![Menu](/media/help/worker/general/menu.png "Menu")

- `DINA` est un lien cliquable qui retourne sur la page d'accueil
- `Sélection des articles`: C'est dans cette section que vous trouverez les articles à commander
  - `Proposition de partage`: Vous y trouverez la proposition de partage
  - `Commande individuelle`: C'est l'interface principale pour passer commande
- `Gestion des articles`: Cette section permet de gérer (ajouter / modifier) les articles à suggérer, en extra ou en favoris
  - `Favoris`: Cette page permet de consulter vos articles favoris
  - `Articles Extra`: Cette page permet de commander des articles qui ne seraient pas présents dans les catalogues proposés. Exemple: les articles en promotions de Relais Vert
  - `Suggestion d'articles`: Cette page permet de consulter les articles que vous avez suggérer pour la prochaine proposition de partage
- `Panier`: Cette page dresse l'inventaire des articles ajoutés au panier et permet de valider la commande
- `Commande`: cette page récapitule votre commande
- `Historique des commandes`: Historique de vos commandes

## ÉCRAN "Pas de distribution au partage"
Vous pourrez peut être parfois apercevoir cet écran:

![nodistrib](/media/help/shop/general/nodistribution.png "nodistrib")

Ce message est affiché pour vous signifier que la distributionn'est pas dans l'état "commandes ouvertes". Cependant, il est toujours possible de gérer vos favoris et de suggérer des articles.
