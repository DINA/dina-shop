# Supprimer un favori

Vous pouvez retirer un favori en allant dans le menu `Gestion des articles ↳ Favoris`.
<video width="800" controls muted autoplay>
  <source src="/media/help/shop/bookmark/remove_bookmark.webm" type="video/webm">
</video>

