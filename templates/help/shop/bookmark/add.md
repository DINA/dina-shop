# Ajouter un favori

Les favoris vous permettent de retrouver rapidement un produit que vous commandez souvent.
<video width="800" controls muted autoplay>
  <source src="/media/help/shop/bookmark/add_bookmark.webm" type="video/webm">
</video>
