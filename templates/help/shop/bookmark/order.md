# Commander depuis un favori

Vous pouvez commander directement depuis le menu des favoris:

![Commander](/media/help/shop/bookmark/order.png "Commander")

2 cas sont possibles:
- le bouton "caddie" apparaît à côté du favori: vous pouvez commander
- il n'y a pas de bouton: cela signifie que l'article ne fait parti d'aucun catalogue actuellement ouvert aux commandes.


