# Bon de commande

Une fois la commande effectuée, vous obtiendrez un récapitulatif puis les 3 boutons ci-dessous:

![Boutons](/media/help/shop/order/buttons.png "Boutons")

À ce moment il est **INDISPENSABLE de vérifier le bon de commande** qui sera généré par le site pour en vérifier la conformité (et l'absence de bugs) en cliquant sur le bouton "Vérifier le bon de commande".
Cela télécharge automatiquement la feuille excel. En cas d'erreur de formatage du fichier, prévenez immédiatement:
- le responsable du poste de commande pour qu'il sache qu'il doit corriger l'erreur avant l'insertion dans l'automate DINA
- l"administrateur du site, pour que le bug soit corrigé.

Vous pouvez aussi: 
- modifier votre commande en cliquant sur "modifier": ajouter d'autres produits, modifier des quantités, etc...
- supprimer votre commande: ceci est utile si finalement vous ne pourrez pas venir récupérer votre commande le jour de la livraison (par exemple)
