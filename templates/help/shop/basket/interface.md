# Panier

Le formulaire de la proposition de partage ce présente comme ceci: Il faut globalement le lire de la même manière que la feuille excel remplie à la main.

En haut, la proposition de partage. Il ne reste qu'à remplir la quantité souhaitée.
En bas, la commande individuelle. Il faut remplir la quantité souhaitée, mais aussi spécifier si vous commandez au colis ou à la pièce.

![Formulaire](/media/help/shop/basket/interface.png "Formulaire")


## Les unités
Lorsque le produit n'est pas décolisable, le choix `colis` est imposé. Sinon, un choix vous est demandé entre `pièce` et `colis`. 

![Unité](/media/help/shop/basket/units.png "Unité")

## La quantité
C'est la quantité que vous voulez commander.


## Précommande
Le site détectee automatiquemet si le produit doit être précommandé ou non. En cas de précommande, la case est cochée (il n'y a rien à cocher de votre part). 

![Précommande](/media/help/shop/basket/precommande.png "Précommande")

## Prix
Tout en bas, une estimation du montant de votre commande est calculée.


## Supprimer un article
Cliquez sur le bouton "poubelle"

## Valider le formulaire
- le bouton "vérifier le panier" permet de s'assurer que le panier est valide et ne comporte pas d'erreurs. **À noter que l'enregistrement du panier est persistant**: cela signifie que vous pouvez fermer votre navigateur, changez d'ordinateur, finir votre commande un autre jour, vous retrouverez votre panier comme vous l'avez laissé!
- le bouton "Commander" permet de commander (cette action est réversible!). La commande n'est pas possible tant que le formulaire signale une erreur. **Attention si le panier n'est pas validé au moment de la fermeture des commandes, ce dernier sera supprimé et vous n'aurez aucun produit.**