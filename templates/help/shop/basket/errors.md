# Erreur sur le formulaire du panier

Certaines erreurs peuvent vous empêcher de valider le formulaire de commande. En voici a liste.

## Erreurs de votre part

### Quantité trop importante
Vous ne pouvez pas commander plus de 99 unités

![erreur1](/media/help/shop/basket/errors/100.png "erreur1")

### Quantité incohérente
Une picèce doit forcément être un nombre entier. 

![erreur2](/media/help/shop/basket/errors/incoherence.png "erreur2")

### Colisage non renseigné
Vous devez renseigner si vous commandez à la pièce ou au colis

![erreur3](/media/help/shop/basket/errors/select.png "erreur3")

### La quantité ne respecte pas le colisage minimum
Vous devez respecter le colisage minimum.

![erreur4](/media/help/shop/basket/errors/decolisage.png "erreur4")

