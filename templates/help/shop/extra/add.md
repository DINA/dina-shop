# Ajouter un article extra

Les "articles extra" sont un moyen pour l'utilisateur de commander des articles qui ne seraient pas présents dans les catalogues "standards". Le cas le plus évident est la commande d'un article depuis le catalogue "promotions" de relais Vert.

Ci-dessous une démonstration (avec le son):
<video width="800" controls>
  <source src="/media/help/shop/extra/demo_extra_item.webm" type="video/webm">
</video>

## Sélection du type de catalogue
Premièrement, se rendre dans le menu `Gestion des Articles ↳ Articles Extra`, puis sélectionner le type de catalogue correspondant à l'article que vous souhaitez ajouter.

Bien que vous puissiez choisir à peu près n'importe quel type de catalogue pour n'importe quel produit, essayez de choisir au mieux le type de catalogue, l'interface pour ajouter le produit tiendra compte de votre choix.
Exemple: si vous souhaitez ajouter des yaourts mais que vous choisissez un catalogue de type "Millésime 86", vous verrrez apparaître des champs pour remplir le cépage et la couleur du ~~vin~~ yaourt...

## Renseigner l'article désiré
Vous pouvez maintenant renseigner les informations du produit.
Les champs obligatoires sont:
- Référence
- Désignation
- Prix HT
- TVA (automatiquement remplie à 5,5%)

Cependant, il peut être utile de renseigner d'autres informations si elles sont disponibles: le conditionnement, le colisage, etc... Cela facilitera la commande des autres membres qui n'auront alors plus besoin d'aller chercher le reste des informations eux-même.

**Attention**! Une fois le produit ajouté, **vous ne pourrez plus le modifier vous même en cas d'erreur**. Vérifiez donc bien les informations avant de valider! Si une erreur s'est glissée magré tout, demandez au responsable du poste de commande de modifier le produit en question.

## Ajouter au panier
Une fois le produit ajouté, vous pouvez l'ajouter directement depuis l'interface des "articles extra". Mais vous pouvez aussi l'ajouter depuis l'interface principale "Sélection des articles". Un nouveau catalogue "Extra" a été ajouté avec des articles "extra" dedans!

<video width="800" controls muted autoplay>
  <source src="/media/help/shop/extra/extra_item.webm" type="video/webm">
</video>
