# Perte de vos identifiants

## Perte de son adresse email
Si vous ne savez plus quel adresse mail est utilisée pour vous connecter, faite une recherche dans votre historique de mail :-)
Sinon, contactez le coordinateur qui pourra vous redonner le mail à utiliser.

## Perte de son mot de passe
Si vous avez perdu votre mot de passe, vous devez cliquer sur le lien "mot de passe perdu'. Il ne sert à rien de contacter le coordinateur ou l'administrateur, personne ne pourra récupérer votre mot de passe qui est stockée de manière sécurisée (indéchiffrable) dans la base de données. Entrer votre mail, vous devez recevoir un lien de réinitialisation du mot de passe.
<video width="800" controls muted autoplay>
  <source src="/media/help/user/credentials/lost_password.webm" type="video/webm">
</video>
