# Modifier votre profil

## Modifier son nom d'utilisateur
Par souci de cohérence, le nom utilisé est le nom utilisé dans l'automate. Vous ne pouvez pas le modifier vous même. Contacter le coordinateur si le nom contient une erreur.

## Modifier son adresse email
Par souci de cohérence, le mail utilisé est le mail utilisé dans l'automate. Vous ne pouvez pas le modifier vous même. Contacter le coordinateur si le mail contient une erreur ou si vous vouler changer d'adresse email.

## Modifier son mot de passe
Vous pouvez changer votre mot de passe en cliquant sur le bouton de profil. Vous devez recevoir un mail de confirmation à la fin de l'opération.

<video width="800" controls muted autoplay>
  <source src="/media/help/user/credentials/edit_password.webm" type="video/webm">
</video>
