# Modifier ou supprimer un article

## Modifier un article
Il est possible de modifier des articles dans un catalogue.

Se rendre dans la vue article, puis sélectionner le catalogue dans lequel vous souhaitez modifier un article ("catalogue vide dans cet exemple").

![Menu Articles](/media/help/manager/item/menu.png "Menu Articles")

Cliquez sur le bouton "modifier" (crayon) en face de l'article désiré puis modifier les champs voulus.
Cliquez sur "mettre à jour" pour enregistrer les modifications.


<video width="800" controls muted autoplay>
  <source src="/media/help/manager/item/edit_item.webm" type="video/webm">
</video>

## Supprimer un article

Cliquez sur le bouton "modifier" (crayon) ou "Visualier" en face de l'article désiré puis sur le bouton "supprimer".

<video width="800" controls muted autoplay>
  <source src="/media/help/manager/item/delete_item.webm" type="video/webm">
</video>