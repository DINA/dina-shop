# Importer des articles

Il est possible d'importer des articles dans un catalogue, qu'il soit vide ou contienne déjà des articles. **Attention** si ce catalogue contenait auparavant des articles, tous seraient supprimés et remplacés par les nouveaux articles!

Se rendre dans la vue article, puis sélectionner le catalogue dans lequel vous souhaitez ajouter un article ("catalogue vide dans cet exemple").

![Menu Articles](/media/help/manager/item/menu.png "Menu Articles")

Cliquez sur le bouton "Import" puis sélectionner un fichier.

<video width="800" controls muted autoplay>
  <source src="/media/help/manager/item/import_item.webm" type="video/webm">
</video>