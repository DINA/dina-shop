# Ajouter un article

Il est possible d'ajouter des articles à un catalogue, qu'il soit vide ou contienne déjà des articles.

Se rendre dans la vue article, puis sélectionner le catalogue dans lequel vous souhaitez ajouter un article ("catalogue vide dans cet exemple").

![Menu Articles](/media/help/manager/item/menu.png "Menu Articles")

Cliquez sur le bouton "plus" puis renseigner les 3 champs suivants (**obligatoires**):
- Référence
- Désignation
- Prix HT

La TVA est ajoutée automatiquement à un taux de 5,5% (modifiable). Les autres champs sont facultatifs.

<video width="800" controls muted autoplay>
  <source src="/media/help/manager/item/add_item.webm" type="video/webm">
</video>