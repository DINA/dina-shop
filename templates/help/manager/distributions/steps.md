# Faire avancer une distribution

Il existe 3 manières de faire avancer une distribution:
- Dans le menu latéral gauche, vous pouvez sélectionner l'étape à laquelle vous souhaitez aller. Attention, vous ne pouvez cependant pas lancer la proposition de partage via ce menu.
- En cliquant sur le bouton vert "Étapes" associé à une distribution
- En cliquant sur le bouton "Consulter" puis dans le cadre "Progresser dans le cycle de la distribution".

<video width="800" controls muted autoplay>
  <source src="/media/help/manager/distribution/steps_distribution.webm" type="video/webm">
</video>



