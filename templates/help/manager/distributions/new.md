# Créer une distribution

Se rendre dans la liste des distributions via le menu latéral puis cliquer tout en bas de la page sur le bouton "+":
![Menu Distributions](/media/help/manager/distribution/new.png "Menu Distributions")

Sélectionner une date pour la livraison (l'heure n'a aucune importance) puis sélectionner des catalogues si vous le souhaitez. **Important: à cette étape, le choix des catalogues est facultatif**.

Pour insérer un catalogue, cliquer sur le champ `Catalogues` et séléectionner le catalogue désiré.

<video width="800" controls muted autoplay>
  <source src="/media/help/manager/distribution/new_distribution.webm" type="video/webm">
</video>



