# Modifier la commande d'un membre
## Consulter les commandes des membres

Les commandes des membres visible directement dans l'onget `Commande` d'une distribution.

<video width="800" controls muted autoplay>
  <source src="/media/help/manager/distribution/orders_view.webm" type="video/webm">
</video>

## Modifier la commande d'un membre

Tant que les commandes sont ouvertes, c'est au membre de modifier sa commande. 
Cependant, si les commandes sont fermées et que vous détectez une erreur ou qu'un membre vous notifie d'une erreur dans sa commande (il a commandé un colis ou lieu d'une pièce par exemple), vous pouvez modifier la commande à sa place.

**Attention, le module d'édition est extrèmement limité**: vous ne pouvez ni ajouter, ni supprimer de produit. Seulement modifier les quantités et les unitées.

**Important: pensez bien à renvoyer les bons de commande sur l'adresse mail de dINA!**

<video width="800" controls muted autoplay>
  <source src="/media/help/manager/distribution/edit_order.webm" type="video/webm">
</video>

