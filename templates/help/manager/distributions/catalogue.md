# Ajouter ou retirer un catalogue d'une distribution

Sélectionner la distribution en cours (bouton "Consulter") puis l'onglet `Informations`. Ensuite, cliquer sur "Modifier".

- Pour insérer un catalogue, cliquer sur le champ `Catalogues` et séléectionner le catalogue désiré.
- Pour retirer un catalogue, cliquer sur la croix située à gauche du catalogue à retirer.

![Menu Distributions](/media/help/manager/distribution/remove_catalogue.png "Menu Distributions")

