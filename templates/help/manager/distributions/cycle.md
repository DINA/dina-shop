# Cycle d'une distribution

La distribution est l'entité qui gère l'intégralité des étapes nécessaires depuis l'élaboration de la proposition de partage jusqu'à la livraison des producteurs le jour de la distribution. C'est une sorte d'équivalent "web" de l'automate DINA.

Pour le moment, la distribution ne gère que la proposition de partage et les commandes individuelles des membres. Le "plus", les commandes fournisseurs, etc... sont toujours gérées par l'automate.

## État courant

Pour connaître l'état d'une distribution, il faut se rendre dans la liste des distributions via le menu latéral:
![Menu Distributions](/media/help/manager/distribution/menu.png "Menu Distributions")

Le champ `Statut` renseigne l'état courant de chaque distribution.

Dans le détail de la distribution (cliquez sur le bouton "Consulter"), une barre de progression vous renseigne sur son avancement.
![Barre de progression](/media/help/manager/distribution/progressBar.png "Barre de progression")

## Liste des états 

### Étape 1: L'état "Nouveau"
C'est l'état initial d'une distribution après sa création. La distribution n'est pas encore considérée comme "en cours".
Il ne peut y avoir qu'une seule distribution à la fois dans l'état `nouveau`.
**Vous pouvez passer à l'étape suivante si et seulement si il n'y a pas d'autre distribution en cours**.

### Étape 2: L'état "Partage en cours"
La distribution est réellement "lancée", la personne en charge de la proposition de partage doit l'élaborer avant la date limite. 
Il n'est pas possible de revenir à l'étape 1.

### Étape 3: L'état "Partage effectué"
La proposition de partage est réalisée, mais il est encore temps de la modifier en cas besoin (en revenant à l'étape 2).

### Étape 4: L'état "Commandes ouvertes"
La proposition de partage ets désormais figée, les membres peuvent commander de manière individuelle. 
Il n'est pas possible de revenir à l'étape 3 sauf pour l'admnistrateur qui possède ce privilège.

### Étape 5: L'état "Commandes Fermées"
Les commandes individuelles ne sont plus autorisées, cependant, il est encore temps de procéder à d'ultimes vérifications et modifications en cas d'erreur de commande de la part d'un membre.
Les bons de commandes sont envoyés à l'automate DINA.
Il n'est pas possible de revenir à l'étape 4 sauf pour l'admnistrateur qui possède ce privilège.

### Étape 6: L'état "Clôturée"
La livraison a eu lieu, la distribution peut être clôturée. Plus aucune modification n'est possible
Il n'est pas possible de revenir à l'étape 5.