# Modifier la proposition de partage

## Consulter la proposition de partage

### Lorsque la distribution est dans l'état "Partage en cours"

Vous pouvez consulter la proposition sans rien modifier en cliquant sur le bouton: "Voir le détail de la proposition de partage". Cliquez sur "Revenir à la vue précédente" si vous ne souhaitez pas modifier la proposition.

<video width="800" controls muted autoplay>
  <source src="/media/help/manager/distribution/PP_inProgress.webm" type="video/webm">
</video>

### Lorsque la distribution est dans l'état "Partage effectué" (ou postérieur)

La proposition de partage est visible directement dans l'onget `Proposition de partage`.

<video width="800" controls muted autoplay>
  <source src="/media/help/manager/distribution/PP_done.webm" type="video/webm">
</video>

## Modifier / Rejeter la proposition de partage

Il y a plusieurs situations dans lesquelles le poste de commande peut être amené à modifier la proposition de partage:
- un membre n'arrive pas à élaborer la proposition de partage, une erreur l'empêche de soumettre sa proposition.
- la proposition de partage est complète mais comporte des erreurs
-  la proposition de partage est incomplète

**Important**: Vous pouvez aussi vous déconnecter du poste de commande et vous reconnecter en tant que poste de partage pour effectuer des modifications, mais dans ce cas soyez sûr de ne pas être 2 membres à modifier la proposition en même temps!

### La proposition de partage n'est pas validée
(Mettez le son)
<video width="800" controls>
  <source src="/media/help/manager/distribution/validation_PP.webm" type="video/webm">
</video>

### La proposition de partage comporte des erreurs
(Mettez le son)
<video width="800" controls>
  <source src="/media/help/manager/distribution/modif_PP.webm" type="video/webm">
</video>

### La proposition de partage est incomplète

La proposition de partage est correctement complétée mais le membre n'a pas respecté les consignes: il y a beaucoup trop de produits, il manque au contraire beaucoup de produits, il y en a en doublon, des produits avec un bilan carbone très élevé, etc...
La proposition de partage est à revoir. Dans ce cas il n'y a pas d'autre choix que de rejeter la proposition de partage en expliquant au membre en charge de la proposition ce qu'il faut modifier. **En effet, le poste de commande ne peut pas rajouter des produits à la proposition, seul le poste de partage le peut**.
![Rejeter](/media/help/manager/distribution/reject.png "Rejeter")
