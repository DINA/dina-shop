# Archiver un catalogue

Lorsqu'un catalogue n'est plus utilisé, il faut l'archiver. 
L'archivage permet:
- d'éviter des modifications accidentelles d'articles à postériori
- **d'optimiser les performances du site** en supprimant certaines informations en base de données

## Comment archiver?
Se rendre dans la liste des catalogues via le menu latéral:
![Menu Catalogues](/media/help/manager/catalogue/menu.png "Menu Catalogues")

### Via le bouton "Archiver" dans la liste des catalogues
Dans la colonne "Actions", le bouton rouge "Archiver" est disponible si l'archivage est possible.
![Bouton "Archiver"](/media/help/manager/catalogue/archive_button.png "Bouton Archiver")
### Via le menu "Modifier"
Clic sur "Modifier" puis sélectiionner "Archiver" dans le champ `statut`.
<video width="800" controls muted autoplay>
  <source src="/media/help/manager/catalogue/archive_menu.webm" type="video/webm">
</video>

## Quand archiver?
L'archivage est opportun lorsqu'un catalogue n'a plus d'utilité, typiquement lorsque le fournisseur vient d'envoyer son nouveau catalogue. Cependant, **l'archivage d'un catalogue n'est possible que lorsque toutes les distributions qui utilisaient ce catalogues sont à l'état "clôturée"**.
Dans ce cas, le bouton "archivage" est affiché. Si le bouton n'est pas affiché, cela signifie qu'une distribution "utilise" le catalogue et qu'il faut la clôturer préalablement.

#### À quelle fréquence?
Le cycle de vie est cependant différent selon les catalogues: 
- Relais Vert, Frais et Sec: un catalogue par mois, archiver l'ancien quand le nouveau est disponible
- Mercuriales, producteurs DINA (Coline, Laurence, etc...): archivage à chaque fin de distribution
- Autres producteurs: quelques catalogues par an, archiver l'ancien quand le nouveau est disponible

## Qui?
Voir les fiches de postes.

