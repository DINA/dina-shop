# Erreurs lors de l'importation d'un catalogue

## Quelques considérations techniques

## Erreurs de sélection

Pour éviter les erreurs de sélection de fichier (c'est à dire déclarer un catalogue "sec" et téléverser le catalogue "frais"), il existe des contrôles sur les noms de fichier.

Lors d'une importation de fichier, les contrôles sont les suivants:
- `Relais Vert - Catalogue Sec`: le nom de fichier doit contenir le mot `sec`
- `Relais Vert - Catalogue Frais`: le nom de fichier doit contenir le mot `frais`
- `Coline`: le nom de fichier doit contenir le mot `Coline`
- `Laurence`: le nom de fichier doit contenir le mot `Laurence`
- `Katia`: le nom de fichier doit contenir le mot `Katia`
- Autre (futur) producteur DINA: le nom de fichier doit contenir le nom du producteur

L'erreur est remontée comme suit:

![Erreur Type](/media/help/manager/catalogue/errorType.png "Erreur type")

## Erreurs de formatage catalogue
Lorsque le fichier est téléversé, l'importation du catalogue se fait en 2 temps.
- le site commence par rechercher des "clés de référence"
- une fois trouvées, le site cherche à établir une "cartographie des clés", c'est à dire savoir que les références des produits sont dans les colonne "C", les prix dans la colonne "F", etc...

Il faut se rendre sur la page `Paramètres des catalogues` pour avoir plus de détails.
<video width="800" controls muted autoplay>
  <source src="/media/help/manager/catalogue/catalogue_settings.webm" type="video/webm">
</video>

### Erreur 13056 - Clés de réference non trouvées
La colonne `Clés de référence` indique quelles sont les clés à regarder dans la colonne `Cartographie des clés`.

![Clés](/media/help/manager/catalogue/keys_import.png "Clés")

Dans cet exemple (mercuriale viande), le site cherche les colonnes `Ref` et `Désignation` en priorité. S'il ne les trouvent pas, l'erreur 13056 apparait:

`Erreur 13056 - Impossible d'ajouter un catalogue: Les colonnes 'XXX' - 'XXX' - | n'ont pas été trouvées`

Dans cet autre exemple (LCA Huiles Essentielles), les clés recherchées seront `Réf. LCA` et `Désignations articles`.
![Clés2](/media/help/manager/catalogue/keys_import2.png "Clés2")

### Erreur 13057 - Cartographie des clés incorrectes
Une fois que les clés de référence ont été trouvées, le site essaie de cartographier le catalogue et va essayer d'établr une correspondance pour toutes les clés listées dans la colonne `Cartographie des clés`.
Si le site ne trouve pas toutes les clés, l'erreur 13057 est renvoyée avec le nom des colonnes qui n"ont pas été trouvées dans le ficher:

`Erreur 13057 - Impossible d'ajouter un catalogue: Le catalogue ne semble pas correspondre pas au type donné: les colonnes 'Origine' - 'Condt' - n'ont pas été trouvées`

## Autres erreurs

Voir la vidéo explicative

<video width="800" controls>
  <source src="/media/help/manager/catalogue/catalogue_import_error.webm" type="video/webm">
</video>
