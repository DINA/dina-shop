# Ajouter un catalogue sans importer d'articles

Il tout à fait possible de créer un catalogue sans spécifier de fichier à importer.
Cela n'a de sens que pour un petit producteur pour lequel on pourrait se permettre de rentrer ensuite quelques articles à la main.

(La vidéo ci-dessous montre également comment ajouter un article manuellement. Pour plus de détails, consulter la section "Articles" de l'aide.)
<video width="800" controls muted autoplay>
  <source src="/media/help/manager/catalogue/add_catalogue.webm" type="video/webm">
</video>