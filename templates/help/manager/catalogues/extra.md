# Les catalogues extra

Seul le poste de commande a le pouvoir d'ajouter des articles à un catalogue. Cependant, il est possible pour les utilisateurs ainsi que pour le poste de proposition de partage de rajouter au site des "articles extra", pour commander des articles qui ne seraient pas présents dans les catalogues officiels. L'exemple le plus parlant est la possibilité de rajouter des articles en promotion depuis la fiche promotions de Relais Vert. 
Ces "articles extra" sont "rangés" dans des "catalogues extra", visibles depuis la page de listing des catalogues. Ils portent la mention `Extra` comme préfixe de leur nom ainsi qu'un commentaire `Auto generated extra catalogue`.

À moins qu'un utilisateur vous demande explicitement de modifier une erreur de saisie dans un de ces catalogues, **vous ne devez pas y toucher**. Ces catalogues **s'archivent eux même à la fin de la distribution**.

