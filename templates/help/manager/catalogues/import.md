# Import de catalogues

Lorsque vous créez un nouveau catalogue, vous avez la posssibilité d'importer un catalogue producteur au format, `.xls, .xlsx ou .ods`.
**Les `.pdf` ne sont pas utilisables tels quels**!

## Import
Pour importer un catalogue:
- donnez lui un nom (ex: catalogue sec octobre)
- choisissez un type
- sélectionner un fichier à importer dans une des 2 boites de dialogue

### Depuis la Dropbox

Cliquez sur le bouton "Dropbox" pour sélectionner le fichier à importer.
![Dropbox](/media/help/manager/catalogue/dropbox_button.png "Dropbox")

Si le bouton dropbox n'apparait pas, consulter le paragraphe "vie privée" en bas de page.

<video width="800" controls muted autoplay>
  <source src="/media/help/manager/catalogue/import_dropbox.webm" type="video/webm">
</video>

### Depuis un fichier local
- Commencer par télécharger le catalogue sur votre ordinateur:
  - depuis la boite gmail de DINA
  - ou bien depuis le site web du producteur
- Sélectionner le fichier depuis la boite de dialogue
<video width="800" controls muted autoplay>
  <source src="/media/help/manager/catalogue/import_local.webm" type="video/webm">
</video>

## Conversion de pdf
La conversion de fichiers pdf est techniquement compliquée, c'est pourquoi il faut passer par un service tiers pour transformer un catalogue `.pdf` en `.xls`.
Pour ce faire, cliquez sur le lien "Online-convert" qui doit vous rediriger vers un formulaire de conversion PDF -> XLS(X). **Important**: tous les convertisseurs ne se valent pas, utilisez bien ce service là et pas un autre!
<video width="800" controls>
<source src="/media/help/manager/catalogue/conversionPDF.webm" type="video/webm">
</video>


## Qui importe?
Voir les fiches de postes.

## Considérations sur le pistage et la vie privée
Si le bouton "Dropbox" n'apparait pas, BRAVO! Vous utilisez un bloqueur de traceurs qui protège votre vie privée! Le bouton dropbox permet malheureusement le pistage des utilisateurs, c'est pourquoi il peut être bloqué par votre navigateur ou un module tiers protégeant la vie privée.
Tant que DINA utilisera massivement la dropbox, il sera difficile de contourner ce problème.

Vous pouvez cependant (du moins protecteur au plus protecteur):
- désactiver votre bloqueur uniquement pour le site de DINA
- passer en "fenêtre de navigation privée" avec votre navigateur. A la fermeture de la fenêtre, tous les cookies laissés par dropbox seront supprimés mais d'autres forment de pistage perdureront (fingerprinting, etc...)
- utiliser un autre navigateur web uniquement pour DINA (mais ni Edge ni Chrome)
- utiliser un ordinateur de DINA (pas de vie pirvée à protéger)
- le laisser activé et téléverser les fichiers "à la main"
