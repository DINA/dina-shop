# Gestion des utilisateurs

Se rendre dans la liste des membres via le menu latéral:

![Menu Membres](/media/help/manager/user/menu.png "Menu Membres")

## Ajouter un membre

**Important**: les identifiants des membres doivent être strictement identiques à ceux utlisés pour l'Automate. 
Les informations `Email` et `Alias` (nom d'utilisateur) doivent donc être reprises depuis le fichier `Liste Adhérent.e.s` de l'association.

Vous devez choisir un mot de passe pour le membre, que vous lui communiquerez ensuite (par email, de vive voix, etc...). À charge pour le membre de changer son mot de passe lui-même par la suite.

<video width="800" controls muted autoplay>
  <source src="/media/help/manager/user/add_user.webm" type="video/webm">
</video>

Une fois le membre créé, un message appraît en haut de l'écran pour confirmer la création et un mail est envoyé au membre pour lui signifier la création de son compte:
![Mail](/media/help/manager/user/mail_add_user.png "Mail")

## Modifier les accès d'un membre
À la demande d'un membre (**et seulement sur sa demande**) vous pouvez modifier son email, son mot de passe et son nom d'utilisateur en cliquant sur le bouton modifier (icone crayon) en face du membre concerné.

Lorsque vous modifiez les informations d'un membre, **laissez vide les champs "mots de passe"**!
<video width="800" controls muted autoplay>
  <source src="/media/help/manager/user/edit_user.webm" type="video/webm">
</video>

Le membre doit recevoir un mail de notification lui signalant la modification de son compte.

### Modification du mot de passe
Vous ne devriez pas modifier le mot de passe d'un membre:
- Les membres peuvent eux même modifier leur mot de passe via leur profil
- En cas de perte de mot de passe, un lien de réinitialisation du mot de passe est disponible sur la page d'accueil.

Si toutefois une réinitialisation est nécessaire, modifiez le mot de passe du membre.

## Désactiver (ou réactiver) un membre
La désactivation d'un membre le sort de la liste des membres actifs du site. Il ne recevra plus de mails automatique à chaque distribution. Son accès au site est également temporairement bloqué.

<video width="800" controls muted autoplay>
  <source src="/media/help/manager/user/inactive_user.webm" type="video/webm">
</video>

La désactivation est utile pour les membres qui ne commandent qu'une partie de l'année.

Une fois son compte désactivé, le membre reçoit un mail l'informant que son accès est désactivé.
![Désactivation](/media/help/manager/user/mail_inactive_user.png "Désactivation")

La procédure pour réactiver un membre est identique, il suffit de sélectionner `Actif` dans le champ `Statut`.

## Supprimer un membre

Pour supprimer un membre, cliquez sur le bouton "Consulter" ou "Modifier" puis le bouton "Supprimer".
** Attention cette opération est irréversible!**

<video width="800" controls muted autoplay>
  <source src="/media/help/manager/user/delete_user.webm" type="video/webm">
</video>

## Qui?
Seuls l'administrateur du site et le coordinateur ont accès à l'espace "Membres".

