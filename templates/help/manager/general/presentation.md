# Présentation générale

## 1) Mise en place de la distrition

<video width="800" controls>
  <source src="/media/help/manager/general/commande-part1.webm" type="video/webm">
</video>

## 2) Fermeture des ventes
<video width="800" controls>
  <source src="/media/help/manager/general/commande-part2.webm" type="video/webm">
</video>
