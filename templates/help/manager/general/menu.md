# Interface

Le menu se présente de la façon suivante:

![Menu](/media/help/manager/general/menu.png "Menu")

Interface principale
- `DINA` est un lien cliquable qui retourne sur la page d'accueil
- `Tableau de bord`: permet d'afficher les widgets du tableau de bord
- `Membres`: Section dédiée à la gestion des membres
  - Ajouter, modifier, supprimer des membres
- `Producteurs`: Section dédiée à la gestion des producteurs
   - Ajouter, modifier, supprimer des producteurs
- `Catalogues`: Section dédiée à la gestion des catalogues
  - Ajouter, importer , archiver des catalogues
- `Articles`: Section dédiée à la gestion des articles
  - Modifier, ajouter ou supprimer des articles dans des catalogues
- `Distributions`: Section dédiée à la gestion des distributions
  - Lancer des distributions, contrôler la proposition de partage, visualiser les commandes des membres

Sous menu `Distribution courante`
- `Visualiser`: Permet d'accéder directement à la distribution courante
- `Actions`: Permet de chnager le statut de la distribution courante