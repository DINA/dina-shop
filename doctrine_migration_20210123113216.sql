-- Doctrine Migration File Generated on 2021-01-23 11:32:16

-- Version DoctrineMigrations\Version20200630081541
CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT '(DC2Type:json)', password VARCHAR(255) NOT NULL, username VARCHAR(32) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;

-- Version DoctrineMigrations\Version20200630100925
CREATE TABLE producer (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(64) NOT NULL, phone VARCHAR(12) DEFAULT NULL, email VARCHAR(64) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;

-- Version DoctrineMigrations\Version20200630124657
CREATE TABLE catalogue (id INT AUTO_INCREMENT NOT NULL, producer_id_id INT NOT NULL, name VARCHAR(255) NOT NULL, status INT NOT NULL, type INT NOT NULL, comment LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_59A699F5897B5A86 (producer_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
ALTER TABLE catalogue ADD CONSTRAINT FK_59A699F5897B5A86 FOREIGN KEY (producer_id_id) REFERENCES producer (id);

-- Version DoctrineMigrations\Version20200630175742
CREATE TABLE item (id INT AUTO_INCREMENT NOT NULL, catalogue_id_id INT NOT NULL, reference VARCHAR(32) NOT NULL, designation VARCHAR(255) NOT NULL, brand VARCHAR(255) DEFAULT NULL, packaging VARCHAR(32) DEFAULT NULL, inspection VARCHAR(32) DEFAULT NULL, origin VARCHAR(32) DEFAULT NULL, label VARCHAR(32) DEFAULT NULL, unitary_price NUMERIC(6, 2) NOT NULL, vat NUMERIC(4, 2) NOT NULL, family VARCHAR(64) DEFAULT NULL, packing SMALLINT DEFAULT NULL, min_packing SMALLINT DEFAULT NULL, unpacked_price NUMERIC(6, 2) DEFAULT NULL, calibre_categ VARCHAR(16) DEFAULT NULL, comment LONGTEXT DEFAULT NULL, ed VARCHAR(16) DEFAULT NULL, min_order SMALLINT DEFAULT NULL, custom_field1 VARCHAR(255) DEFAULT NULL, custom_field2 VARCHAR(255) DEFAULT NULL, custom_field3 VARCHAR(255) DEFAULT NULL, custom_field4 VARCHAR(255) DEFAULT NULL, INDEX IDX_1F1B251E6758ECE6 (catalogue_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
CREATE TABLE tmp (id INT AUTO_INCREMENT NOT NULL, catalogue_id_id INT NOT NULL, reference VARCHAR(32) NOT NULL, designation VARCHAR(255) NOT NULL, brand VARCHAR(255) DEFAULT NULL, packaging VARCHAR(32) DEFAULT NULL, inspection VARCHAR(32) DEFAULT NULL, origin VARCHAR(32) DEFAULT NULL, label VARCHAR(32) DEFAULT NULL, unitary_price NUMERIC(6, 2) NOT NULL, vat NUMERIC(4, 2) NOT NULL, family VARCHAR(64) DEFAULT NULL, packing SMALLINT DEFAULT NULL, min_packing SMALLINT DEFAULT NULL, unpacked_price NUMERIC(6, 2) DEFAULT NULL, calibre_categ VARCHAR(16) DEFAULT NULL, comment LONGTEXT DEFAULT NULL, ed VARCHAR(16) DEFAULT NULL, min_order SMALLINT DEFAULT NULL, custom_field1 VARCHAR(255) DEFAULT NULL, custom_field2 VARCHAR(255) DEFAULT NULL, custom_field3 VARCHAR(255) DEFAULT NULL, custom_field4 VARCHAR(255) DEFAULT NULL, INDEX IDX_2CEF7D486758ECE6 (catalogue_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
ALTER TABLE item ADD CONSTRAINT FK_1F1B251E6758ECE6 FOREIGN KEY (catalogue_id_id) REFERENCES catalogue (id);
ALTER TABLE tmp ADD CONSTRAINT FK_2CEF7D486758ECE6 FOREIGN KEY (catalogue_id_id) REFERENCES catalogue (id);

-- Version DoctrineMigrations\Version20200704150915
CREATE TABLE distribution (id INT AUTO_INCREMENT NOT NULL, status VARCHAR(32) NOT NULL, delivery_date DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
CREATE TABLE distribution_catalogue (distribution_id INT NOT NULL, catalogue_id INT NOT NULL, INDEX IDX_1E31D9E96EB6DDB5 (distribution_id), INDEX IDX_1E31D9E94A7843DC (catalogue_id), PRIMARY KEY(distribution_id, catalogue_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
ALTER TABLE distribution_catalogue ADD CONSTRAINT FK_1E31D9E96EB6DDB5 FOREIGN KEY (distribution_id) REFERENCES distribution (id) ON DELETE CASCADE;
ALTER TABLE distribution_catalogue ADD CONSTRAINT FK_1E31D9E94A7843DC FOREIGN KEY (catalogue_id) REFERENCES catalogue (id) ON DELETE CASCADE;

-- Version DoctrineMigrations\Version20200705130550
ALTER TABLE catalogue ADD spreadsheet VARCHAR(255) NOT NULL;

-- Version DoctrineMigrations\Version20200705131936
ALTER TABLE catalogue ADD spreadsheet_name VARCHAR(255) NOT NULL;

-- Version DoctrineMigrations\Version20200705171927
ALTER TABLE catalogue CHANGE spreadsheet spreadsheet VARCHAR(255) DEFAULT NULL, CHANGE spreadsheet_name spreadsheet_name VARCHAR(255) DEFAULT NULL;

-- Version DoctrineMigrations\Version20200706080707
CREATE INDEX idx_reference ON item (reference);
CREATE INDEX idx_designation ON item (designation);

-- Version DoctrineMigrations\Version20200709095009
CREATE TABLE sharing_proposal (id INT AUTO_INCREMENT NOT NULL, distribution_id INT NOT NULL, UNIQUE INDEX UNIQ_AF9653586EB6DDB5 (distribution_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
ALTER TABLE sharing_proposal ADD CONSTRAINT FK_AF9653586EB6DDB5 FOREIGN KEY (distribution_id) REFERENCES distribution (id);
ALTER TABLE item ADD sharing_proposal_id INT DEFAULT NULL;
ALTER TABLE item ADD CONSTRAINT FK_1F1B251E8C148271 FOREIGN KEY (sharing_proposal_id) REFERENCES sharing_proposal (id);
CREATE INDEX IDX_1F1B251E8C148271 ON item (sharing_proposal_id);

-- Version DoctrineMigrations\Version20200711135344
CREATE TABLE basket (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_2246507BA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
CREATE TABLE `order` (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, INDEX IDX_F5299398A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, item_id INT NOT NULL, basket_id INT DEFAULT NULL, linked_order_id INT DEFAULT NULL, product_unit SMALLINT NOT NULL, order_origin SMALLINT NOT NULL, quantity_int SMALLINT DEFAULT NULL, quantity_dec NUMERIC(5, 2) DEFAULT NULL, INDEX IDX_D34A04AD126F525E (item_id), INDEX IDX_D34A04AD1BE1FB52 (basket_id), INDEX IDX_D34A04ADE39F23E7 (linked_order_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
ALTER TABLE basket ADD CONSTRAINT FK_2246507BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id);
ALTER TABLE `order` ADD CONSTRAINT FK_F5299398A76ED395 FOREIGN KEY (user_id) REFERENCES user (id);
ALTER TABLE product ADD CONSTRAINT FK_D34A04AD126F525E FOREIGN KEY (item_id) REFERENCES item (id);
ALTER TABLE product ADD CONSTRAINT FK_D34A04AD1BE1FB52 FOREIGN KEY (basket_id) REFERENCES basket (id);
ALTER TABLE product ADD CONSTRAINT FK_D34A04ADE39F23E7 FOREIGN KEY (linked_order_id) REFERENCES `order` (id);

-- Version DoctrineMigrations\Version20200713094106
ALTER TABLE `order` ADD disribution_id INT NOT NULL;
ALTER TABLE `order` ADD CONSTRAINT FK_F5299398EDA6DE3D FOREIGN KEY (disribution_id) REFERENCES distribution (id);
CREATE INDEX IDX_F5299398EDA6DE3D ON `order` (disribution_id);

-- Version DoctrineMigrations\Version20200713132609
ALTER TABLE `order` DROP FOREIGN KEY FK_F5299398EDA6DE3D;
DROP INDEX IDX_F5299398EDA6DE3D ON `order`;
ALTER TABLE `order` CHANGE disribution_id distribution_id INT NOT NULL;
ALTER TABLE `order` ADD CONSTRAINT FK_F52993986EB6DDB5 FOREIGN KEY (distribution_id) REFERENCES distribution (id);
CREATE INDEX IDX_F52993986EB6DDB5 ON `order` (distribution_id);

-- Version DoctrineMigrations\Version20200713144802
ALTER TABLE basket ADD price NUMERIC(7, 2) NOT NULL;

-- Version DoctrineMigrations\Version20200713153108
ALTER TABLE `order` ADD price NUMERIC(7, 2) NOT NULL;

-- Version DoctrineMigrations\Version20200715093955
ALTER TABLE item DROP FOREIGN KEY FK_1F1B251E8C148271;
ALTER TABLE item ADD CONSTRAINT FK_1F1B251E8C148271 FOREIGN KEY (sharing_proposal_id) REFERENCES sharing_proposal (id) ON DELETE SET NULL;

-- Version DoctrineMigrations\Version20200716170924
DROP INDEX idx_reference ON item;
DROP INDEX idx_designation ON item;
CREATE INDEX idx_reference ON item (catalogue_id_id, reference);
CREATE INDEX idx_designation ON item (catalogue_id_id, designation);

-- Version DoctrineMigrations\Version20200717134303
CREATE TABLE bookmark (id INT AUTO_INCREMENT NOT NULL, producer_name VARCHAR(64) NOT NULL, reference VARCHAR(32) NOT NULL, designation VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
CREATE TABLE user_bookmark (user_id INT NOT NULL, bookmark_id INT NOT NULL, INDEX IDX_3AEF761DA76ED395 (user_id), INDEX IDX_3AEF761D92741D25 (bookmark_id), PRIMARY KEY(user_id, bookmark_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
ALTER TABLE user_bookmark ADD CONSTRAINT FK_3AEF761DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE;
ALTER TABLE user_bookmark ADD CONSTRAINT FK_3AEF761D92741D25 FOREIGN KEY (bookmark_id) REFERENCES bookmark (id) ON DELETE CASCADE;

-- Version DoctrineMigrations\Version20200718094035
ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD1BE1FB52;
ALTER TABLE product ADD CONSTRAINT FK_D34A04AD1BE1FB52 FOREIGN KEY (basket_id) REFERENCES basket (id) ON DELETE SET NULL;

-- Version DoctrineMigrations\Version20200718094740
ALTER TABLE user ADD basket_id INT DEFAULT NULL;
ALTER TABLE user ADD CONSTRAINT FK_8D93D6491BE1FB52 FOREIGN KEY (basket_id) REFERENCES basket (id) ON DELETE SET NULL;
CREATE UNIQUE INDEX UNIQ_8D93D6491BE1FB52 ON user (basket_id);

-- Version DoctrineMigrations\Version20200721095331
CREATE TABLE suggested_item (id INT AUTO_INCREMENT NOT NULL, distribution_id INT DEFAULT NULL, producer_name VARCHAR(64) NOT NULL, reference VARCHAR(32) NOT NULL, designation VARCHAR(255) NOT NULL, INDEX IDX_DD4605AA6EB6DDB5 (distribution_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
CREATE TABLE suggested_item_user (suggested_item_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_9D7E9B7750660DBB (suggested_item_id), INDEX IDX_9D7E9B77A76ED395 (user_id), PRIMARY KEY(suggested_item_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
ALTER TABLE suggested_item ADD CONSTRAINT FK_DD4605AA6EB6DDB5 FOREIGN KEY (distribution_id) REFERENCES distribution (id);
ALTER TABLE suggested_item_user ADD CONSTRAINT FK_9D7E9B7750660DBB FOREIGN KEY (suggested_item_id) REFERENCES suggested_item (id) ON DELETE CASCADE;
ALTER TABLE suggested_item_user ADD CONSTRAINT FK_9D7E9B77A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE;
DROP TABLE tmp;

-- Version DoctrineMigrations\Version20200724142414
ALTER TABLE catalogue DROP FOREIGN KEY FK_59A699F5897B5A86;
ALTER TABLE catalogue CHANGE producer_id_id producer_id_id INT DEFAULT NULL;
ALTER TABLE catalogue ADD CONSTRAINT FK_59A699F5897B5A86 FOREIGN KEY (producer_id_id) REFERENCES producer (id) ON DELETE SET NULL;

-- Version DoctrineMigrations\Version20200725093001
CREATE TABLE reset_password_request (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL COMMENT '(DC2Type:datetime_immutable)', expires_at DATETIME NOT NULL COMMENT '(DC2Type:datetime_immutable)', INDEX IDX_7CE748AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id);

-- Version DoctrineMigrations\Version20200725125823
ALTER TABLE user ADD status SMALLINT NOT NULL;
CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON user (username);

-- Version DoctrineMigrations\Version20200731150427
ALTER TABLE item ADD ean13 VARCHAR(16) DEFAULT NULL, ADD sku VARCHAR(32) DEFAULT NULL;

-- Version DoctrineMigrations\Version20200822143033
CREATE TABLE shared_product (id INT AUTO_INCREMENT NOT NULL, item_id INT NOT NULL, sharing_proposal_id INT NOT NULL, product_unit SMALLINT NOT NULL, order_origin SMALLINT NOT NULL, INDEX IDX_F29D081A126F525E (item_id), INDEX IDX_F29D081A8C148271 (sharing_proposal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
ALTER TABLE shared_product ADD CONSTRAINT FK_F29D081A126F525E FOREIGN KEY (item_id) REFERENCES item (id);
ALTER TABLE shared_product ADD CONSTRAINT FK_F29D081A8C148271 FOREIGN KEY (sharing_proposal_id) REFERENCES sharing_proposal (id);

-- Version DoctrineMigrations\Version20200823150623
ALTER TABLE catalogue ADD extra TINYINT(1) DEFAULT NULL;

-- Version DoctrineMigrations\Version20200825072937
ALTER TABLE item CHANGE calibre_categ calibre_categ VARCHAR(64) DEFAULT NULL;

-- Version DoctrineMigrations\Version20200825102020
ALTER TABLE shared_product ADD piece_number SMALLINT DEFAULT NULL;

-- Version DoctrineMigrations\Version20200825152849
ALTER TABLE shared_product ADD sub_type VARCHAR(16) DEFAULT NULL;

-- Version DoctrineMigrations\Version20200825154406
ALTER TABLE shared_product ADD is_cheese TINYINT(1) DEFAULT NULL;

-- Version DoctrineMigrations\Version20200826163410
ALTER TABLE producer ADD code VARCHAR(16) NOT NULL;

-- Version DoctrineMigrations\Version20200827071815
ALTER TABLE catalogue CHANGE type type VARCHAR(64) NOT NULL;

-- Version DoctrineMigrations\Version20200828081852
ALTER TABLE product ADD is_meet TINYINT(1) DEFAULT NULL;

-- Version DoctrineMigrations\Version20200828164420
ALTER TABLE catalogue ADD format VARCHAR(64) NOT NULL;

-- Version DoctrineMigrations\Version20200831150813
ALTER TABLE item DROP FOREIGN KEY FK_1F1B251E8C148271;
DROP INDEX IDX_1F1B251E8C148271 ON item;
ALTER TABLE item DROP sharing_proposal_id, DROP min_order;

-- Version DoctrineMigrations\Version20200831151255
ALTER TABLE item ADD sharing_proposal_id INT DEFAULT NULL;
ALTER TABLE item ADD CONSTRAINT FK_1F1B251E8C148271 FOREIGN KEY (sharing_proposal_id) REFERENCES sharing_proposal (id) ON DELETE SET NULL;
CREATE INDEX IDX_1F1B251E8C148271 ON item (sharing_proposal_id);

-- Version DoctrineMigrations\Version20200903074538
ALTER TABLE shared_product ADD packaging VARCHAR(32) DEFAULT NULL;

-- Version DoctrineMigrations\Version20200903085931
ALTER TABLE product CHANGE is_meet pre_order TINYINT(1) DEFAULT NULL;

-- Version DoctrineMigrations\Version20200903094925
ALTER TABLE shared_product ADD comment VARCHAR(255) DEFAULT NULL;

-- Version DoctrineMigrations\Version20200903104937
ALTER TABLE product ADD comment VARCHAR(255) DEFAULT NULL;

-- Version DoctrineMigrations\Version20200903110527
ALTER TABLE product ADD packaging VARCHAR(32) DEFAULT NULL;

-- Version DoctrineMigrations\Version20200921135430
ALTER TABLE product ADD related_shared_product_id INT DEFAULT NULL;
ALTER TABLE product ADD CONSTRAINT FK_D34A04AD6ED0316D FOREIGN KEY (related_shared_product_id) REFERENCES shared_product (id);
CREATE INDEX IDX_D34A04AD6ED0316D ON product (related_shared_product_id);

-- Version DoctrineMigrations\Version20200921145939
ALTER TABLE product DROP comment;

-- Version DoctrineMigrations\Version20200922094935
ALTER TABLE item CHANGE ean13 ean13 VARCHAR(32) DEFAULT NULL;

-- Version DoctrineMigrations\Version20201015101006
ALTER TABLE user CHANGE email email VARCHAR(180) DEFAULT NULL;

-- Version DoctrineMigrations\Version20201015101247
DROP INDEX UNIQ_8D93D649F85E0677 ON user;

-- Version DoctrineMigrations\Version20210123100827
ALTER TABLE catalogue ADD auto_archive TINYINT(1) DEFAULT NULL;
