<?php

namespace App\Tests\Controller;

use App\Repository\UserRepository;
use App\Tests\NeedLogin;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class UserProfileControllerTest extends WebTestCase
{

    use FixturesTrait;
    use NeedLogin;

    public function testDisplayEditUser()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $users = $this->loadFixtureFiles([(dirname(__DIR__)) . '/Fixtures/User.yaml']);
        $this->login($client, $users['user_active']);
        $client->request('GET', '/userprofile/2/edit');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertSelectorTextContains('h1', 'Edit User');
        $this->assertSelectorNotExists('.alert.alert-danger');
    }

    public function testUpdateOnlyMail()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $users = $this->loadFixtureFiles([(dirname(__DIR__)) . '/Fixtures/User.yaml']);
        $this->login($client, $users['user_active']);
        $crawler = $client->request('GET', '/userprofile/2/edit');
        $form = $crawler->selectButton('Update')->form([
            'user_profile_edit[email]' => 'newusermail@mail.com',
        ]);
        $client->submit($form);
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        $this->assertSelectorNotExists('.alert.alert-success');
        $user = self::$container->get(UserRepository::class)->findOneBy(['email' => 'newusermail@mail.com']);
        $this->assertEquals('newusermail@mail.com', $user->getEmail());
    }

    public function testUpdateOnlyUsername()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $users = $this->loadFixtureFiles([(dirname(__DIR__)) . '/Fixtures/User.yaml']);
        $this->login($client, $users['user_active']);
        $crawler = $client->request('GET', '/userprofile/2/edit');
        $form = $crawler->selectButton('Update')->form([
            'user_profile_edit[username]' => 'newuser',
        ]);
        $client->submit($form);
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        $this->assertSelectorNotExists('.alert.alert-success');
        $user = self::$container->get(UserRepository::class)->findOneBy(['username' => 'newuser']);
        $this->assertEquals('newuser', $user->getUsername());
    }

    //public function testUpdateOnlyPassword()
    //{
    //    self::ensureKernelShutdown();
    //    $client = static::createClient();
    //    $users = $this->loadFixtureFiles([(dirname(__DIR__)) . '/Fixtures/User.yaml']);
    //    $this->login($client, $users['user_active']);
    //    $crawler = $client->request('GET', '/userprofile/2/edit');
    //    $form = $crawler->selectButton('Update')->form([
    //        'user_profile_edit[plainPassword][first]' => 'newpassword',
    //        'user_profile_edit[plainPassword][second]' => 'newpassword',
    //    ]);
    //    $client->submit($form);
    //    $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
    //    $this->assertSelectorNotExists('.alert.alert-success');
    //    //$user = self::$container->get(UserRepository::class)->findOneBy(['username' => 'newuser']);
    //    //$password = '$2y$10$hVzDS4EVIYwcDuBhxmiwceu4Z7Nj8MYvz7wq3V30pJyLEBvfhKkq.';
    //    //$this->assertEquals($password, $user->getPassword());
    //}

    

}
