<?php

namespace App\Tests\Controller;

use App\Tests\NeedLogin;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ResetPasswordControllerTest extends WebTestCase
{

    use FixturesTrait;
    use NeedLogin;

    public function testDisplayResetPassword()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('GET', '/reset-password');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertSelectorTextContains('h1', 'Reset your password');
        $this->assertSelectorNotExists('.alert.alert-danger');
    }

    public function testResetPasswordWithGoodCredentials()
    {
        $this->loadFixtureFiles([dirname(__DIR__) . '/Fixtures/User.yaml']);
        self::ensureKernelShutdown();
        $client = static::createClient();
        $crawler = $client->request('GET', '/reset-password');
        $form = $crawler->selectButton('Send password reset email')->form([
            'reset_password_request_form[email]' => 'user@mail.com',
        ]);
        $client->submit($form);
        $this->assertResponseRedirects('/reset-password/check-email');

        $this->assertEmailCount(1);
    }

    public function testResetPasswordWithBadCredentials()
    {
        $this->loadFixtureFiles([dirname(__DIR__) . '/Fixtures/User.yaml']);
        self::ensureKernelShutdown();
        $client = static::createClient();
        $crawler = $client->request('GET', '/reset-password');
        $form = $crawler->selectButton('Send password reset email')->form([
            'reset_password_request_form[email]' => 'azeaze@mail.com',
        ]);
        $client->submit($form);
        $this->assertResponseRedirects('/reset-password/check-email');
        // No mail sent
        $this->assertEmailCount(0);
    }
}