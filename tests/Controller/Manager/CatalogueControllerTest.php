<?php

namespace App\Tests\Controller;

use App\Entity\Catalogue;
use App\Repository\CatalogueRepository;
use App\Repository\UserRepository;
use App\Tests\NeedLogin;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class CatalogueControllerTest extends WebTestCase
{
    use FixturesTrait;
    use NeedLogin;

    private $em;
    private $users;
    private $client;

    public function setup(): void
    {
        $this->users = $this->loadFixtureFiles([
            (dirname(dirname(__DIR__))) . '/Fixtures/User.yaml',
            (dirname(dirname(__DIR__))) . '/Fixtures/Producer.yaml'
        ]);
        $this->em = self::$container->get('doctrine')->getManager();
        self::ensureKernelShutdown();
        $this->client = static::createClient();
        $this->login($this->client, $this->users['user_manager']);
    }

    public function testDeleteCatalogueArchivedWithFile()
    {
        $crawler = $this->client->request('GET', '/manager/catalogue/new');
        $form = $crawler->selectButton('Save')->form([
            'catalogue[name]' => 'catalogue mercuriale',
            'catalogue[type]' => '1',
            'catalogue[producerId]' => '1',
            'catalogue[status]' => '1',
        ]);
        $form['catalogue[spreadsheetFile]']->upload(dirname(dirname(__DIR__)) . '/Files/mercurialeFruitsLegumes.xlsx');
        $this->client->submit($form);
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        $this->assertTrue($crawler->filter('.error_list')->count() == 0);
        //$this->assertSelectorExists('.alert.alert-success');
        $catalogue = $this->em->getRepository(Catalogue::class)->createQueryBuilder('c')
            ->where('c.name LIKE :name')
            ->setParameter('name', 'catalogue mercuriale%')
            ->getQuery()
            ->getSingleResult();
        $this->assertContains('catalogue mercuriale', $catalogue->getName());

        //Archive
        $crawler = $this->client->request('GET', '/manager/catalogue/' . $catalogue->getId());
        $form = $crawler->selectButton('Archive')->form([]);
        $this->client->submit($form);
        $catalogue = self::$container->get(CatalogueRepository::class)->findOneBy(['status' => Catalogue::STATUS_ARCHIVED]);
        $this->assertEquals(Catalogue::STATUS_ARCHIVED, $catalogue->getStatus());

        //Delete
        $crawler = $this->client->request('GET', '/manager/catalogue/' . $catalogue->getId());
        $form = $crawler->selectButton('Delete')->form([]);
        $this->client->submit($form);
        //$this->assertSelectorExists('.alert.alert-danger');
        $catalogue = $this->em->getRepository(Catalogue::class)->createQueryBuilder('c')
            ->where('c.name LIKE :name')
            ->setParameter('name', 'catalogue mercuriale%')
            ->getQuery()
            ->getOneOrNullResult();
        $this->assertNull($catalogue);
    }

    public function testDeleteCatalogueOnlineWithFile()
    {
        $crawler = $this->client->request('GET', '/manager/catalogue/new');
        $form = $crawler->selectButton('Save')->form([
            'catalogue[name]' => 'catalogue mercuriale',
            'catalogue[type]' => '1',
            'catalogue[producerId]' => '1',
            'catalogue[status]' => '1',
        ]);
        $form['catalogue[spreadsheetFile]']->upload(dirname(dirname(__DIR__)) . '/Files/mercurialeFruitsLegumes.xlsx');
        $this->client->submit($form);
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        $this->assertTrue($crawler->filter('.error_list')->count() == 0);
        //$this->assertSelectorExists('.alert.alert-success');
        $catalogue = $this->em->getRepository(Catalogue::class)->createQueryBuilder('c')
            ->where('c.name LIKE :name')
            ->setParameter('name', 'catalogue mercuriale%')
            ->getQuery()
            ->getSingleResult();
        $this->assertContains('catalogue mercuriale', $catalogue->getName());

        //Delete
        $crawler = $this->client->request('GET', '/manager/catalogue/' . $catalogue->getId());
        $form = $crawler->selectButton('Delete')->form([]);
        $this->client->submit($form);
        //$this->assertSelectorExists('.alert.alert-danger');
        $catalogue = $this->em->getRepository(Catalogue::class)->createQueryBuilder('c')
            ->where('c.name LIKE :name')
            ->setParameter('name', 'catalogue mercuriale%')
            ->getQuery()
            ->getSingleResult();
        $this->assertContains('catalogue mercuriale', $catalogue->getName());
    }

    public function testDisplayAddCatalogue()
    {
        $this->client->request('GET', 'manager/catalogue/new');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        //$this->assertSelectorTextContains('h1', 'Create new Catalogue');
        $this->assertSelectorNotExists('.alert.alert-danger');
    }

    public function testAddCatalogueWithoutFile()
    {
        $crawler = $this->client->request('GET', '/manager/catalogue/new');
        $form = $crawler->selectButton('Save')->form([
            'catalogue[name]' => 'catalogue de test',
            'catalogue[type]' => '0',
            'catalogue[producerId]' => '1',
            'catalogue[status]' => '1',
        ]);
        $this->client->submit($form);
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        $this->assertTrue($crawler->filter('.error_list')->count() == 0);
        //$this->assertSelectorExists('.alert.alert-success');
        $catalogue = $this->em->getRepository(Catalogue::class)->createQueryBuilder('c')
            ->where('c.name LIKE :name')
            ->setParameter('name', 'catalogue de test%')
            ->getQuery()
            ->getSingleResult();
        $this->assertContains('catalogue de test', $catalogue->getName());   
    }

    public function testAddCatalogueWithFileSecFrais()
    {
        $crawler = $this->client->request('GET', '/manager/catalogue/new');
        $form = $crawler->selectButton('Save')->form([
            'catalogue[name]' => 'catalogue frais',
            'catalogue[type]' => '0',
            'catalogue[producerId]' => '1',
            'catalogue[status]' => '1',
        ]);
        $form['catalogue[spreadsheetFile]']->upload(dirname(dirname(__DIR__)) . '/Files/CatalogueFraisdésignationFévrier2020.xls');
        $this->client->submit($form);
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        $this->assertTrue($crawler->filter('.error_list')->count() == 0);
        //$this->assertSelectorExists('.alert.alert-success');
        $catalogue = $this->em->getRepository(Catalogue::class)->createQueryBuilder('c')
            ->where('c.name LIKE :name')
            ->setParameter('name', 'catalogue frais%')
            ->getQuery()
            ->getSingleResult();
        $this->assertContains('catalogue frais', $catalogue->getName());
    }

    public function testAddCatalogueWithFileMercurialeFruitsLegumes()
    {
        $crawler = $this->client->request('GET', '/manager/catalogue/new');
        $form = $crawler->selectButton('Save')->form([
            'catalogue[name]' => 'catalogue mercuriale',
            'catalogue[type]' => '1',
            'catalogue[producerId]' => '1',
            'catalogue[status]' => '1',
        ]);
        $form['catalogue[spreadsheetFile]']->upload(dirname(dirname(__DIR__)) . '/Files/mercurialeFruitsLegumes.xlsx');
        $this->client->submit($form);
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        $this->assertTrue($crawler->filter('.error_list')->count() == 0);
        //$this->assertSelectorExists('.alert.alert-success');
        $catalogue = $this->em->getRepository(Catalogue::class)->createQueryBuilder('c')
            ->where('c.name LIKE :name')
            ->setParameter('name', 'catalogue mercuriale%')
            ->getQuery()
            ->getSingleResult();
        $this->assertContains('catalogue mercuriale', $catalogue->getName());
    }

}