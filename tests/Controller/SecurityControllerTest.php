<?php

namespace App\Tests\Controller;

use App\Tests\NeedLogin;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class SecurityControllerTest extends WebTestCase
{

    use FixturesTrait;
    use NeedLogin;

    public function testDisplayLogin()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('GET', '/login');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertSelectorTextContains('h1', 'Please sign in');
        $this->assertSelectorNotExists('.alert.alert-danger');
    }

    public function testLoginWithBadCredentials()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('Sign in')->form([
            'email' => 'john@doe.fr',
            'password' => 'fakepassword'
        ]);
        $client->submit($form);
        $this->assertResponseRedirects('/login');
        $client->followRedirect();
        $this->assertSelectorExists('.alert.alert-danger');
    }

    public function testSuccessfullLogin()
    {
        $this->loadFixtureFiles([dirname(__DIR__) . '/Fixtures/User.yaml']);
        self::ensureKernelShutdown();
        $client = static::createClient();
        
       /*  $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('Sign in')->form([
            'email' => 'user@mail.com',
            'password' => 'password'
        ]);
        $client->submit($form); */
        
        $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('authenticate');
        $client->request('POST', '/login', [
            '_csrf_token' => $csrfToken,
            'email' => 'user@mail.com',
            'password' => 'password'
        ]);
        $this->assertResponseRedirects('/');
    }

    public function testSuccessfullLoginForManager()
    {
        $this->loadFixtureFiles([dirname(__DIR__) . '/Fixtures/User.yaml']);
        self::ensureKernelShutdown();
        $client = static::createClient();

        $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('authenticate');
        $client->request('POST', '/login', [
            '_csrf_token' => $csrfToken,
            'email' => 'manager@mail.com',
            'password' => 'password'
        ]);
        $this->assertResponseRedirects('/manager/');
    }

    public function testSuccessfullLoginForWorker()
    {
        $this->loadFixtureFiles([dirname(__DIR__) . '/Fixtures/User.yaml']);
        self::ensureKernelShutdown();
        $client = static::createClient();

        $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('authenticate');
        $client->request('POST', '/login', [
            '_csrf_token' => $csrfToken,
            'email' => 'worker@mail.com',
            'password' => 'password'
        ]);
        $this->assertResponseRedirects('/worker/sharingproposal/');
    }

    public function testLetAuthencatedUserAccessShop()
    {
        $client = static::createClient();
        $users = $this->loadFixtureFiles([(dirname(__DIR__)) . '/Fixtures/User.yaml']);
        $this->login($client, $users['user_active']);
        $client->request('GET', '/shop/item');
        $this->assertResponseStatusCodeSame(Response::HTTP_MOVED_PERMANENTLY);
        $client->followRedirect('/shop/item');
    }

    public function testLetAuthencatedUserInactiveAccessShop()
    {
        $client = static::createClient();
        $users = $this->loadFixtureFiles([(dirname(__DIR__)) . '/Fixtures/User.yaml']);
        $this->login($client, $users['user_inactive']);
        $client->request('GET', '/shop/item');
        $this->assertResponseStatusCodeSame(Response::HTTP_MOVED_PERMANENTLY);
        $this->assertResponseRedirects('/');
    }

    public function testLetAuthencatedUserNotApprovedAccessShop()
    {
        $client = static::createClient();
        $users = $this->loadFixtureFiles([(dirname(__DIR__)) . '/Fixtures/User.yaml']);
        $this->login($client, $users['user']);
        $client->request('GET', '/shop/item');
        $this->assertResponseStatusCodeSame(Response::HTTP_MOVED_PERMANENTLY);
        $this->assertResponseRedirects('/');
    }

    public function testSuperUserManagerAccessShop()
    {
        $client = static::createClient();
        $users = $this->loadFixtureFiles([(dirname(__DIR__)) . '/Fixtures/User.yaml']);
        $this->login($client, $users['user_manager']);
        $client->request('GET', '/shop/item');
        $this->assertResponseRedirects('/manager/');
    }

    public function testSuperUserWorkerAccessShop()
    {
        $client = static::createClient();
        $users = $this->loadFixtureFiles([(dirname(__DIR__)) . '/Fixtures/User.yaml']);
        $this->login($client, $users['user_worker']);
        $client->request('GET', '/shop/item');
        $this->assertResponseRedirects('/worker/sharingproposal/');
    }

    public function testManagerRequireManagerRole()
    {
        $client = static::createClient();
        $users = $this->loadFixtureFiles([(dirname(__DIR__)) . '/Fixtures/User.yaml']);
        $this->login($client, $users['user']);
        $client->request('GET', '/manager');
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

    public function testManagerRequireManagerRoleWithSufficientRole()
    {
        $client = static::createClient();
        $users = $this->loadFixtureFiles([(dirname(__DIR__)) . '/Fixtures/User.yaml']);
        $this->login($client, $users['user_manager']);
        $client->request('GET', '/manager/');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }
}
