<?php

namespace App\Tests\Entity;

use App\Entity\User;
use App\Repository\UserRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolation;

class UserTest extends KernelTestCase
{

    use FixturesTrait;

    public function getEntity(): User
    {
        return (new User())
            ->setEmail('usertest@mail.com')
            ->setUsername('usertest');
    }

    public function assertHasErrors(User $user, int $number = 0)
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($user);
        $messages = [];
        /** @var ConstraintViolation $error */
        foreach ($errors as $error) {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }
        $this->assertCount($number, $errors, implode(', ', $messages));
    }

    public function testValidEntity()
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    public function testInvalidUserMail()
    {
        $this->assertHasErrors($this->getEntity()->setEmail('usermail.com'), 1);
        $this->assertHasErrors($this->getEntity()->setEmail(''), 1);
    }

    public function testInvalidUserUsername()
    {
        $this->assertHasErrors($this->getEntity()->setUsername('username0'), 1);
        $this->assertHasErrors($this->getEntity()->setUsername('user name'), 1);
        $this->assertHasErrors($this->getEntity()->setUsername('u'), 1);
        $this->assertHasErrors($this->getEntity()->setUsername(''), 1);
    }

    public function testInvalidUsedEmail()
    {
        $this->loadFixtureFiles([dirname(__DIR__) . '/Fixtures/User.yaml']);
        $this->assertHasErrors($this->getEntity()->setEmail('user@mail.com'), 1);
    }

    public function testInvalidUsedUsername()
    {
        $this->loadFixtureFiles([dirname(__DIR__) . '/Fixtures/User.yaml']);
        $this->assertHasErrors($this->getEntity()->setUsername('user'), 1);
    }

    public function testNewUserStatus()
    {
        $this->loadFixtureFiles([dirname(__DIR__) . '/Fixtures/User.yaml']);
        $user = self::$container->get(UserRepository::class)->findOneBy(['email' => 'user@mail.com']);
        $this->assertEquals(User::STATUS_WAITING_APPROVAL,$user->getStatus());
    }
}
