<?php
namespace App\Tests\EventSubscriber;

use App\Entity\Distribution;
use App\EventSubscriber\DistributionSubscriber;
use App\EventSubscriber\ExceptionSubscriber;
use App\Repository\BasketRepository;
use App\Repository\DistributionRepository;
use App\Repository\SuggestedItemRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Workflow\Event\GuardEvent;

class DistributionSubscriberTest extends TestCase
{
    public function testEventSubscription()
    {
        $this->assertArrayHasKey('workflow.distribution.guard.share', DistributionSubscriber::getSubscribedEvents());
        $this->assertArrayHasKey('workflow.distribution.guard.close', DistributionSubscriber::getSubscribedEvents());
        $this->assertArrayHasKey('workflow.distribution.entered.opened', DistributionSubscriber::getSubscribedEvents());
        $this->assertArrayHasKey('workflow.distribution.entered.ended', DistributionSubscriber::getSubscribedEvents());
        $this->assertArrayHasKey('workflow.distribution.entered.closed', DistributionSubscriber::getSubscribedEvents());    
    }

 //   public function testGoToShareState()
 //   {
 //       $params = $this->getMockBuilder(ParameterBagInterface::class)
 //           ->disableOriginalConstructor()
 //           ->getMock();
 //       $entityManager = $this->getMockBuilder(EntityManagerInterface::class)
 //           ->disableOriginalConstructor()
 //           ->getMock();
 //       $basketRepository = $this->getMockBuilder(BasketRepository::class)
 //           ->disableOriginalConstructor()
 //           ->getMock();
 //       $distributionRepository = $this->getMockBuilder(DistributionRepository::class)
 //           ->disableOriginalConstructor()
 //           ->getMock();
 //       $suggestedItemRepository = $this->getMockBuilder(SuggestedItemRepository::class)
 //           ->disableOriginalConstructor()
 //           ->getMock();
//
 //       $subscriber = new DistributionSubscriber($params,$entityManager,$basketRepository,$distributionRepository,$suggestedItemRepository);
 //       //$kernel = $this->getMockBuilder(KernelInterface::class)->getMock();
 //       $event = $this->getMockBuilder(GuardEvent::class)
 //       ->disableOriginalConstructor()
 //       ->getMock();
//
 //       $dispatcher = new EventDispatcher();
 //       $dispatcher->addSubscriber($subscriber);
 //       $dispatcher->dispatch($event);
//
 //       
//
//
 //   }

}