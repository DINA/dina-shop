Liste des tests
==============

Back Office "Manager"
===============

Membres
---------------

### Accès autorisés à la page membre
L'accès est autorisé pour:
- `ROLE_ADMIN`
- `ROLE_COORDINATOR`

#### Modification des super utilisateurs par `ROLE_ADMIN`
- L'administrateur peut modifier:
    - l'email
    - l'alias
    - administrateur mot de passe
- L'administrateur ne peut pas modifier: 
    - son statut
- L'administrateur ne peut pas supprimer le compte utilisateur

#### Modification des super utilisateurs par `ROLE_COORDINATOR`
- Aucune modification possible

#### Modification des utilisateurs par `ROLE_ADMIN` ou `ROLE_COORDINATOR`
- Les supers utilisateurs peuvent tout modifier et supprimer le compte utilisateur

#### Ajout d'un utilisateur par `ROLE_ADMIN` ou `ROLE_COORDINATOR`
- Les supers utilisateurs peuvent créer un compte utilisateur

Producteurs
---------------

#### Accès autorisés à la page producteurs
L'accès est autorisé pour `ROLE_ADMIN`.

#### Ajout et modification d'un producteur
- Les champs `Nom` et `Code` sont obligatoires
- On ne peut pas rentrer un code qui ne serait pas présent dans le fichier de défintion des catalogues

#### Suppression d'un producteur
- les producteurs sont supprimables
- les catalogues dépendants de ces producteurs restent en place
- il n'est PLUS possible de créer un catalogue dépendant de ce producteur
- aucun impact sur les favoris et l'historique de commande des embres


Catalogues
---------------

#### Accès autorisés à la page catalogues
L'accès est autorisé pour:
- `ROLE_ADMIN`
- `ROLE_COORDINATOR`
- `ROLE_MANAGER`

### Création d'un catalogue depuis l'interface "manager"
Seuls les champs `Nom` et `Type` sont obligatoires
#### Erreurs de validation
- Lorsque le catalogue "dropbox" et le catalogue "local" sont renseignés
- Si extension du fichier catalogue différente de `.ods, .xls, .xlsx` 
- Si le nom de fichier ne correspond pas au catalogue choisi
- Si la structure du fichier n'est pas reconnue

### Édtion d'un catalogue
#### Quand le catalogue n'est pas encore lié à une distribution
- Il est possible d'insérer un fichier catalogue dans un catalogue déjà créé
- Si des articles étaient déjà présent, le nouveau catalogue efface l'ancien (le lien vers le fichier .xls est mis à jour)
- l'archivage d'un catalogue en même temps que l'ajout d'un fichier catalogue provoque une erreur
- l'archivage est autorisé
#### Quand le catalogue est (ou a été) lié à une distribution
- l'archivage n'est PAS autorisé
- l'édition n'est plus autorisée
#### Quand le catalogue est archivé
- l'édition n'est PAS autorisée

### Archivage d'un catalogue
#### Quand le catalogue n'est pas encore lié à une distribution
- l'archivage est autorisé
#### Quand le catalogue est lié à une distribution
- l'archivage n'est PAS autorisé

### Suppression d'un catalogue
#### Quand le catalogue n'est pas encore archivé
- la suppression n'est pas autorisée
#### Quand le catalogue est archivé
- la suppression est autorisée si le catalogue n'a jamais été utilisé
- la suppression n'est PAS autorisée si le catalogue a été utilisé lors d'une distribution

### Consultation d'un catalogue
- le lien vers le fichier tableur doit disparaître lors de l'archivage du catalogue
- le bouton d'archivage doit disparaître une fois le catalogue archivé

Articles
---------------
#### Ajout d'un article
- Aucun test
#### Édition d'un article
- Toujours autorisé dans un catalogue en ligne
#### Suppression d'un article
- autorisé si catalogue pas utilisé par une distribution
- interdit sinon
#### Import
- renseignement d'un fichier obligatoire
#### Filtres
- Il faut cliquer sur "Rechercher" pour mettre à jour tous les champs

Distributions
---------------

#### Accès autorisés à la page distribution
L'accès est autorisé pour:
- `ROLE_ADMIN`
- `ROLE_MANAGER`

### Workflow
#### Passage à l'état `shareinprogress`
- Seulement si aucune autre distribution en cours
- Au moins un catalogue lié à la distribution
#### Passage à l'état `sharedone`
- Aucun test
#### Retour à l'état `shareinprogress`
- Aucun test
#### Passage à l'état `opened`
- Aucun test
- Tous les `SuggestedItems` sont supprimés
#### Retour à l'état `sharedone`
- Autorisé seulement pour `ROLE_ADMIN`
#### Passage à l'état `closed`
- Aucun test
- Tous les `Basket` sont  supprimés
- Un mail avec les bons de commande est envoyé automatiquement à DINA
#### Retour à l'état `opened`
- Autorisé seulement pour `ROLE_ADMIN`
#### Passage à l'état `ended`
- En environnement de prod:
    - La date de livraison doit être antérieure à la date du jour
    - Sauf pour l'utilisateur `ROLE_ADMIN` qui peut clore avant la date de livraison
- En environnement de dev:
    - Pas de restrictions
- Les fichiers de bons de commandes sont supprimés du serveur
- Les "Extra" catalogues sont automatiquement archivés
### Ajout d'une distribution
- Ajout valide si aucune autre distribution en cours à l'état `new`
- Date obligatoire
- Catalogues pas obligatoires
### Édition d'une distribution
#### Édition de l'entité distribution
- État < `opened`: édition de tous les champs possible
- État == `opened`: édition de la date possible
- État > `opened`: édition impossible
#### Suppression d'un catalogue de la distribution
- Tous les `SharedProducts` qui lui étaient liés sont supprimés
#### Modifications de la proposition de partage
- Le `ROLE_MANAGER` peut éditer ou supprimer des articles mais pas en rajouter 
#### Modifications des commandes des membres
- Le `ROLE_MANAGER` peut éditer des articles mais ni en rajouter ni en supprimer
### Suppression d'une distribution
- Autorisé seulement pour `ROLE_ADMIN`
- Uniquement pour les distribution dans les états `new` ou `ended`

Back Office "Worker"
===============
#### Accès autorisés à l'espace worker
L'accès est autorisé pour:
- `ROLE_WORKER`

Catalogues
---------------
#### Ajout
- Le catalogue ajouté est automatiquement lié à la distribution
- Il est obligatoire de sélectionner un fichier
#### Édition
- Seuls le nom et le commentaire sont modifiables
#### Liens avec la distribution
- Il n'est pas possible d'ajouter ou de retirer un catalogue autre que:
    - Mercuriale Fruits & Legumes
    - Producteur DINA
#### Suppression
- Il n'est pas possible de supprimer un catalogue

Articles (Gestion)
---------------
### Gestion des articles
#### Création
- Il n'est pas possible de créer des articles
#### Édition
- Il est possible d'étider un article
#### Suppression
- Il n'est pas possible de supprimer un article

### Gestion des articles extra
#### Création
- Il est possible de créer des articles extra
#### Édition
- Il est possible d'éditer un article extra
#### Suppression
- Il est possible de supprimer un article extra

Articles (Sélection)
---------------
### Sélection des articles
- Une popup s'affiche à la 1ère connexion pour avertir s'il y a des articles suggérés ou non
- Pas de tests
### Articles suggérés
- Doivent indiquer quel membre demande le produit
- Si un membre supprime le produit suggéré, son nom doit disparaître de la liste
- Si le membre était le seul membre à demander, le produit doit disparaitre.

Proposition de partage
---------------
### Produits au kg
- champ kg sélectionné si la chaîne "kg" est détectée dans le conditionnement
#### Fromage
- précoché si article fait parti de la famille "fromage" et conditionnement au kg
#### Autres
- vérifie que le conditionnement est valide
- vérifie que le champ "pièces" est vide
### Produits à la pièce
- Vérifie que le champ pièce est renseigné
- vérifie que le champ pièce est < à 100
- auto remplissage si pièces détectées (voir fichier productManager)
### Génération de la feuille excel
- Boutons mail et téléchargement

Front Office
===============

Articles (Sélection)
---------------

### Proposition de partage
#### Achat
- un achat doit mettre le produit dans le panier avec une quantité de 1
- un achat depuis la PP doit créer un nouvau produit même s'il existe déjà en tant que commande individuelle
#### Vue
- la vue est disponible dans tous les états de la distribution sauf `new` et `ended` 
### Commande individuelle
#### Vue
- la vue n'est disponible que lorsque la distribution est dans l'état `opened`
#### Favoris
- la vue favori ne doit afficher que les items achetables
#### Achat
- un achat n'est possible que lorsque la distribution est dans l'état `opened`
- un achat doit mettre le produit dans le panier avec une quantité de 1
- un achat doit créer un nouvau produit même s'il existe déjà en tant que produit dans la PP
#### Suggestion quand la distribution est >= à l'état `opened`
- si une distribution existe à l'état `new`: une suggestion doit mettre le produit dans la liste des articles suggérés de la distribution à l'état `new`
- sinon: suggestion impossible
#### Suggestion quand la distribution est < à l'état `opened`
- une suggestion doit mettre le produit dans la liste des articles suggérés de la distribution en cours

Articles (Gestion)
---------------

### Favoris
- suppression d'un favori
#### Vue
- la vue est toujours disponible
### Suggestion d'articles
- suppression d'une suggestion
#### Vue
- la vue est toujours disponible
### Articles Extra
#### Vue
- la vue n'est disponible que lorsque la distribution est dans l'état `opened`
#### Création
- Il est possible de créer des articles extra
#### Édition
- Il n'est pas possible d'éditer un article extra
#### Suppression
- Il n'est pas possible de supprimer un article extra

Panier (vue)
---------------
#### Vue
- la vue n'est disponible que lorsque la distribution est dans l'état `opened`
### Choix des unités
#### Proposition de partage
- les unités sont grisées
#### Commande individuelle
- les unités sont préselectionnées
- si le produit est décolisable, pas de pré-sélection
### Quantité
- si le produit est au kg, nombre décimal autorisé
- nombre entier sinon
- quantitié commandée doit être >= colisage mini si produit décolisé
### Précommande
- si produit commandé depuis le frais: pas de précommande
- si produit commandé depuis la mercuriale ET présent dans le frais: pas de précommande
- si produit commandé depuis la mercuriale MAIS pas présent dans le frais: précommande
### Boutons
- "Enregistrer" met les prix à jour et effectue une validation
- "Commander" vide le panier et les ajoute à la commande (`Order`)

Commande (vue)
---------------
#### Vue
- la vue n'est disponible que lorsque la distribution est dans l'état `opened`
#### Commande dans le panier d'un produit déjà commandé
- on additionne les quantitiés
#### Suppression de la commande
- la commande doit être supprimée
#### Modification de la commande
- tous les produits repassent dans le panier (`Basket`)

Historique (vue)
---------------
#### Vue
- la vue est toujours disponible
### Commande
#### RE-commande d'un article
- affichage du bouton caddie uniquement si le produit est présent dans les catalogues en cours et que la distribution est dans l'état `opened`
#### RE-commande de toute la commande
- bouton "repasser toute la commande" si la distribution courante est dans l'état `opened` (SAUF pour la commande "en cours")
### Favoris
#### Ajout favori
- ajouter un nouveau favori
- ajouter un favori déjà existant
- ajout des favoris depuis des articles anciens ou non

Autre
===============

Profil utilisateur
---------------

#### Modification d'un profil `ROLE_USER, ROLE_ADMIN`
- L'utilisateur peut modifier: 
    - son mot de passe
- L'utilisateur ne peut pas modifier: 
    - son email
    - son alias
    - son statut

#### Modification d'un profil `ROLE_WORKER, ROLE_MANAGER, ROLE_COORDINATOR`
- Le super utilisateur ne peut rien modifier




