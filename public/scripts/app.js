/************
 * This files only contains pure JS code
 * This script is used only to manage item's: 
 * - add to basket
 * - add / remove a suggestion
 * - add / remove a bookmark
*************/

/* Voir aussi: http://blog.michaelperrin.fr/2013/03/07/notification-flash-messages-for-json-responses-with-symfony2/ */

let flashOptions = {
    progress: true, // displays a progress bar at the bottom of the flash message
    interactive: true, // Define flash message actions (pause on mouseover, close on click) 
    timeout: 3000, // Flash message timeout
    appear_delay: 200, // Delay before flash message appears
    container: '.flash-container', // Flash messages container element selector
    theme: 'dark', // CSS theme (availables: default, dark)
    classes: {
        container: 'flash-container', // Custom container css class
        flash: 'flash-message', // Flash message element css class
        visible: 'is-visible', // Flash message element css visible class
        progress: false, // Flash message progress bar element css class
        progress_hidden: 'is-hidden' // Flash message progress bar element hidden css class
    }
};

let post = async function (form, data) {

    try {
        //let response = await fetch(form.getAttribute('action'), {
        let response = await fetch(form.action, {
            method: 'POST',
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            },
            body: data
        })
        return response;
    } catch (e) {
        alert(e)
    }
}

let get = async function (url) {
    try {
        let response = await fetch(url, {
            method: "GET",
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            },
            body: undefined,
        });
        return response;
    } catch (e) {
        alert(e)
    }
}

let addToSharingSuggestions = async function (form, data) {

    response = await post(form, data);

    try {
        let responseData = await response.json()
        if (response.ok === false) {
            console.log("erreur");
            window.FlashMessage.error("Suggestion error", flashOptions);
        } else {
            console.log(responseData.flash.message);
            if (responseData.error == 0) {
                window.FlashMessage.success(responseData.flash.message, flashOptions);
            } else if (responseData.error == 1) {
                window.FlashMessage.warning(responseData.flash.message, flashOptions);
            } else if (responseData.error == 2) {
                window.FlashMessage.error(responseData.flash.message, flashOptions);
            }
        }
    } catch (e) {
        alert(e)
    }
}

let addToSharingProposal = async function (form, data) {

    response = await post(form, data);

    try {
        let responseData = await response.json()
        if (response.ok === false) {
            console.log("erreur");
            window.FlashMessage.error("Add to sharingProposal error", flashOptions);
        } else {
            console.log(responseData.flash.message);
            document.querySelector('#worker__sharingProposal__count').innerText = responseData.nbItems;
            window.FlashMessage.success(responseData.flash.message, flashOptions);
        }
    } catch (e) {
        alert(e)
    }
}

let addToBasket = async function (form, data) {

    response = await post(form, data);

    try {
    let responseData = await response.json()
    if (response.ok === false) {
        console.log("erreur");
        window.FlashMessage.error("Add to basket error", flashOptions);
    } else {
        console.log(responseData.flash.message);
        document.querySelector('#shop__basket__sharedcount').innerText = responseData.nbSharedItems;
        document.querySelector('#shop__basket__individualcount').innerText = responseData.nbIndividualItems;
        if ('success' == responseData.flash.alert) {
            window.FlashMessage.success(responseData.flash.message, flashOptions);
        } else {
            window.FlashMessage.error(responseData.flash.message, flashOptions);
        }
    }
    } catch (e) {
        alert(e)
    }    

}

let addToBookmark = async function (form, data) {

    response = await post(form, data);

    function createShopFormButton(action)
    {
        //create a form
        var f = document.createElement("form");
        f.setAttribute('method', "post");
        f.setAttribute('action', action);
        f.setAttribute('class', "btn-bookmark-shop");

        //create a button
        var b = document.createElement("button");
        b.type = "submit";
        b.setAttribute('class', "btn btn-primary mr-1");
        b.setAttribute('data-toggle', "tooltip");
        b.setAttribute('title', "commander");
        b.value = "";

        let s = document.createElement("span");
        s.setAttribute('class', "oi oi-cart");

        // add all elements to the form
        b.appendChild(s);
        f.appendChild(b);

        //Add a listener for this shop button
        f.addEventListener('submit', async function (e) {
            e.preventDefault();
            let data = new FormData(f);
            let response = await addToBasket(f, data);
        })

        return f;
    }

    function insert_Row(id, designation, disabled) {
        if (document.getElementById('bookmark-list')) {
            
            let row = document.getElementById('bookmark-list').insertRow(-1);
            
            let c0 = row.insertCell(0);
            let c1 = row.insertCell(1);
            /* See basket_item_add for URL */
            if (!disabled) {
                let form = createShopFormButton('/shop/basket/' + id + '/add');
                c0.appendChild(form);
            }
            
            c1.innerHTML = designation ;
        }
    }

    try {
        let responseData = await response.json()
        if (response.ok === false) {
            console.log("erreur");
            alert("server error")
        } else {
            console.log(responseData.flash.message);
            if (responseData.error == 0) {
                window.FlashMessage.success(responseData.flash.message, flashOptions);
                insert_Row(responseData.id, responseData.designation, responseData.disabled);
            } else if(responseData.error == 1) {
                window.FlashMessage.warning(responseData.flash.message, flashOptions);
            }
        }
    } catch (e) {
        alert(e)
    }

}

let removeFromList = async function (form, data) {

    response = await post(form, data);

    try {
        let responseData = await response.json()
        if (response.ok === false) {
            console.log("list remove erreur");
            window.FlashMessage.error("list error", flashOptions);
        } else {
            console.log(responseData.flash.message);
            if (responseData.error == 0) {
                window.FlashMessage.warning(responseData.flash.message, flashOptions);
                /* Remove tr line */
                let tr = form.closest('tr');
                console.log('suppression', tr)
                tr.remove();
                /* Refresh page to regenerate the form */
                if (!responseData.url) {
                    return;
                } else {
                    window.location.href = responseData.url;
                }
            }
        }
    } catch (e) {
        alert(e)
    }
}

/* Warning! This function is called twice:
    - first time at init
    - 2nd time when datatble is redrawn
    => Do not add an event listener on all buttons here, otherwise some requests will be sent twice
 */
let catchActionsButtons = function () {

    let shopForms = document.querySelectorAll('.btn-shop');
    shopForms.forEach(form => {
        form.addEventListener('submit', async function (e) {
            e.preventDefault();
            let data = new FormData(form);
            let response = await addToBasket(form, data);
        })
    })

    let bookmarkForms = document.querySelectorAll('.btn-bookmark');
    bookmarkForms.forEach(form => {
        form.addEventListener('submit', async function (e) {
            e.preventDefault();
            let data = new FormData(form);
            let response = await addToBookmark(form, data);
        })
    })

    let shareForms = document.querySelectorAll('.btn-share');
    shareForms.forEach(form => {
        form.addEventListener('submit', async function (e) {
            e.preventDefault();
            let data = new FormData(form);
            let response = await addToSharingProposal(form, data);
        })
    })

    let suggestForms = document.querySelectorAll('.btn-suggest');
    suggestForms.forEach(form => {
        form.addEventListener('submit', async function (e) {
            e.preventDefault();
            let data = new FormData(form);
            let response = await addToSharingSuggestions(form, data);
        })
    })

    let removeForms = document.querySelectorAll('.btn-remove');
    removeForms.forEach(form => {
        form.addEventListener('submit', async function (e) {
            e.preventDefault();
            let data = new FormData(form);
            let response = await removeFromList(form, data);
        })
    })
}

/* This function is called once, not called after a redraw */
let catchOtherActionsButtons = function () {
    let shareForms = document.querySelectorAll('.btn-share-all');
    shareForms.forEach(form => {
        form.addEventListener('submit', async function (e) {
            e.preventDefault();
            let data = new FormData(form);
            let response = await addToSharingProposal(form, data);
        })
    })

    let shopBoorkmarkForms = document.querySelectorAll('.btn-bookmark-shop');
    shopBoorkmarkForms.forEach(form => {
        form.addEventListener('submit', async function (e) {
            e.preventDefault();
            let data = new FormData(form);
            let response = await addToBasket(form, data);
        })
    })

    let shopSuggestForms = document.querySelectorAll('.btn-suggest-shop');
    shopSuggestForms.forEach(form => {
        form.addEventListener('submit', async function (e) {
            e.preventDefault();
            let data = new FormData(form);
            let response = await addToSharingSuggestions(form, data);
        })
    })
}

let getCatalogueFormat = function (selectedCatalogue) {
    return selectedCatalogue.options[selectedCatalogue.selectedIndex].getAttribute("data-format");
}

function check() {
    document.getElementById("catalogue_autoArchive").checked = true;
}

function uncheck() {
    document.getElementById("catalogue_autoArchive").checked = false;
}

function setAutoArchiveCheckBox(catalogueFormat) {
    if ((catalogueFormat == 'Format_RelaisVert_Mercuriale_FruitsLegumes')
        || (catalogueFormat == 'Format_DINA_Producer')) {
        check();
    } else {
        uncheck();
    }
}

let autoArchiveManager = function () {
    /* Modal for empty catalogues */
    let fileInput = document.querySelector('#catalogue_spreadsheetFile');
    let dropboxFileInput = document.querySelector('#catalogue_dropboxFile');
    let catalogueNew = document.querySelector('#catalogue-new');
    let catalogueUpdate = document.querySelector('#catalogue-update');

    if (!fileInput) {
        fileInput = document.querySelector('#catalogue_import_spreadsheetFile');
    }

    if (catalogueNew) {

        catalogueNew.addEventListener('click', async function (e) {
            let filename = null;
            if (fileInput.files[0]) {
                filename = fileInput.files[0].name;
            }
            let dropboxFilename = dropboxFileInput.value

            /* Show a modal if no file provided */
            if (!dropboxFilename && !filename) {
                e.preventDefault();
                $('#catalogue-warning').modal(options)
            } else {
                document.body.style.cursor = 'wait';
            }
        })
    }
    if (catalogueUpdate) {
        catalogueUpdate.addEventListener('click', async function (e) {
            document.body.style.cursor = 'wait';
        })
    }

    //let buttonOK = document.querySelector('#modal-ok');
    //buttonOK.addEventListener('click', async function (e) {
    //    document.body.style.cursor = 'wait';
    //})

    /* Auto Archive checkbox */
    let selectedCatalogue = document.querySelector('#catalogue_type');
    if (selectedCatalogue) {

        format = getCatalogueFormat(selectedCatalogue);
        setAutoArchiveCheckBox(format);
        selectedCatalogue.onchange = function (e) {
            format = getCatalogueFormat(this);
            setAutoArchiveCheckBox(format);

        }
    }
}


$(document).ready(function () {
    /* Catch all actions buttons at startup  */
    catchActionsButtons();
    catchOtherActionsButtons();
    autoArchiveManager();

    /* Show appropriate pill based on #anchor in URL - https://github.com/twbs/bootstrap/issues/25220 */
    const anchor = window.location.hash;
    $(`a[href="${anchor}"]`).tab('show')
})

