/* JS for dropbox button and modal popup */

options = {

    // Required. Called when a user selects an item in the Chooser.
    success: function (files) { // alert("Here's the file link: " + files[0].link)
        let fileInput = document.getElementById("catalogue_dropboxFile")
        let fileInputLink = document.getElementById("catalogue_dropboxFileLink")

        fileInput.value = files[0].name
        fileInputLink.value = files[0].link
    },

    // Optional. Called when the user closes the dialog without selecting a file
    // and does not include any parameters.
    cancel: function () { },

    // Optional. "preview" (default) is a preview link to the document for sharing,
    // "direct" is an expiring link to download the contents of the file. For more
    // information about link types, see Link types below.
    linkType: "direct",
    // or "direct"

    // Optional. A value of false (default) limits selection to a single file, while
    // true enables multiple file selection.
    multiselect: false,
    // or true

    // Optional. This is a list of file extensions. If specified, the user will
    // only be able to select files with these extensions. You may also specify
    // file types, such as "video" or "images" in the list. For more information,
    // see File types below. By default, all extensions are allowed.
    extensions: [
        '.ods', '.xls', '.xlsx'
    ],

    // Optional. A value of false (default) limits selection to files,
    // while true allows the user to select both folders and files.
    // You cannot specify `linkType: "direct"` when using `folderselect: true`.
    folderselect: false, // or true
};


var button = Dropbox.createChooseButton(options);
document.getElementById("dropbox_container").appendChild(button);
