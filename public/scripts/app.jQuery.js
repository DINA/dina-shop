/************ 
 * This files only contains jQuery code
 * This script is used only in baskets, for sharing proposal and member's order 
*************/

let cb = function (updateUrl, selectValue, quantity, selectCheese, comment) {
    $.ajax({
        type: "POST",
        url: updateUrl,
        data: {
            unit: selectValue,
            quantity: quantity,
            cheese: selectCheese,
            comment: comment,
        },
        success: function (response) {
            console.log("Mise à jour produit");
            //window.FlashMessage.success(response.flash.message, flashOptions);

            //compue price only if basket is in the url
            let isBasket = updateUrl.indexOf('basket');
            if (isBasket != -1) {   
                $("#price-value").html('€' + parseFloat(response.price.price).toFixed(2));
                $("#price-vat-value").html('€' + parseFloat(response.price.VATprice).toFixed(2));
            }
        }
    });
}


$(document).on('change', '.select-unit', function () {
    let selectUnit = this.value;
    let productId = this.dataset.productId;
    let updateUrl = this.dataset.updateUrl;
    console.log(updateUrl, selectUnit);

    cb(updateUrl, selectUnit, null, null, null);
})

$(document).on('change', '.quantity-input', function () {
    let quantity = this.value;
    let productId = this.dataset.productId;
    let updateUrl = this.dataset.updateUrl;
    console.log(updateUrl, quantity);

    cb(updateUrl, null, quantity, null, null);
})

$(document).on('change', '.piece-input', function () {
    let quantity = this.value;
    let productId = this.dataset.productId;
    let updateUrl = this.dataset.updateUrl;
    console.log(updateUrl, quantity);

    cb(updateUrl, null, quantity, null, null);
})

$(document).on('change', '.select-cheese', function () {
    let selectCheese = this.checked;
    let productId = this.dataset.productId;
    let updateUrl = this.dataset.updateUrl;
    console.log(updateUrl, selectCheese);

    cb(updateUrl, null, null, selectCheese, null);
})

$(document).on('change', '.shareProductComment', function () {
    let comment = this.value;
    let productId = this.dataset.productId;
    let updateUrl = this.dataset.updateUrl;
    console.log(updateUrl, comment);

    cb(updateUrl, null, null, null, comment);
})

$(document).on('click', '.form-delete-button', function () {

    let target = this;
    let deleteUrl = this.dataset.removeUrl;
    let deleteToken = this.dataset.deleteToken;

    $.ajax({
        dataType: 'json',
        method: 'DELETE',
        headers: {
            'x-remove-product-token': deleteToken
        },
        url: deleteUrl,
        success: function (data) {
            console.log("suppression produit");
            window.FlashMessage.warning(data.flash.message, flashOptions);
            /* Remove tr line */
            let tr = $(target).closest('tr');
            let product = tr.nextUntil('.spacer');
            tr.fadeOut().remove();
            product.fadeOut().remove();
            /* Refresh page to regenerate the form */
            if (!data.url) {
                return;
            } else {
                window.location.href = data.url;
            }
        },
        error: function () {
            alert('An unexpected error has occurred.');
        }
    });
  
});