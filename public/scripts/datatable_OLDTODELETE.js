


$('#datatable').on('click', 'button.btn-shop', function () {
    var btnId = $(this).attr('id');
    const valId = btnId.split('_');

    $.ajax({
        type: "POST",
        url: '/shop/basket/' + valId[1] + '/add',

       success: function (response) {
           console.log(response.flash.message);
           window.FlashMessage.success(response.flash.message, {
               progress: true, // displays a progress bar at the bottom of the flash message
               interactive: true, // Define flash message actions (pause on mouseover, close on click) 
               timeout: 4000, // Flash message timeout
               appear_delay: 200, // Delay before flash message appears
               container: '.flash-container', // Flash messages container element selector
               theme: 'dark', // CSS theme (availables: default, dark)
               classes: {
                   container: 'flash-container', // Custom container css class
                   flash: 'flash-message', // Flash message element css class
                   visible: 'is-visible', // Flash message element css visible class
                   progress: false, // Flash message progress bar element css class
                   progress_hidden: 'is-hidden' // Flash message progress bar element hidden css class
               }
           });
        }
   });

});   


function format(d) {
    console.log(d);
    string = '<ul>'
        + '<li>' + 'TVA: ' + d.vat + '%' + '</li>'
        + '<li>' + 'Prix décolisé: ' + d.unpackedPrice + '€' + '</li>'
        + '<li>' + 'Contrôle: ' + d.inspection + '</li>'
        + '<li>' + 'Label: ' + d.label + '</li>'
        + '<li>' + 'DLC: ' + d.ed + '</li>'
        + '<li>' + 'EAN13: ' + d.EAN13 + '</li>'
        + '<li>' + 'UV: ' + d.SKU + '</li>'
        + '</ul>';
/* inspection - label - family - EAN13 - ed (expiration date) - SKU - unpackedPrice */
    
    return string;
}

$(document).ready(function () {
    console.log(mapping)
    let table = document.querySelector('#datatable');
    let catalogueId = table.dataset.catalogueId;
    console.log(catalogueId)
    let dt = $('#datatable').DataTable({
        language: {
            url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/French.json'
        },
        // Server-side parameters
        "processing": true,
        "serverSide": true,
        // Ajax call
        "ajax": {
            //"url": "{{ path('search_item_ajax') }}",
            "url": "/search/item/" + catalogueId,
            "type": "POST",
            //"dataSrc": "",
            'data': function (data) {
                // Read values
                let origins = $('#searchText').val();
                let origins = $('#origins').val();
                let brands = $('#brands').val();
                let families = $('#families').val();

                // Append to data
                data.origins = origins;
                data.searchText = searchText;
                data.families = families;
                data.brands = brands;
            }
        },
        // Pour retirer les colonnes vides
        "fnDrawCallback": function () {
            var api = this.api();
            setTimeout(function () {
                api.columns().flatten().each(function (colIdx) {
                    var columnData = api.columns(colIdx).data().join('');
                    if (columnData.length == (api.rows().count() - 1) && colIdx != 0) {
                        api.column(colIdx).visible(false);
                    }
                });
            }, 0)
        },
        "columnDefs": [
            {
                "targets": [0],
                "class": "details-control",
                "orderable": false,
                "data": null,
                "defaultContent": "",
                "width": "80px",
            },
            {
                "targets": -1,
                "data": null,
                "render": function (data, type, full, meta) {
                    return '<button id="btn_' + data.id +'" class="btn btn-primary btn-shop mr-1" data-toggle="tooltip" data-placement="top"  href=""><span style="font-size:smaller" data-feather="shopping-cart"></span>Add</button>'
                }
            },   
            {
                "data": "reference",
                "title": "Réference",
                "targets": [1],
                "visible": true,
                //"searchable": false
                "width": "100px",
            },
            {
                "data": "designation",
                "title": "Désignation",
                "targets": [2],
            },
            {
                "data": "packaging",
                "title": "Condition-nement",
                "targets": [3],
                "width": "100px",
            },
            {
                "data": "brand",
                "name": "brand",
                "title": "Marque",
                "targets": [4],
                "width": "200px",
            },
            {
                "data": "origin",
                "title": "Origine",
                "targets": [5],
                "width": "150px",
            },
            {
                "data": "calibre_categ",
                "title": "calibre/categ",
                "targets": [6],
            },
            {
                "data": "family",
                "title": "Famille",
                "targets": [7],
            },
            {
                "data": "unitaryPrice",
                "title": "Prix",
                "targets": [8],
                "width": "100px",
                "render": function (data) {
                    return data + '€';
                },
            },
            {
                "data": "packing",
                "title": "Colisage",
                "targets": [9],
                "width": "100px",
            },
            {
                "data": "minPacking",
                "title": "Colisage mini",
                "targets": [10],
                "width": "100px",
            },
            {
                "data": "comment",
                "title": "Commentaire",
                "targets": [11],
                //"visible": false
            },
            {
                "data": "customField1",
                "title": "customField1",
                "targets": [12],
            },
            {
                "data": "customField2",
                "title": "customField2",
                "targets": [13],
            },
            {
                "data": "customField3",
                "title": "customField3",
                "targets": [14],
            },
            {
                "data": "customField4",
                "title": "customField4",
                "targets": [15],
            },
        ],
        //noms de colonnes définis ici avec le html désactivé
        //"columns": [
        //    {
        //        "class": "details-control",
        //        "orderable": false,
        //        "data": null,
        //        "defaultContent": ""
        //    },
        //    { "data": "reference",  "title": "reference" },
        //    { "data": "designation", "title": "designation" },
        //    { "data": "packaging", "title": "Conditionnement"},
        //    { "data": "brand", "title": "Marque" },
        //    { "data": "origin", "title": "Origine"},
        //    { "data": "calibre_categ", "title": "calibre/categ"},
        //    { "data": "unitaryPrice", "title": "prix" },
        //    { "data": "packing", "title": "Colisage" },
        //    { "data": "minPacking", "title": "Colisage mini" },
        //    { "data": "comment", "title": "Commentaire" },
        //],
        //
        // Classic DataTables parameters
        //"lengthChange": true,
        //"pageLength": [[50, 100, 200, -1], [50, 100, 200, "All"]],
        //"bInfo": false,
        //"dom": '<l<t> ip>',

    });

    // Array to track the ids of the details displayed rows
    var detailRows = [];

    $('#datatable tbody').on('click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = dt.row(tr);
        var idx = $.inArray(tr.attr('id'), detailRows);

        if (row.child.isShown()) {
            tr.removeClass('details');
            row.child.hide();

            // Remove from the 'open' array
            detailRows.splice(idx, 1);
        }
        else {
            tr.addClass('details');
            row.child(format(row.data())).show();

            // Add to the 'open' array
            if (idx === -1) {
                detailRows.push(tr.attr('id'));
            }
        }
    });

    // On each draw, loop over the `detailRows` array and show any child rows
    dt.on('draw', function () {
        $.each(detailRows, function (i, id) {
            $('#' + id + ' td.details-control').trigger('click');
        });
    });

    $('#searchText').keyup(function () {
        dt.draw();
    });
    $('#origins').change(function () {
        dt.draw();
    });
    $('#brands').change(function () {
        dt.draw();
    });
    $('#families').change(function () {
        dt.draw();
    });

    // Sonstructs the suggestion engine
    var names = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        // prefetch super rapide mais marche pas avec mes filtres
        //prefetch: {
        //    url: "/search/item-names/" + catalogueId,
        //    cache: false,
        //},
        remote: {
            wildcard: '%QUERY',
            url: "/search/item-names/" + catalogueId +'?searchText=%QUERY',
            cache: false,
            replace: function (url, uriEncodedQuery) {
                let origins = $('#origins').val();
                let brands = $('#brands').val();
                let families = $('#families').val();
                //if (!origins) return url;
                //if (!brands) return url;
                //if (!families) return url;
                //correction here
                return  url.replace("%QUERY", uriEncodedQuery) + '&origins=' + encodeURIComponent(origins) + '&brands=' + encodeURIComponent(brands) + '&families=' + encodeURIComponent(families) 
            },
        },
        
    });

    // Initializing the typeahead with remote dataset
    $('#searchText').typeahead(null, {
        name: 'names',
        source: names,
        limit: 10 /* Specify maximum number of suggestions to be displayed */
    });
    
});



//
//// Avec des noms de colonnes dynamiques
//function getData(catalogueId, cb_func) {
//    $.ajax({
//        url: "/search/item/" + catalogueId,
//        type: "POST",
//        success: cb_func
//    });
//}
//
//function capitalizeFirstLetter(string) {
//    return string.charAt(0).toUpperCase() + string.slice(1);
//}
//
//$(document).ready(function () {
//    let table = document.querySelector('#datatable');
//    let catalogueId = table.dataset.catalogueId;
//    console.log(catalogueId)
//
//    getData(catalogueId, function (data) {
//        var columns = [];
//        //data = JSON.parse(data);
//        columnNames = Object.keys(data.data[0]);
//        for (var i in columnNames) {
//            columns.push({
//                data: columnNames[i],
//                title: capitalizeFirstLetter(columnNames[i])
//            });
//        }
//        $('#datatable').DataTable({
//            language: {
//                url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/French.json'
//            },
//            "processing": true,
//            "serverSide": true,
//            //data: data.data,
//            columns: columns,
//            // Classic DataTables parameters
//            "lengthChange": false,
//            "pageLength": 100,
//            "bInfo": false,
//            "dom": '<lf<t> ip>',
//        });
//    });
//
//
//});
//