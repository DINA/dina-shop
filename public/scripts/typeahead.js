$(document).ready(function () {
  
    let table = document.querySelector('#datatable');
    let catalogueId = table.dataset.catalogueId;


    // Sonstructs the suggestion engine
    var names = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        // prefetch super rapide mais marche pas avec mes filtres
        //prefetch: {
        //    url: "/search/item-names/" + catalogueId,
        //    //cache: false,
        //},
        remote: {
            wildcard: '%QUERY',
            url: "/search/item-names/" + catalogueId +'?searchText=%QUERY',
            cache: false,
            replace: function (url, uriEncodedQuery) {
                let origins = $('#origins').val();
                let brands = $('#brands').val();
                let families = $('#families').val();
                //if (!origins) return url;
                //if (!brands) return url;
                //if (!families) return url;
                //correction here
                return  url.replace("%QUERY", uriEncodedQuery) + '&origins=' + encodeURIComponent(origins) + '&brands=' + encodeURIComponent(brands) + '&families=' + encodeURIComponent(families) 
            },
        },
        
    });


    $('#searchText').typeahead({
        highlight: true,
        hint: true,
        minLength: 3,
        limit: 10 /* Specify maximum number of suggestions to be displayed */
    }, {
        name: 'names',
        source: names,
       
    });

});

