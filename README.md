# Dina Shop
Dina Shop is a web interface to generate spreadsheet orders for the program called "L'automate de DINA".

## Installation

### Production installation
- Clone this repository in your public_html folder
- Set the `.env` file with production values

### Local installation
- Clone this repository
- Keep the `.env` file in `dev` environment and update dev dependencies with `composer install`

## Home page & Copyright
- HomePage <http://dina-bio.shop/>
- Copyright © 2020 Polochon: pol dot ochon777 at aliceadsl dot fr 

## License
[![agpl v3 logo](public/agplv3-88x31.png)](https://www.gnu.org/licenses/agpl-3.0.html)

## Contributing
Pull requests are __not__ welcome. This project is a "teacher project" for my personnal usage in order to learn web development. 

## Stack
- Symfony 4
- Bootstrap 4
- Javascript

## History
### 2020-09-14
- v0.2.0-beta: First working release

